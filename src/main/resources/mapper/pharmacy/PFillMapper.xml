<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<!--
DB 쿼리문을 관리하며 DB와 상호작용하는 파일

id : @Mapper에서 정의한 이름과 동일해야 함
parameterType: 파라미터 자료형
resultType: 리턴타입 자료형
-->
<mapper namespace="com.hellodoctor.mapper.pharmacy.PFillMapper">
    <sql id="joinTable">
        hd_pharmacy_request as A /*조제요청정보*/
        INNER JOIN hd_medical as B on A.medical_idx = B.idx /*진료정보*/
        INNER JOIN hd_member as C on B.mb_idx = C.idx /*회원정보*/
        LEFT JOIN hd_member_family as D on B.patient_idx != 0 AND B.patient_idx = D.idx /*나의가족관리*/
    </sql>

    <sql id="where">
        A.pharmacy_idx = #{pharmacyIdx}
        ${sqlSearch} /*검색조건*/
    </sql>

    <sql id="limit">
        LIMIT #{startRecord}, #{endRecord}
    </sql>

    <!-- 약 조제요청 목록 수 -->
    <select id="getFillRequestCount" resultType="map">
        SELECT
            /*검색에 필요한 컬럼*/
            A.req_status AS reqStatus,
            IF(B.patient_idx = 0, C.mb_name, D.fam_name) AS patientName, /*환자이름*/
            IF(B.patient_idx = 0, B.patient_hp, D.fam_hp) AS patientHp, /*연락처*/
            C.mb_name AS mbName
        FROM
            <include refid="joinTable"/>
        WHERE
            <include refid="where"/>
    </select>

    <!-- 약 조제요청 목록 -->
    <select id="getFillRequest" resultType="map">
        SELECT
            A.idx,
            A.pres_idx AS presIdx,
            A.medical_idx AS medicalIdx,
            A.pharmacy_idx AS pharmacyIdx,
            A.req_status AS reqStatus,
            A.regdate,
            A.bill_amt AS billAmt,
            A.complete_date AS completeDate,
            A.courier,
            A.tracking_no AS trackingNo,
            A.cancel_reason AS cancelReason,
            A.cancel_date AS cancelDate,
            B.rcpt_no AS rcptNo,
            B.receive_method AS receiveMethod,
            B.is_pres AS isPres,
            IF(B.patient_idx = 0, C.mb_name, D.fam_name) AS patientName, /*환자이름*/
            IF(B.patient_idx = 0, B.patient_hp, D.fam_hp) AS patientHp, /*연락처*/
            C.idx AS mb_idx,
            C.address_idx AS addressIdx
        FROM
            <include refid="joinTable"/>
        WHERE
            <include refid="where"/>
        ORDER BY ( /*1.미접수 2. 요청시간 최신순*/
        CASE WHEN A.req_status ='R' THEN 1
        ELSE 2 END), A.regdate DESC
        <if test="endRecord != 0">
            <include refid="limit"/> /*endRecord가 0이면 LIMIT X*/
        </if>
    </select>

    <!-- 배송지 정보 -->
    <select id="getAddrInfo" resultType="map">
        SELECT
            IF(A.patient_idx = 0, B.mb_name, C.fam_name) AS patientName, /*환자이름*/
            IF(A.patient_idx = 0, A.patient_hp, C.fam_hp) AS patientHp, /*연락처*/
            D.main_addr AS mainAddr,
            D.detail_addr AS detailAddr
        FROM
            hd_medical AS A
            INNER JOIN hd_member AS B ON A.mb_idx = B.idx /*회원정보*/
            LEFT JOIN hd_member_family AS C ON A.patient_idx != 0 AND A.patient_idx = C.idx /*나의가족관리*/
            INNER JOIN hd_member_address AS D ON B.address_idx = D.idx /*회원배송지정보*/
        WHERE A.idx = #{medicalIdx}
    </select>

    <!--약 조제요청 접수-->
    <update id="statusComplete">
        UPDATE hd_pharmacy_request SET req_status = 'Y', bill_amt = #{billAmt} WHERE idx = #{idx}
    </update>

    <!--약 조제요청 거부-->
    <update id="statusReject">
        UPDATE hd_pharmacy_request SET req_status = 'N', cancel_date = #{cancelDate}, cancel_reason = #{cancelReason} WHERE idx = #{idx}
    </update>

    <!-- 약 수령방식 변경-->
    <update id="receiveMethodChange">
        UPDATE hd_medical SET receive_method = #{receiveMethod} WHERE idx = #{medicalIdx}
    </update>

    <!-- 약 조제접수 목록 수 -->
    <select id="getFillReceiveCount" resultType="map">
        SELECT
        /*검색에 필요한 컬럼*/
        A.req_status AS reqStatus,
        A.bill_stlm_idx AS billStlmIdx,
        A.complete_date AS completeDate,
        IF(B.patient_idx = 0, C.mb_name, D.fam_name) AS patientName, /*환자이름*/
        IF(B.patient_idx = 0, B.patient_hp, D.fam_hp) AS patientHp, /*연락처*/
        C.mb_name AS mbName
        FROM
        <include refid="joinTable"/>
        WHERE
        <include refid="where"/>
    </select>

    <!-- 약 조제접수 목록 -->
    <select id="getFillReceive" resultType="map">
        SELECT
        A.idx,
        A.pres_idx AS presIdx,
        A.medical_idx AS medicalIdx,
        A.pharmacy_idx AS pharmacyIdx,
        A.req_status AS reqStatus,
        A.regdate,
        A.bill_amt AS billAmt,
        A.complete_date AS completeDate,
        A.courier,
        A.tracking_no AS trackingNo,
        A.cancel_reason AS cancelReason,
        A.cancel_date AS cancelDate,
        A.bill_stlm_idx AS billStlmIdx,
        B.rcpt_no AS rcptNo,
        B.receive_method AS receiveMethod,
        B.is_pres AS isPres,
        IF(B.patient_idx = 0, C.mb_name, D.fam_name) AS patientName, /*환자이름*/
        IF(B.patient_idx = 0, B.patient_hp, D.fam_hp) AS patientHp, /*연락처*/
        C.idx AS mb_idx,
        C.address_idx AS addressIdx,
        (SELECT COUNT(*) AS count FROM hd_medicine_guide WHERE medical_idx = B.idx) AS guideCount
        FROM
            <include refid="joinTable"/>
        WHERE
            <include refid="where"/>
        ORDER BY A.regdate DESC
        <if test="endRecord != 0">
            <include refid="limit"/> /*endRecord가 0이면 LIMIT X*/
        </if>
    </select>

    <!--약 조제접수 완료-->
    <update id="fillComplete">
        UPDATE hd_pharmacy_request SET complete_date = #{completeDate} WHERE idx = #{idx}
    </update>

    <!--의약품 검색 (자동완성)-->
    <select id="mediAutocomplete" resultType="map">
        SELECT
            idx,
            item_name AS itemName,
            item_code AS itemCode,
            how_take AS howTake
        FROM hd_medicine
        WHERE pharmacy_idx = #{pharmacyIdx}
        ${search}
        ORDER BY idx
    </select>

    <!--복약지도 INSERT-->
    <insert id="saveGuide" parameterType="java.util.ArrayList">
        INSERT INTO hd_medicine_guide (medical_idx, medicine_idx, item_name, item_code, how_take, item_img) VALUES
        <foreach collection="guideList" item="i" separator=",">
            (#{medicalIdx}, #{i.medicineIdx}, #{i.itemName}, #{i.itemCode}, #{i.howTake}, #{i.itemImg})
        </foreach>
    </insert>

    <!--복약지도 DELETE-->
    <delete id="deleteGuide">
        DELETE FROM hd_medicine_guide WHERE medical_idx = #{medicalIdx}
    </delete>

    <!--복약지도 정보-->
    <select id="mediGuideInfo" resultType="map">
        SELECT
            idx,
            medical_idx AS medicalIdx,
            medicine_idx AS medicineIdx,
            item_name AS itemName,
            item_code AS itemCode,
            how_take AS howTake,
            item_img AS itemImg
        FROM hd_medicine_guide
        WHERE medical_idx = #{medicalIdx}
    </select>

    <!--약 조제내역 목록 수-->
    <select id="getFillListCount" resultType="map">
        SELECT
            /*검색에 필요한 컬럼*/
            A.req_status AS reqStatus,
            A.bill_amt AS billAmt,
            A.bill_stlm_idx AS billStlmIdx,
            A.complete_date AS completeDate,
            IF(B.patient_idx = 0, C.mb_name, D.fam_name) AS patientName, /*환자이름*/
            IF(B.patient_idx = 0, B.patient_hp, D.fam_hp) AS patientHp, /*연락처*/
            C.mb_name AS mbName
        FROM
            <include refid="joinTable"/>
        WHERE
            A.pharmacy_idx = #{pharmacyIdx} AND A.complete_date IS NOT NULL /*조제완료데이터*/
            ${sqlSearch} /*검색조건*/
    </select>

    <!--약 조재내역 목록-->
    <select id="getFillList" resultType="map">
        SELECT
            A.idx,
            A.pres_idx AS presIdx,
            A.medical_idx AS medicalIdx,
            A.pharmacy_idx AS pharmacyIdx,
            A.req_status AS reqStatus,
            A.regdate,
            A.bill_amt AS billAmt,
            A.complete_date AS completeDate,
            A.courier,
            A.tracking_no AS trackingNo,
            A.cancel_reason AS cancelReason,
            A.cancel_date AS cancelDate,
            A.bill_stlm_idx AS billStlmIdx,
            B.rcpt_no AS rcptNo,
            B.receive_method AS receiveMethod,
            B.is_pres AS isPres,
            IF(B.patient_idx = 0, C.mb_name, D.fam_name) AS patientName, /*환자이름*/
            IF(B.patient_idx = 0, B.patient_hp, D.fam_hp) AS patientHp, /*연락처*/
            C.idx AS mb_idx,
            C.address_idx AS addressIdx,
            (SELECT COUNT(*) AS count FROM hd_medicine_guide WHERE medical_idx = B.idx) AS guideCount
        FROM
            <include refid="joinTable"/>
        WHERE
            A.pharmacy_idx = #{pharmacyIdx} AND A.complete_date IS NOT NULL /*조제완료데이터*/
            ${sqlSearch} /*검색조건*/
        ORDER BY A.regdate DESC
        <if test="endRecord != 0">
            <include refid="limit"/> /*endRecord가 0이면 LIMIT X*/
        </if>
    </select>

    <!--약 조제요청 정보 (단건)-->
    <select id="getPharmacyRequestInfo" resultType="PharmacyRequest">
        SELECT * FROM hd_pharmacy_request WHERE idx = #{idx}
    </select>
</mapper>