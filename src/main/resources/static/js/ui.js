$(document).ready(function(){

	//체크박스디자인 div추가
	$('input[type=checkbox] + label').prepend('<div></div>');
	$('input[type=radio] + label').prepend('<div></div>');


	//헤드 스크롤반응
	//스크롤이 발생 될 때
	$(window).scroll(function(){
		var sct = $(window).scrollTop();
		if(sct>=10){
			// #header에 클래스on이 추가됨.
			$("#header").addClass("scroll_top");
			//$("#idx_hd.bgw").addClass("on");
		};
		// 만약 sct의 값이 150미만이면
		if(sct<=10){
			$("#header").removeClass("scroll_top");
			//$("#idx_hd.bgw").removeClass("on");
		}
	}); //스크롤이벤트끝


	//약관동의 내용보기
	$("#agree .btn-agr").click(function () {
		var dis = $(this).parents(".dl").find(".agr_textarea").css("display");
		if (dis == "none")
			$(this).addClass("on"),
				$(this).parents(".dl").find(".agr_textarea").slideDown(100);
		else
			$(this).removeClass("on"),
				$(this).parents(".dl").find(".agr_textarea").slideUp(100);
	});

	//상세숨기기 (원격진료 완료상세/배송조회)
	$(document).on("click", ".view_btn", function() { //$(".view_btn").click(function () {
		var dis = $(this).parents(".li").find(".drop_view").css("display");
		if ($(this).parents(".li").find(".drop_view").length==0) return; // 충돌 방지추가
		if (dis == "none")
			$(this).addClass("on"),
				$(this).parents(".li").find(".drop_view").slideDown(100);
		else
			$(this).removeClass("on"),
				$(this).parents(".li").find(".drop_view").slideUp(100);
	});


	//병원소개 - 사진확대
	if (typeof Swiper != "undefined") {
		var swiper = new Swiper(".gall_zoom", {
			autoHeight: true,
			spaceBetween: 20,
			navigation: {
				nextEl: ".swiper-button-next",
				prevEl: ".swiper-button-prev",
			},
			pagination: {
				el: ".swiper-pagination",
				clickable: true,
			},
		});
	}

	//별점
	$(function(){
		$('.starRev span').click(function(){
			$(this).parent().children('span').removeClass('fas');
			$(this).addClass('fas').prevAll('span').addClass('fas');
			return false;
		});
	})
});