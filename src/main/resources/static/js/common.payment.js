/**
 * 앱 결제관련
 */
// 결제카드 없을시 등록확인 알림창
function checkCreditCard() {
    Swal.fire({
        title: '결제카드를 등록해주세요.',
        html: '비대면 진료 및 약 배송 서비스를<br>위해 결제카드 등록이 필요합니다.',
        confirmButtonText: '등록하기',
        didDestroy: function () {
            history.back();
        }
    }).then((result) => {
        if (result.isConfirmed) {
            history.replaceState({data: "replaceState"}, "", "/app/main");
            location.href="/app/credit";
        }
    });
}

/**
 * 앱 1원결제
 * 1. 등록카드로 1원결제 요청
 * 2. 결제성공/실패 > 1원결제 취소
 * 3. 페이지 별 NEXT
 *
 * 페이지 별 필요변수
 * paymentCheck = 1원결제 요청
 * paymentTid = 결제 TID
 * pageName = 호출 페이지명
 */
//<!--/* 1. 등록카드로 1원결제 요청 */-->
function memberPaymentCheck(hospitalIdx) {
    //<!--/* 결제카드 선택 */-->
    let cardList = document.querySelectorAll(`#clf input[name="cidx[]"]`);
    let tag = `<select id="creditCardList">`;
    tag += `<option value="">선택하세요</option>`;
    if (cardList.length > 0) {
        cardList.forEach(function (elem) {
            let _option = `${elem.getAttribute('data-cnm')}카드 (${elem.getAttribute('data-cno')})`;
            tag += `<option value="${elem.value}">${_option}</option>`;
        });
    }
    tag += `</select>`;

    swal.fire({
        html:'등록하신 카드로<br>1원 결제가 진행됩니다.' + tag,
        confirmButtonText : '결제하기',
        showDenyButton: false,
        preConfirm: function () {
            //<!--/* 결제카드 선택유무 체크 */-->
            let selectCardIdx = document.querySelector("#creditCardList option:checked").value;
            if (selectCardIdx == "") {
                let selectBox = $("#creditCardList");
                if (selectBox.nextAll("p").length == 0) {
                    let error = `<p class="color_red">결제카드를 선택해주세요.</p>`;
                    selectBox.after(error); // document.querySelector("#creditCardList").after(error);
                }
                return false;
            } else {
                //document.querySelector("[name=cardIdx]").value = selectCardIdx;
                //<!--/* 추후 진료비 결제시 사용되는 결제카드 인덱스 */-->
                sessionStorage.setItem("user_card_idx", selectCardIdx);
                return true;
            }
        }
    }).then((result) => {
        if (result.isConfirmed) {
            //<!--/* 1원결제 요청 */-->
            let paySuccess = getPaymentTest(true);
            memberPaymentCheckResult(hospitalIdx, paySuccess);
        }
    });
}

//<!--/* 2. 결제성공/실패 - 1원결제 취소 */-->
function memberPaymentCheckResult(hospitalIdx, success, errMsg) {
    //<!--/* 결제실패 */-->
    if (!success) {
        swal.fire({html: "1원 결제에<br>실패했습니다.", confirmButtonText : '확인',});
        return false;
    }

    //<!--/* 결제성공 */-->
    getPaymentTest(false); // 1원결체 취소요청
    paymentCheck = true;

    swal.fire({
        html: '1원 결제가<br>자동 취소 되었습니다.',
        confirmButtonText : '확인',
        showDenyButton: false,
    }).then((result) => {
        let now = new Date();
        now.setMinutes(now.getMinutes()+5);    //<!--/* 3분간 저장 */-->
        // now.setSeconds(now.getSeconds()+10);  //<!--/* test 10초 */-->
        sessionStorage.setItem("user_pay_check_ts", now.getTime()+"");

        if (result.isConfirmed) {
            if (pageName == "hospitalSelect") { // 원격진료신청 병원선택 후 진료신청
                medicalAppointment(hospitalIdx, true);
            } else if (pageName == "myDoctor") { // 우리동네주치의 빠른진료신청

            }
        }
    });
}

// 1원결제 결제승인/결제취소 api
function getPaymentTest(flag) {
    let selectCardIdx = sessionStorage.getItem("user_card_idx"); //document.querySelector("[name=cardIdx]").value;
    let billKey = document.querySelector(`#clf input[value='${selectCardIdx}']`).getAttribute("data-bk");
    let medicalIdx = document.querySelector("[name=idx]").value;
    let url = (flag)? "/api/paymentTestRequest" : "/api/paymentTestCancel";

    //<!--/* 1원결제/취소 */-->
    let result = false;
    let formData = new FormData();
    formData.append("billKey", billKey);
    formData.append("medicalIdx", medicalIdx);
    if (!flag) formData.append("tid", paymentTid);

    $.ajax({
        url: url,
        type: "post",
        data: formData,
        processData: false,
        contentType: false,
        async : false,
    }).done(function(data, textStatus, xhr) {
        if (flag) {
            let json = JSON.parse(data);
            console.log("getPaymentTest 결과=", json);
            result = json.result;
            paymentTid = json.tid;
        }
    }).fail(function(data, textStatus, errorThrown) {
        console.log(data);
    }).always(function() {
        ;
    });

    return result;
}