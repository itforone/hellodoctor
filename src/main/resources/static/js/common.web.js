/**
 * 공통함수 - web
 */

//<!--* 에러메시지 출력후 이동 */-->
function getErrAlert(errMsg, url) {
    swal.fire({
        html:errMsg, confirmButtonText : '확인',
        didDestroy: function () {
            if (typeof url == 'undefined') {
                if(document.location.href.indexOf("/hospital/") != -1) url = "/hospital/login";
                else if(document.location.href.indexOf("/pharmacy/") != -1) url = "/pharmacy/login";
            }
            history.replaceState({data: "replaceState"}, "", url);
            location.reload();
        }
    });
}

//<!--* 휴대폰번호 하이픈 자동생성 */-->
function validatePhone(str) {
    var tmp = '';
    str = str.replace(/[^0-9]/g, '');

    if (str.length < 4) {
        tmp = str;
    } else if (str.length < 7) {
        tmp += str.substr(0, 3) + '-' + str.substr(3);
    } else if (str.length < 11) {
        tmp += str.substr(0, 3) + '-' + str.substr(3, 3) + '-' + str.substr(6);
    } else {
        tmp += str.substr(0, 3) + '-' + str.substr(3, 4) + '-' + str.substr(7);
    }
    return tmp;
}

//<!--* 비밀번호 정규식 (영문, 숫자, 특수문자 조합 8자리 이상 20자 이하) */-->
function passwordRegExp(password) {
    var pw = password;
    var num = pw.search(/[0-9]/g);
    var eng = pw.search(/[a-z]/gi);
    var spe = pw.search(/[~#?!@$%^&*-]/gi);

    var flag = true;
    if((pw.length < 8) || num < 0 || eng < 0 || spe < 0 || (pw.length > 20)) flag = false;

    return flag;
}

//<!--* 천단위 콤마 처리 */-->
function addComma(data) {
    document.querySelector(`#${data.id}`).value = data.value.replace(/[^\d]+/g, ''); //<!--* 숫자만 입력
    document.querySelector(`#${data.id}`).value = data.value.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,'); //<!--* 천단위 콤마
}

//<!--* 숫자만 입력되도록 */-->
function onlyNumber(data) {
    document.querySelector(`#${data.id}`).value = data.value.replace(/[^\d]+/g, '');
}

//<!--* 자리수만큼 남는 앞부분을 0으로 채움 */-->
function fillZero(width, str){
    return str.length >= width ? str : new Array(width-str.length + 1).join('0') + str;
}

//<!--* 객체 value값으로 key값 찾기 */-->
function getKeyByValue(obj, val) {
    return Object.keys(obj).find(key => obj[key] === val);
}

//<!--/* 로딩중 true, false */-->
function callPageLoading(flag) {
    let block = document.getElementById("loading");
    block.style.display = (flag)? "block" :"none";
}

// 헤더 유저명
window.onload = function() {
    bindHeaderUserName();
}
function bindHeaderUserName() {
    let name = (sessionStorage.getItem("userName") != null)? sessionStorage.getItem("userName") : "";
    if (typeof bindUserName != "undefined" && bindUserName != "") {
        name = bindUserName;
        sessionStorage.setItem("userName", bindUserName);
    }

    if (name != "") document.querySelector("#header .user_name").innerHTML = name;
}