/**
 * 공통함수
 */
window.onload = function() {
    // 로그아웃 체크
    if (typeof isLogout != 'undefined' && isLogout == "true") {
        getLoginPage();
    }
}

// 회원 로그인페이지 이동
// @param closeNoAction : true/1 = 닫기 선택시 이동액션 없음
function getLoginPage(closeNoAction) {
    swal.fire({
        html:'로그인이 필요합니다.<br>로그인페이지로 이동하시겠습니까?',
        confirmButtonText: '확인',
        cancelButtonText: "닫기",
        showCancelButton: true,
        didDestroy: function() {
            //location.href = "/app/main";
        }
    }).then((result) => {
        if (result.isConfirmed) {
            history.replaceState({data: "replaceState"}, "", "/");
            location.href = "/";
        } else {
            if (!closeNoAction)
                location.href = "/app/main";
        }
    });
}

// 에러메시지 출력후 이동
function getErrAlert(errMsg, url) {
    swal.fire({
        html:errMsg, confirmButtonText : '확인',
        didDestroy: function () {
            if (typeof url == 'undefined') url = "/app/main";
            history.replaceState({data: "replaceState"}, "", url);
            location.reload();
        }
    });
}

// 파라미터 추출
function getParam(sname) {
    var params = location.search.substr(location.search.indexOf("?") + 1);
    var sval = "";
    params = params.split("&");
    for (var i = 0; i < params.length; i++) {
        temp = params[i].split("=");
        if ([temp[0]] == sname) { sval = temp[1]; }
    }
    return sval;
}

// 뒤로가기 체크
window.onhashchange = function (event) {
    var old_url = event.oldURL,
        new_url = event.newURL;
    console.log("old_url: ",old_url);
    console.log("new_url: ",new_url);
    if(!new_url.match("#modal")) { // 배송지관리 모달 닫기
        document.querySelector(".modal").classList.remove("in");
        document.querySelector(".modal").style.display = "none";
    }
}

// 비밀번호 정규식 (영문, 숫자, 특수문자 조합 8자리 이상 20자 이하)
function mbPasswordChk(password) {
    var pw = password;
    var num = pw.search(/[0-9]/g);
    var eng = pw.search(/[a-z]/gi);
    var spe = pw.search(/[~#?!@$%^&*-]/gi);

    if((pw.length < 8) || num < 0 || eng < 0 || spe < 0 || (pw.length > 20)) {
        return false;
    } else {
        return true;
    }
}

//=================================
// 배송지(주소록)관리 관련 함수
// --------------------------------
// 배송지관리 모달 open
function getAddress() {
    addressModal("/app/addressList", "");
    // document.querySelector(".modal").classList.add("in");
    // document.querySelector(".modal").style.display = "block";
    document.querySelector("#addressModal").classList.add("in");
    document.querySelector("#addressModal").style.display = "block";
    history.pushState(null, null, '#modal');
}

// 배송지관리 모달 내 move (mapping url, 추가/수정 구분)
var addressListHtml, addressWriteHtml = "";
function addressModal(url, mode) {
    if(mode === undefined) mode = "";
    if(url.match("addressWrite") && mode.match("update")) { // addressList에서 새 배송지 추가/수정
        if (document.querySelectorAll('input[name="addr[]"]:checked').length === 0) {
            swal.fire({html: "배송지를 선택해 주세요."});
            return false;
        }
        url = url+'/'+document.querySelector('input[name="addr[]"]:checked').value;
    } else if(url.match("addressSearch") && mode.match("update")) { // addressWrite에서 주소 검색
        url = url+'/'+document.addrForm.querySelector('input[name="idx"]').value;
    }
    $.ajax({
        url: url,
        type: "post",
        data: {mode: mode},
        success: function (data) {
            if(url.match("addressList")) addressListHtml = data;
            else if(url.match("addressWrite")) addressWriteHtml = data;
            $('#addressModal .modal-content').html(data);
        }
    });
}

// 배송지관리 모달 내 back
function addressModalBack(url) {
    var data = "";
    if(url !== undefined) {
        if(url === "addressWrite") data = addressWriteHtml;
        else if(url ==="addressList") data = addressListHtml;
        $('#addressModal .modal-content').html(data);
    }
}

//=================================
// 원격진료 관련 함수
// --------------------------------
// 예약취소
function medicalCancel(el, page) {
    if (typeof el == "undefined") return;
    let idx = el.getAttribute("data-idx");
    let errMsg = "예약 취소에 실패했습니다.<br>잠시 후 다시 시도해 주세요.";

    swal.fire({
        title: "<strong>예약 취소</strong>",
        html: "정말 예약 취소 하시겠습니까?<br>입력 정보는 삭제됩니다.",
        denyButtonText : '취소',
        confirmButtonText : '확인',
        showDenyButton: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type : "post",
                url : "/app/remoteFormCancelAction",
                data : {idx: idx},
            }).done(function(data, textStatus, xhr) {
                let json = JSON.parse(data);
                // console.log(json);
                if (json.result) {
                    // location.reload();
                    swal.fire({
                        html: "예약 취소가<br>완료 되었습니다.", showConfirmButton: false,
                        didDestroy: function () {
                            if (page == 'list') { // 원격진료 목록
                                $(el).parents("li").remove(); // 목록 html 삭제
                            } else {    // 원격진료 상세
                                history.replaceState({data: "replaceState"}, "", "/app/main");
                                location.href = "/app/main";
                            }
                        },
                    });
                } else {
                    swal.fire({html: errMsg, confirmButtonText : '확인',});
                }

            }).fail(function(data, textStatus, errorThrown) {
                // console.log(data);
                swal.fire({html: errMsg, confirmButtonText : '확인',});
            });
        }
    });
}
// 원격진료신청 상세보기
function getRemoteStatus(idx, params) {
    let url = `/app/remoteStatus?idx=${idx}`;
    if (params != undefined) url += params;

    let lat = sessionStorage.getItem("appUsrLat")!=null? sessionStorage.getItem("appUsrLat") : "";
    let lng = sessionStorage.getItem("appUsrLng")!=null? sessionStorage.getItem("appUsrLng") : "";
    url += `&lat=${lat}&lng=${lng}`;

    location.href = url;
}


//=================================
// 세션스토리지 set, get
function setSsnStorage(key, value) {
    sessionStorage.setItem(key, value);
}
function getSsnStorage(key) {
    return sessionStorage.getItem(key);
}
function remoteSsnStorage(key) {
    sessionStorage.removeItem(key);
}

// 로딩중 true, false
function callPageLoading(flag) {
    let block = document.getElementById("loading");
    // let bg = document.createElement("div");
    // bg.classList.add("swal2-container");
    // bg.classList.add("swal2-backdrop-show");
    // bg.setAttribute("id", "loadingBg");

    if (flag) {
        // document.body.appendChild(bg);
        block.style.display = "block";
    }
    else {
        // document.querySelector("#loadingBg").remove();
        block.style.display = "none";
    }
}

// 앱 현재위치
function getMyLocation(lat, lng) {
    console.log(`lat=${lat}, lng=${lng}`);
    sessionStorage.setItem("appUsrLat", lat);
    sessionStorage.setItem("appUsrLng", lng);
    /*$.ajax({
        type : "post",
        url : "/app/saveUserLocation",
        data : {lat: lat, lng: lng},
    }).done(function(data, textStatus, xhr) {
        let json = JSON.parse(data);

    }).fail(function(data, textStatus, errorThrown) {
        ;
    });*/
}

// 서비스준비중
function tmpAlim() {
    swal.fire({
        html: '서비스 준비중입니다.', confirmButtonText : '확인',
        didDestroy: function () {
            return false;
        }
    });
}