//<!--/* 연락처 인증번호 js */-->
let completeHp; //<!--/* 인증완료 된 연락처 */-->
let timeoutCheck = false; //<!--/* 타이머시작 */-->
let issueCertData = {};
let canResendDate = 0; //<!--/* 재발송시간 1분 TS */-->
let completeCert = false; //<!--/* 인증확인 됨 */-->

$(document).ready(function () {
    //<!--/* 연락처 하이픈 자동생성 */-->
    $("[name=userHp]").on("keyup", function(e) {
        let formatter = validatePhone($.trim($(this).val()));
        let hp = formatter.substring(0, 13);
        let btnReq = document.getElementById("btnReq");
        this.value = hp;

        //<!--/* 에러문구 삭제 */-->
        frmErrChk("userHp", false)

        if (hp.length < 12) btnReq.classList.remove("confirm");
        else btnReq.classList.add("confirm");

        if(completeHp != hp) completeCert = false;
        userHpChk();
    });
});

//<!--/* 연락처/인증번호확인 버튼 설정 */-->
function userHpChk() {
    if(completeCert) {
        document.querySelector("#btnReq").classList.remove("btn_find");
        document.querySelector("#btnReq").classList.add("blueline");
        document.querySelector("#btnCert").classList.remove("btn_find");
        document.querySelector("#btnCert").classList.add("blueline");
        document.querySelector("#btnCert").innerHTML = "인증 완료";
        document.querySelector("#certNo").readOnly = true;
    } else {
        document.querySelector("#btnReq").classList.add("btn_find");
        document.querySelector("#btnReq").classList.remove("blueline");
        document.querySelector("#btnCert").classList.add("btn_find");
        document.querySelector("#btnCert").classList.remove("blueline");
        document.querySelector("#btnCert").innerHTML = "확인";
        document.querySelector("#certNo").readOnly = false;
    }
}

//<!--/* 인증번호요청,재발송 */-->
function requestCertNo() {
    let hp = document.querySelector("[name=userHp]").value;
    if (hp.length < 12) {
        frmErrChk("userHp", true, "전화번호를 입력해 주세요.");
        return;
    }

    if (completeCert) return;

    //<!--/* (타이머 시작) 재발송 가능한지 확인 */-->
    let btnReq = document.getElementById("btnReq");
    let btnResendCheck = (btnReq.classList.contains("re"))? true : false; //<!--/* 재발송 : 인증번호요청 */-->
    if (timeoutCheck) {
        let currentDate = new Date();
        if (btnResendCheck && currentDate < canResendDate) {
            document.getElementById("msgHpRow").innerHTML = "1분 후 재발송 가능합니다.";
            return false;
        } else {
            //<!--/* 재발송 가능시간 재설정 */-->
            canResendDate = new Date(Date.parse(currentDate) + 1000*60);
        }
    }

    $.ajax({
        type : "post",
        url : "/hospital/certify/requestCertNo",
        data : {hp: hp},
    }).done(function(data, textStatus, xhr) {
        let json = JSON.parse(data);
        issueCertData = json;
        document.getElementById("msgHpRow").innerHTML = "인증번호가 발송되었습니다.";
        //document.getElementById("msgHpRow").classList.remove("errHidee");

        // TBC: 임시
        swal.fire({html: "인증번호: "+issueCertData['certNo']});

        //<!--/* 타이머 시작 */-->
        if (!btnResendCheck) {
            frmErrChk("certNo", false);
            startCertTime();
        }
    }).fail(function(data, textStatus, errorThrown) {
        swal.fire({
            html:'서버와의 통신에 문제가<br>발생했습니다.<br>잠시 후 다시 시도해 주세요.',
            confirmButtonText : '확인',
            didDestroy: function () {
                location.reload();
            }
        });
    });
}

//<!--/* 인증번호확인 */-->
function compareCertNo() {
    if (!timeoutCheck) return; //<!--/* 인증번호요청 안누름 */-->
    if (completeCert) return; //<!--/* 인증확인 완료됨 */-->

    let inputCertNo = document.querySelector("[name=certNo]").value.trim();
    if (inputCertNo != issueCertData['certNo']) {
        document.getElementById("msgCertRow").innerHTML = "인증번호를 다시 확인해 주세요.";
        return;
    }

    frmErrChk('certNo', false);
    frmErrChk('userHp', false);
    completeCert = true;

    let btnCert = document.getElementById("btnCert"); //<!--/* 인증번호 확인 확인버튼 */-->
    btnCert.classList.remove("confirm");
    btnCert.classList.add("re");
    btnCert.innerHTML = "인증완료";
    document.querySelector("[name=certNo]").readOnly = true;    //<!--/* 인증번호 비활성화 */-->
    document.querySelector('#certTime').style.display = "none"; //<!--/* 인증요청시간 삭제 */-->
}

//<!--/* 인증요청시간 시작 (3분) */-->
function startCertTime() {
    timeoutCheck = true;
    let btnReq = document.getElementById("btnReq");
    btnReq.classList.remove("confirm"); //<!--/* 재발송버튼 활성화 */-->
    btnReq.classList.add("re"); //<!--/* 재발송버튼 활성화 */-->
    btnReq.innerHTML = "재발송";
    //<!--/* 재발송가능시간 (1분후) */-->
    let currentDate = new Date();
    canResendDate = new Date(Date.parse(currentDate) + 1000*60);

    let btnCert = document.getElementById("btnCert");
    btnCert.classList.add("confirm"); //<!--/* 확인버튼 활성화 */-->

    document.querySelector("[name=userHp]").readOnly = true; //<!--/* 연락처 비활성화 */-->

    let timerArea = document.getElementById("certTime");
    let minArea = document.querySelector("[data-min='1']");
    let secArea = document.querySelector("[data-sec='1']");

    timerArea.style.display = "block";

    let min = 2;
    let sec = 60;
    let timer = setInterval(function() {
        sec--;

        if (sec == -1 && min > 0) {
            sec = 59;
            min--;
        }

        //<!--/* 타이머 종료 - 시간만료 */-->
        if (sec == 0 && min == 0) {
            clearInterval(timer);
            timerArea.style.display = "none";
            frmErrChk('certNo', true, '인증번호 유효시간이 지났습니다.');
            timeoutCheck = false;
            issueCertData = {};

            btnCert.classList.remove("confirm"); //<!--/* 확인버튼 비활성화 */-->
            btnReq.classList.remove("re"); //<!--/* 재발송버튼 비활성화 */-->
            btnReq.classList.add("confirm");
            btnReq.innerHTML = "인증번호 요청";
        }
        //<!--/* 타이머종료 - 인증함 */-->
        if (completeCert) {
            clearInterval(timer);
        }

        minArea.innerHTML = min;
        secArea.innerHTML = (sec < 10)? "0" + sec : sec;
        //<!--/* console.log(min, sec); */-->
    }, 1000);
}