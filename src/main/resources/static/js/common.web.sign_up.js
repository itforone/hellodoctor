/**
 * 의사/약사 회원가입 공통 js
 */

let nowMenu = "";
$(document).ready(function() {
    //<!--/* 현재메뉴 */-->
    let href = document.location.href;
    if (href.indexOf("/hospital/") != -1) nowMenu = "hospital"; //<!--/* 의사 */-->
    else if (href.indexOf("/pharmacy/") != -1) nowMenu = "pharmacy" //<!--/* 약사 */-->
});

//<!--/* 비밀번호 포맷 */-->
function userPwFormatChk(target) {
    var userPassword = document.querySelector("#userPassword").value;
    var userPasswordRe = document.querySelector("#userPasswordRe").value;

    if (userPassword == userPasswordRe) {
        frmErrChk("userPasswordRe", false);
    } else {
        frmErrChk("userPasswordRe", true, "비밀번호가 일치하지 않습니다.");
    }

    if(target === "userPassword") {
        //<!--/* 정규식 */-->
        if (passwordRegExp(userPassword)) {
            frmErrChk("userPassword", false);
        } else {
            frmErrChk("userPassword", true, "비밀번호가 유효하지 않습니다.");
            return false;
        }
    }
    else if(target === "userPasswordRe") {
        if(userPassword != userPasswordRe){
            frmErrChk("userPasswordRe", true, "비밀번호가 일치하지 않습니다.");
            return false;
        }
    }

    return true;
}

//<!--/* 비밀번호 보기 */-->
function passwordShow(elem, name) {
    if($(elem).hasClass("pw_hide")) { //<!--/* 비밀번호보기 */-->
        $(elem).addClass("pw_show")
        $(elem).removeClass("pw_hide");
        $(`#${name}`).prop("type", "text");
    } else { //<!--/* 비밀번호숨기기 */-->
        $(elem).addClass("pw_hide")
        $(elem).removeClass("pw_show");
        $(`#${name}`).prop("type", "password");
    }
}

//<!--/* 기본이미지로 */-->
function defaultImg(num, url) {
    document.querySelector(`.prev${num} img`).src = url;
    document.querySelector(`[data-area="${num}"] img`).src = url;
    document.getElementById(`file${num}`).value = "";
    document.getElementById("btnHide").click();
}

//<!--/* 미리보기 이미지 생성 */-->
function createPrevImage(num, url, name) {
    if(num === 1) { //<!--/* 이미지 표시 */-->
        document.querySelector(`.prev${num} img`).src = url;
        document.querySelector(`[data-area="${num}"] img`).src = url;
        document.getElementById("btnHide").click();
    } else { //<!--/* 파일명 표시 */-->
        let html = `${name}<span class="ic_close"></span>`; //<!--/* 파일명 */-->
        let area = document.querySelector(`[data-area="${num}"]`); //<!--/* 파일명 표시 위치 */-->
        let confirm = document.querySelector(`#fileConfirm${num}`); //<!--/* 파일 확인 버튼 */-->
        let add = document.querySelector(`#fileAdd${num}`);  //<!--/* 파일 첨부 버튼 */-->

        area.innerHTML = html;
        confirm.style.setProperty("display", "block", "important"); //<!--/* 파일 확인 버튼 숨김 해제 */-->
        add.style.setProperty("display", "none", "important"); //<!--/* 파일 첨부 버튼 숨김 */-->
        $(`#fileConfirm${num}`).attr("onclick", `fileConfirm('${num}', '${url}')`); //<!--/* 파일 확인 버튼 이벤트 */-->
    }
}

//<!--/* 미리보기 이미지 404 처리 */-->
function onErrorPrevImage(num) {
    let area = document.querySelector(`[data-area="${num}"]`);
    area.innerHTML = "";
}

//<!--/* 파일업로드 */-->
function uploadImagePrev(input, num) {
    let reg_ext = /(.*?)\.(jpg|jpeg|png|pdf)$/;

    if (input.files[0] == undefined) {
        //<!--/* input.value = ""; */-->
        console.log("undefined");
        return false;
    }

    if (!reg_ext.test(input.files[0].name)) {
        swal.fire({
            html:'이미지만 등록이 가능합니다. ',
            confirmButtonText: '확인',
            didDestroy: function() {
                input.value = "";
            }
        });
        return false;
    }

    //<!--/* 최대용량 체크 */-->
    let maxSizeMb = 10; //10mb
    let maxByte = maxSizeMb * 1024 * 1024;
    if (input.files[0].size > maxByte) {
        swal.fire({
            html: "이미지가 최대 용량 (" + maxSizeMb + "mb)을 초과합니다.",
            confirmButtonText: '확인',
            didDestroy: function() {
                input.value = "";
            }
        });
        return false;
    }

    //<!--/* 미리보기 */-->
    //<!--/* console.log(num) */-->
    if (input.files && input.files[0]) {
        let reader = new FileReader();
        reader.onload = function (e) {
            let src = e.target.result;
            let name = input.files[0].name; //<!--/* 파일명 */-->
            createPrevImage(num, src, name);
        }
        reader.readAsDataURL(input.files[0]);
    }

}

//<!--*/ 첨부파일삭제 */-->
$(document).on("click", ".ic_close", function(e) {
    let num = $(this).parents(".file_name").data("area");
    deleteImage(num);
});
function deleteImage(num) {
    let area = document.querySelector(`[data-area="${num}"]`);
    let confirm = document.querySelector(`#fileConfirm${num}`); //<!--/* 파일 확인 버튼 */-->
    let add = document.querySelector(`#fileAdd${num}`); //<!--/* 파일 첨부 버튼 */-->

    confirm.style.setProperty("display", "none", "important"); //<!--/* 파일 확인 버튼 숨김 */-->
    add.style.setProperty("display", "block", "important"); //<!--/* 파일 첨부 버튼 숨김 해제 */-->
    area.innerHTML = "";
    document.getElementById(`file${num}`).value = "";
}

//<!--/* 파일 확인 버튼 (미리보기) */-->
function fileConfirm(num, url) {
    document.querySelector('#file_pop01 .modal-body img').src = url;
    document.querySelector('#file_pop01 .modal-body img').setAttribute("onError", `onErrorPrevImage(${num})`);
}

//<!--/* 입력값 있는지 체크 */-->
function inputChk(target) {
    let char;
    switch (target) {
        //<!--/* TBC: 의사명/의사면허번호/병원이름 약국이랑 공용으로 쓸 수 있게 변경 ==> common.sign_up.js로 변경 예정 */-->
        case "userId":
            char = "이메일(아이디)를"
            break;
        case "userName":
            if(nowMenu == "hospital") char = "의사명을";
            else if(nowMenu == "pharmacy") char = "약사명을";
            break;
        case "userHp":
            char = "전화번호를";
            break;
        case "userPassword":
            char = "비밀번호를";
            break;
        case "userPasswordRe":
            char = "비밀번호를 확인을";
            break;
        case "licenseNo":
            if(nowMenu == "hospital") char = "의사 면허 번호를";
            else if(nowMenu == "pharmacy") char = "약사 면허 번호를";
            break;
        case "hospitalName":
            char = "병원 이름을";
            break;
        case "pharmacyName":
            char = "약국 이름을";
            break;
        case "zipCode":
            char = "주소를";
            break;
        case "detailAddr":
            char = "상세 주소를";
            break;
        case "brNo":
            char = "사업자 등록 번호를";
            break;
        case "introduce":
            char = "병원 소개 내용을";
            break;
        case "accountName":
            char = "예금주를";
            break;
        case "accountNum":
            char = "계좌번호를";
            break;
        case "subPosition":
            char = "직급을";
            break;
    }

    if(char === undefined) char = "";
    else char = char+" 입력해 주세요.";

    var input = document.querySelector("#"+target).value;
    if(input.trim().length === 0) {
        frmErrChk(target, true, char);
        return false;
    } else {
        frmErrChk(target, false);
        return true;
    }
}

//<!--/* 유효성 검사 결과 표시 (에러메세지) */-->
function frmErrChk(selector, error, msg) {
    if (msg === undefined) msg = "";

    var elem = document.querySelector(`.${selector}`);
    var errIcon = document.querySelector(`.${selector} .del`);
    var errMsg = document.querySelector(`.${selector} .msg`);
    errMsg.textContent = msg;

    if(error) {
        //<!--/* 에러 표시 */-->
        elem.classList.add("error");
        errIcon.classList.remove("errHide");
    } else {
        //<!--/* 에러 숨김 */-->
        elem.classList.remove("error");
        errIcon.classList.add("errHide");
    }
}

//<!--/* swal 알림창 */-->
function swalMsg(msg) {
    swal.fire({html: msg});
}

//<!--/* *** 다음 주소 api *** */-->
var element_layer = document.getElementById('layer');
function closeDaumPostcode() {
    //<!--/* iframe을 넣은 element를 안보이게 한다. */-->
    element_layer.style.display = 'none';
}

function sample2_execDaumPostcode() {
    new daum.Postcode({
        oncomplete: function(data) {
            //<!--/* 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분. */-->

            //<!--/* 각 주소의 노출 규칙에 따라 주소를 조합한다. */-->
            //<!--/* 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다. */-->
            var addr = ''; // 주소 변수
            var extraAddr = ''; // 참고항목 변수

            //<!--/* 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다. */-->
            if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                addr = data.roadAddress;
            } else { // 사용자가 지번 주소를 선택했을 경우(J)
                addr = data.jibunAddress;
            }

            //<!--/* 사용자가 선택한 주소가 도로명 타입일때 참고항목을 조합한다. */-->
            if(data.userSelectedType === 'R') {
                //<!--/* 법정동명이 있을 경우 추가한다. (법정리는 제외) */-->
                //<!--/* 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다. */-->
                if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                    extraAddr += data.bname;
                }
                //<!--/* 건물명이 있고, 공동주택일 경우 추가한다. */-->
                if(data.buildingName !== '' && data.apartment === 'Y'){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                //<!--/* 표시할 참고항목이 있을 경우, 괄호까지 추가한 최종 문자열을 만든다. */-->
                if(extraAddr !== ''){
                    extraAddr = ' (' + extraAddr + ')';
                }
            }

            //<!--/* 우편번호와 주소 정보를 해당 필드에 넣는다. */-->
            document.getElementById("zipCode").value = data.zonecode;
            document.getElementById("mainAddr").value = addr +' '+extraAddr;
            //<!--/* 커서를 상세주소 필드로 이동한다. */-->
            document.getElementById("detailAddr").focus();
            document.getElementById("jibunAddr").value = data.jibunAddress;

            getGeocoder();  //<!--/* 주소 위도경도 *-->/
            if(document.getElementById("zipCode").value.length !== 0 && document.getElementById("mainAddr").value.length !== 0) {
                frmErrChk("zipCode", false);
                frmErrChk("mainAddr", false);
            }

            //<!--/* iframe을 넣은 element를 안보이게 한다. */-->
            //<!--/* (autoClose:false 기능을 이용한다면, 아래 코드를 제거해야 화면에서 사라지지 않는다.) */-->
            element_layer.style.display = 'none';
        },
        width : '100%',
        height : '100%',
        maxSuggestItems : 5
    }).embed(element_layer);

    //<!--/* iframe을 넣은 element를 보이게 한다. */-->
    element_layer.style.display = 'block';

    //<!--/* iframe을 넣은 element의 위치를 화면의 가운데로 이동시킨다. */-->
    initLayerPosition();
}

//<!--/* 브라우저의 크기 변경에 따라 레이어를 가운데로 이동시키고자 하실때에는 */-->
//<!--/* resize이벤트나, orientationchange이벤트를 이용하여 값이 변경될때마다 아래 함수를 실행 시켜 주시거나, */-->
//<!--/* 직접 element_layer의 top,left값을 수정해 주시면 됩니다. */-->
function initLayerPosition(){
    var width = "450"; //<!--/* 우편번호서비스가 들어갈 element의 width 350 */-->
    var height = "500"; //<!--/* 우편번호서비스가 들어갈 element의 height 400 */-->
    var borderWidth = 1; //<!--/* 샘플에서 사용하는 border의 두께 */-->

    //<!--/* 위에서 선언한 값들을 실제 element에 넣는다. */-->
    element_layer.style.width = width + 'px';
    element_layer.style.height = height + 'px';
    element_layer.style.border = borderWidth + 'px solid';
    //<!--/* 실행되는 순간의 화면 너비와 높이 값을 가져와서 중앙에 뜰 수 있도록 위치를 계산한다. */-->
    element_layer.style.left = (((window.innerWidth || document.documentElement.clientWidth) - width)/2 - borderWidth) + 'px';
    element_layer.style.top = (((window.innerHeight || document.documentElement.clientHeight) - height)/2 - borderWidth) + 'px';
}
//<!--/*  *** 다음 주소 api *** */-->