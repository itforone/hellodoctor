/**
 * 의사-원격진료/진료내역 공통 js
 */

let nowUrl = "";
$(document).ready(function() {
    //<!--/* 현재메뉴 */-->
    let href = document.location.href;
    if(href.indexOf("remoteList") != -1) nowUrl = "remoteList"; //<!--/* 원격진료-예약신청 */-->
    else if(href.indexOf("remoteReceipt") != -1) nowUrl = "remoteReceipt" //<!--/* 원격진료-접수 */-->
    else if(href.indexOf("treatList") != -1) nowUrl = "treatList"; // <!--/* 진료내역 */-->

    //<!--/* 목록 */-->
    getList(1);

    //<!--/* 진료 예약 거부/진료 취소 select */-->
    $("#cancelCode").change(function() {
        if(this.value == 1 || this.value == 2) {
            $("#cancelReason").val($("#cancelCode option:checked").text());
        } else {
            $("#cancelReason").val("");
        }
    });
});

//<!--/* 페이징 클릭시 ajax 호출 */-->
$(document).on("click", ".paging a", function(e) {
    e.preventDefault();
    let href = this.getAttribute("href");
    let page = href.split("page=")[1];

    $(".paging a").removeClass("active");
    $(this).addClass("active");

    getList(page);
});

//<!--/* 목록노출개수 */-->
var limit = 15; //<!--/* 디폴트 15 */-->
function limitFilter(filter) {
    limit = filter.value;
    document.querySelector("[name=limit]").value = filter.value;

    getList(1);
}

//<!--/* 환자 정보 모달 */-->
var medicalIdx; //<!--/* 선택 진료 idx */-->
function patientPop(idx) {
    medicalIdx = idx;
    $.ajax({
        url: "/hospital/patientInfo",
        type: "POST",
        data: {idx: medicalIdx, schDate: document.querySelector("#schDate").value},
        async: false,
        success: function(result) {
            document.querySelector("#patientInfo").innerHTML = result;
            $("#patient_pop").modal("show");

            //<!--/* 첨부파일 사진 확대 */-->
            if (typeof Swiper != "undefined") {
                var swiper = new Swiper(".gall_zoom", {
                    autoHeight: true,
                    spaceBetween: 20,
                    navigation: {
                        nextEl: ".swiper-button-next",
                        prevEl: ".swiper-button-prev",
                    },
                    pagination: {
                        el: ".swiper-pagination",
                        clickable: true,
                    },
                });
            }

            //<!--/* 더보기 */-->
            $(".view_btn").click(function() {
                var drop_view = document.querySelector(".drop_view");
                var down = document.querySelector(".down");
                var up = document.querySelector(".up");
                if($(".view_btn").hasClass("more")) {
                    $(".view_btn").removeClass("more");
                    drop_view.style.setProperty("display", "table-row");
                    down.style.setProperty("display", "none");
                    up.style.setProperty("display", "inline-block");
                } else {
                    $(".view_btn").addClass("more");
                    drop_view.style.setProperty("display", "none");
                    down.style.setProperty("display", "inline-block");
                    up.style.setProperty("display", "none");
                }
            });
        },
        error : function(result) {
            console.log(result);
        },
        beforeSend: function() {
            callPageLoading(1);
        },
        complete: function() {
            callPageLoading(0);
        }
    });
}

//<!--/* 처방전 정보 모달 */-->
function prescriptionPop(idx, presIdx, mode) {
    medicalIdx = idx;

    if(mode != "comp") {
        // 처방전 등록 전 진료소견 입력여부 확인
        var opinionIdx = document.querySelector(`input[name="opinionIdx[${idx}]"]`).value;
        if (opinionIdx == 0) {
            swal.fire({html: "진료소견이 작성되지 않아<br><span class='color_green'>처방전 등록이 불가능</span>합니다.<br>진료실에 입장하셔서<br><strong>진료소견을 저장</strong>해 주세요."});
            return false;
        }
    }

    $.ajax({
        url: "/hospital/prescriptionInfo",
        type: "POST",
        data: {idx: medicalIdx, presIdx: presIdx},
        async: false,
        success: function(result) {
            document.querySelector("#prescriptionInfo").innerHTML = result;
            if(mode == "comp") { //<!--/* 비용 청구 완료 시 */-->
                $("#px_zoom_pop").modal("show");
            } else {
                $("#px_pop").modal("show");

                //<!--/* 처방전이 없습니다. */-->
                var fileName = document.querySelector(".file_name");
                var btnWrap = document.querySelector(".btn_wrap");
                document.querySelector("#noPres").addEventListener("click", function() {
                    if(this.checked) {
                        fileName.style.setProperty("display", "none", "important");
                        btnWrap.style.setProperty("display", "none", "important");
                        deleteImage(1);
                    } else {
                        fileName.style.setProperty("display", "block", "important");
                        btnWrap.style.setProperty("display", "block", "important");
                    }
                });

                //<!--/* 수정 시 처방전이 없습니다 */-->
                if(document.querySelector("#noPres").checked) {
                    fileName.style.setProperty("display", "none", "important");
                    btnWrap.style.setProperty("display", "none", "important");
                }

                //<!--/* 수정 시 업로드된 파일 확인 */-->
                let _originUrl = document.querySelector(`[name=originFile1]`).value;
                let _fileName = _originUrl.split("/").reverse()[0]; //<!--/* 서버파일명 */-->
                let _originName = document.querySelector(`[name=originFileName1]`).value; //<!--/* 실제파일명 */-->
                if (_originUrl !== "") {
                    createPrevImage(1, _originUrl, _originName);
                }
            }
        },
        error : function(result) {
            console.log(result);
        },
        beforeSend: function() {
            callPageLoading(1);
        },
        complete: function() {
            callPageLoading(0);
        }
    });
}

//<!--/* 처방전 저장 */ -->
let errMsg = "오류가 발생하였습니다.";
function prescriptionSave(idx) {
    medicalIdx = idx;

    if(document.querySelector("#file1").value === "" && document.querySelector("[name=originFile1]").value === "" && !document.querySelector("#noPres").checked) { //<!--/* 파일체크 */-->
        swal.fire({html: "처방전 이미지를 첨부하세요."});
        return false;
    }

    var formData = new FormData(document.presFrm);
    formData.append("idx", medicalIdx);
    $.ajax({
        url: "/hospital/prescriptionSave",
        data: formData,
        type: "POST",
        async: false,
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        success: function(response) {
            let data = JSON.parse(response);
            if (data.result) {
                swal.fire({
                    html: "처방전이 "+document.querySelector("#dtext").textContent+"되었습니다."
                }).then(()=>{
                    $("#px_pop").modal("hide");
                    getList(document.frmSearch.page.value);
                });
            }
        },
        error: function(xhr) {
            console.log("[serverUploadImage] : [error] : ", xhr);
            swal.fire({html: errMsg, confirmButtonText : '확인',});
        },
        complete:function(data,textStatus) {
        }
    });
}

//<!--/* 진료 예약 거부/진료 취소 모달 */-->
function denyPop(idx) {
    medicalIdx = idx;
    $("#deny_pop").modal("show");
}

//<!--/* 상태-거부/진료취소 */-->
function receiptReject() {
    var cancelReason = $("#cancelReason").val();
    if(cancelReason.trim().length == 0) {
        swal.fire({html: "취소 사유를 입력하세요."});
        return false;
    }

    var msg = "진료 예약을 거부하였습니다.";
    if(nowUrl == "remoteReceipt") msg = "진료가 취소되었습니다.";

    $.ajax({
        url: "/hospital/receiptReject",
        type: "POST",
        data: {idx: medicalIdx, cancelReason: cancelReason},
        async: false,
        success: function(result) {
            if(result) {
                $("#deny_pop").modal("hide");
                swal.fire({
                    html: msg
                }).then(()=>{
                    getList(document.frmSearch.page.value);
                });
            }
        },
        error: function(result) {
            console.log(result);
        },
    });
}

//<!--/* 취소 사유 모달 */-->
function reasonPop(reason) {
    $("#reason_pop textarea").val(reason);
    $("#reason_pop").modal("show");
}

//<!--/* 목록 */-->
function getList(page) {
    let f = document.frmSearch;
    if(page != undefined) f.page.value = page;

    let url = "";
    var msg = "";
    var schFlag = true;
    if(nowUrl == "treatList") {
        msg = f.schDate.value;
        url = "/hospital/ajaxTreatList"
        if(f.schDateSt.value == "" && f.schDateEd.value == "") schFlag = false;
    }
    else {
        if(nowUrl == "remoteList") { //<!--/* 원격진료-예약신청 */-->
            url = "/hospital/ajaxRemoteList";
            msg = "접수일";
        }
        else if(nowUrl == "remoteReceipt") { //<!--/* 원격진료-접수 */-->
            url = "/hospital/ajaxRemoteReceipt"
            msg = "진료일";
        }
        if(f.schDate.value == "") schFlag = false;
    }

    //<!--/* 날짜 검색 */-->
    if(!schFlag) {
        swal.fire({html: msg+"을 선택하세요."});
        return false;
    }

    //<!--/* 검색조건 */-->
    if (f.schKeyword.value.trim() != "" && f.schKeyword.value.trim().length < 2) {
        swal.fire({
            html:'검색어를 2자 이상 입력해 주세요.',
            confirmButtonText : '확인',
            didDestroy: function () {
                f.stx.focus();
            }
        });
        return false;
    }

    var formData = new FormData(f);
    $.ajax({
        url: url,
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        async: false,
        success: function(result) {
            document.querySelector(".remoteList").innerHTML = result;
            document.querySelector("#limitFilter").value = limit; // 목록 갯수 필터
        },
        error: function(result) {
            console.log(result);
        },
        beforeSend: function() {
            callPageLoading(1);
        },
        complete: function() {
            setTimeout(function() {
                callPageLoading(0);
            }, 500);
        }
    });

    return false;
}

//<!--/* 미리보기 이미지 생성 */-->
function createPrevImage(num, url, name) {
    let html = `${name}<span class="ic_close"></span>`; //<!--/* 파일명 */-->
    let area = document.querySelector(`[data-area="${num}"]`); //<!--/* 파일명 표시 위치 */-->
    let confirm = document.querySelector(`#fileConfirm${num}`); //<!--/* 파일 확인 버튼 */-->
    let add = document.querySelector(`#fileAdd${num}`);  //<!--/* 파일 첨부 버튼 */-->

    area.innerHTML = html;
    confirm.style.setProperty("display", "block", "important"); //<!--/* 파일 확인 버튼 숨김 해제 */-->
    add.style.setProperty("display", "none", "important"); //<!--/* 파일 첨부 버튼 숨김 */-->
    $(`#fileConfirm${num}`).attr("onclick", `fileConfirm('${num}', '${url}')`); //<!--/* 파일 확인 버튼 이벤트 */-->
}

//<!--/* 미리보기 이미지 404 처리 */-->
function onErrorPrevImage(num) {
    let area = document.querySelector(`[data-area="${num}"]`);
    area.innerHTML = "";
}

//<!--/* 파일업로드 */-->
function uploadImagePrev(input, num) {
    let reg_ext = /(.*?)\.(jpg|jpeg|png|pdf)$/;

    if (input.files[0] == undefined) {
        input.value = "";
        return false;
    }

    if (!reg_ext.test(input.files[0].name)) {
        swal.fire({
            html:'이미지만 등록이 가능합니다. ',
            confirmButtonText: '확인',
            didDestroy: function() {
                input.value = "";
            }
        });
        return false;
    }

    //<!--/* 최대용량 체크 */-->
    let maxSizeMb = 10; //10mb
    let maxByte = maxSizeMb * 1024 * 1024;
    if (input.files[0].size > maxByte) {
        swal.fire({
            html: "이미지가 최대 용량 (" + maxSizeMb + "mb)을 초과합니다.",
            confirmButtonText: '확인',
            didDestroy: function() {
                input.value = "";
            }
        });
        return false;
    }

    //<!--/* 미리보기 */-->
    //<!--/* console.log(num) */-->
    if (input.files && input.files[0]) {
        let reader = new FileReader();
        reader.onload = function (e) {
            let src = e.target.result;
            let name = input.files[0].name; //<!--/* 파일명 */-->
            createPrevImage(num, src, name);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

//<!--*/ 첨부파일삭제 */-->
$(document).on("click", ".ic_close", function(e) {
    let num = $(this).parents(".file_name").data("area");
    deleteImage(num);
});
function deleteImage(num) {
    let area = document.querySelector(`[data-area="${num}"]`);
    let confirm = document.querySelector(`#fileConfirm${num}`); //<!--/* 파일 확인 버튼 */-->
    let add = document.querySelector(`#fileAdd${num}`); //<!--/* 파일 첨부 버튼 */-->

    confirm.style.setProperty("display", "none", "important"); //<!--/* 파일 확인 버튼 숨김 */-->
    add.style.setProperty("display", "block", "important"); //<!--/* 파일 첨부 버튼 숨김 해제 */-->
    area.innerHTML = "&nbsp;";
    document.getElementById(`file${num}`).value = "";
    document.querySelector(`[name=originFile${num}]`).value = "";
}

//<!--/* 파일 확인 버튼 (미리보기) */-->
function fileConfirm(num, url) {
    document.querySelector('#file_pop01 .modal-body img').src = url;
    document.querySelector('#file_pop01 .modal-body img').setAttribute("onError", `onErrorPrevImage(${num})`);
}