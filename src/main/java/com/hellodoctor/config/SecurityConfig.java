package com.hellodoctor.config;

import com.hellodoctor.security.CommonAuthFailureHandler;
import com.hellodoctor.security.SessionListner;
import com.hellodoctor.service.app.LoginService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.http.HttpSessionListener;
import javax.sql.DataSource;

/**
 * 앱 로그인
 *
 * form에 있는 action을 시큐리티가 가로챔 (컨트롤러 라우터 만들 필요없음)
 * 로그인 성공하면 시큐리티에서 자동으로 세션 생성 (세션에 등록되는 Type : UserDetails)
 */
@Slf4j
@RequiredArgsConstructor
@Configuration
@EnableWebSecurity // security 적용
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    LoginService loginService;

    // 로그인 실패 핸들러 의존성 주입
    private final CommonAuthFailureHandler customFailureHandler;

    private final DataSource dataSource;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        log.info("SecurityConfig - 회원");
        http
            .csrf().disable() // 위조요청 방지
            .authorizeRequests() // 인가요청 확인
                .antMatchers("/api/**").permitAll() // 통과
                // 해당 요청들 로그인 체크
                .antMatchers("/app/mypage").authenticated()
                // /app 요청에 대해서는 ROLE 확인 -> 적용하기에 로그인을 안해도 확인가능한 페이지가 많음
                //.antMatchers("/app/**").hasRole("MEMBER")
                // 나머지 로그인 체크 X
                .anyRequest().permitAll()
                .and()
            .exceptionHandling()
                // .accessDeniedHandler(new CustomAccessDeniedHandler()) // 권한체크
                // .authenticationEntryPoint(new CustomAuthenticationEntryPoint()) // 인증체크
                .and()
            .formLogin()
                .loginPage("/app/emailLogin")
                .defaultSuccessUrl("/app/main", true)
                .usernameParameter("email")
                .passwordParameter("password")
                // .failureUrl("/app/emailLogin/error")
                .failureHandler(customFailureHandler) // 로그인 실패 핸들러
                .and()
            .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/")
                .and()
            .rememberMe()   // 자동로그인
                .rememberMeParameter("remember-me")
                .tokenValiditySeconds(86400 * 30) // 유효시간 (86400 = 24시간)
                .userDetailsService(loginService)
                .tokenRepository(tokenRepository())
                .and()
            .sessionManagement()
                .maximumSessions(10) // 최대허용개수
                .maxSessionsPreventsLogin(false) // 동시로그인차단 (true: 현재사용자 로그인불가, false: 기존사용자 세션만료)
                .expiredUrl("/app/main");   // 세션만료 시 이동할 페이지


        // 중복로그인확인
        //http.sessionManagement()
                //.maximumSessions(1) //세션 최대 허용 수
                //.maxSessionsPreventsLogin(false); // false이면 중복 로그인하면 이전 로그인이 풀린다.
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        // form값 loginService로 전달
        auth.userDetailsService(loginService).passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        //시큐리티가 로그인 과정에서 password를 가로챌때 어떤 해쉬로 암호화 했는지 확인
        return new BCryptPasswordEncoder();
    }

    // 세션타임 확장
    @Bean
    public HttpSessionListener httpSessionListener() {
        return new SessionListner();
    }

    // JDBC 기반 자동로그인
    @Bean
    public PersistentTokenRepository tokenRepository() {
        JdbcTokenRepositoryImpl jdbcTokenRepository = new JdbcTokenRepositoryImpl();
        jdbcTokenRepository.setDataSource(dataSource);
        return jdbcTokenRepository;
    }


}
