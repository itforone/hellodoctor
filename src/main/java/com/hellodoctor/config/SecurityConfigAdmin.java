package com.hellodoctor.config;

import com.hellodoctor.security.*;
import com.hellodoctor.service.admin.ALoginService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * 관리자 로그인
 */
@Slf4j
@RequiredArgsConstructor
@EnableWebSecurity // security 적용
@Order(3) // 낮을수록 우선순위 높음
public class SecurityConfigAdmin extends WebSecurityConfigurerAdapter {
    @Autowired
    ALoginService loginService;

    // 로그인 실패 핸들러 의존성 주입
    private final CommonAuthFailureHandler customFailureHandler;

    private final PasswordEncoder passwordEncoder;
    private final PersistentTokenRepository tokenRepository;

    String loginPage = "/admin/login";

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        log.info("SecurityConfig - 관리자");
        http
            .headers().frameOptions().sameOrigin()  // 동일도메인 iframe 접근 허용
            .and()
            .csrf().disable() // 위조요청 방지
            .httpBasic()
            .and()
            .requestMatchers() // 아래 명시한 path는 HospitalSecurityConfig 에서 담당
                .antMatchers("/admin/**")
                .and()
            .authorizeRequests() // 인가요청 확인
                // 해당페이지들 권한체크 PASS
                .antMatchers("/admin/login", "/admin/certify/**").permitAll()
                // /hospital 요청에 대해서는 ROLE 확인
                .antMatchers("/admin/**").hasAnyRole("ADMIN")
                .and()
            .exceptionHandling()
                .accessDeniedHandler(new CustomAccessDeniedHandler("ADMIN")) // 인증안됨 핸들러
                .authenticationEntryPoint(new CustomAuthenticationEntryPoint("ADMIN")) // 권한없음 핸들러
                .and()
            .formLogin()
                .loginPage(loginPage)
                .defaultSuccessUrl("/admin/memberList", true)
                .usernameParameter("email")
                .passwordParameter("password")
                .failureUrl(loginPage)
                .failureHandler(customFailureHandler) // 로그인 실패 핸들러
                .and()
            .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/admin/logout"))
                .logoutSuccessUrl(loginPage)
                .and()
            .rememberMe()   // 자동로그인
                .rememberMeParameter("remember-me")
                .tokenValiditySeconds(86400 * 30) // 유효시간 (86400 = 24시간)
                .userDetailsService(loginService)
                .tokenRepository(tokenRepository)
                .and()
            .sessionManagement()
                .maximumSessions(10) // 최대허용개수
                .maxSessionsPreventsLogin(false) // 동시로그인차단 (true: 현재사용자 로그인불가, false: 기존사용자 세션만료)
                .expiredUrl(loginPage);   // 세션만료 시 이동할 페이지
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // form값 HospitalLoginService로 전달
        auth.userDetailsService(loginService).passwordEncoder(passwordEncoder);
    }
}
