package com.hellodoctor.config;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * AmazonS3Config 생성
 *
 * Bucket : 파일이 저장될 프로젝트
 * Object : 저장되는 파일
 */
@Slf4j
@Configuration
public class NCloudStorageConfig {
    @Value("${ncloud.endPoint}")
    private String endPoint;

    @Value("${ncloud.regionName}")
    private String regionName;

    @Value("${ncloud.accessKey}")
    private String accessKey;

    @Value("${ncloud.secretKey}")
    private String secretKey;

    @Value("${ncloud.bucketName}")
    private String bucketName;

    @Bean
    public AmazonS3 amazonS3() {
        AmazonS3 s3 = AmazonS3ClientBuilder.standard()
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endPoint, regionName))
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey)))
                .build();

        log.info("NCloudStorageConfig=" + s3.toString());

        /*
        try {
            // 버킷 생성 - 미리 생성되어 있어 필요없음
            // if (s3.doesBucketExistV2(bucketName)) {
            //     log.info("Bucket %s already exists.\n", bucketName);
            // } else {
            //     s3.createBucket(bucketName);
            //     log.info("Bucket %s has been created.\n", bucketName);
            // }

            // 버킷 목록조회
            List<Bucket> buckets = s3.listBuckets();
            log.info("Bucket List: ");
            for (Bucket bucket : buckets) {
                log.info("    name=" + bucket.getName() + ", creation_date=" + bucket.getCreationDate() + ", owner=" + bucket.getOwner().getId());
            }
        } catch (AmazonS3Exception e) {
            log.info("AmazonS3Exception=" + e.toString());
        } catch(SdkClientException e) {
            log.info("SdkClientException=" + e.toString());
        }
        */

        //initFolders();
        return s3;
    }

    //@PostConstruct
    // 초기폴더 생성
    // public void initFolders () {
    //     String[] folders = {"test1", "test2"};
    //     log.info("initFolders()");
    //     for (String name : folders) {
    //         log.info(name);
    //         nCloudStorageUtils.createFolder(name);
    //     }
    // }
}
