package com.hellodoctor.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class rootController {
    @GetMapping("/app")
    public String appMain() {
        return "redirect:/app/main";
    }

    @GetMapping("/hospital")
    public String hospitalMain() {
        return "redirect:/hospital/login";
    }

    @GetMapping("/pharmacy")
    public String pharmacyMain() {
        return "redirect:/pharmacy/login";
    }

    @GetMapping("/admin")
    public String adminMain() {
        return "redirect:/admin/login";
    }
}
