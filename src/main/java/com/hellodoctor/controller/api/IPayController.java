package com.hellodoctor.controller.api;

import com.google.gson.JsonObject;
import com.hellodoctor.common.AuthMember;
import com.hellodoctor.common.Functions;
import com.hellodoctor.model.app.Member;
import com.hellodoctor.service.api.IPayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.Map;

/**
 * 이노페이 PG 컨트롤러
 */
@Slf4j
@Controller
@RequestMapping("/api")
public class IPayController {
    @Autowired
    IPayService iPayService;

    // [앱] 결제관리 - 카드등록 - 이노페이 즉시카드등록
    @PostMapping("/regAutoCardBill")
    @ResponseBody
    public String regAutoCardBill(HttpServletRequest request, @AuthMember Member member) { //@RequestParam Map<String, String> map,
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);
        obj.addProperty("errMsg", "");

        if (member == null) {
            obj.addProperty("errMsg", "로그인 세션이 만료되었습니다.");
        } else {
            Map<String, Object> response = iPayService.regAutoCardBill(request, member);
            obj.addProperty("result", (Boolean) response.get("result"));
            obj.addProperty("errMsg", String.valueOf(response.get("errMsg")));
        }

        return obj.toString();
    }

    // [앱] 원격진료 조제비 카드결제
    @PostMapping("/paymentPharmacy")
    @ResponseBody
    public String paymentPharmacy(HttpServletRequest request, @AuthMember Member member) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);
        obj.addProperty("errMsg", "");

        // 결제승인API
        Map<String, Object> response = iPayService.paymentRequest(request, member, "P");
        obj.addProperty("result", (Boolean) response.get("result"));

        return obj.toString();
    }

    // [앱] 원격진료 1원결제 승인요청
    @PostMapping("/paymentTestRequest")
    @ResponseBody
    public String paymentTestRequest(HttpServletRequest request, @AuthMember Member member) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);
        obj.addProperty("errMsg", "");

        // 결제승인API
        Map<String, Object> response = iPayService.paymentRequest(request, member, "T");
        obj.addProperty("result", (Boolean) response.get("result"));
        obj.addProperty("tid", String.valueOf(response.get("tid")));

        return obj.toString();
    }

    // [앱] 원격진료 1원결제 취소요청
    @PostMapping("/paymentTestCancel")
    @ResponseBody
    public void paymentTestCancel(HttpServletRequest request, @AuthMember Member member) {
        String tid = String.valueOf(request.getParameter("tid"));
        iPayService.cancelPayment(tid, 1, "1원결제");
    }

    // [의사웹] 원격진료 진료비 승인요청
    @PostMapping("/billAmtCharge")
    @ResponseBody
    public String billAmtCharge(HttpServletRequest request) { //, @RequestParam Map<String, Integer> params
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);
        obj.addProperty("errMsg", "");

        Map<String, Object> response = iPayService.billAmtCharge(request);
        obj.addProperty("result", (Boolean) response.get("result"));
        obj.addProperty("failedSendPush", (Boolean) response.get("failedSendPush"));

        String resultCode = Functions.isNull(response.get("resultCode"))? "" : response.get("resultCode").toString();
        String resultMsg = Functions.isNull(response.get("resultMsg"))? "" : response.get("resultMsg").toString();
        obj.addProperty("resultCode", resultCode);
        obj.addProperty("resultMsg", resultMsg);

        return obj.toString();
    }


}