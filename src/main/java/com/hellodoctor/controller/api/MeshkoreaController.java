package com.hellodoctor.controller.api;

import com.google.gson.JsonObject;
import com.hellodoctor.common.AuthMember;
import com.hellodoctor.model.app.Member;
import com.hellodoctor.service.api.MeshkoreaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 메쉬코리아(퀵) 컨트롤러
 */
@Slf4j
@Controller
@RequestMapping("/api")
public class MeshkoreaController {
    @Autowired
    MeshkoreaService meshKoreaService;

    // 메쉬코리아 API 테스트 페이지
    @GetMapping("/meshkorea")
    public String meshkorea() {
        return "app/api_meshkorea";
    }

    // 메쉬코리아 API
    @PostMapping("/meshkoreaAPI")
    @ResponseBody
    public String meshkoreaAPI(HttpServletRequest request, @AuthMember Member member) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);
        obj.addProperty("errMsg", "");

        // if (member == null) {
        //     obj.addProperty("errMsg", "로그인 세션이 만료되었습니다.");
        // } else {
            Map<String, Object> response = meshKoreaService.meshkoreaAPI(request, member);
            obj.addProperty("result", (Boolean) response.get("result"));
            obj.addProperty("errMsg", String.valueOf(response.get("errMsg")));
            log.info("obj: "+obj);
        // }

        return obj.toString();
    }
}
