package com.hellodoctor.controller.api;

import com.google.gson.JsonObject;
import com.hellodoctor.common.AuthMember;
import com.hellodoctor.model.app.Member;
import com.hellodoctor.service.api.DoobalheroService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 두발히어로(택배) 컨트롤러
 */
@Slf4j
@Controller
@RequestMapping("/api")
public class DoobalheroController {
    @Autowired
    DoobalheroService doobalheroService;

    // 두발히어로 API 테스트 페이지
    @GetMapping("/doobalhero")
    public String meshkorea() {
        return "app/api_doobalhero";
    }

    // 두발히어로 API
    @PostMapping("/doobalheroAPI")
    @ResponseBody
    public String doobalheroAPI(HttpServletRequest request, @AuthMember Member member) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);
        obj.addProperty("errMsg", "");

        // if (member == null) {
        //     obj.addProperty("errMsg", "로그인 세션이 만료되었습니다.");
        // } else {
            Map<String, Object> response = doobalheroService.doobalheroAPI(request, member);
            obj.addProperty("result", (Boolean) response.get("result"));
            obj.addProperty("errMsg", String.valueOf(response.get("errMsg")));
            log.info("obj: "+obj);
        // }

        return obj.toString();
    }
}
