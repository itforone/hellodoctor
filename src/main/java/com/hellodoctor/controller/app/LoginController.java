package com.hellodoctor.controller.app;

import com.hellodoctor.common.Constants;
import com.hellodoctor.common.AuthMember;
import com.hellodoctor.model.app.Member;
import com.hellodoctor.service.app.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;

/**
 * 로그인 (네이버, 카카오, 이메일)
 */

@Controller // 컨트롤러임을 명시
@RequestMapping("/app") // /app
@Slf4j
public class LoginController {

    @Autowired
    MemberService memberService;

    // 이메일로 로그인
    @GetMapping("/emailLogin") // /app/emailLogin
    public String emailLogin(@RequestParam(value = "error", required = false) String error,
                             @RequestParam(value = "code", required = false) String errorCode,
                             @AuthMember Member member,
                             Model model) {

        if (error != null) {    // 로그인 에러메시지 출력
            Integer code = Integer.parseInt(errorCode);
            model.addAttribute("error", error);
            model.addAttribute("exception", Constants.MEMBER_LOGIN_ERR_MSG.get(code));
        }

        // 로그인 체크
        if (member == null) {
            return "app/email_login";   // templates/app/email_login.html
        } else {
            log.info("로그인사용자=" + member.toString());
            return "redirect:/app/main";
        }
    }

    // 1.2.5 아이디 찾기
    @GetMapping("/findId/{name}/{birth}") // /app/findId
    public String findId(@AuthMember Member authMember, @PathVariable String name, @PathVariable String birth, Model model) {
        // 로그인 체크
        if (authMember == null) {
            // 본인인증 성공 시 이름과 생년월일을 받아와서 회원정보에 있는지 확인
            Member member = memberService.isMember("", name, birth);
            //log.info("data: "+member);
            if(member == null) { // 일치하는 정보 없음
                model.addAttribute("cert", false);
            } else {
                model.addAttribute("cert", true);
                model.addAttribute("result", member.getMbId());
            }
        } else { // 로그인 중
            return "redirect:/app/main";
        }
        return "app/find_id"; // templates/app/find_id.html
    }

    // 1.2.6 비밀번호 찾기
    @GetMapping(value = {"/findPw/{id}/{name}/{birth}", "/findPw"})
    public String findPw(@AuthMember Member authMember, @PathVariable(required = false) String id, @PathVariable(required = false) String name, @PathVariable(required = false) String birth, Model model) {
        // 로그인 체크
        if (authMember == null) {
            model.addAttribute("cert", false);
            model.addAttribute("cert_id", "");
            if(id != null) {
                // 본인인증 후 입력받은 아이디와 인증정보가 일치하는지 확인
                model.addAttribute("isParam", true);
                Member member = memberService.isMember(id, name, birth);
                //log.info("data: "+member);
                if(member != null) { // 일치하는 정보 있음
                    model.addAttribute("cert", true);
                    model.addAttribute("cert_id", id);
                }
            } else {
                model.addAttribute("isParam", false);
            }
        } else { // 로그인 중
            return "redirect:/app/main";
        }
        return "app/find_pw"; // templates/app/find_pw.html
    }

    // 1.2.7 비밀번호 재설정
    @GetMapping("/resetPw/{id}") // /app/resetPw
    public String resetPw(@PathVariable String id, Model model) {
        model.addAttribute("id", id);
        return "app/reset_pw"; // templates/app/reset_pw.html
    }

    // 비밀번호 재설정 액션
    @PostMapping("/resetPwAction")
    @ResponseBody
    public int resetPwAction(@RequestParam("id") String id, @RequestParam("pw") String pw) {
        return memberService.resetPassword(id, pw);
    }

}