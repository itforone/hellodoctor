package com.hellodoctor.controller.app;

import com.hellodoctor.common.AuthMember;
import com.hellodoctor.common.Constants;
import com.hellodoctor.common.Functions;
import com.hellodoctor.config.ApiKey;
import com.hellodoctor.model.app.Member;
import com.hellodoctor.service.app.MapService;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 병원/약국 지도 controller
 */

@Controller
@RequestMapping("/app") // /app
@Slf4j
public class MapController {
    @Autowired
    MapService mapService;

    // 4.1.1 지도보기
    @GetMapping(value = {"/hospitalPharmacyMap/{gubun}", "/hospitalPharmacyMap"})
    public String hospitalPharmacyMap(@AuthMember Member member, @PathVariable(required = false) String gubun, Model model) {
        if (member == null) {
            model.addAttribute("isLogout", true);
        } else {
            model.addAttribute("kakao_js_key", ApiKey.KAKAO_JS_KEY);
            model.addAttribute("err", false);
            if(gubun == null) {
                model.addAttribute("gubun", "hospital");
            } else {
                model.addAttribute("gubun", gubun);
            }
        }

        return "app/hospital_pharmacy_map";
    }

    // 4.1.2 목록보기
    @PostMapping("/hospitalPharmacyList")
    public String hospitalParmacyList(HttpServletRequest request, Model model) {
        model.addAttribute("err", false);

        if(request.getParameter("gubun") == null) {
            model.addAttribute("err", true);
        } else {
            // 위도/경도
            String lat = (request.getParameter("lat") == null)? Constants.EMPTY_LAT : request.getParameter("lat");
            String lng = (request.getParameter("lng") == null)? Constants.EMPTY_LNG : request.getParameter("lng");

            model.addAttribute("kakao_js_key", ApiKey.KAKAO_JS_KEY);
            List<Map<String, Object>> dataList = new ArrayList();
            if(request.getParameter("gubun").equals("hospital")) {
                // 병원 목록 | @param: 지역(구), 지역(동), 위도, 경도
                dataList = mapService.getHospitalList(request.getParameter("gu"), request.getParameter("dong"), lat, lng);
            } else {
                // 약국 목록 | @param: 지역(구), 지역(동), 위도, 경도
                dataList = mapService.getPharmacyList(request.getParameter("gu"), request.getParameter("dong"), lat, lng);
            }
            model.addAttribute("dataList", dataList);
            model.addAttribute("listCount", dataList.size());
            model.addAttribute("gubun", request.getParameter("gubun"));
        }

        return "app/hospital_pharmacy_list";
    }

    // 병원/약국 위치 좌표 목록
    @PostMapping("/getLatLng")
    @ResponseBody
    public ResponseEntity<?> getLatLng(String gubun) {
        log.info("getLntLng");
        JSONObject obj = new JSONObject();
        // 위치 좌표 목록 | @param: 병원 약국 구분
        if(gubun != null) {
            obj = mapService.getLatLng(gubun);
            //log.info("getLatLng|obj= "+ obj);
            obj.put("errMsg", "");
        } else {
            obj.put("errMsg", "정보를 불러오지 못하였습니다.");
        }
        return new ResponseEntity<>(obj.toString() ,HttpStatus.OK);
    }

    // 선택 병원/약국 정보
    @PostMapping("/getHospitalPharmacyInfo")
    public String getHospitalPharmacyInfo(@RequestParam("idx") Integer idx, @RequestParam("lat") String lat, @RequestParam("lng") String lng, @RequestParam("gubun") String gubun, Model model) {
        log.info("gubun: "+gubun);
        log.info("idx: "+idx);
        if(idx == null || gubun == null) {
            model.addAttribute("err", true);
        } else {
            // 위도/경도
            if(Functions.isNull(lat)) lat = Constants.EMPTY_LAT;
            if(Functions.isNull(lng)) lng = Constants.EMPTY_LNG;

            Map<String, Object> map = new HashMap<>();
            if(gubun.equals("hospital")) map = mapService.getHospitalInfo(idx, lat, lng);
            else if(gubun.equals("pharmacy")) map = mapService.getPharmacyInfo(idx, lat, lng);

            if(map.isEmpty()) {
                model.addAttribute("err", true);
            } else {
                model.addAttribute("returnData", map);
                model.addAttribute("err", false);
            }

            if(gubun.equals("hospital")) return "app/hospital_info";
            else if(gubun.equals("pharmacy")) return "app/pharmacy_info";
        }

        return "redirect:/app/hospitalPharmacyMap";
    }

    /*
    // 병원/약국 목록
    @PostMapping("/getCenterList")
    @ResponseBody
    public ResponseEntity<?> getCenterList(String gu, String dong, String gubun) {
        JSONObject obj = new JSONObject();
        if(gu != null && dong != null && gubun != null) {
            if(gubun.equals("hospital")) {
                obj = mapService.getHospitalList(gu, dong);
                obj.put("errMsg", "");
            } else {
                //mapService.getPharmacyList(gu, dong);
            }
        } else {
            obj.put("errMsg", "정보를 불러오지 못하였습니다.");
        }
        return new ResponseEntity<>(obj.toString() ,HttpStatus.OK);
    }*/
}
