package com.hellodoctor.controller.app;

import com.hellodoctor.service.app.NiceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * NICE 본인인증 controller
 */

@Controller
@RequestMapping("/app") // /app
@Slf4j
public class NiceController {
    @Autowired
    NiceService niceService;
    
    // 1.2.4 본인인증
    @GetMapping(value = {"/certify/{mode}", "/certify/{mode}/{id}"}) // /app/certify
    public String certify(HttpSession session, @PathVariable String mode, @PathVariable(required = false) String id, Model model) {
        log.info("certifyMode: "+mode);

        if(id != null) { log.info("certifyId: "+id); }
        else { id = ""; }
        model.addAttribute("mode", mode);

        String EncodeData = niceService.certify(session, mode, id);
        model.addAttribute("EncodeData", EncodeData);

        return "app/certify"; // templates/app/certify.html
    }

    // nice 인증성공
    @GetMapping(value = {"/niceSuccess/{mode}", "/niceSuccess/{mode}/{id}"})
    public String niceSuccess(HttpServletRequest request, HttpSession session, @PathVariable String mode, @PathVariable(required = false) String id) {
        log.info("niceSuccess");

        List<Map<String, String>> certData = niceService.niceSuccess(request, session);
        String sName = certData.get(0).get("sName"); // 이름
        String sBirthDate = certData.get(0).get("sBirthDate"); // 생년월일
        String sHp = certData.get(0).get("sMobileNo"); // 휴대폰번호
        String sGender = certData.get(0).get("sGender"); // 성별 (0: 여성 / 1: 남성)

        String redirect = "redirect:/app";
        if(mode.equals("findId")) { // 아이디찾기
            redirect += "/findId/"+sName+"/"+sBirthDate;
        } else if(mode.equals("findPw")) { // 비밀번호찾기
            redirect += "/findPw/"+id+"/"+sName+"/"+sBirthDate;
        } else if(mode.equals("changeHp")) { // 연락처변경
            redirect += "/changeHp/"+sHp;
        } else if(mode.equals("signUp")) { // 회원가입
            redirect += "/signUp/"+sName+"/"+sHp+"/"+sBirthDate+"/"+sGender;
        }

        return redirect;
    }

    // nice 인증실패
    @GetMapping("/niceFail")
    public String niceFail(HttpServletRequest request, Model model) {
        log.info("niceFail");

        String sMessage = niceService.niceFail(request);
        model.addAttribute("sMessage", sMessage);

        return "app/certify"; // templates/app/certify.html
    }
}
