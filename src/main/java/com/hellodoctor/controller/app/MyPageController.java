package com.hellodoctor.controller.app;

import com.google.gson.JsonObject;
import com.hellodoctor.common.AuthMember;
import com.hellodoctor.common.Constants;
import com.hellodoctor.common.NCloudStorageUtils;
import com.hellodoctor.config.ApiKey;
import com.hellodoctor.model.app.Hospital;
import com.hellodoctor.model.app.MedicineGuideDetail;
import com.hellodoctor.model.app.Member;
import com.hellodoctor.service.admin.ASystemService;
import com.hellodoctor.service.app.MemberService;
import com.hellodoctor.service.app.MyPageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.*;

import static com.hellodoctor.common.Functions.getHyphenHp;
import static com.hellodoctor.common.Functions.isNull;

/**
 * 마이페이지
 */
@Slf4j
@Controller // 컨트롤러임을 명시
@RequestMapping("/app") // /app
public class MyPageController {
    @Autowired
    MyPageService myPageService;
    @Autowired
    NCloudStorageUtils nCloudStorageUtils;
    @Autowired
    MemberService memberService;
    @Autowired
    ASystemService aSystemService;

    // 5.1.1 마이페이지
    @GetMapping("/mypage")
    public String mypage(@AuthMember Member member, Model model) {
        if (member == null) {
            model.addAttribute("isLogout", true);
        } else {
            // TODO: 개발완료 시 운영키로 변경 필요
            model.addAttribute("kakao_js_key", ApiKey.KAKAO_JS_KEY);
        }

        return "app/mypage";
    }

    // 나의가족관리 목록
    @GetMapping("/myFamily") // /app/myFamily
    public String myFamily(@AuthMember Member member, Model model) {
        model.addAttribute("listCount", 0);

        if (member == null) {
            model.addAttribute("isLogout", true);
        } else {
            List<Object> familyList = myPageService.getFamilyList(member.getIdx());
            model.addAttribute("familyList", familyList);
            model.addAttribute("listCount", familyList.size());
        }

        // 파일목록 확인 - test
        //nCloudStorageUtils.searchObjectList("myFamily");

        return "app/myfamliy_list"; // templates/app/sign_up.html
    }

    // 나의가족관리 등록폼
    @GetMapping("/myFamilyForm") // /app/myFamilyForm
    public String myFamilyForm(Model model) {
        model.addAttribute("famRel", Constants.MEMBER_FAMILY_REL); // 가족관계
        return "app/myfamliy_form"; // templates/app/sign_up_form.html
    }

    // 나의가족관리 수정폼
    @GetMapping("/myFamilyModify/{idx}") // /app/myFamilyForm
    public String myFamilyModify(@PathVariable String idx, Model model, @AuthMember Member member) {
        model.addAttribute("famRel", Constants.MEMBER_FAMILY_REL); // 가족관계

        if (member == null) {
            model.addAttribute("isLogout", true);
        } else {
            int famIdx = Integer.parseInt(idx);
            Map<String, Object> modifyData = myPageService.getFamilyDetail(famIdx);
            model.addAttribute("idx", idx);
            model.addAttribute("data", modifyData);
        }

        //log.info(model.toString());
        return "app/myfamliy_form"; // templates/app/sign_up_form.html
    }

    // 가족추가/수정 처리
    @PostMapping("/myFamilyFormAction")
    @ResponseBody
    public String myFamilyFormAction(MultipartHttpServletRequest request, @AuthMember Member member) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        if (member == null) {
            // 회원정보 없음
            obj.addProperty("errMsg", "로그인 세션이 만료되었습니다.");
        } else {
            // 추가/수정
            int count = myPageService.registerMyFamily(request, member.getIdx());
            if (count == 1) obj.addProperty("result", true);
        }

        return obj.toString();
    }

    // 가족삭제
    @PostMapping("/myFamilyDelete")
    @ResponseBody
    public String myFamilyDelete(@RequestParam Map<String, Object> map, @AuthMember Member member) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        // 파라미터 없음
        if (map.get("idx") == null || map.get("idx").toString().equals("")) {
            return obj.toString();
        }

        // 회원정보 없음
        if (member == null) return obj.toString();

        // 삭제
        int idx = Integer.parseInt(map.get("idx").toString());
        int count = myPageService.deleteMyFamily(idx);
        if (count == 1) obj.addProperty("result", true);

        return obj.toString();
    }

    // 5.1.6 개인정보
    @GetMapping("/profile")
    public String profile(@AuthMember Member member, Model model) {
        if (member == null) {
            model.addAttribute("isLogout", true);
        } else {
            // log.info("mbId: "+member.getMbId());
            model.addAttribute("id", member.getIdx());
            model.addAttribute("name", member.getMbName());

            // 성별
            String sex = "";
            if(member.getMbSex() != null) sex = member.getMbSex();
            model.addAttribute("sex", sex);

            // 생년월일
            String birth = "";
            if(member.getMbBirth() != null) birth = member.getMbBirth().substring(0,4)+"."+member.getMbBirth().substring(4,6)+"."+member.getMbBirth().substring(6);
            model.addAttribute("birth", birth);

            // 연락처
            String hp = "";
            if(member.getMbHp() != null) hp = member.getMbHp();
            model.addAttribute("hp", hp);

            // 주치전담병원
            int hospitalIdx = 0;
            String hospitalName = "";
            if(member.getMyHospitalIdx() != 0) {
                Hospital hospital = myPageService.getMyHospital(member.getMyHospitalIdx()); // 주치전담병원 정보
                if(hospital != null) {
                    hospitalName = hospital.getHospitalName();
                    hospitalIdx = hospital.getIdx();
                }
            }
            model.addAttribute("hospitalIdx", hospitalIdx); // 병원idx
            model.addAttribute("hospitalName", hospitalName); // 병원이름
        }
        return "app/profile";
    }

    // 5.2.11 연락처 변경
    @GetMapping("/changeHp/{hp}")
    public String changeHp(@AuthMember Member member, @PathVariable String hp, Model model) {
        if (member == null) {
            model.addAttribute("isLogout", true);
        } else {
            if(hp != null) {
                hp = getHyphenHp(hp);
                int result = memberService.resetHp(member.getMbId(), hp);
                if(result != 0) {
                    model.addAttribute("hp", hp);
                }
            }
        }
        return "redirect:/app/profile";
    }

    //5.1.17 앱설정 setting.html
    @GetMapping("/setting")
    public String setting(@AuthMember Member member, Model model) {
        if (member == null) {
            model.addAttribute("isLogout", true);
        } else {
            model.addAttribute("pushInfoYn", member.getPushInfoYn());
            model.addAttribute("pushMarketingYn", member.getPushMarketingYn());
            model.addAttribute("memberIdx", member.getIdx());
        }

        return "app/setting";
    }

    // 앱설정 변경처리
    @PostMapping("/changeSettingAction")
    @ResponseBody
    public String changeSettingAction(
            @RequestParam("memberIdx") String memberIdx, @RequestParam("pushInfo") String pushInfo,
            @RequestParam("pushMk") String pushMk, @AuthMember Member member) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);
        obj.addProperty("memberIdx", memberIdx);
        obj.addProperty("pushInfo", pushInfo);
        obj.addProperty("pushMk", pushMk);

        int cnt = myPageService.updatePushSetting(memberIdx, pushInfo, pushMk);
        if (cnt == 1) {
            obj.addProperty("result", true);
            if (member != null) {
                member.setPushInfoYn(pushInfo);
                member.setPushMarketingYn(pushMk);
            }
        }

        return obj.toString();
    }

    // 5.1.8 주치 전담병원 선택 my_hospital.html
    @GetMapping("/myHospital") // /myHospital?lat=xxx&lng=xxx
    public String myHospital(@AuthMember Member member, Model model,
                             @RequestParam(value="lat", required=false, defaultValue="") String lat,
                             @RequestParam(value="lng", required=false, defaultValue="") String lng) {
        if (member == null) {
            model.addAttribute("isLogout", true);
        } else {
            int mbIdx = member.getIdx();
            if (isNull(lat)) lat = Constants.EMPTY_LAT;
            if (isNull(lng)) lng = Constants.EMPTY_LNG;
            log.info("lat=" + lat + ", lng=" + lng);

            if (mbIdx != 0) {
                List<Map<String, Object>> dataList = myPageService.getMyHospitalList(mbIdx, lat, lng);
                model.addAttribute("dataList", dataList);
                model.addAttribute("dataListCount", dataList.size());
                // log.info("dataList= "+dataList);
            } else {
                model.addAttribute("dataList", "");
                model.addAttribute("dataListCount", 0);
            }
        }

        return "app/my_hospital";
    }

    // 주치 전담병원 삭제
    @PostMapping("/deleteMyHospital")
    @ResponseBody
    public int deleteMyHospital(@AuthMember Member member, @RequestParam("delHospitalIdx") String delHospitalIdx, @RequestParam(value = "profile", required = false) String profile) {
        int result = 0;
        if(member != null) {
            result = myPageService.deleteMyHospital(member, delHospitalIdx, profile); // @param : 회원정보, 삭제할 병원idx, 삭제경로
        }
        return result;
    }

    // 주치 전담병원 지정
    @PostMapping("/updateMyHospital")
    @ResponseBody
    public int updateMyHospital(@AuthMember Member member, @RequestParam("selHospitalIdx") String selHospitalIdx) {
        int result = 0;
        if(member != null) {
            result = myPageService.updateMyHospital(member, selHospitalIdx); // @param : 회원정보, 지정할 병원idx
        }
        return result;
    }

    //5.1.14 서비스문의 / 5.1.15 일반문의 목록 contact_list.html
    @GetMapping("/contactList") // contactList?tab=1
    public String contactList(
            @RequestParam(value="tab", required=false, defaultValue="0") int tab,
            @AuthMember Member member,
            Model model) {
        tab = (tab != 1)? 0 : 1;
        model.addAttribute("tab", tab);

        if (member == null) {
            model.addAttribute("isLogout", true);
            model.addAttribute("showHtml", false);
        } else {
            model.addAttribute("showHtml", true);
            model.addAttribute("mbIdx", member.getIdx());
        }

        return "app/contact_list";
    }

    // 서비스문의목록 ajax load
    @GetMapping("/ajaxContactList")
    public String ajaxContactList(
            @RequestParam("tab") int tab, @RequestParam("mbIdx") int mbIdx,
            @RequestParam("searchKey") int month, Model model) {
        Map<Integer, Object> result = myPageService.getContactList(mbIdx, tab, month);
        model.addAttribute("listData", result);
        // model.addAttribute("category", Constants.CS_CATEGORY);
        // log.info("listData=" + result);

        // 타임리프 개행처리
        model.addAttribute("nlString", System.getProperty("line.separator").toString());

        return (tab==0)? "app/contact_list_tab1" : "app/contact_list_tab2"; // ajax html load
    }

    //5.1.16 문의 작성하기 contact_form.html
    @GetMapping("/contactForm") // contactForm?tab=1
    public String contactForm(@RequestParam(value="tab", required=false, defaultValue="0") int tab, Model model) {
        tab = (tab != 1)? 0 : 1;
        model.addAttribute("tab", tab);
        return "app/contact_form";
    }

    // 문의하기 등록처리
    @PostMapping("/contactFormAction")
    @ResponseBody
    public String contactFormAction(MultipartHttpServletRequest request, @AuthMember Member member) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        if (member == null) {
            // 회원정보 없음
            obj.addProperty("errMsg", "로그인 세션이 만료되었습니다.");
        } else {
            // 등록하기
            int csIdx = myPageService.registerContactForm(request, member.getIdx());
            if (csIdx > 0) obj.addProperty("result", true);
        }

        return obj.toString();
    }

    // 5.1.4 복약지도 medi_guide.html
    @GetMapping("/mediGuide") // /mediGuide?month=3
    public String mediGuide(
            @RequestParam(value="month", required=false, defaultValue="3") int month,
            @AuthMember Member member, Model model) {
        if (member == null) {
            // 회원정보 없음
            model.addAttribute("isLogout", true);
        } else {
            String monthStr = month + "개월";
            if (month == 0) monthStr = "전체보기";
            else if (month == 12) monthStr += "(1년)";
            model.addAttribute("monthStr", monthStr);
            model.addAttribute("month", month);

            List<Map<String, String>> list = myPageService.getMedicineGuideList(member.getIdx(), member.getMbName(), month);
            model.addAttribute("listData", list);
        }

        return "app/medi_guide";
    }

    // 5.1.5 상세보기 medi_guide_view.html
    @GetMapping("/mediGuideView")
    public String mediGuideView(
            @RequestParam(value="month", required=false, defaultValue="3") int month,
            @RequestParam("idx") int medicalIdx, // 진료 인덱스
            Model model) {


        List<MedicineGuideDetail> viewData = myPageService.getMedicineGuideDetail(medicalIdx);
        model.addAttribute("viewData", viewData);

        log.info("model=" + model);
        return "app/medi_guide_view";
    }

    // 5.1.2 결제관리 credit.html
    @GetMapping("/credit") // /credit?month=3
    public String credit(
            @RequestParam(value="month", required=false, defaultValue="3") int month,
            @RequestParam(value="page", required=false, defaultValue="1") int page,
            @AuthMember Member member, Model model) {
        if (member == null) {
            // 회원정보 없음
            model.addAttribute("isLogout", true);
        } else {
            model.addAttribute("memberName", member.getMbName());
            model.addAttribute("cardCodeList", Constants.CARD_CODE);

            // 회원카드목록
            //List<MemberCreditCard> cardList = myPageService.getMemberCreditCardList(member.getIdx());
            List<Map<String, Object>> cardList = myPageService.getMemberCreditCardList(member.getIdx());
            model.addAttribute("cardList", cardList);

            // 검색필터
            String monthStr = month + "개월";
            if (month == 0) monthStr = "전체보기";
            else if (month == 12) monthStr += "(1년)";
            model.addAttribute("monthStr", monthStr);
            model.addAttribute("month", month);
        }

        return "app/credit";
    }

    // 결제관리 - 카드등록
    @GetMapping("/creditForm")
    public String creditForm(HttpServletRequest request, HttpServletResponse response,
                             @AuthMember Member member, Model model) {
        if (member == null) {
            // 회원정보 없음
            model.addAttribute("isLogout", true);
            model.addAttribute("showHtml", false);
        } else {
            model.addAttribute("showHtml", true);
            model.addAttribute("memberName", member.getMbName());
            model.addAttribute("memberId", member.getMbId());
            model.addAttribute("memberIdx", member.getIdx()); // 고유번호
        }

        return "app/credit_form";
    }

    // 결제관리 - 카드삭제
    @PostMapping("/deleteCreditCard")
    @ResponseBody
    public String deleteMemberCreditCard(@RequestParam("idx") int idx, @AuthMember Member member) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);
        obj.addProperty("errMsg", "서버와의 통신에 문제가 발생했습니다. 잠시 후 다시 시도해 주세요.");

        if (member == null) {
            // 회원정보 없음
            obj.addProperty("errMsg", "로그인 세션이 만료되었습니다.");
        } else {
            // 삭제처리
            int chk = myPageService.deleteMemberCreditCard(member.getIdx(), idx); //, member.getMbId()

            if (chk > 0) obj.addProperty("result", true);
            else if (chk == -1) {
                obj.addProperty("errMsg", "<span class='color_green'>해당 카드로 결제예정인 진료</span>가 있어<br>삭제가 불가능합니다.<br><strong>진료비 결제 후 삭제가 가능</strong>합니다.");
            }
        }

        return obj.toString();
    }

    // 결제관리 - 결제목록 ajax load
    @PostMapping("/ajaxPayList")
    public String ajaxPaymentHistory(
            @AuthMember Member member, Model model,
            @RequestParam(value="month", required=false, defaultValue="3") int month,
            @RequestParam(value="page", required=false, defaultValue="1") int page) {
        /**
         * page : 호출페이지
         * month : 기간 (n개월, 0=전체)
         * listRow : 한페이지 글 개수
         */
        int listRow = 10;

        // 결제내역
        List<Map<String, Object>> listData = myPageService.ajaxPaymentHistory(page, month, member.getIdx(), listRow, member.getMbName());
        String lastPage = listData.size() < listRow? "Y" : "N"; // 마지막페이지?

        model.addAttribute("page", page);
        model.addAttribute("listData", listData);
        model.addAttribute("lastPage", lastPage);

        return "app/credit_payment_list";
    }

    // 이용약관
    @GetMapping("/policy")
    public String policy(Model model) {
        List<Map<String, String>> termsList = aSystemService.getTermsList();
        // 이용약관 역순
        List<Map<String, String>> termsListReverse = new ArrayList<>();
        for (int i=termsList.size()-1; i >=0; i--) {
            termsListReverse.add(termsList.get(i));
        }

        model.addAttribute("termsList", termsListReverse);

        return "app/policy";
    }

    // 이용약관 상세보기
    @GetMapping("/policyView/{key}")
    public String policyView(Model model, @PathVariable String key) {
        // 약관 key 존재여부 확인
        if (Constants.SITE_TERMS_LIST.containsKey(key)) {
            model.addAttribute("title", Constants.SITE_TERMS_LIST.get(key));
            model.addAttribute("data", aSystemService.getTerms(key));
            model.addAttribute("key", key);
            model.addAttribute("nlString", System.getProperty("line.separator").toString()); // 타임리프 개행처리
            model.addAttribute("newLineChar", "\n"); // 타임리프 개행처리
        } else {
            model.addAttribute("data", null);
        }
        log.info(model.toString());

        return "app/policy_view";

    }

}