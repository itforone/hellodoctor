package com.hellodoctor.controller.app;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 임시 라우터
 * 기능 작업 시 사용 컨트롤러로 빼고 여기서 삭제할 것!
 */

@Controller
@RequestMapping("/app") // /app
public class TempController {

    // 3.1.5 진료완료 remote_done.thml
    @GetMapping("/remoteDone")
    public String remoteDone() {
        return "app/remote_done";
    }

    // 5.1.3 배송조회 medi_deli.html
    @GetMapping("/mediDeli")
    public String mediDeli() {
        return "app/medi_deli";
    }

    // 5.1.9 회원탈퇴 mb_leave.html
    @GetMapping("/mbLeave")
    public String mbLeave() {
        return "app/mb_leave";
    }

    //5.1.13 나의쿠폰 my_coupon.html
    @GetMapping("/myCoupon")
    public String myCoupon() {
        return "app/my_coupon";
    }
}
