package com.hellodoctor.controller.app;

import com.google.gson.JsonObject;
import com.hellodoctor.common.AuthMember;
import com.hellodoctor.common.Constants;
import com.hellodoctor.config.ApiKey;
import com.hellodoctor.model.app.MedicalReview;
import com.hellodoctor.model.app.Member;
import com.hellodoctor.model.app.MemberAddress;
import com.hellodoctor.service.app.MyPageService;
import com.hellodoctor.service.app.RemoteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.hellodoctor.common.Functions.isNull;

/**
 * 원격진료 관련 controller
 */

@Controller
@RequestMapping("/app") // /app
@Slf4j
public class RemoteController {
    @Autowired
    RemoteService remoteService;

    @Autowired
    MyPageService myPageService;

    // 2.1.3 배송지 관리 (목록)
    @PostMapping("/addressList")
    public String addressList(@AuthMember Member member, Model model) {
        // 로그인 체크
        if (member == null) {
            model.addAttribute("isLogout", true);
        } else {
            List<MemberAddress> addrList = remoteService.getAddressList(member.getIdx()); // 배송지 목록
            model.addAttribute("addrList", addrList);
            model.addAttribute("addrIdx", member.getAddressIdx()); // 기본 배송지 주소 idx
        }

        return "app/address_list";
    }

    // 2.1.3 배송지 관리 (추가)
    @PostMapping(value={"/addressWrite/{addrIdx}", "/addressWrite"})
    public String addressWrite(@AuthMember Member member,
                               MemberAddress formData, // addressSearch - mapFrm
                               @PathVariable(required = false) Integer addrIdx,
                               Model model) {
        // 로그인 체크
        if (member == null) {
            model.addAttribute("isLogout", true);
        } else {
            // 로그인 정보
            model.addAttribute("mbIdx", member.getIdx());
            model.addAttribute("mbName", member.getMbName());
            model.addAttribute("mbHp", member.getMbHp());
            model.addAttribute("err", false);
            if(addrIdx != null) { // 수정
                model.addAttribute("mode", "update");
                MemberAddress memberAddress = remoteService.getAddressOne(addrIdx); // 주소 idx로 불러온 주소 정보
                if(memberAddress != null) {
                    if(formData.getMainAddr() != null) model.addAttribute("memberAddr", formData); // 주소 재검색 한 정보를 폼에 담음
                    else model.addAttribute("memberAddr", memberAddress);
                } else {
                    model.addAttribute("err", true);
                }
            } else { // 등록
                model.addAttribute("mode", "insert");
                model.addAttribute("memberAddr", formData); // formData == null
            }
        }

        return "app/address_write";
    }

    // 배송지 추가/수정 완료-->배송지 목록으로 이동
    @PostMapping("/addressWriteAction")
    public String addressWriteAction(@AuthMember Member member,
                                     MemberAddress memberAddress,
                                     HttpServletRequest httpServletRequest, // addressWrite - addrForm
                                     Model model) {
        remoteService.addressWriteAction(member, memberAddress, httpServletRequest); // 배송지 추가/수정
        List<MemberAddress> addrList = remoteService.getAddressList(member.getIdx()); // 배송지 목록
        model.addAttribute("addrList", addrList);
        model.addAttribute("addrIdx", member.getAddressIdx()); // 기본 배송지 주소 idx
        return "app/address_list";
    }

    // 2.1.4 주소검색
    @PostMapping(value={"/addressSearch/{addrIdx}", "/addressSearch"})
    public String addressSearch(@PathVariable(required = false) Integer addrIdx, Model model) {
        // TODO: 개발완료 시 운영키로 변경 필요
        model.addAttribute("juso_api_key", ApiKey.JUSO_API_KEY);

        if(addrIdx != null) model.addAttribute("mode", "update"); // 수정
        else model.addAttribute("mode", "insert"); // 추가

        return "app/address_search";
    }

    // 배송지 삭제
    @PostMapping("/deleteMemberAddress")
    public String deleteMemberAddress(@AuthMember Member member, @RequestParam("idx") int idx, Model model) {
        int result = remoteService.deleteMemberAddress(member, idx);
        if(result > 0) {
            List<MemberAddress> addrList = remoteService.getAddressList(member.getIdx()); // 배송지 목록
            model.addAttribute("addrList", addrList);
            model.addAttribute("addrIdx", member.getAddressIdx());
        }

        return "app/modal.address_list";
    }

    // 기본배송지 설정
    @PostMapping("/setDefaultAddress")
    @ResponseBody
    public int setDefaultAddress(@AuthMember Member member, @RequestParam("idx") Integer idx) {
        int result = 0;
        if(idx != null) result = remoteService.setDefaultAddress(member, idx);
        return result;
    }

    // 원격진료 신청등록 - 임시저장 상태
    @PostMapping("/remoteFormAction")
    @ResponseBody
    public String remoteFormAction(MultipartHttpServletRequest request, @AuthMember Member member) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        if (member == null) {
            // 회원정보 없음
            obj.addProperty("errMsg", "로그인 세션이 만료되었습니다.");
        } else {
            // 신청하기
            int medicalIdx = remoteService.registerRemoteForm(request, member.getIdx());
            if (medicalIdx > 0) {
                obj.addProperty("result", true);
                obj.addProperty("idx", medicalIdx); // insert return idx
            }
        }

        return obj.toString();
    }

    // 2.1.5 병원선택
    @GetMapping("/hospitalSelect/{idx}")
    public String hospitalSelect(
            @PathVariable(required = false) String idx, Model model, @AuthMember Member member
    ) {
        // log.info("idx=" + idx);
        model.addAttribute("errMsg", "");
        model.addAttribute("idx", "");

        if (idx == null) {
            model.addAttribute("errMsg", "올바른 경로로 이용해 주세요.");
        } else if (member == null) {
            model.addAttribute("errMsg", "로그인 세션이 만료되었습니다.");
        } else {
            model.addAttribute("idx", idx); // 진료신청 인덱스 (임시저장된 진료신청)
            Map<String, Object> result = remoteService.getRemoteForm(Integer.parseInt(idx), "R");

            if (result.size() == 0) { // db조회결과 없음
                model.addAttribute("errMsg", "올바른 경로로 이용해 주세요.");
            } else {
                // 진료신청한 병원이 존재하면
                if ((int)result.get("hospitalIdx") > 0) {
                    model.addAttribute("errMsg", "진료신청 완료된 건입니다.");
                }
                // 오늘 신청한 진료가 아니면 ?
                // if (오늘 신청한 진료가 아니면) {
                //}
                model.addAttribute("formData", result);
                model.addAttribute("memberName", member.getMbName());
                model.addAttribute("memberIdx", member.getIdx());
                // log.info(model.toString());

                // 결제카드 조회
                List<Map<String, Object>> cardList = myPageService.getMemberCreditCardList(member.getIdx());
                model.addAttribute("cardList", cardList);
                log.info("cardList=" + cardList);
            }
        }

        return "app/hospital_select";
    }

    // 병원선택 - 진료시간 변경
    @PostMapping("/changeHopeHourAction")
    @ResponseBody
    public String changeHopeHourAction(@RequestParam("hourRange") String hourRange, @RequestParam("idx") String idx) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);
        // obj.addProperty("hourRange", hourRange);
        // obj.addProperty("idx", idx);

        int cnt = remoteService.changeRemoteFormHopeHour(hourRange, idx);
        if (cnt == 1) obj.addProperty("result", true);

        return obj.toString();
    }

    // 병원선택 - 주치전담병원/진료가능병원 목록 ajax html
    // tab : 1=주치전담병원, 2=진료가능병원
    @GetMapping("/hospitalSelectList")
    //@ResponseBody
    public String hospitalSelectList(@RequestParam Map<String, String> params, Model model) {
        int tab = Integer.parseInt(params.get("tab"));
        String lat = params.get("lat").equals("")? Constants.EMPTY_LAT : params.get("lat");
        String lng = params.get("lng").equals("")? Constants.EMPTY_LNG : params.get("lng");

        Map<Integer, Object> result = remoteService.getHospitalSelectList(params, lat, lng); //, tab, hopeTime, idx, page, mbIdx, lng, lat, matchingCode);
        // log.info("주치전담병원 result=" + result.get(1));
        // log.info("진료가능병원 result=" + result.get(2));

        model.addAttribute("firstTabData", result.get(1));
        model.addAttribute("secondTabData", result.get(2));

        return (tab==1)? "app/hospital_select_list1" : "app/hospital_select_list2"; // ajax html load
    }

    // 원격진료 신청완료 - 상태,접수번호 업데이트
    @PostMapping("/remoteFormUpdateAction")
    @ResponseBody
    public String remoteFormUpdateAction(
            @RequestParam("hospitalIdx") int hospitalIdx, @RequestParam("idx") int medicalIdx,
            @RequestParam("subjectCode") String subjectCode,
            @RequestParam("hopeRange") String hopeRange,
            @RequestParam("cardIdx") String cardIdx,
            @AuthMember Member member
    ) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        if (member == null) {
            // 회원정보 없음
            obj.addProperty("errMsg", "로그인 세션이 만료되었습니다.");
        } else {
            // 진료신청 완료
            int result = remoteService.updateRemoteForm(hospitalIdx, medicalIdx, subjectCode, hopeRange, cardIdx);
            if (result == 1) obj.addProperty("result", true);
        }

        return obj.toString();
    }


    // 3.1.1 원격진료 진료신청 완료 > 원격진료 신청내역 상세보기
    @GetMapping("/remoteStatus") // /remoteStatus?idx=1
    public String remoteStatus(
            @RequestParam(value="idx", required=false, defaultValue="0") int idx, // 진료 idx
            @RequestParam(value="step", required=false, defaultValue="1") int step,
            @RequestParam(value="reqIdx", required=false, defaultValue="0") int reqIdx, // 약조제요청 idx
            @RequestParam(value="lat", required=false, defaultValue="") String lat,
            @RequestParam(value="lng", required=false, defaultValue="") String lng,
            Model model, @AuthMember Member member) {

        if (member == null) {
            model.addAttribute("isLogout", true);
        } else if (idx == 0) {
            model.addAttribute("errMsg", "올바른 경로로 이용해 주세요.");
        } else {
            if (isNull(lat)) lat = Constants.EMPTY_LAT;
            if (isNull(lng)) lng = Constants.EMPTY_LNG;
            log.info("lat=" + lat + ", lng=" + lng);

            // 원격진료 상세조회
            Map<String, Object> result = remoteService.getMedicalStatus(idx, lat, lng);
            // 진행중이면 약국/조제비 정보
            Map<String, String> ingData = new HashMap<>();

            if (result == null) {
                model.addAttribute("errMsg", "존재하지 않는 진료내역입니다.");
            } else {
                // 진료진행중 - 상세보기 상태가 '조제중'이면 약국,조제비 정보 조회
                if (step == 3) {
                    int memberAddressIdx = Integer.parseInt(String.valueOf(result.get("mbAddressIdx")));
                    ingData = remoteService.getRemoteStatusIngData(reqIdx, idx, memberAddressIdx);
                    // log.info(ingData+"");

                    // 결제카드 조회
                    List<Map<String, Object>> cardList = myPageService.getMemberCreditCardList(member.getIdx());
                    model.addAttribute("cardList", cardList);
                    log.info("cardList=" + cardList);
                }
            }

            model.addAttribute("data", result);
            model.addAttribute("memberName", member.getMbName());
            model.addAttribute("idx", idx);
            model.addAttribute("step", step);
            model.addAttribute("ingData", ingData);

            log.info("remoteStatus data=" + result.toString());
        }

        return "app/remote_status";
    }

    // 3.1.1 원격진료 진료신청 완료상태 - 환자 취소
    @PostMapping("/remoteFormCancelAction")
    @ResponseBody
    public String remoteFormCancelAction(@RequestParam("idx") int idx) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);
        obj.addProperty("idx", idx);

        int cnt = remoteService.cancelRemoteForm(idx);
        // log.info("cnt" + cnt);
        if (cnt == 1) obj.addProperty("result", true);

        return obj.toString();
    }

    // 3.1.6 리뷰작성 review_write.html
    @GetMapping("/reviewWrite")
    public String reviewWrite(
            @RequestParam(value="idx", required=false, defaultValue="0") int idx,
            @AuthMember Member member, Model model) {
        if (member == null) { // 회원정보 없음
            model.addAttribute("errMsg", "로그인 세션이 만료되었습니다.");
        } else {
            model.addAttribute("mbIdx", member.getIdx());
            model.addAttribute("medicalIdx", idx);
        }

        return "app/review_write";
    }

    // 원격진료 리뷰작성 등록
    @PostMapping("/reviewWriteAction")
    @ResponseBody
    public int reviewWriteAction(MedicalReview medicalReview, HttpServletRequest httpServletRequest) {
        return remoteService.reviewWriteAction(medicalReview, httpServletRequest);
    }

    /**
     * 원격진료 목록
     * @param status : 상태 (0=진료접수, 1=진료진행중, 2=진료완료, 3=진료취소)
     */
    @GetMapping("/remoteList")
    public String remoteList(@AuthMember Member member, Model model,
                             @RequestParam(value="status", required=false, defaultValue="0") int status,
                             @RequestParam(value="month", required=false, defaultValue="3") int month) {
        if (member == null) { // 회원정보 없음
            model.addAttribute("isLogout", true);
            model.addAttribute("showHtml", false);
        } else {
            model.addAttribute("showHtml", true);
        }

        model.addAttribute("statusValue", status);
        model.addAttribute("monthValue", month);

        // 검색 기본값
        model.addAttribute("statusDefaultValue", 0);
        model.addAttribute("monthDefaultValue", 3);
        return "app/remote_list";
    }

    // 원격진료 목록 ajax load
    @PostMapping("/ajaxRemoteList")
    public String ajaxRemoteList(
            @AuthMember Member member, Model model,
            @RequestParam Map<String, Object> params) {
        /**
         * page : 호출페이지
         * month : 기간 (n개월, 0=전체)
         * status : 상태 (0=진료접수, 1=진료진행중, 2=진료완료, 3=진료취소)
         * listRow : 한페이지 글 개수
         */
        int status = (params.get("status")!=null)? Integer.parseInt(params.get("status").toString()) : 0;
        int month = (params.get("month")!=null)? Integer.parseInt(params.get("month").toString()) : 3;
        int page = (params.get("page")!=null)? Integer.parseInt(params.get("page").toString()) : 1;
        int listRow = 5;

        List<Map<String, Object>> listData = remoteService.ajaxRemoteList(page, month, status, member.getIdx(), listRow);

        String lastPage = listData.size() < listRow? "Y" : "N"; // 마지막페이지?

        model.addAttribute("page", page);
        model.addAttribute("listData", listData);
        model.addAttribute("lastPage", lastPage);

        switch (status) {
            case 1 : // 진료진행중
                return "app/remote_list_tab2"; // break;
            case 2 : // 진료완료
                model.addAttribute("memberName", member.getMbName());
                return "app/remote_list_tab3";
            case 3 : // 진료취소
                return "app/remote_list_tab4";
            default: // 진료접수중
                return "app/remote_list_tab1";
        }
    }

    // 원격진료 진료진행중 - 약국선택
    @PostMapping("/selectPharmacyAction")
    @ResponseBody
    public String selectPharmacyAction(@RequestParam("medicalIdx") int medicalIdx, @RequestParam("pharmacyIdx") int pharmacyIdx) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        //boolean result = remoteService.selectPharmacyAction(medicalIdx, pharmacyIdx);
        // if (result) obj.addProperty("result", true);
        int pharmacyRequestIdx = remoteService.selectPharmacyAction(medicalIdx, pharmacyIdx);
        if (pharmacyRequestIdx > 0) {
            obj.addProperty("result", true);
            obj.addProperty("reqIdx", pharmacyRequestIdx); // 성공하면 insert된 약조제요청 인덱스 return
        }

        return obj.toString();
    }

    // 원격진료 목록 진료완료 > 상세보기 결제정보
    @PostMapping("/remoteCompleteDetailAction")
    // @ResponseBody
    public String getRemoteCompleteDetail(
            @RequestParam("pharmacyReqIdx") int pharmacyReqIdx, @RequestParam("mbAddrIdx") int mbAddrIdx,
            @RequestParam("totalBillAmt") String totalBillAmt, @RequestParam("deliveryAmt") String deliveryAmt,
            @RequestParam("medicalStatus") String medicalStatus, @AuthMember Member member, Model model) {

        Map<String, String> data = remoteService.getRemoteCompleteDetail(pharmacyReqIdx, mbAddrIdx);
        data.put("totalBillAmt", totalBillAmt);         // 진료비 총액
        data.put("deliveryAmt", deliveryAmt);           // 배달비
        data.put("memberName", member.getMbName());     // 주문자명
        data.put("medicalStatus", medicalStatus);       // 진료상태 (Y=완료이면 약수령완료 처리)

        model.addAttribute("data", data);
        log.info(data.toString());

        return "app/remote_list_complete_payment"; // ajax html load
    }

    // 리뷰보기
    @GetMapping("/reviewView")
    public String reviewView(@RequestParam(value="idx", required=false, defaultValue="0") int idx, Model model) {
        Map<String, Object> data = remoteService.getMedicalReview(idx);
        model.addAttribute("data", data);
        log.info("reviewView()" + data);

        return "app/remote_list_review"; // ajax html load
    }

    // 처방전보기
    @GetMapping("/medicalPrescription")
    public String medicalPrescription(@RequestParam(value="idx", required=false, defaultValue="0") int idx, // 진료 idx
                                      Model model) {

        String source = remoteService.getPrescriptionImage(idx);
        //log.info("처방전 이미지 = " + source);
        model.addAttribute("imageSrc", source);
        return "app/prescription";
    }

}
