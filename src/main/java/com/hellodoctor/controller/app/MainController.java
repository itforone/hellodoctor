package com.hellodoctor.controller.app;

import com.hellodoctor.common.AuthMember;
import com.hellodoctor.common.Constants;
import com.hellodoctor.config.ApiKey;
import com.hellodoctor.model.app.Member;
import com.hellodoctor.service.app.MyDoctorService;
import com.hellodoctor.service.app.RemoteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * 메인 (우리동네 주치의/원격진료 신청하기)
 */

@Slf4j
@Controller
@RequestMapping("/app") // /app
public class MainController {
    @Autowired
    RemoteService remoteService;
    @Autowired
    MyDoctorService myDoctorService;

    // 2.1.1 메인
   @GetMapping("/main")
    public String main(@AuthMember Member member, Model model) {
       model.addAttribute("memberName", "");
       model.addAttribute("listCount", 0); // 나의가족(진료환자) 카운트
       model.addAttribute("reqList", "");

       if (member == null) {
           // model.addAttribute("isLogout", true);
           model.addAttribute("isLoggedIn", false);
       } else {
           model.addAttribute("isLoggedIn", true);
           model.addAttribute("memberName", member.getMbName());
           model.addAttribute("memberIdx", member.getIdx());

           // (진료환자) 내정보 + 가족정보
           List<Object> familyList = remoteService.getMedicalFamilyList(member);
           // log.info("familyList=" + familyList);

           model.addAttribute("familyList", familyList);
           model.addAttribute("listCount", familyList.size());

           // 우리동네주치의 요청내역 조회
           List<Map<String, String>> reqList = myDoctorService.requestMyDoctorList(member.getIdx(), member.getMbHp());
           model.addAttribute("reqList", reqList);
           // log.info("우리동네주치의 요청내역=" + reqList);
           // 전체 수
           int totalCount = myDoctorService.myDoctorTotalCount(member.getIdx());
           model.addAttribute("nbhHospitalCnt", totalCount);
       }
       return "app/main"; // templates/app/main.html
    }

    // 2.1.2 원격진료신청하기
    // @param familyIdx : 가족인덱스, 없으면 본인진료 신청
    @GetMapping(value = {"/remoteForm/{familyIdx}", "/remoteForm"})
    public String remoteForm(@PathVariable(required = false, name="familyIdx") Integer familyIdx, @AuthMember Member member, Model model) {
        // TBC: 개발완료 시 운영키로 변경 필요
        model.addAttribute("kakao_js_key", ApiKey.KAKAO_JS_KEY);

        if (member == null) {
            model.addAttribute("isLogout", true);
        } else {
            Map<String, Object> targetInfo = remoteService.getTargetDetail(familyIdx, member);
            // log.info("targetInfo" + targetInfo.toString());

            if (targetInfo.size() == 0) {
                model.addAttribute("errMsg", "잘못된 정보입니다."); // 진료환자 DB정보없음
            } else {
                // 진료환자 정보
                model.addAttribute("targetInfo", targetInfo);
            }
        }

        // 질환명, 설명
        model.addAttribute("subjectList", Constants.MEDICAL_REQUEST_SUBJECT);
        model.addAttribute("subjectListDesc", Constants.MEDICAL_REQUEST_SUBJECT_DESC);

        return "app/remote_form";
    }
}