package com.hellodoctor.controller.app;

import com.hellodoctor.common.AuthMember;
import com.hellodoctor.model.app.Member;
import com.hellodoctor.model.app.MemberAddress;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 비대면 화상진료 컨트롤러
 */

@Controller
@RequestMapping("/app") // /app
@Slf4j
public class WebRTCController {
    // 3.1.3 비대면 화상 진료 remote_treat.thml
    @GetMapping("/remoteTreat")
    public String remoteTreat(@AuthMember Member member, Model model, @RequestParam(value="roomId", required=false, defaultValue="0") int roomId) {
        if (member == null) {
            // 로그아웃 상태
            //model.addAttribute("isLogout", true);
        } else {
            // 로그인 상태
            // log.info(member.getIdx()+"");
        }

        // model.addAttribute("test", "컨트롤러 데이터 전송");
        model.addAttribute("roomId",roomId);
        model.addAttribute("user",member.getMbId());

        log.info("roomId=" + roomId);

        return "app/remote_treat";
    }
}
