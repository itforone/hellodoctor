package com.hellodoctor.controller.app;

import com.hellodoctor.common.AuthMember;
import com.hellodoctor.service.admin.ASystemService;
import com.hellodoctor.service.app.MemberService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.hellodoctor.model.app.Member;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.hellodoctor.common.Functions.getHyphenHp;

/**
 * 회원 관련 controller 클래스
 * @Autowired: 해당 변수 및 메서드에 스프링이 관리하는 Bean을 자동으로 매핑 (의존관계를 자동으로 설정)
 */
@Controller // 컨트롤러임을 명시
@RequestMapping("/app") // /app
@Slf4j
public class MemberController {
    @Autowired
    MemberService memberService;
    @Autowired
    ASystemService aSystemService;

    // 이용약관 동의
    @GetMapping(value = {"/signUp", "/signUp/{error}", "/signUp/{name}/{hp}/{birth}/{gender}"}) // /app/signUp
    public String signUp(@AuthMember Member member, @PathVariable(required = false) String error, @PathVariable String name, @PathVariable String hp, @PathVariable String birth, @PathVariable int gender, Model model) {
        // 로그인 체크
        if(member == null) {
            model.addAttribute("err", false);
            if (error != null && error.equals("error")) { // 회원가입 에러
                model.addAttribute("err", true);
            } else {
                model.addAttribute("name", name);
                model.addAttribute("hp", getHyphenHp(hp));
                model.addAttribute("birth", birth);
                String sex = "";
                if(gender == 0) sex = "여";
                else if(gender == 1) sex = "남";
                model.addAttribute("sex", sex); // 0: 여성 / 1: 남성

                // 이용약관
                List<Map<String, String>> termsList = aSystemService.getTermsList();
                // 이용약관 역순
                List<Map<String, String>> termsListReverse = new ArrayList<>();
                for (int i=termsList.size()-1; i >=0; i--) {
                    termsListReverse.add(termsList.get(i));
                }
                model.addAttribute("termsList", termsListReverse);
            }
            log.info(model.toString());
            return "app/sign_up"; // templates/app/sign_up.html
        } else {
            return "redirect:/app/main";
        }
    }

    // 회원가입 폼 작성
    @GetMapping("/signUpForm/{name}/{hp}/{birth}/{sex}") // /app/signUpForm
    public String signUpForm(@AuthMember Member member, @PathVariable String name, @PathVariable String hp, @PathVariable String birth, @PathVariable String sex, Model model) {
        // 로그인 체크
        if(member == null) {
            model.addAttribute("name", name);
            model.addAttribute("hp", hp);
            model.addAttribute("birth", birth);
            model.addAttribute("sex", sex);
            return "app/sign_up_form"; // templates/app/sign_up_form.html
        } else {
            return "redirect:/app/main";
        }
    }

    // 회원가입 액션
    @PostMapping("/signUpAction")
    public String signUpAction(Member member) {
        int result = memberService.signUpMember(member);
        if(result != 0) {
            return "redirect:/app/signUpComplete/"+member.getMbId(); // 회원가입 완료 페이지로 이동
        } else {
            return "app/signUp/error"; // 회원가입 오류
        }
    }

    // 아이디 중복체크
    @PostMapping("/idCheck")
    @ResponseBody // 값 반환을 위해 필요
    public int idCheck(@RequestParam("id") String id) {
        return memberService.idCheck(id);
    }

    // 회원가입 완료
    @GetMapping("/signUpComplete/{id}") // /app/signUpComplete
    public String signUpComplete(@PathVariable String id, Model model) {
        String name = memberService.getMemberInfo("mb_name", id); // 이름
        //log.info("getMemberInfo: " + name);
        model.addAttribute("name", name+"님,");
        return "app/sign_up_complete"; // templates/app/sign_up_complete.html
    }
}