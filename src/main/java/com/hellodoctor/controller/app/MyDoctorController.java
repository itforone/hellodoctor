package com.hellodoctor.controller.app;

import com.google.gson.JsonObject;
import com.hellodoctor.common.AuthMember;
import com.hellodoctor.common.Constants;
import com.hellodoctor.mapper.app.MemberAddressMapper;
import com.hellodoctor.model.app.Member;
import com.hellodoctor.model.app.MemberAddress;
import com.hellodoctor.service.app.MyDoctorService;
import com.hellodoctor.service.app.MyPageService;
import com.hellodoctor.service.app.RemoteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

import static com.hellodoctor.common.Functions.isNull;

/**
 * 우리동네주치의
 */

@Slf4j
@Controller
@RequestMapping("/app") // /app
public class MyDoctorController {
    @Autowired
    MyDoctorService myDoctorService;
    @Autowired
    RemoteService remoteService;
    @Autowired
    MemberAddressMapper memberAddressMapper;
    @Autowired
    MyPageService myPageService;

    // 메인 우리동네주치의 수락, 거절 처리
    @PostMapping("/reqNbhHospitalAction")
    @ResponseBody
    public String requestNbhHospitalAction(@RequestParam("idx") int idx, @RequestParam("status") String status, @RequestParam("mbIdx") int mbIdx) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int chk = myDoctorService.requestMyDoctorAction(idx, status, mbIdx);
        if (chk > 0) obj.addProperty("result", true);

        return obj.toString();
    }

    // 우리동네주치의 선택목록
    @GetMapping("/myDoctorSelect")
    public String myDoctorSelect(@AuthMember Member member, Model model,
                                 @RequestParam(value="lat", required=false, defaultValue="") String lat,
                                 @RequestParam(value="lng", required=false, defaultValue="") String lng) {
        if (member == null) {
            model.addAttribute("isLogout", true);
        } else {
            int mbIdx = member.getIdx();
            if (isNull(lat)) lat = Constants.EMPTY_LAT;
            if (isNull(lng)) lng = Constants.EMPTY_LNG;

            // 전체 수
            int totalCount = myDoctorService.myDoctorTotalCount(mbIdx);
            if (totalCount == 1) {
                return "redirect:/app/myDoctor?lat=" + lat + "&lng=" + lng; // 우리동네주치의 1개면 빠른진료예약으로 이동
            }
            model.addAttribute("totalCount", totalCount);

            // 목록
            List<Map<String, Object>> dataList = myDoctorService.getMyDoctorList(mbIdx, lat, lng);
            model.addAttribute("dataList", dataList);
            model.addAttribute("dataListCount", dataList.size());
            // log.info("dataList" + dataList);
        }

        return "app/my_doctor_list";
    }

    // 우리동네주치의 빠른진료신청
    @GetMapping("/myDoctor") // ./myDoctor?idx=1
    public String myDoctor(@AuthMember Member member, Model model,
                           @RequestParam(value="idx", required=false, defaultValue="0") int myDoctorIdx,
                           @RequestParam(value="lat", required=false, defaultValue="") String lat,
                           @RequestParam(value="lng", required=false, defaultValue="") String lng) {
        if (member == null) {
            model.addAttribute("isLogout", true);
        } else {
            model.addAttribute("memberName", member.getMbName());

            int mbIdx = member.getIdx();
            if (isNull(lat)) lat = Constants.EMPTY_LAT;
            if (isNull(lng)) lng = Constants.EMPTY_LNG;

            // 병원정보
            Map<String, Object> viewData = myDoctorService.getMyDoctorOne(mbIdx, lat, lng, myDoctorIdx);
            model.addAttribute("viewData", viewData);
            log.info("병원정보" + viewData.toString());

            // 진료환자 정보
            // Map<String, Object> targetInfo = remoteService.getTargetDetail(null, member);
            // model.addAttribute("targetInfo", targetInfo);
            // log.info("targetInfo=" + targetInfo);

            // 배송지 정보
            String address = "";
            if(member.getAddressIdx() != 0) { // 기본 배송지가 있으면
                MemberAddress memberAddress = memberAddressMapper.getAddressOne(member.getAddressIdx());
                address = memberAddress.getMainAddr() + " " + memberAddress.getDetailAddr();
            }
            model.addAttribute("address", address);
            model.addAttribute("addrIdx", member.getAddressIdx());

            // 회원카드목록
            List<Map<String, Object>> cardList = myPageService.getMemberCreditCardList(member.getIdx());
            model.addAttribute("cardList", cardList);
            model.addAttribute("cardCnt", cardList.size());

            // 주민번호
            String rrNo = myDoctorService.getMemberLastRrNo(member.getIdx()); // 마지막진료기록
            String[] rrNoArr = new String[2];
            if (rrNo==null) {
                rrNoArr[0] = member.getMbBirth().substring(2);
                rrNoArr[1] = "";
            }
            else rrNoArr = rrNo.split("-");
            model.addAttribute("rrNoArr", rrNoArr);
            // log.info(rrNoArr[0] + "/" + rrNoArr[1]);
        }
        log.info("myDoctor=" + model.toString());

        return "app/my_doctor";
    }

    // 빠른진료신청 진료신청 DB 임시등록 / 수정
    @PostMapping("/saveQuickMedical")
    @ResponseBody
    public String saveQuickMedical(HttpServletRequest request, @AuthMember Member member) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int resultIdx = myDoctorService.saveQuickMedical(request, member);
        obj.addProperty("resultIdx", resultIdx);
        if (resultIdx > 0) obj.addProperty("result", true);

        return obj.toString();
    }



}