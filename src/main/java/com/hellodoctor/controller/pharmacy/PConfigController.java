package com.hellodoctor.controller.pharmacy;

import com.google.gson.JsonObject;
import com.hellodoctor.common.AuthDoctor;
import com.hellodoctor.common.AuthPharmacist;
import com.hellodoctor.common.Constants;
import com.hellodoctor.common.Paging;
import com.hellodoctor.config.ApiKey;
import com.hellodoctor.mapper.app.FileUploadMapper;
import com.hellodoctor.model.app.Pharmacy;
import com.hellodoctor.model.hospital.Doctor;
import com.hellodoctor.model.pharmacy.Medicine;
import com.hellodoctor.model.pharmacy.Pharmacist;
import com.hellodoctor.service.pharmacy.PConfigService;
import com.hellodoctor.service.pharmacy.PharmacistService;
import lombok.extern.slf4j.Slf4j;
import org.codehaus.groovy.runtime.ArrayUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.thymeleaf.util.ArrayUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.IntStream;

/**
 * 약국-관리 controller 클래스
 */
@Controller // 컨트롤러임을 명시
@RequestMapping("/pharmacy")
@Slf4j
public class PConfigController {
    @Autowired
    PConfigService pConfigService;
    @Autowired
    FileUploadMapper fileUploadMapper;
    @Autowired
    PharmacistService pharmacistService;

    // 계정관리
    @GetMapping("/admAccount")
    public String admAccount(@AuthPharmacist Pharmacist pharmacist, Model model) {
        if (pharmacist == null) {
            model.addAttribute("isLogout", true);
        } else {
            // 약사정보
            model.addAttribute("data", pharmacist);
            log.info("pharmacist: " + pharmacist);
        }

        return "pharmacy/adm_account";
    }

    // 계정관리 저장
    @PostMapping("/saveAdmAccount")
    @ResponseBody
    public String saveAdmAccount(@AuthPharmacist Pharmacist pharmacist, MultipartHttpServletRequest request) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int chk = pConfigService.saveAdmAccount(pharmacist, request);
        if (chk != 0) obj.addProperty("result", true);

        return obj.toString();
    }

    // 서브계정관리
    @GetMapping("/admSubAccount")
    public String admSubAccount(@AuthPharmacist Pharmacist pharmacist, Model model) {
        if (pharmacist == null) {
            model.addAttribute("isLogout", true);
        }

        return "pharmacy/adm_sub_account";
    }

    // 서브계정관리 목록
    @PostMapping("/ajaxAdmSubAccount")
    public String getAdmSubAccount(@AuthPharmacist Pharmacist pharmacist, Model model, @RequestParam(value = "page", required = false, defaultValue = "1") int page, HttpServletRequest request) {
        if (pharmacist == null) {
            model.addAttribute("isLogout", true);
        } else {
            // 서브계정관리 목록 (부계정만 조회)
            String pageUrl = "./admSubAccount?page=";
            int totalCount = pConfigService.getAdmSubAccountCount(pharmacist.getPharmacyIdx(), request);
            // 페이징
            Paging paging = new Paging(pageUrl, totalCount, 15, page, 10);
            model.addAttribute("paging", paging);

            //  목록
            Map<String, Object> listMap = pConfigService.getAdmSubAccount(pharmacist.getPharmacyIdx(), request, paging.getStartRecord(), paging.getEndRecord());
            model.addAttribute("listData", listMap.get("listData"));
        }

        return "pharmacy/adm_sub_account_info";
    }

    // 서브계정관리 등록폼
    @GetMapping(value = {"/admSubForm", "/admSubForm/{idx}"})
    public String admSubForm(@AuthPharmacist Pharmacist pharmacist, @PathVariable(required = false) Integer idx, Model model) {
        if (pharmacist == null) {
            model.addAttribute("isLogout", true);
        } else {
            if (idx != null) { // 수정
                // 약사 정보
                Pharmacist pharmacistData = pharmacistService.getPharmacistInfo(idx);
                model.addAttribute("data", pharmacistData);
            }
        }

        return "pharmacy/adm_sub_form";
    }

    // 서브계정관리 저장
    @PostMapping("/saveAdmSubAccount")
    @ResponseBody
    public String saveAdmSubAccount(@AuthPharmacist Pharmacist pharmacist, MultipartHttpServletRequest request) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int chk = pConfigService.saveAdmSubAccount(pharmacist, request);
        if (chk != 0) obj.addProperty("result", true);

        return obj.toString();
    }

    // 서브계정관리 삭제
    @PostMapping("deletePharmacist")
    @ResponseBody
    public String deletePharmacist(@RequestParam(value = "idx") int idx) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int chk = pConfigService.deletePharmacist(idx);
        if (chk != 0) obj.addProperty("result", true);

        return obj.toString();
    }

    // 약국정보관리
    @GetMapping("/admPharmacy")
    public String admPharmacy(@AuthPharmacist Pharmacist pharmacist, Model model) {
        if (pharmacist == null) {
            model.addAttribute("isLogout", true);
        } else {
            // TODO: 개발완료 시 운영키로 변경 필요
            model.addAttribute("kakao_js_key", ApiKey.KAKAO_JS_KEY);

            // 약국정보
            Map<String, Object> pharmacy = pharmacistService.getPharmacyInfo(pharmacist.getPharmacyIdx());
            model.addAttribute("data", pharmacy);
            log.info("pharmacy: " + pharmacy);
        }

        return "pharmacy/adm_pharmacy";
    }

    // 약국정보관리 저장
    @PostMapping("/saveAdmPharmacy")
    @ResponseBody
    public String saveAdmPharmacy(@AuthPharmacist Pharmacist pharmacist, Pharmacy pharmacy, MultipartHttpServletRequest request) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int chk = pConfigService.saveAdmPharmacy(pharmacist, pharmacy, request);
        if (chk != 0) obj.addProperty("result", true);

        return obj.toString();
    }

    // 정산계좌관리
    @GetMapping("/admBank")
    public String admBank(@AuthPharmacist Pharmacist pharmacist, Model model) {
        if (pharmacist == null) {
            model.addAttribute("isLogout", true);
        } else {
            model.addAttribute("bankCode", Constants.BANK_CODE);

            // 약국정보
            Pharmacy pharmacy = pConfigService.getPharmacyInfo(pharmacist.getPharmacyIdx());
            model.addAttribute("data", pharmacy);
        }

        return "pharmacy/adm_bank";
    }

    // 정산계좌관리 저장
    @PostMapping("/saveAccount")
    @ResponseBody
    public String saveAccount(MultipartHttpServletRequest request) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int chk = pConfigService.saveAccount(request);
        if (chk != 0) obj.addProperty("result", true);

        return obj.toString();
    }

    // 지정병원
    @GetMapping("/admHospital")
    public String admHospital(@AuthPharmacist Pharmacist pharmacist, Model model, @RequestParam(value = "page", required = false, defaultValue = "1") int page) {
        if (pharmacist == null) {
            model.addAttribute("isLogout", true);
        } else {
            // 전체글수
            String pageUrl = "./admHospital?page=";
            int totalCount = pConfigService.getAdmHospitalTotalCount(pharmacist.getPharmacyIdx());
            // 페이징
            Paging paging = new Paging(pageUrl, totalCount, 15, page, 10);
            model.addAttribute("paging", paging);

            // 목록
            List<Map<String, String>> hospitalList = pConfigService.getAdmHospitalList(pharmacist.getPharmacyIdx(), paging.getStartRecord(), paging.getEndRecord());
            model.addAttribute("hospitalList", hospitalList);
            model.addAttribute("totalCount", totalCount);
        }

        return "pharmacy/adm_hospital";
    }

    // 의약품관리
    @GetMapping("/admFormulary") // ./admFormulary?sfl=name&stx=약품명
    public String admFormulary(
            @AuthPharmacist Pharmacist pharmacist, Model model,
            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @RequestParam(value="sfl", required=false, defaultValue="name") String sfl,
            @RequestParam(value="stx", required=false, defaultValue="") String stx,
            @RequestParam(value = "listRow", required = false, defaultValue = "15") int listRow
    ) {
        if (pharmacist == null) {
            model.addAttribute("isLogout", true);
        } else {
            // 검색옵션
            Map<String, String> options = new LinkedHashMap<>();
            options.put("name", "의약품명");
            options.put("code", "코드");

            // 목록수 옵션
            int defaultRow = 15;
            Integer[] rowOptions = {15,30,50,100};
            List<Integer> rowOptionsArr = Arrays.asList(rowOptions);
            if (!rowOptionsArr.contains(listRow)) listRow = defaultRow;

            model.addAttribute("schOptions", options);
            model.addAttribute("rowOptions", rowOptionsArr);
            model.addAttribute("page", page);
            model.addAttribute("sfl", sfl);
            model.addAttribute("stx", stx);
            model.addAttribute("listRow", listRow);
            model.addAttribute("defaultRow", defaultRow);

            // 페이징 파리미터 추가
            List<String> urlParams = new ArrayList<>();
            if (!stx.equals("")) urlParams.add("sfl=" + sfl + "&stx=" + stx);
            if (listRow > defaultRow) urlParams.add("listRow=" + listRow);

            // 전체글수
            String pageUrl = (urlParams.size()==0)? "./admFormulary?page=" : "./admFormulary?" + String.join("&", urlParams) + "&page=";
            int totalCount = pConfigService.getMedicineTotalCount(pharmacist.getPharmacyIdx(), sfl, stx);
            // 페이징
            Paging paging = new Paging(pageUrl, totalCount, listRow, page, 10);
            model.addAttribute("paging", paging);

            // 목록
            List<Medicine> medicineList = pConfigService.getMedicineList(pharmacist.getPharmacyIdx(), paging.getStartRecord(), paging.getEndRecord(), sfl, stx);
            model.addAttribute("medicineList", medicineList);
            model.addAttribute("totalCount", totalCount);
        }

        return"pharmacy/adm_formulary";
    }

    // 의약품관리 등록/수정
    @GetMapping("/admFormularyForm") // ./admFormularyForm?idx=1
    public String admFormularyForm(@AuthPharmacist Pharmacist pharmacist, Model model, @RequestParam(value="idx", required=false, defaultValue="0") int idx) {
        model.addAttribute("errMsg", "");

        if (pharmacist == null) {
            model.addAttribute("isLogout", true);
        } else {
            model.addAttribute("idx", idx);
            model.addAttribute("pharmacyIdx", pharmacist.getPharmacyIdx());

            Medicine medicine = pConfigService.getMedicineOne(idx);
            model.addAttribute("medicine", medicine);

            // 수정
            if (idx > 0) {
                if (medicine == null) {
                    // 1. 없는 글
                    model.addAttribute("errMsg", "잘못된 접근입니다.");
                } else if (pharmacist.getPharmacyIdx() != medicine.getPharmacyIdx()) {
                    // 2. 본인 작성 글 확인
                    model.addAttribute("errMsg", "잘못된 접근입니다.");
                    model.addAttribute("medicine", null);
                }
            }
        }

        return "pharmacy/adm_formulary_form";
    }

    // 의약품관리 등록/수정 - 처리
    @PostMapping("/medicineAction")
    @ResponseBody
    public String medicineAction(MultipartHttpServletRequest request, Medicine medicine) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        // 등록/수정
        int chk = pConfigService.registerMedicine(request, medicine);
        if (chk != 0) obj.addProperty("result", true);

        return obj.toString();
    }

    // 의약품관리 삭제
    @PostMapping("/deleteMedicine")
    @ResponseBody
    public String deleteMedicine(@RequestParam("idx") int idx) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        // 등록/수정
        int chk = pConfigService.deleteMedicine(idx);
        if (chk != 0) obj.addProperty("result", true);

        return obj.toString();
    }




}