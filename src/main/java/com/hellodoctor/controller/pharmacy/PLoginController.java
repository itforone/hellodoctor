package com.hellodoctor.controller.pharmacy;

import com.google.gson.JsonObject;
import com.hellodoctor.common.AuthPharmacist;
import com.hellodoctor.common.Constants;
import com.hellodoctor.model.pharmacy.Pharmacist;
import com.hellodoctor.service.pharmacy.PharmacistService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 로그인, 아이디/비밀번호찾기
 */
@Controller
@RequestMapping("/pharmacy") // /pharmacy
@Slf4j
public class PLoginController {
    @Autowired
    PharmacistService pharmacistService;

    // 로그인 페이지
    @GetMapping(value={"/login", "/"})
    public String emailLogin(@RequestParam(value = "error", required = false) String error,
                             @RequestParam(value = "code", required = false) String errorCode,
                             @AuthPharmacist Pharmacist pharmacist,
                             Model model) {

        if (error != null) {    // 로그인 에러메시지 출력
            Integer code = Integer.parseInt(errorCode);
            model.addAttribute("error", error);
            model.addAttribute("exception", Constants.MEMBER_LOGIN_ERR_MSG.get(code));
        }

        // 로그인 체크
        if (pharmacist == null) {
            return "pharmacy/email_login";
        } else {
            log.info("로그인사용자=" + pharmacist.toString());
            return "redirect:/pharmacy/fillRequest";
        }
    }

    // 아이디 찾기
    @GetMapping("/certify/findId")
    public String findId() {
        return "pharmacy/find_id";
    }

    // 아이디찾기/비밀번호찾기 - 인증번호요청
    @PostMapping("/certify/requestCertNo")
    @ResponseBody
    public String requestCertNo(
            @RequestParam("hp") String hp,
            @RequestParam(value = "email", required = false, defaultValue = "") String email) {
        JsonObject obj = new JsonObject();
        Map<String, String> map = pharmacistService.hpCheck(hp, email);

        obj.addProperty("result", false);
        obj.addProperty("certNo", map.get("certNo")); // 인증번호
        log.info("인증번호: " + map.get("certNo"));

        if (map.get("flag").equals("Y")) { // 성공시 아이디,가입일 정보 추가
            obj.addProperty("result", true);
            obj.addProperty("email", map.get("email"));
            obj.addProperty("regdate", map.get("regdate"));
        }

        // ajax json return
        return obj.toString();
    }

    // 비밀번호 찾기
    @GetMapping("/certify/findPw")
    public String findPw() {
        return "/pharmacy/find_pw";
    }

    // 비밀번호 찾기 get방식 접근불가
    @GetMapping("/certify/resetPw")
    public String resetPw() {
        return "redirect:/pharmacy/login";
    }

    // 비밀번호 재설정
    @PostMapping("/certify/resetPw")
    public String resetPw(HttpServletRequest request, Model model) {
        if (request.getParameter("email") == null) {
            // return "redirect:/pharmacy/findPw";
            model.addAttribute("email", "");
        } else {
            model.addAttribute("email", request.getParameter("email"));
        }

        return "pharmacy/reset_pw";
    }

    // 비밀번호 재설정 처리
    @PostMapping("/certify/resetPwAction")
    @ResponseBody
    public String resetPwAction(
            @RequestParam("password") String password, @RequestParam("email") String email) {
        JsonObject obj = new JsonObject();
        int cnt = pharmacistService.resetPassword(email, password);
        obj.addProperty("resultCnt", cnt);
        // ajax json return
        return obj.toString();
    }
}
