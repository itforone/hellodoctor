package com.hellodoctor.controller.pharmacy;

import com.google.gson.JsonObject;
import com.hellodoctor.common.*;
import com.hellodoctor.mapper.app.FileUploadMapper;
import com.hellodoctor.model.app.FileUpload;
import com.hellodoctor.model.hospital.AdminNotice;
import com.hellodoctor.model.hospital.CSSite;
import com.hellodoctor.model.hospital.Doctor;
import com.hellodoctor.model.pharmacy.Pharmacist;
import com.hellodoctor.service.hospital.HBoardService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.util.List;

/**
 * 게시판 관련 controller 클래스
 * @Autowired: 해당 변수 및 메서드에 스프링이 관리하는 Bean을 자동으로 매핑 (의존관계를 자동으로 설정)
 */
@Controller // 컨트롤러임을 명시
@RequestMapping("/pharmacy")
@Slf4j
public class PBoardController {
    @Autowired
    HBoardService HBoardService;
    @Autowired
    FileUploadMapper fileUploadMapper;

    String GroupType = "P"; // 약국

    // 공지사항
    @GetMapping("/notice")
    public String notice(@AuthPharmacist Pharmacist pharmacist, Model model, @RequestParam(value="page", required=false, defaultValue="1") int page) {
        // 전체글수
        String pageUrl = "./notice?page=";
        int noticeTotalCnt = HBoardService.getNoticeTotalCount(GroupType);
        // 페이징
        Paging paging = new Paging(pageUrl, noticeTotalCnt, 15, page, 10);
        model.addAttribute("paging", paging);
        // log.info(paging.toString());

        //  목록
        List<AdminNotice> noticeList = HBoardService.getNotice(GroupType, paging.getStartRecord(), paging.getEndRecord());
        model.addAttribute("category", Constants.ADM_NOTICE_CATEGORY);
        model.addAttribute("noticeList", noticeList);
        model.addAttribute("nlString", System.getProperty("line.separator").toString()); // 타임리프 개행처리
        return "pharmacy/notice";
    }

    // CS문의 목록
    @GetMapping("/csList")
    public String csList(@AuthPharmacist Pharmacist pharmacist, Model model,
                         @RequestParam(value="page", required=false, defaultValue="1") int page) {
        if (pharmacist == null) {
            model.addAttribute("isLogout", true);
        } else {
            // 전체글수
            int totalCount = HBoardService.getCSTotalCount(GroupType, pharmacist.getPharmacyIdx());
            String pageUrl = "./csList?page=";
            // 페이징
            Paging paging = new Paging(pageUrl, totalCount, 15, page, 10);
            model.addAttribute("paging", paging);

            // 목록
            List<CSSite> csList = HBoardService.getCSList(GroupType, pharmacist.getPharmacyIdx(), paging.getStartRecord(), paging.getEndRecord());
            model.addAttribute("csList", csList);
        }
        // log.info("model=" + model);

        return "pharmacy/cs_list";
    }

    // CS문의 등록/수정
    @GetMapping("/csForm") // 수정: ?idx=1
    public String csForm(@AuthPharmacist Pharmacist pharmacist, Model model,
                         @RequestParam(value="idx", required=false, defaultValue="0") int idx) {
        model.addAttribute("errMsg", "");

        if (pharmacist == null) {
            model.addAttribute("isLogout", true);
        } else {
            model.addAttribute("category", Constants.CS_WEB_CATEGORY);
            model.addAttribute("idx", idx);
            model.addAttribute("groupIdx", pharmacist.getPharmacyIdx());
            model.addAttribute("userIdx", pharmacist.getIdx());
            model.addAttribute("groupType", "H");

            log.info(String.valueOf(idx));

            // 수정
            if (idx > 0) {
                CSSite csData = HBoardService.getCSForm(idx, pharmacist.getIdx());
                if (csData != null) {
                    model.addAttribute("data", csData);
                    // 파일업로드
                    List<FileUpload> files = fileUploadMapper.getUploadFiles(csData.getFileCode(), "csSite", 0);
                    model.addAttribute("files", files);
                    // log.info("수정데이터=" + csData);
                    // log.info("파일업로드=" + String.valueOf(files));

                    if (pharmacist.getIdx() != csData.getUserIdx()) {
                        // 1. 본인 작성 글 확인
                        model.addAttribute("errMsg", "본인이 작성한 글만 수정이 가능합니다.");
                    } else if (!Functions.isNull(csData.getAnswerDate())) {
                        // 2. 관리자답변여부 확인 (여:수정불가)
                        model.addAttribute("errMsg", "답변완료된 글은 수정이 불가능합니다.");
                    }

                } else {
                    model.addAttribute("errMsg", "잘못된 접근입니다.");
                }
            }
        }
        // log.info(model.toString());
        return "pharmacy/cs_form";
    }

    // CS문의 등록/수정 - 처리
    @PostMapping("/csFormAction")
    @ResponseBody
    public String csFormAction(MultipartHttpServletRequest request, @AuthPharmacist Pharmacist pharmacist) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        if (pharmacist == null) {
            // 회원정보 없음
            obj.addProperty("errMsg", "로그인 세션이 만료되었습니다.");
        } else {
            // 등록/수정
            int csIdx = HBoardService.registerCSForm(request, pharmacist.getIdx(), pharmacist.getPharmacyIdx(), GroupType);
            if (csIdx > 0) obj.addProperty("result", true);
        }

        return obj.toString();
    }

    @GetMapping("/csView/{idx}")
    public String csView(@PathVariable int idx, @AuthPharmacist Pharmacist pharmacist, Model model) {
        if (pharmacist == null) {
            model.addAttribute("isLogout", true);
        } else {
            model.addAttribute("category", Constants.CS_WEB_CATEGORY);
            model.addAttribute("idx", idx);

            CSSite csData = HBoardService.getCSForm(idx, pharmacist.getIdx());
            if (csData != null) {
                model.addAttribute("data", csData);
                // 파일업로드(유저)
                List<FileUpload> files = fileUploadMapper.getUploadFiles(csData.getFileCode(), "csSite", 0);
                model.addAttribute("files", files);
                // 파일업로드(관리자)
                // log.info(csData.getAnswerFileCode() + "");
                List<FileUpload> adminFiles = fileUploadMapper.getUploadFiles(csData.getAnswerFileCode(), "csSite", 0);
                model.addAttribute("adminFiles", adminFiles);

            } else {
                model.addAttribute("errMsg", "잘못된 접근입니다.");
            }
        }
        return "pharmacy/cs_view";
    }

    // CS문의 삭제
    @PostMapping("/deleteCS")
    @ResponseBody
    public String deleteCSForm(@AuthPharmacist Pharmacist pharmacist, @RequestParam("idx") int idx) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        if (pharmacist == null) {
            // 회원정보 없음
            obj.addProperty("errMsg", "로그인 세션이 만료되었습니다.");
        } else {
            // 등록/수정
            int chk = HBoardService.deleteCSForm(idx);
            if (chk > 0) obj.addProperty("result", true);
        }

        return obj.toString();
    }

}