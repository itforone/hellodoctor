package com.hellodoctor.controller.pharmacy;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 임시 라우터
 * 기능 작업 시 사용 컨트롤러로 빼고 여기서 삭제할 것!
 */

@Controller
@RequestMapping("/pharmacy") // /pharmacy
@Slf4j
public class TempController3 {
    // 정산
    @GetMapping("/calculate")
    public String calculate() {
        return "pharmacy/calculate";
    }
}
