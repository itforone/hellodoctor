package com.hellodoctor.controller.pharmacy;

import com.google.gson.JsonObject;
import com.hellodoctor.common.AuthPharmacist;
import com.hellodoctor.common.Paging;
import com.hellodoctor.model.pharmacy.Pharmacist;
import com.hellodoctor.service.hospital.HRemoteService;
import com.hellodoctor.service.pharmacy.PFillService;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.hellodoctor.common.Functions.isNull;

/**
 * 약사 약 조제 관리 controller
 */

@Controller
@RequestMapping("/pharmacy")
@Slf4j
public class PFillController {
    @Autowired
    PFillService pFillService;
    @Autowired
    HRemoteService hRemoteService;

    // 약 조제관리 - 조제요청
    @GetMapping("/fillRequest")
    public String fillRequest(@AuthPharmacist Pharmacist pharmacist, Model model) {
        if (pharmacist == null) {
            model.addAttribute("isLogout", true);
        } else {
            model.addAttribute("userName", pharmacist.getUserName()); // header bind
        }
        return "pharmacy/fill_request";
    }

    // 약 조제관리 - 조제요청 목록
    @PostMapping("/getFillRequest")
    public String getFillRequest(@AuthPharmacist Pharmacist pharmacist, Model model, @RequestParam(value="page", required=false, defaultValue="1") int page, HttpServletRequest request) {
        if (pharmacist == null) {
            model.addAttribute("isLogout", true);
        } else {
            // 전체 목록 수
            Map<String, String> countMap = pFillService.getFillRequestCount(pharmacist, request);
            String pageUrl = "./fillRequest/page=";
            int listTotalCnt = Integer.parseInt(String.valueOf(countMap.get("listTotalCnt")));
            Paging paging = new Paging(pageUrl, listTotalCnt, 15, page, 10);
            model.addAttribute("listCnt", countMap); // 상태별 카운트
            model.addAttribute("paging", paging); // 페이징

            // 목록
            Map<String, Object> listMap = pFillService.getFillRequest(pharmacist, request, paging.getStartRecord(), paging.getEndRecord());
            model.addAttribute("listData", listMap.get("listData"));
        }

        return "pharmacy/fill_request_info";
    }

    // 처방전 정보
    @PostMapping("/prescriptionInfo")
    public String prescriptionInfo(HttpServletRequest request, Model model) {
        Map<String, Object> info = hRemoteService.getPrescriptionInfo(request);
        if(!isNull(info)) model.addAttribute("info", info);

        return "hospital/remote_prescription_info";
    }

    // 배송지 정보
    @PostMapping("/addrInfo")
    public String addrInfo(@RequestParam(value="medicalIdx") Integer medicalIdx, Model model) {
        Map<String, String> info = pFillService.getAddrInfo(medicalIdx); // 원격진료idx
        if(!isNull(info)) model.addAttribute("info", info);

        return "pharmacy/fill_addr_info";
    }

    // 약 조제관리 - 조제요청 접수
    @PostMapping("/statusComplete")
    @ResponseBody
    public int statusComplete(@RequestParam Integer idx, @RequestParam Integer billAmt) {
        return pFillService.statusComplete(idx, billAmt);
    }

    // 약 조제관리 - 조제요청 거부
    @PostMapping("/statusReject")
    @ResponseBody
    public int statusReject(@RequestParam Integer idx, @RequestParam String cancelReason) {
        return pFillService.statusReject(idx, cancelReason);
    }

    // 약 수령방식 변경
    @PostMapping("/receiveMethodChange")
    @ResponseBody
    public int receiveMethodChange(@RequestParam Integer medicalIdx, @RequestParam String receiveMethod) {
        return pFillService.receiveMethodChange(medicalIdx, receiveMethod);
    }

    // 약 조제관리- 조제접수
    @GetMapping("/fillReceive")
    public String fillReceive(@AuthPharmacist Pharmacist pharmacist, Model model) {
        if (pharmacist == null) {
            model.addAttribute("isLogout", true);
        } else {
            model.addAttribute("userName", pharmacist.getUserName()); // header bind
        }

        return "pharmacy/fill_receive";
    }

    // 약 조제관리 - 조제접수 목록
    @PostMapping("/getFillReceive")
    public String getFillReceive(@AuthPharmacist Pharmacist pharmacist, Model model, @RequestParam(value="page", required=false, defaultValue="1") int page, HttpServletRequest request) {
        if (pharmacist == null) {
            model.addAttribute("isLogout", true);
        } else {
            // 전체 목록 수
            Map<String, String> countMap = pFillService.getFillReceiveCount(pharmacist, request);
            String pageUrl = "./fillReceive/page=";
            int listTotalCnt = Integer.parseInt(String.valueOf(countMap.get("listTotalCnt")));
            Paging paging = new Paging(pageUrl, listTotalCnt, 15, page, 10);
            model.addAttribute("listCnt", countMap); // 상태별 카운트
            model.addAttribute("paging", paging); // 페이징

            // 목록
            Map<String, Object> listMap = pFillService.getFillReceive(pharmacist, request, paging.getStartRecord(), paging.getEndRecord());
            model.addAttribute("listData", listMap.get("listData"));
        }

        return "pharmacy/fill_receive_info";
    }

    // 약 조제관리 - 조제접수 완료
    @PostMapping("/fillComplete")
    @ResponseBody
    public String fillComplete(@AuthPharmacist Pharmacist pharmacist, @RequestParam Integer idx) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        if(!isNull(pharmacist) && !isNull(idx)) {
            // 등록/수정
            int chk = pFillService.fillComplete(idx);
            if (chk != 0) obj.addProperty("result", true);
        }

        return obj.toString();
    }

    // 복약지도
    @PostMapping("/mediGuideInfo")
    public String mediGuideInfo(@RequestParam Integer medicalIdx, Model model) {
        model.addAttribute("medicalIdx", medicalIdx);

        List<Map<String, String>> data = pFillService.mediGuideInfo(medicalIdx);
        model.addAttribute("listData", data);

        return "pharmacy/fill_medi_guide_info";
    }

    // 의약품 검색 (자동완성)
    @PostMapping("/mediAutocomplete")
    @ResponseBody
    public String mediAutocomplete(@AuthPharmacist Pharmacist pharmacist, @RequestParam String keyword, @RequestParam String gubun) {
        JSONObject obj = new JSONObject();

        List<Map<String, String>> data = pFillService.mediAutocomplete(pharmacist.getPharmacyIdx(), keyword, gubun);
        Map<String, List<Map<String, String>>> map = new HashMap<>();
        map.put("result", data);
        if(!isNull(data)) {
            obj.putAll(map);
        }

        return obj.toString();
    }

    // 복약지도 저장 (등록/수정)
    @PostMapping("/saveGuide")
    @ResponseBody
    public String saveGuide(MultipartHttpServletRequest request) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        // 등록/수정
        int chk = pFillService.saveGuide(request);
        if (chk != 0) obj.addProperty("result", true);

        return obj.toString();
    }

    // 약 조제내역
    @GetMapping("/fillList")
    public String fillList(@AuthPharmacist Pharmacist pharmacist, Model model) {
        if (pharmacist == null) {
            model.addAttribute("isLogout", true);
        } else {
            // 현재 월의 마지막날짜 구하기
            LocalDate date = LocalDate.now();
            String today = date.toString();
            String year = today.substring(0,4);
            String month = today.substring(5,7);
            Calendar cal = Calendar.getInstance();
            cal.set(Integer.parseInt(year),Integer.parseInt(month)-1,1);
            String lastDay = Integer.toString(cal.getActualMaximum(Calendar.DAY_OF_MONTH));
            model.addAttribute("stDate", year+"-"+month+"-01");
            model.addAttribute("edDate", year+"-"+month+"-"+lastDay);
        }

        return "pharmacy/fill_list";
    }

    // 약 조제내역 목록
    @PostMapping("/getFillList")
    public String getFillList(@AuthPharmacist Pharmacist pharmacist, Model model, @RequestParam(value="page", required=false, defaultValue="1") int page, HttpServletRequest request) {
        if (pharmacist == null) {
            model.addAttribute("isLogout", true);
        } else {
            // 전체 목록 수
            Map<String, String> countMap = pFillService.getFillListCount(pharmacist, request);
            String pageUrl = "./fillList/page=";
            int listTotalCnt = Integer.parseInt(String.valueOf(countMap.get("listTotalCnt")));
            Paging paging = new Paging(pageUrl, listTotalCnt, 15, page, 10);
            model.addAttribute("listCnt", countMap); // 상태별 카운트
            model.addAttribute("paging", paging); // 페이징

            // 목록
            Map<String, Object> listMap = pFillService.getFillList(pharmacist, request, paging.getStartRecord(), paging.getEndRecord());
            model.addAttribute("listData", listMap.get("listData"));
        }

        return "pharmacy/fill_list_info";
    }
}
