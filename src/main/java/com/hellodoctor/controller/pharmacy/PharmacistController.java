package com.hellodoctor.controller.pharmacy;

import com.google.gson.JsonObject;
import com.hellodoctor.common.AuthPharmacist;
import com.hellodoctor.common.Constants;
import com.hellodoctor.config.ApiKey;
import com.hellodoctor.model.app.Pharmacy;
import com.hellodoctor.model.pharmacy.Pharmacist;
import com.hellodoctor.service.pharmacy.PharmacistService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.util.Map;

/**
 * 약사 관련 controller 클래스
 */
@Controller // 컨트롤러임을 명시
@RequestMapping("/pharmacy")
@Slf4j
public class PharmacistController {
    @Autowired
    PharmacistService pharmacistService;

    // 회원가입 - 계정정보
    @GetMapping(value = {"/certify/signUpForm1", "/certify/signUpForm1/{idx}"})
    public String signUpForm1(@AuthPharmacist Pharmacist pharmacist, @PathVariable(required = false) Integer idx, Model model) {
        if (pharmacist == null) { // 로그인 X
            if(idx != null) { // 수정
                model.addAttribute("idx", idx);

                // 약사 정보
                Pharmacist pharmacistData = pharmacistService.getPharmacistInfo(idx);
                model.addAttribute("data", pharmacistData);
            }
        } else {
            return "redirect:/pharmacy/fillRequest";
        }

        return "pharmacy/sign_up_form1";
    }

    // 아이디 중복체크
    @PostMapping("/certify/idCheck")
    @ResponseBody
    public int idCheck(@RequestParam("id") String id) {
        return pharmacistService.idCheck(id);
    }

    // 회원가입 - 약국정보
    @GetMapping("/certify/signUpForm2/{idx}")
    public String signUpForm2(@PathVariable Integer idx, Model model) {
        log.info("signup2");
        if(idx != null) {
            // TODO: 개발완료 시 운영키로 변경 필요
            model.addAttribute("kakao_js_key", ApiKey.KAKAO_JS_KEY);
            model.addAttribute("idx", idx);
            model.addAttribute("errMsg", "");

            // 약사정보
            Pharmacist pharmacistData = pharmacistService.getPharmacistInfo(idx);
            int pharmacyIdx = Integer.parseInt(String.valueOf(pharmacistData.getPharmacyIdx()));
            if(pharmacyIdx != 0) {
                // 약국정보
                Map<String, Object> pharmacyData = pharmacistService.getPharmacyInfo(pharmacyIdx);
                model.addAttribute("data", pharmacyData);
                model.addAttribute("pharmacyIdx", pharmacyIdx);
            }
        } else {
            model.addAttribute("errMsg", "오류가 발생하였습니다.");
        }

        return "pharmacy/sign_up_form2";
    }

    // 회원가입 - 정산계좌정보
    @GetMapping("/certify/signUpForm3/{idx}")
    public String signUpForm3(@PathVariable Integer idx, Model model) {
        if(idx != null) {
            model.addAttribute("bankCodeList", Constants.BANK_CODE);
            model.addAttribute("idx", idx);
            model.addAttribute("errMsg", "");

            // 약사정보
            Pharmacist pharmacistData = pharmacistService.getPharmacistInfo(idx);
            int pharmachIdx = Integer.parseInt(String.valueOf(pharmacistData.getPharmacyIdx()));
            if(pharmachIdx != 0) {
                // 약국정보
                Map<String, Object> hospitalData = pharmacistService.getPharmacyInfo(pharmachIdx);
                model.addAttribute("data", hospitalData);
                model.addAttribute("pharmacyIdx", pharmachIdx);
            } else {
                model.addAttribute("errMsg", "오류가 발생하였습니다.");
            }
        } else {
            model.addAttribute("errMsg", "오류가 발생하였습니다.");
        }

        return "pharmacy/sign_up_form3";
    }

    // 회원가입 1(계정정보), 2(약국정보) 액션 (등록/수정)
    @PostMapping("/certify/signUpFormAction")
    @ResponseBody
    public String signUpFormAction(Pharmacist pharmacist, Pharmacy pharmacy, MultipartHttpServletRequest request) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);
        obj.addProperty("errMsg", "");

        // 추가/수정
        int idx = pharmacistService.signUpFormAction(pharmacist, pharmacy, request);
        if (idx != 0) {
            obj.addProperty("result", true);
            obj.addProperty("idx", idx);
        }

        return obj.toString();
    }
}
