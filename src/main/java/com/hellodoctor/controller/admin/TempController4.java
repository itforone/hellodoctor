package com.hellodoctor.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 임시 라우터
 * 기능 작업 시 사용 컨트롤러로 빼고 여기서 삭제할 것!
 */

@Controller
@RequestMapping("/admin") // /admin
public class TempController4 {

    // 병원 정산관리 페이지
    @GetMapping("/hosCalculate")
    public String hosCalculate() {
        return "admin/hos_calculate";
    }

    // 약국 정산관리 페이지
    @GetMapping("/phaCalculate")
    public String phaCalculate() {
        return "admin/pha_calculate";
    }

}
