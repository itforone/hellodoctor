package com.hellodoctor.controller.admin;

import com.hellodoctor.common.AuthAdmin;
import com.hellodoctor.common.Functions;
import com.hellodoctor.common.Paging;
import com.hellodoctor.model.admin.Admin;
import com.hellodoctor.model.app.Member;
import com.hellodoctor.model.app.MemberFamily;
import com.hellodoctor.service.admin.AMemberService;
import com.hellodoctor.service.app.MyPageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 회원관리
 */
@Controller
@RequestMapping("/admin")
@Slf4j
public class AMemberController {
    @Autowired
    AMemberService aMemberService;
    @Autowired
    MyPageService myPageService;

    // 회원관리
    @GetMapping("/memberList")
    public String memberList(@AuthAdmin Admin admin, Model model) {
        if (admin == null) {
            model.addAttribute("isLogout", true);
        } else {
            model.addAttribute("userName", admin.getUserName());  // header bind

            // 검색옵션
            Map<String, String> options = new LinkedHashMap<>();
            options.put("num", "회원번호");
            options.put("name", "회원명");
            options.put("email", "이메일(아이디)");
            options.put("hp", "연락처");
            model.addAttribute("schOptions", options);

            // 목록수 옵션
            //int defaultRow = 15;
            Integer[] rowOptions = {15,30,50,100};
            List<Integer> rowOptionsArr = Arrays.asList(rowOptions);
            model.addAttribute("rowOptions", rowOptionsArr);
        }

        return "admin/member";
    }

    // 회원관리 목록
    @GetMapping("/ajaxMemberList")
    public String ajaxMemberList(Model model, @RequestParam Map<String, String> params) {
        int page = Integer.parseInt(params.get("page"));
        int listRow = Integer.parseInt(params.get("listRow"));

        // 전체글수
        String pageUrl = "./memberList?page=";
        int listTotalCnt = aMemberService.getMemberTotalCount(params);
        model.addAttribute("listTotalCnt", listTotalCnt);
        // 페이징
        Paging paging = new Paging(pageUrl, listTotalCnt, listRow, page, 10);
        model.addAttribute("paging", paging);

        // 목록
        List<Map<String, Object>> memberList = aMemberService.getMemberList(paging.getStartRecord(), paging.getEndRecord(), params);
        model.addAttribute("memberList", memberList);

        return "admin/member_list"; // ajax html load
    }

    // 회원관리 상세보기
    @GetMapping("/ajaxMemberInfo")
    public String ajaxMemberInfo(Model model, @RequestParam(value="idx", required=false, defaultValue="0") int idx) {
        // 회원정보
        Map<String, Object> member = aMemberService.getMemberInfo(idx);
        model.addAttribute("member", member);

        // 가족정보
        List<Object> familyList = myPageService.getFamilyList(idx);
        model.addAttribute("familyList", familyList);
        model.addAttribute("listCount", familyList.size());
        log.info(String.valueOf(familyList));

        return "admin/member_info"; // ajax html load
    }
}
