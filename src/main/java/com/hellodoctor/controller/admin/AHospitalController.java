package com.hellodoctor.controller.admin;

import com.google.gson.JsonObject;
import com.hellodoctor.common.AuthAdmin;
import com.hellodoctor.common.Constants;
import com.hellodoctor.common.Paging;
import com.hellodoctor.model.admin.Admin;
import com.hellodoctor.model.app.Hospital;
import com.hellodoctor.model.hospital.Doctor;
import com.hellodoctor.service.admin.AHospitalService;
import com.hellodoctor.service.hospital.DoctorService;
import com.hellodoctor.service.hospital.HConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 병원관리
 */
@Controller
@RequestMapping("/admin")
@Slf4j
public class AHospitalController {
    @Autowired
    AHospitalService aHospitalService;
    @Autowired
    HConfigService hConfigService;
    @Autowired
    DoctorService doctorService;

    // 병원관리
    @GetMapping("/hospitalList")
    public String hospitalList(@AuthAdmin Admin admin, Model model) {
        if (admin == null) {
            model.addAttribute("isLogout", true);
        } else {
            // 검색옵션
            Map<String, String> options = new LinkedHashMap<>();
            options.put("name", "병원명");
            options.put("tel", "연락처");
            options.put("addr", "주소");
            model.addAttribute("schOptions", options);

            // 목록수 옵션
            Integer[] rowOptions = {15,30,50,100};
            List<Integer> rowOptionsArr = Arrays.asList(rowOptions);
            model.addAttribute("rowOptions", rowOptionsArr);
        }

        return "admin/hospital";
    }

    // 병원관리 목록
    @GetMapping("/ajaxHospitalList")
    public String ajaxHospitalList(Model model, @RequestParam Map<String, String> params) {
        int page = Integer.parseInt(params.get("page"));
        int listRow = Integer.parseInt(params.get("listRow"));

        // 전체글수
        String pageUrl = "./hospitalList?page=";
        int listTotalCnt = aHospitalService.getHospitalTotalCount(params);
        model.addAttribute("listTotalCnt", listTotalCnt);
        // 페이징
        Paging paging = new Paging(pageUrl, listTotalCnt, listRow, page, 10);
        model.addAttribute("paging", paging);

        // 목록
        List<Map<String, Object>> hospitalList = aHospitalService.getHospitalList(paging.getStartRecord(), paging.getEndRecord(), params);
        model.addAttribute("hospitalList", hospitalList);

        return "admin/hospital_list"; // ajax html load
    }

    // 병원 승인상태 변경
    @PostMapping("/changeHospitalAuth")
    @ResponseBody
    public String changeHospitalAuth(@RequestParam("idx") int idx, @RequestParam("authYn") String authYn) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int chk = aHospitalService.changeHospitalAuth(idx, authYn);
        if (chk > 0) obj.addProperty("result", true);

        return obj.toString();
    }

    // 병원 계정정보
    @GetMapping("/ajaxHospitalAccount")
    public String ajaxHospitalAccount(Model model, @RequestParam("idx") int idx) {
        List<Doctor> account = aHospitalService.getHospitalAccount(idx);
        model.addAttribute("account", account);

        return "admin/hospital_pop_account"; // ajax html load
    }

    // 병원 정보 상세
    @GetMapping("/ajaxHospitalInfo")
    public String ajaxHospitalInfo(Model model, @RequestParam("idx") int idx) {
        Hospital info = aHospitalService.getHospitalInfo(idx);
        model.addAttribute("info", info);

        Map<String, String> files = aHospitalService.getHospitalImgFiles(info);
        model.addAttribute("files", files);
        // log.info(files.toString());

        return "admin/hospital_pop_info"; // ajax html load
    }

    // 병원정보 수정요청
    @GetMapping("/hospitalUpdateList")
    public String hospitalUpdateList(@AuthAdmin Admin admin, Model model) {
        if (admin == null) {
            model.addAttribute("isLogout", true);
        } else {
            // 검색옵션
            Map<String, String> options = new LinkedHashMap<>();
            options.put("name", "병원명");
            options.put("tel", "연락처");
            options.put("addr", "주소");
            model.addAttribute("schOptions", options);

            // 목록수 옵션
            Integer[] rowOptions = {15,30,50,100};
            List<Integer> rowOptionsArr = Arrays.asList(rowOptions);
            model.addAttribute("rowOptions", rowOptionsArr);
        }

        return "admin/hospital_update";
    }

    // 병원정보 수정요청 목록
    @GetMapping("/ajaxHospitalUpdateList")
    public String ajaxHospitalUpdateList(Model model, @RequestParam Map<String, String> params) {
        int page = Integer.parseInt(params.get("page"));
        int listRow = Integer.parseInt(params.get("listRow"));

        // 전체글수
        String pageUrl = "./hospitalUpdateList?page=";
        int listTotalCnt = aHospitalService.getHospitalUpdateTotalCount(params);
        model.addAttribute("listTotalCnt", listTotalCnt);
        // 페이징
        Paging paging = new Paging(pageUrl, listTotalCnt, listRow, page, 10);
        model.addAttribute("paging", paging);

        // 목록
        List<Map<String, Object>> hospitalList = aHospitalService.getHospitalUpdateList(paging.getStartRecord(), paging.getEndRecord(), params);
        model.addAttribute("hospitalList", hospitalList);

        return "admin/hospital_update_list"; // ajax html load
    }

    //  병원정보 수정요청 상세
    @GetMapping("/hospitalUpdateView/{idx}")
    public String hospitalUpdateView(@PathVariable("idx") int idx, Model model) {
        model.addAttribute("subjectCodeList", Constants.HOSPITAL_SUBJECT_CODE); // 진료과목코드

        // 병원정보
        Map<String, Object> hospital = doctorService.getHospitalInfo(idx);
        Map<String, Object> hospitalUpdate = aHospitalService.getHospitalUpdateInfo(idx);
        model.addAttribute("data", hospital);
        model.addAttribute("updateData", hospitalUpdate);

        return "admin/hospital_update_view";
    }

    // 병원정보 수정요청 승인
    @PostMapping("/hospitalUpdateApproval")
    @ResponseBody
    public String hospitalUpdateApproval(@RequestParam("hospitalIdx") int hospitalIdx) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int chk = aHospitalService.hospitalUpdateApproval(hospitalIdx);
        if (chk != 0) obj.addProperty("result", true);

        return obj.toString();
    }
}
