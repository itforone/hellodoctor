package com.hellodoctor.controller.admin;

import com.google.gson.JsonObject;
import com.hellodoctor.common.AuthAdmin;
import com.hellodoctor.common.AuthDoctor;
import com.hellodoctor.common.Constants;
import com.hellodoctor.common.Paging;
import com.hellodoctor.mapper.app.FileUploadMapper;
import com.hellodoctor.model.admin.Admin;
import com.hellodoctor.model.app.FileUpload;
import com.hellodoctor.model.hospital.AdminNotice;
import com.hellodoctor.model.hospital.Doctor;
import com.hellodoctor.service.admin.ABoardService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 커뮤니티 controller 클래스
 */
@Controller
@RequestMapping("/admin")
@Slf4j
public class ABoardController {
    @Autowired
    ABoardService aBoardService;
    @Autowired
    FileUploadMapper fileUploadMapper;

    // 리뷰관리 목록
    @GetMapping("/reviewList")
    public String reviewList(@AuthAdmin Admin admin, Model model,
                             @RequestParam(value="page", required=false, defaultValue="1") int page) {
        if (admin == null) {
            model.addAttribute("isLogout", true);
        } else {
            // 전체글수
            int totalCount = aBoardService.getReviewTotalCount(0);
            String pageUrl = "./reviewList?page=";
            // 페이징
            Paging paging = new Paging(pageUrl, totalCount, 15, page, 10);
            model.addAttribute("paging", paging);

            //  목록
            List<Map<String, String>> reviewList = aBoardService.getReviewList(0, paging.getStartRecord(), paging.getEndRecord());
            model.addAttribute("reviewList", reviewList);
        }

        return "admin/review_list";
    }

    // 리뷰 상세
    @GetMapping("/reviewView") // ?idx=1
    public String reviewView(@AuthAdmin Admin admin, Model model,
                             @RequestParam(value="idx", required=false, defaultValue="0") int reviewIdx) {
        if (admin == null) {
            model.addAttribute("isLogout", true);
        } else {
            Map<String, String> result = aBoardService.getReviewDetail(reviewIdx);
            // log.info("result=" + result);
            model.addAttribute("viewData", result);
            model.addAttribute("reviewIdx", reviewIdx);
            // model.addAttribute("doctorIdx", doctor.getIdx());
            model.addAttribute("nlString", System.getProperty("line.separator").toString()); // 타임리프 개행처리
        }
        return "admin/review_view";
    }

    // CS문의
    @GetMapping(value={"/csListApp", "/csList"})
    public String csList(
            @AuthAdmin Admin admin, Model model,
            HttpServletRequest request, @RequestParam Map<String, String> getParams
    ) {
        // get 파라미터
        if (getParams.get("page") == null) getParams.put("page", "1");
        if (getParams.get("svc") == null) getParams.put("svc", "");
        if (getParams.get("stx") == null) getParams.put("stx", "");
        if (getParams.get("sfl") == null) getParams.put("sfl", "");
        model.addAttribute("getParams", getParams);
        // log.info("getParams" + getParams.toString());

        if (request.getRequestURI().contains("/csListApp")) { // 1. 앱
            // 문의유형
            model.addAttribute("svcOptions", Constants.CS_CATEGORY);

            // 검색옵션
            Map<String, String> options = new LinkedHashMap<>();
            options.put("name", "작성자");
            options.put("tel", "연락처");
            options.put("content", "내용");
            model.addAttribute("schOptions", options);

            return "admin/cs_app_list";

        } else { // 2. 의/약
            // 문의유형
            model.addAttribute("svcOptions", Constants.CS_WEB_CATEGORY);

            // 검색옵션
            Map<String, String> options = new LinkedHashMap<>();
            options.put("gname", "병원/약국명");
            options.put("name", "작성자");
            options.put("tel", "연락처");
            options.put("title", "제목");
            options.put("content", "내용");
            model.addAttribute("schOptions", options);

            return "admin/cs_list";
        }
    }

    // CS문의 목록
    @GetMapping("/ajaxCSList")
    public String ajaxCSList(Model model, @RequestParam Map<String, String> params) {
        int page = Integer.parseInt(params.get("page"));
        String pageTarget = params.get("pageTarget");

        // 전체글수
        String pageUrl = "./csListApp?page=";
        int listTotalCnt = aBoardService.getCSTotalCount(params);
        model.addAttribute("listTotalCnt", listTotalCnt);
        // 페이징
        Paging paging = new Paging(pageUrl, listTotalCnt, 15, page, 10);
        model.addAttribute("paging", paging);

        // 목록
        List<Map<String, String>> csList = aBoardService.getCSList(paging.getStartRecord(), paging.getEndRecord(), params);
        model.addAttribute("csList", csList);

        // log.info("ajaxCSList" + model.toString());

        // 앱 : 의/약
        return (pageTarget.equals("APP"))? "admin/cs_app_list_load" : "admin/cs_list_load"; // ajax html load
    }

    // CS문의 상세
    @GetMapping(value={"/csViewApp/{idx}", "/csView/{idx}"})
    public String csView(
            @PathVariable int idx, @AuthAdmin Admin admin, Model model, HttpServletRequest request
    ) {
        String pageTarget = request.getRequestURI().contains("/csViewApp")? "APP" : "SITE"; // 앱 : 의/약

        if (admin == null) {
            model.addAttribute("isLogout", true);
        } else {
            Map<String, String> cs = aBoardService.getCSForm(idx, pageTarget);

            if (cs != null) {
                model.addAttribute("viewData", cs);
                model.addAttribute("idx", idx);
                model.addAttribute("admIdx", admin.getIdx());
                model.addAttribute("nlString", System.getProperty("line.separator").toString()); // 타임리프 개행처리

                // 의/약 첨부파일조회
                if (pageTarget.equals("SITE")) {
                    // 파일업로드(유저)
                    List<FileUpload> files = fileUploadMapper.getUploadFiles(cs.get("fileCode"), "csSite", 0);
                    model.addAttribute("files", files);
                    // 파일업로드(관리자)
                    List<FileUpload> adminFiles = fileUploadMapper.getUploadFiles(cs.get("answerFileCode"), "csSite", 0);
                    model.addAttribute("adminFiles", adminFiles);
                }

            } else {
                model.addAttribute("errMsg", "잘못된 접근입니다.");
            }
        }

        return pageTarget.equals("APP")? "admin/cs_app_view" : "admin/cs_view";
    }

    // CS문의 답변등록 - 앱 (텍스트만 업로드)
    @PostMapping("/saveAnswerApp")
    @ResponseBody
    public String saveAnswerApp(@RequestParam Map<String, String> params) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int chk = aBoardService.saveAnswerApp(params);
        if (chk > 0) obj.addProperty("result", true);

        return obj.toString();
    }

    // CS문의 답변등록 - 의/약 (이미지 포함)
    @PostMapping("/saveAnswerSite")
    @ResponseBody
    public String saveAnswerSite(MultipartHttpServletRequest request) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int chk = aBoardService.saveAnswerSite(request);
        if (chk > 0) obj.addProperty("result", true);

        return obj.toString();
    }

    // 공지사항
    @GetMapping("/notice")
    public String notice(@AuthAdmin Admin admin, Model model, @RequestParam Map<String, String> getParams) {
        if (admin == null) {
            model.addAttribute("isLogout", true);
        }

        // 열람대상
        model.addAttribute("tgtOptions", Constants.ADM_NOTICE_TARGET);

        // 검색옵션
        Map<String, String> options = new LinkedHashMap<>();
        options.put("title", "제목");
        options.put("content", "내용");
        model.addAttribute("schOptions", options);

        // get 파라미터
        if (getParams.get("page")==null) getParams.put("page", "1");
        if (getParams.get("tgt")==null) getParams.put("tgt", "");
        if (getParams.get("stx")==null) getParams.put("stx", "");
        if (getParams.get("sfl")==null) getParams.put("sfl", "");
        model.addAttribute("getParams", getParams);
        // log.info("getParams" + getParams.toString());

        return "admin/notice";
    }

    // 공지사항 목록
    @GetMapping("/ajaxNoticeList")
    public String ajaxMemberList(Model model, @RequestParam Map<String, String> params) {
        int page = Integer.parseInt(params.get("page"));

        // 전체글수
        String pageUrl = "./notice?page=";
        int listTotalCnt = aBoardService.getNoticeTotalCount(params);
        model.addAttribute("listTotalCnt", listTotalCnt);
        // 페이징
        Paging paging = new Paging(pageUrl, listTotalCnt, 15, page, 10);
        model.addAttribute("paging", paging);

        // 목록
        List<AdminNotice> noticeList = aBoardService.getNoticeList(paging.getStartRecord(), paging.getEndRecord(), params);
        model.addAttribute("category", Constants.ADM_NOTICE_CATEGORY); // 공지유형
        model.addAttribute("noticeList", noticeList);
        model.addAttribute("nlString", System.getProperty("line.separator").toString()); // 타임리프 개행처리
        // log.info(noticeList.toString());

        return "admin/notice_list"; // ajax html load
    }

    // 공지사항 등록/수정
    @GetMapping("/noticeForm") // 수정: ?idx=1
    public String noticeForm(@AuthAdmin Admin admin, Model model,
                             @RequestParam(value="idx", required=false, defaultValue="0") int idx) {
        model.addAttribute("errMsg", "");

        if (admin == null) {
            model.addAttribute("isLogout", true);
        } else {
            model.addAttribute("idx", idx);
            model.addAttribute("admIdx", admin.getIdx());
            model.addAttribute("tgtOptions", Constants.ADM_NOTICE_TARGET); // 열람대상
            model.addAttribute("category", Constants.ADM_NOTICE_CATEGORY); // 공지유형

            // 수정
            if (idx > 0) {
                AdminNotice notice = aBoardService.getNoticeForm(idx);
                if (notice != null) {
                    model.addAttribute("data", notice);
                } else {
                    model.addAttribute("errMsg", "잘못된 접근입니다.");
                }
            }

        }

        return "admin/notice_form";
    }

    // 공지사항 등록/수정 - 처리
    @PostMapping("/noticeFormAction")
    @ResponseBody
    public String noticeFormAction(AdminNotice form) { //MultipartHttpServletRequest request, -> 추후 파일업로드 생기면
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int chk = aBoardService.saveNoticeForm(form);
        if (chk != 0) {
            obj.addProperty("result", true);
            obj.addProperty("idx", form.getIdx());
        }

        return obj.toString();
    }

    // 공지사항 삭제
    @PostMapping("/deleteNotice")
    @ResponseBody
    public String deleteNotice(@AuthAdmin Admin admin, @RequestParam("idx") int idx) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        if (admin == null) {
            // 회원정보 없음
            obj.addProperty("errMsg", "로그인 세션이 만료되었습니다.");
        } else {
            // 등록/수정
            int chk = aBoardService.deleteNotice(idx);
            if (chk > 0) obj.addProperty("result", true);
        }

        return obj.toString();
    }
}
