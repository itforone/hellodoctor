package com.hellodoctor.controller.admin;

import com.hellodoctor.common.AuthAdmin;
import com.hellodoctor.common.AuthPharmacist;
import com.hellodoctor.common.Constants;
import com.hellodoctor.model.admin.Admin;
import com.hellodoctor.model.pharmacy.Pharmacist;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * 로그인
 */
@Controller
@RequestMapping("/admin")
@Slf4j
public class ALoginController {
    // 로그인 페이지
    @GetMapping(value={"/login", "/"})
    public String emailLogin(@RequestParam(value = "error", required = false) String error,
                             @RequestParam(value = "code", required = false) String errorCode,
                             @AuthAdmin Admin admin,
                             Model model) {

        if (error != null) {    // 로그인 에러메시지 출력
            Integer code = Integer.parseInt(errorCode);
            model.addAttribute("error", error);
            model.addAttribute("exception", Constants.MEMBER_LOGIN_ERR_MSG.get(code));
        }

        // 로그인 체크
        if (admin == null) {
            return "admin/login";
        } else {
            log.info("로그인사용자=" + admin.toString());
            return "redirect:/admin/memberList";
        }
    }

}
