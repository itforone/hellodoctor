package com.hellodoctor.controller.admin;

import com.google.gson.JsonObject;
import com.hellodoctor.common.AuthAdmin;
import com.hellodoctor.common.Constants;
import com.hellodoctor.model.admin.Admin;
import com.hellodoctor.model.admin.Terms;
import com.hellodoctor.model.hospital.AdminNotice;
import com.hellodoctor.model.pharmacy.Pharmacist;
import com.hellodoctor.service.admin.ASystemService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 시스템관리 controller 클래스
 */
@Controller
@RequestMapping("/admin")
@Slf4j
public class ASystemController {
    @Autowired
    ASystemService aSystemService;

    // 약관관리 목록
    @GetMapping("/term")
    public String term(@AuthAdmin Admin admin, Model model) {
        if (admin == null) {
            model.addAttribute("isLogout", true);
        } else {
            List<Map<String, String>> termsList = aSystemService.getTermsList();
            model.addAttribute("termsList", termsList);
            model.addAttribute("startNo", termsList.size());
            // model.addAttribute("data", terms);
            // model.addAttribute("termsList", Constants.SITE_TERMS_LIST);
            // model.addAttribute("startNo", Constants.SITE_TERMS_LIST.size());
        }

        return "admin/term";
    }

    // 약관관리 등록
    @GetMapping("/termForm/{key}")
    public String termForm(@AuthAdmin Admin admin, Model model, @PathVariable String key) {
        model.addAttribute("errMsg", "");

        if (admin == null) {
            model.addAttribute("isLogout", true);
        } else {
            // 약관 key 존재여부 확인
            if (Constants.SITE_TERMS_LIST.containsKey(key)) {
                model.addAttribute("title", Constants.SITE_TERMS_LIST.get(key));
                model.addAttribute("data", aSystemService.getTerms(key));
                model.addAttribute("key", key);
            } else {
                model.addAttribute("errMsg", "잘못된 접근입니다.");
            }
        }

        log.info(model.toString());
        return "admin/term_form";
    }

    // 약관 저장
    // 약 조제관리 - 조제요청 접수
    @PostMapping("/saveTerms")
    @ResponseBody
    public String saveTerms(Terms form) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int chk = aSystemService.saveTerms(form);
        if (chk > 0) obj.addProperty("result", true);

        return obj.toString();
    }

    // 관리자관리 목록
    @GetMapping("/adminList")
    public String adminList(@AuthAdmin Admin admin, Model model) {
        if (admin == null) {
            model.addAttribute("isLogout", true);
        } else {
            List<Admin> adminList = aSystemService.getAdminList();
            model.addAttribute("adminList", adminList);
        }

        return "admin/admin_list";
    }

    // 관리자관리 등록/수정 폼
    @GetMapping("/ajaxAdminAccount")
    public String ajaxAdminAccount(Model model, @RequestParam(value="id", required=false, defaultValue="") String id) {
        model.addAttribute("data", aSystemService.getAdminAccount(id));
        model.addAttribute("readonly", false);

        if (!id.equals("")) { // 수정
            model.addAttribute("readonly", true);
        }

        return "admin/admin_pop"; // ajax html load
    }

    // 관리자 아이디 중복체크
    @PostMapping("/adminIdCheck")
    @ResponseBody
    public String adminIdCheck(@RequestParam("id") String id) {
        JsonObject obj = new JsonObject();
        int count = aSystemService.adminIdCheck(id);
        obj.addProperty("count", count);

        return obj.toString();
    }

    // 관리자 등록/수정 처리
    @PostMapping("/adminFormAction")
    @ResponseBody
    public String adminFormAction(Admin form, HttpServletRequest request) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int chk = aSystemService.saveAdmin(form, request);
        if (chk > 0) obj.addProperty("result", true);

        return obj.toString();
    }

    // 관리자 삭제
    @PostMapping("/deleteAdmin")
    @ResponseBody
    public String deleteAdmin(@RequestParam("idx") int idx) {
        JsonObject obj = new JsonObject();

        int chk = aSystemService.deleteAdmin(idx);
        if (chk > 0) obj.addProperty("result", true);

        return obj.toString();
    }

}
