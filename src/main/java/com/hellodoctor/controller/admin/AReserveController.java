package com.hellodoctor.controller.admin;

import com.hellodoctor.common.AuthAdmin;
import com.hellodoctor.common.Paging;
import com.hellodoctor.model.admin.Admin;
import com.hellodoctor.service.admin.AReserveService;
import com.hellodoctor.service.hospital.HRemoteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static com.hellodoctor.common.Functions.isNull;

/**
 * 예약정보관리
 */

@Controller
@RequestMapping("/admin")
@Slf4j
public class AReserveController {
    @Autowired
    AReserveService aReserveService;
    @Autowired
    HRemoteService hRemoteService;

    // 예약정보관리
    @GetMapping("/reserveList")
    public String reserveList(@AuthAdmin Admin admin, Model model) {
        if (admin == null) {
            model.addAttribute("isLogout", true);
        } else {
            model.addAttribute("userName", admin.getUserName());  // header bind
        }

        return "admin/reserve_list";
    }

    // 예약정보관리 목록
    @PostMapping("/getReserveList")
    public String getReserveList(@AuthAdmin Admin admin, Model model, @RequestParam(value="page", required=false, defaultValue="1") int page, HttpServletRequest request) {
        if (admin == null) {
            model.addAttribute("isLogout", true);
        } else {
            // 전체 목록 수
            Map<String, String> countMap = aReserveService.getReserveListCount(admin, request);
            String pageUrl = "./reserveList/page=";
            int listTotalCnt = Integer.parseInt(String.valueOf(countMap.get("listTotalCnt")));
            Paging paging = new Paging(pageUrl, listTotalCnt, 15, page, 10);
            model.addAttribute("listCnt", countMap); // 상태별 카운트
            model.addAttribute("paging", paging); // 페이징

            // 목록
            Map<String, Object> listMap = aReserveService.getReserveList(admin, request, paging.getStartRecord(), paging.getEndRecord());
            model.addAttribute("listData", listMap.get("listData"));
        }

        return "admin/reserve_list_info";
    }

    // 예약접수
    @PostMapping("/receiptComplete")
    @ResponseBody
    public int receiptComplete(@RequestParam Integer idx, @RequestParam String medicalTime) {
        return hRemoteService.receiptComplete(idx, medicalTime);
    }

    // 예약거부
    @PostMapping("/receiptReject")
    @ResponseBody
    public int receiptReject(@RequestParam Integer idx, @RequestParam String cancelReason) {
        return hRemoteService.receiptReject(idx, cancelReason);
    }

    // 환자 정보
    @PostMapping("/patientInfo")
    public String patientInfo(HttpServletRequest request, Model model) {
        Map<String, Object> info = hRemoteService.getPatientInfo(request);
        if(isNull(info)) model.addAttribute("err", true);
        else model.addAttribute("info", info);

        return "hospital/remote_patient_info";
    }
}