package com.hellodoctor.controller.admin;

import com.google.gson.JsonObject;
import com.hellodoctor.common.AuthAdmin;
import com.hellodoctor.common.Paging;
import com.hellodoctor.model.admin.Admin;
import com.hellodoctor.model.app.Pharmacy;
import com.hellodoctor.model.pharmacy.Pharmacist;
import com.hellodoctor.service.admin.APharmacyService;
import com.hellodoctor.service.pharmacy.PharmacistService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 약국관리
 */
@Controller
@RequestMapping("/admin")
@Slf4j
public class APharmacyController {
    @Autowired
    APharmacyService aPharmacyService;
    @Autowired
    PharmacistService pharmacistService;

    // 약국관리
    @GetMapping("/pharmacyList")
    public String pharmacyList(@AuthAdmin Admin admin, Model model) {
        if (admin == null) {
            model.addAttribute("isLogout", true);
        } else {
            // 검색옵션
            Map<String, String> options = new LinkedHashMap<>();
            options.put("name", "약국명");
            options.put("tel", "연락처");
            options.put("addr", "주소");
            model.addAttribute("schOptions", options);

            // 목록수 옵션
            Integer[] rowOptions = {15,30,50,100};
            List<Integer> rowOptionsArr = Arrays.asList(rowOptions);
            model.addAttribute("rowOptions", rowOptionsArr);
        }

        return "admin/pharmacy";
    }

    // 약국관리 목록
    @GetMapping("/ajaxPharmacyList")
    public String ajaxPharmacyList(Model model, @RequestParam Map<String, String> params) {
        int page = Integer.parseInt(params.get("page"));
        int listRow = Integer.parseInt(params.get("listRow"));

        // 전체글수
        String pageUrl = "./pharmacyList?page=";
        int listTotalCnt = aPharmacyService.getPharmacyTotalCount(params);
        model.addAttribute("listTotalCnt", listTotalCnt);
        // 페이징
        Paging paging = new Paging(pageUrl, listTotalCnt, listRow, page, 10);
        model.addAttribute("paging", paging);

        // 목록
        List<Map<String, Object>> pharmacyList = aPharmacyService.getPharmacyList(paging.getStartRecord(), paging.getEndRecord(), params);
        model.addAttribute("pharmacyList", pharmacyList);

        return "admin/pharmacy_list"; // ajax html load
    }

    // 약국 승인상태 변경
    @PostMapping("/changePharmacyAuth")
    @ResponseBody
    public String changePharmacyAuth(@RequestParam("idx") int idx, @RequestParam("authYn") String authYn) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int chk = aPharmacyService.changePharmacyAuth(idx, authYn);
        if (chk > 0) obj.addProperty("result", true);

        return obj.toString();
    }

    // 약국 계정정보
    @GetMapping("/ajaxPharmacyAccount")
    public String ajaxPharmacyAccount(Model model, @RequestParam("idx") int idx) {
        List<Pharmacist> account = aPharmacyService.getPharmacyAccount(idx);
        model.addAttribute("account", account);

        return "admin/pharmacy_pop_account"; // ajax html load
    }

    // 약국 정보 상세
    @GetMapping("/ajaxPharmacyInfo")
    public String ajaxPharmacyInfo(Model model, @RequestParam("idx") int idx) {
        Pharmacy info = aPharmacyService.getPharmacyInfo(idx);
        model.addAttribute("info", info);

        Map<String, String> files = aPharmacyService.ajaxPharmacyImgFiles(info);
        model.addAttribute("files", files);
        // log.info(files.toString());

        return "admin/pharmacy_pop_info"; // ajax html load
    }

    // 약국정보 수정요청
    @GetMapping("/pharmacyUpdateList")
    public String pharmacyUpdateList(@AuthAdmin Admin admin, Model model) {
        if (admin == null) {
            model.addAttribute("isLogout", true);
        } else {
            // 검색옵션
            Map<String, String> options = new LinkedHashMap<>();
            options.put("name", "약국명");
            options.put("tel", "연락처");
            options.put("addr", "주소");
            model.addAttribute("schOptions", options);

            // 목록수 옵션
            Integer[] rowOptions = {15,30,50,100};
            List<Integer> rowOptionsArr = Arrays.asList(rowOptions);
            model.addAttribute("rowOptions", rowOptionsArr);
        }

        return "admin/pharmacy_update";
    }

    // 약국정보 수정요청 목록
    @GetMapping("/ajaxPharmacyUpdateList")
    public String ajaxPharmacyUpdateList(Model model, @RequestParam Map<String, String> params) {
        int page = Integer.parseInt(params.get("page"));
        int listRow = Integer.parseInt(params.get("listRow"));

        // 전체글수
        String pageUrl = "./pharmacyUpdateList?page=";
        int listTotalCnt = aPharmacyService.getPharmacyUpdateTotalCount(params);
        model.addAttribute("listTotalCnt", listTotalCnt);
        // 페이징
        Paging paging = new Paging(pageUrl, listTotalCnt, listRow, page, 10);
        model.addAttribute("paging", paging);

        // 목록
        List<Map<String, Object>> pharmacyList = aPharmacyService.getPharmacyUpdateList(paging.getStartRecord(), paging.getEndRecord(), params);
        model.addAttribute("pharmacyList", pharmacyList);

        return "admin/pharmacy_update_list"; // ajax html load
    }

    //  약국정보 수정요청 상세
    @GetMapping("/pharmacyUpdateView/{idx}")
    public String pharmacyUpdateView(@PathVariable("idx") int idx, Model model) {
        // 약국정보
        Map<String, Object> pharmacy = pharmacistService.getPharmacyInfo(idx);
        Map<String, Object> pharmacyUpdate = aPharmacyService.getPharmacyUpdateInfo(idx);
        model.addAttribute("data", pharmacy);
        model.addAttribute("updateData", pharmacyUpdate);

        return "admin/pharmacy_update_view";
    }

    // 약국정보 수정요청 승인
    @PostMapping("/pharmacyUpdateApproval")
    @ResponseBody
    public String pharmacyUpdateApproval(@RequestParam(value = "pharmacyIdx") Integer pharmacyIdx) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int chk = aPharmacyService.pharmacyUpdateApproval(pharmacyIdx);
        if (chk != 0) obj.addProperty("result", true);

        return obj.toString();
    }
}
