package com.hellodoctor.controller.admin;

import com.google.gson.JsonObject;
import com.hellodoctor.common.AuthAdmin;
import com.hellodoctor.common.Paging;
import com.hellodoctor.model.admin.Admin;
import com.hellodoctor.service.admin.APrescriptionService;
import com.hellodoctor.service.hospital.HRemoteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static com.hellodoctor.common.Functions.isNull;

/**
 * 처방전관리
 */

@Controller
@RequestMapping("/admin")
@Slf4j
public class APrescriptionController {
    @Autowired
    APrescriptionService aPrescriptionService;
    @Autowired
    HRemoteService hRemoteService;

    // 처방전관리 페이지
    @GetMapping("/pxList")
    public String pxList(@AuthAdmin Admin admin, Model model) {
        if (admin == null) {
            model.addAttribute("isLogout", true);
        } else {
            model.addAttribute("userName", admin.getUserName());  // header bind
        }

        return "admin/px_list";
    }

    // 처방전관리 목록
    @PostMapping("/ajaxPxList")
    public String ajaxPxList(@AuthAdmin Admin admin, Model model, @RequestParam(value="page", required=false, defaultValue="1") int page, HttpServletRequest request) {
        if (admin == null) {
            model.addAttribute("isLogout", true);
        } else {
            // 전체 목록 수
            Map<String, String> countMap = aPrescriptionService.getPxListCount(admin, request);
            String pageUrl = "./pxList/page=";
            int listTotalCnt = Integer.parseInt(String.valueOf(countMap.get("listTotalCnt")));
            Paging paging = new Paging(pageUrl, listTotalCnt, 15, page, 10);
            model.addAttribute("listCnt", countMap); // 상태별 카운트
            model.addAttribute("paging", paging); // 페이징

            // 목록
            Map<String, Object> listMap = aPrescriptionService.getPxList(admin, request, paging.getStartRecord(), paging.getEndRecord());
            model.addAttribute("listData", listMap.get("listData"));
        }

        return "admin/px_list_info";
    }

    // 처방전 정보
    @PostMapping("/prescriptionInfo")
    public String prescriptionInfo(HttpServletRequest request, Model model) {
        Map<String, Object> info = hRemoteService.getPrescriptionInfo(request);
        if(!isNull(info)) model.addAttribute("info", info);

        return "hospital/remote_prescription_info";
    }

    // 처방전 재발행 요청
    @PostMapping("/rePublishRequest")
    @ResponseBody
    public String rePublishRequest(@RequestParam("idx") int presIdx) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int chk = aPrescriptionService.rePublishRequest(presIdx);
        if(chk != 0) obj.addProperty("result", true);

        return obj.toString();
    }
}