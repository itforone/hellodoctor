package com.hellodoctor.controller.hospital;

import com.google.gson.JsonObject;
import com.hellodoctor.common.AuthDoctor;
import com.hellodoctor.model.hospital.Doctor;
import com.hellodoctor.model.hospital.MedicalOpinion;
import com.hellodoctor.service.hospital.HWebRTCService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 원격진료 WebRTC 및 진료소견 컨트롤러
 */

@Controller
@RequestMapping("/hospital") // /hospital
@Slf4j
public class HWebRTCController {
    @Autowired
    HWebRTCService hWebRTCService;

    // 진료실 입장
    @GetMapping("/remoteTreat/{medicalIdx}")
    public String remoteTreat(@AuthDoctor Doctor doctor, @PathVariable int medicalIdx, Model model) {
        if (doctor == null) {
            model.addAttribute("isLogout", true);
        } else {
            model.addAttribute("roomId", medicalIdx);
            model.addAttribute("userId", doctor.getUserId());

            // 환자정보 & 진료소견
            Map<String, Object> result = hWebRTCService.getRemoteMedicalInfo(medicalIdx, doctor.getHospitalIdx());
            model.addAttribute("patientInfo", result.get("patientInfo"));
            model.addAttribute("medicalOpn", result.get("medicalOpinion"));
            model.addAttribute("etcFiles", result.get("etcFiles"));

            // 진료 인덱스, 의사 인덱스
            model.addAttribute("medicalIdx", medicalIdx);
            model.addAttribute("doctorIdx", doctor.getIdx());
        }

        return "hospital/remote_treat";
    }

    // 비대면 화상 진료 iframe
    @GetMapping("/certify/iframeLoad")
    public String remoteTreatIframe(@AuthDoctor Doctor doctor, @RequestParam("roomId") int roomId, Model model) {
        model.addAttribute("roomId", roomId);
        model.addAttribute("userId", doctor.getUserId());
        log.info(model.toString());
        return "hospital/remote_treat_iframe";
    }

    // 진료실 진료소견 저장
    @PostMapping("/saveMedicalOpinion")
    @ResponseBody
    public String saveMedicalOpinion(MedicalOpinion form) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int chk = hWebRTCService.saveMedicalOpinion(form);
        if (chk != 0) {
            obj.addProperty("result", true);
            obj.addProperty("idx", form.getIdx());
        }

        return obj.toString();
    }
}
