package com.hellodoctor.controller.hospital;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 임시 라우터
 * 기능 작업 시 사용 컨트롤러로 빼고 여기서 삭제할 것!
 */

@Controller
@RequestMapping("/hospital") // /hospital
@Slf4j
public class TempController2 {
    // 정산
    @GetMapping("/calculate")
    public String calculate() {
        return "hospital/calculate";
    }
}
