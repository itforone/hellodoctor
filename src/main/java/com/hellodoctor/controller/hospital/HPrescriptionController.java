package com.hellodoctor.controller.hospital;

import com.google.gson.JsonObject;
import com.hellodoctor.common.AuthDoctor;
import com.hellodoctor.common.Paging;
import com.hellodoctor.model.hospital.Doctor;
import com.hellodoctor.service.hospital.HPrescriptionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Map;

import static com.hellodoctor.common.Functions.isNull;

/**
 * 처방전재발행
 */

@Controller
@RequestMapping("/hospital")
@Slf4j
public class HPrescriptionController {
    @Autowired
    HPrescriptionService hPrescriptionService;

    // 처방전 재발행
    @GetMapping("/pxReissue")
    public String pxReissue(@AuthDoctor Doctor doctor, Model model) {
        if (doctor == null) {
            model.addAttribute("isLogout", true);
        } else {
            model.addAttribute("userName", doctor.getUserName());  // header bind

            // 현재 월의 마지막날짜 구하기
            LocalDate date = LocalDate.now();
            String today = date.toString();
            String year = today.substring(0,4);
            String month = today.substring(5,7);
            Calendar cal = Calendar.getInstance();
            cal.set(Integer.parseInt(year),Integer.parseInt(month)-1,1);
            String lastDay = Integer.toString(cal.getActualMaximum(Calendar.DAY_OF_MONTH));
            model.addAttribute("stDate", year+"-"+month+"-01");
            model.addAttribute("edDate", year+"-"+month+"-"+lastDay);
        }

        return "hospital/px_reissue";
    }

    // 처방전 재발행 목록
    @PostMapping("/ajaxPxReissue")
    public String ajaxPxReissue(@AuthDoctor Doctor doctor, Model model, @RequestParam(value="page", required=false, defaultValue="1") int page, HttpServletRequest request) {
        if (doctor == null) {
            model.addAttribute("isLogout", true);
        } else {
            // 전체 목록 수
            Map<String, String> countMap = hPrescriptionService.getPxReissueCount(doctor, request);
            String pageUrl = "./pxReissue/page=";
            int listTotalCnt = Integer.parseInt(String.valueOf(countMap.get("listTotalCnt")));
            Paging paging = new Paging(pageUrl, listTotalCnt, 15, page, 10);
            model.addAttribute("listCnt", countMap); // 상태별 카운트
            model.addAttribute("paging", paging); // 페이징

            // 목록
            Map<String, Object> listMap = hPrescriptionService.getPxReissue(doctor, request, paging.getStartRecord(), paging.getEndRecord());
            model.addAttribute("listData", listMap.get("listData"));
        }

        return "hospital/px_reissue_info";
    }

    // 재발행 처방전 정보
    @PostMapping("/reissuePrescriptionInfo")
    public String reissuePrescriptionInfo(HttpServletRequest request, Model model) {
        Map<String, Object> info = hPrescriptionService.reissuePrescriptionInfo(request);
        if(!isNull(info)) model.addAttribute("info", info);

        return "hospital/remote_prescription_info";
    }

    // 재발행 처방전 등록
    @PostMapping("/saveReissuePrescription")
    @ResponseBody
    public String saveReissuePrescription(@AuthDoctor Doctor doctor, MultipartHttpServletRequest request) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);
        obj.addProperty("errMsg", "");

        // 등록/수정
        int idx = hPrescriptionService.saveReissuePrescription(request);
        if (idx != 0) obj.addProperty("result", true);

        return obj.toString();
    }

    // 재발행 처방전 - 발행
    @PostMapping("/prescriptionPublish")
    @ResponseBody
    public String prescriptionPublish(HttpServletRequest request) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);
        obj.addProperty("errMsg", "");

        int idx = hPrescriptionService.prescriptionPublish(request);
        if (idx != 0) obj.addProperty("result", true);

        return obj.toString();
    }
}