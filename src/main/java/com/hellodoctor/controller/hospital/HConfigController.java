package com.hellodoctor.controller.hospital;

import com.google.gson.JsonObject;
import com.hellodoctor.common.AuthDoctor;
import com.hellodoctor.common.Constants;
import com.hellodoctor.common.Paging;
import com.hellodoctor.config.ApiKey;
import com.hellodoctor.mapper.app.FileUploadMapper;
import com.hellodoctor.model.app.Hospital;
import com.hellodoctor.model.hospital.Doctor;
import com.hellodoctor.model.app.Pharmacy;
import com.hellodoctor.model.hospital.DesignatePatientTemplate;
import com.hellodoctor.service.hospital.DoctorService;
import com.hellodoctor.service.hospital.HConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.hellodoctor.common.Functions.isNull;

/**
 * 관리 관련 controller 클래스
 * @Autowired: 해당 변수 및 메서드에 스프링이 관리하는 Bean을 자동으로 매핑 (의존관계를 자동으로 설정)
 */
@Controller // 컨트롤러임을 명시
@RequestMapping("/hospital")
@Slf4j
public class HConfigController {
    @Autowired
    HConfigService hConfigService;
    @Autowired
    FileUploadMapper fileUploadMapper;
    @Autowired
    DoctorService doctorService;

    // 계정관리
    @GetMapping("/admAccount")
    public String admAccount(@AuthDoctor Doctor doctor, Model model) {
        if (doctor == null) {
            model.addAttribute("isLogout", true);
        } else {
            // 의사정보
            model.addAttribute("data", doctor);
            log.info("doctor: "+doctor);
        }

        return "hospital/adm_account";
    }

    // 계정관리 저장
    @PostMapping("/saveAdmAccount")
    @ResponseBody
    public String saveAdmAccount(@AuthDoctor Doctor doctor, MultipartHttpServletRequest request) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int chk = hConfigService.saveAdmAccount(doctor, request);
        if (chk != 0) obj.addProperty("result", true);

        return obj.toString();
    }

    // 서브계정관리
    @GetMapping("/admSubAccount")
    public String admSubAccount(@AuthDoctor Doctor doctor, Model model) {
        if (doctor == null) {
            model.addAttribute("isLogout", true);
        }

        return "hospital/adm_sub_account";
    }

    // 서브계정관리 목록
    @PostMapping("/ajaxAdmSubAccount")
    public String getAdmSubAccount(@AuthDoctor Doctor doctor, Model model, @RequestParam(value="page", required=false, defaultValue="1") int page, HttpServletRequest request) {
        if (doctor == null) {
            model.addAttribute("isLogout", true);
        } else {
            // 서브계정관리 목록 (부계정만 조회)
            String pageUrl = "./admSubAccount?page=";
            int totalCount = hConfigService.getAdmSubAccountCount(doctor.getHospitalIdx(), request);
            // 페이징
            Paging paging = new Paging(pageUrl, totalCount, 15, page, 10);
            model.addAttribute("paging", paging);

            //  목록
            Map<String, Object> listMap = hConfigService.getAdmSubAccount(doctor.getHospitalIdx(), request, paging.getStartRecord(), paging.getEndRecord());
            model.addAttribute("listData", listMap.get("listData"));
        }

        return "hospital/adm_sub_account_info";
    }

    // 서브계정관리 등록폼
    @GetMapping(value= {"/admSubForm", "/admSubForm/{idx}"})
    public String admSubForm(@AuthDoctor Doctor doctor, @PathVariable(required = false) Integer idx, Model model) {
        if (doctor == null) {
            model.addAttribute("isLogout", true);
        } else {
            // 진료과 정보
            Map<Integer, String> subjectCode = hConfigService.getSubjectCodeInfo(doctor.getHospitalIdx());
            model.addAttribute("subjectCode", subjectCode);

            if(idx != null) { // 수정
                // 의사 정보
                Map<String, Object> doctorData = doctorService.getDoctorInfo(idx);
                model.addAttribute("data", doctorData);
            }
        }

        return "hospital/adm_sub_form";
    }

    // 서브계정관리 저장
    @PostMapping("/saveAdmSubAccount")
    @ResponseBody
    public String saveAdmSubAccount(@AuthDoctor Doctor doctor, MultipartHttpServletRequest request) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int chk = hConfigService.saveAdmSubAccount(doctor, request);
        if (chk != 0) obj.addProperty("result", true);

        return obj.toString();
    }

    // 서브계정관리 삭제
    @PostMapping("deleteDoctor")
    @ResponseBody
    public String deleteDoctor(@RequestParam(value="idx") int idx) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int chk = hConfigService.deleteDoctor(idx);
        if (chk != 0) obj.addProperty("result", true);

        return obj.toString();
    }

    // 병원정보관리
    @GetMapping("/admHospital")
    public String admHospital(@AuthDoctor Doctor doctor, Model model) {
        if (doctor == null) {
            model.addAttribute("isLogout", true);
        } else {
            // TODO: 개발완료 시 운영키로 변경 필요
            model.addAttribute("kakao_js_key", ApiKey.KAKAO_JS_KEY);
            model.addAttribute("subjectCodeList", Constants.HOSPITAL_SUBJECT_CODE); // 진료과목코드

            // 병원정보
            Map<String, Object> hospital = doctorService.getHospitalInfo(doctor.getHospitalIdx());
            model.addAttribute("data", hospital);
            log.info("hospital: "+hospital);
        }

        return "hospital/adm_hospital";
    }

    // 병원정보관리 저장
    @PostMapping("/saveAdmHospital")
    @ResponseBody
    public String saveAdmHospital(@AuthDoctor Doctor doctor, Hospital hospital, MultipartHttpServletRequest request) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int chk = hConfigService.saveAdmHospital(doctor, hospital, request);
        if (chk != 0) obj.addProperty("result", true);

        return obj.toString();
    }

    // 정산계좌관리
    @GetMapping("/admBank")
    public String admBank(@AuthDoctor Doctor doctor, Model model) {
        if (doctor == null) {
            model.addAttribute("isLogout", true);
        } else {
            model.addAttribute("bankCode", Constants.BANK_CODE);

            // 병원정보
            Hospital hospital = hConfigService.getHospitalInfo(doctor.getHospitalIdx());
            model.addAttribute("data", hospital);
        }

        return "hospital/adm_bank";
    }

    // 정산계좌관리 저장
    @PostMapping("/saveAccount")
    @ResponseBody
    public String saveAccount(MultipartHttpServletRequest request) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int chk = hConfigService.saveAccount(request);
        if (chk != 0) obj.addProperty("result", true);

        return obj.toString();
    }

    // 지정약국관리
    @GetMapping("/admPharmacy")
    public String admPharmacy(@AuthDoctor Doctor doctor, Model model) {
        if (doctor == null) {
            model.addAttribute("isLogout", true);
        } else {
            int hospitalIdx = doctor.getHospitalIdx();
            // 병원정보
            Hospital hospital = hConfigService.getHospitalInfo(hospitalIdx);
            model.addAttribute("lng", hospital.getLng());
            model.addAttribute("lat", hospital.getLat());
            model.addAttribute("hospitalIdx", hospitalIdx);

            // 병원 지정환자 수
            int desPatientCnt = hConfigService.getDesignatePatientCnt(hospitalIdx);
            model.addAttribute("desPatientCnt", desPatientCnt);

            // 검색옵션
            Map<String, String> options = new HashMap<>();
            options.put("name", "약국이름");
            options.put("addr", "주소");
            options.put("tel", "연락처");

            model.addAttribute("schOptions", options);
        }

        return "hospital/adm_pharmacy";
    }

    // 지정약국관리 목록
    @GetMapping("/ajaxAdmPharmacyList")
    public String ajaxAdmPharmacyList(@AuthDoctor Doctor doctor, Model model, @RequestParam Map<String, String> params) {
        int hospitalIdx = doctor.getHospitalIdx();
        int page = Integer.parseInt(params.get("page"));

        // 전체글수
        String pageUrl = "./admPharmacy?page=";
        int listTotalCnt = hConfigService.getDesignatePharmacyTotalCount(hospitalIdx, params);
        model.addAttribute("listTotalCnt", listTotalCnt);
        // 페이징
        Paging paging = new Paging(pageUrl, listTotalCnt, 15, page, 10);
        model.addAttribute("paging", paging);
        log.info("paging=" + paging);

        // 목록
        List<Map<String, String>> pharmacyList = hConfigService.getDesignatePharmacyList(paging.getStartRecord(), paging.getEndRecord(), hospitalIdx, params);
        model.addAttribute("pharmacyList", pharmacyList);

        return "hospital/adm_pharmacy_list"; // ajax html load
    }

    // 지정약국관리 신규등록
    @GetMapping("/ajaxPharmacyRegister")
    public String ajaxPharmacyRegister(
            Model model,
            @RequestParam(value="hospitalIdx", required=false, defaultValue="0") int hospitalIdx,
            @RequestParam(value="lat", required=false, defaultValue="0") String lat,
            @RequestParam(value="lng", required=false, defaultValue="0") String lng,
            @RequestParam(value="sfl", required=false, defaultValue="name") String sfl,
            @RequestParam(value="stx", required=false, defaultValue="") String stx) {

        // 검색옵션
        Map<String, String> options = new HashMap<>();
        options.put("name", "약국이름");
        options.put("addr", "주소");
        options.put("tel", "연락처");

        model.addAttribute("schOptions", options);
        model.addAttribute("sfl", sfl);
        model.addAttribute("stx", stx);

        // 약국 목록
        List<Pharmacy> pharmacyList = hConfigService.getSearchPharmacyList(hospitalIdx, lat, lng, sfl, stx);
        model.addAttribute("listData", pharmacyList);

        log.info(model.toString());
        return "hospital/adm_pharmacy_register"; // ajax html load
    }

    // 지정약국관리 신규등록 처리
    @PostMapping("/saveDesignatePharmacy")
    @ResponseBody
    public String saveDesignatePharmacy(@RequestParam("hospitalIdx") int hospitalIdx, @RequestParam("chkIdx") String chkIdx) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int chk = hConfigService.saveDesignatePharmacy(hospitalIdx, chkIdx);
        if (chk != 0) obj.addProperty("result", true);

        return obj.toString();
    }

    // 지정약국관리 해제 처리
    @PostMapping("/deleteDesignatePharmacy")
    @ResponseBody
    public String deleteDesignatePharmacy(@RequestParam("idx") int idx, @RequestParam("hospitalIdx") int hospitalIdx) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int chk = hConfigService.deleteDesignatePharmacy(hospitalIdx, idx);
        if (chk != 0) obj.addProperty("result", true);

        return obj.toString();
    }

    // 지정환자관리
    @GetMapping("/admPatient")
    public String admPatient(@AuthDoctor Doctor doctor, Model model) {
        if (doctor == null) {
            model.addAttribute("isLogout", true);
        } else {
            int hospitalIdx = doctor.getHospitalIdx();

            // 템플릿
            DesignatePatientTemplate template = hConfigService.getHospitalTemplate(hospitalIdx);
            log.info(String.valueOf(template));
            model.addAttribute("template", template);
            model.addAttribute("hospitalIdx", hospitalIdx);
            model.addAttribute("doctorIdx", doctor.getIdx());

            // 등록요청내역 전체개수
            int reqListCnt = hConfigService.getAdmPatientRequestCount(hospitalIdx);
            model.addAttribute("reqListCnt", reqListCnt);

            // 검색옵션
            Map<String, String> options = new HashMap<>();
            options.put("name", "환자이름");
            options.put("tel", "연락처");

            model.addAttribute("schOptions", options);
        }

        return "hospital/adm_patient";
    }

    // 지정환자관리 - 템플릿 저장
    @PostMapping("/saveHospitalTemplate")
    @ResponseBody
    public String saveHospitalTemplate(MultipartHttpServletRequest request, DesignatePatientTemplate template) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int chk = hConfigService.saveHospitalTemplate(request, template);
        if (chk != 0) obj.addProperty("result", true);

        return obj.toString();
    }

    // 지정환자관리 - 등록요청
    @PostMapping("/reqeustDesignatePatient")
    @ResponseBody
    public String reqeustDesignatePatient(@RequestParam Map<String, String> params) {
        JsonObject obj = new JsonObject();
        // obj.addProperty("result", false);

        Map<String, Object> requestResult = hConfigService.reqeustDesignatePatient(params);
        obj.addProperty("result", (Boolean) requestResult.get("result"));
        obj.addProperty("errMsg", requestResult.get("errMsg").toString());

        return obj.toString();
    }

    // 지정환자관리 목록
    @GetMapping("/ajaxAdmPatientList")
    public String ajaxAdmPatientList(@AuthDoctor Doctor doctor, Model model, @RequestParam Map<String, String> params) {
        int hospitalIdx = doctor.getHospitalIdx();
        int page = Integer.parseInt(params.get("page"));

        // 전체글수
        String pageUrl = "./admPatient?page=";
        int listTotalCnt = hConfigService.getDesignatePatientTotalCount(hospitalIdx, params);
        model.addAttribute("listTotalCnt", listTotalCnt);
        // 페이징
        Paging paging = new Paging(pageUrl, listTotalCnt, 15, page, 10);
        model.addAttribute("paging", paging);
        log.info("paging=" + paging);

        // 목록
        List<Map<String, String>> patientList = hConfigService.getDesignatePatientList(paging.getStartRecord(), paging.getEndRecord(), hospitalIdx, params);
        model.addAttribute("patientList", patientList);

        return "hospital/adm_patient_list"; // ajax html load
    }

    // 지정환자관리 등록요청내역
    @PostMapping("/ajaxAdmPatientReqList")
    @ResponseBody
    public String ajaxAdmPatientReqList(@RequestParam Map<String, String> params) {
        JsonObject obj = new JsonObject();

        // 페이징
        int totalCount = Integer.parseInt(params.get("totalCount"));
        int page = Integer.parseInt(params.get("page"));
        Paging paging = new Paging("", totalCount, 15, page, 1);
        obj.addProperty("totalPage", paging.getTotalPages());
        // log.info(paging.toString());

        if (page <= paging.getTotalPages()) { // 불러올 페이지가 존재하면
            int hospitalIdx = Integer.parseInt(params.get("hospitalIdx"));
            List<String> listData = hConfigService.getAdmPatientRequestList(hospitalIdx, paging.getStartRecord(), paging.getEndRecord());
            obj.addProperty("listData", listData.toString());
            // log.info(listData.toString());
        }

        return obj.toString();
    }

    // 진료예약관리
    @GetMapping("/admTreat")
    public String admTreat(@AuthDoctor Doctor doctor, Model model) {
        model.addAttribute("medicalTimeList", Constants.MEDICAL_TIME); // 예약진료시간

        // 지정약국 수
        int pharmacyCount = hConfigService.getPharmacyCount(doctor.getHospitalIdx());
        model.addAttribute("pharmacyCount", pharmacyCount);

        // 병원정보
        Map<String, Object> hospitalData = doctorService.getHospitalInfo(doctor.getHospitalIdx());
        model.addAttribute("data", hospitalData);

        // 진료예약시간정보
        Map<String, Object> appointment = hConfigService.getAppointmentInfo(doctor.getHospitalIdx());
        model.addAttribute("appointment1", appointment.get("list1")); // 지정환자
        model.addAttribute("appointment2", appointment.get("list2")); // 일반환자

        return "hospital/adm_treat";
    }

    // 병원영업시간/진료예약시간 폼
    @PostMapping("/ajaxReserveManage")
    public String reserveManage(@AuthDoctor Doctor doctor, Model model) {
        model.addAttribute("medicalTimeList", Constants.MEDICAL_TIME); // 예약진료시간

        // 지정약국 수
        int pharmacyCount = hConfigService.getPharmacyCount(doctor.getHospitalIdx());
        model.addAttribute("pharmacyCount", pharmacyCount);

        return "hospital/reserve_manage";
    }

    // 진료예약시간(특정일) 목록
    @PostMapping("/getAppointmentList")
    @ResponseBody
    public String getAppointmentList(@AuthDoctor Doctor doctor) {
        JsonObject obj = new JsonObject();

        int hospitalIdx = doctor.getHospitalIdx();
        String data = hConfigService.getAppointmentList(hospitalIdx);
        obj.addProperty("data", data);

        return obj.toString();
    }

    // 진료예약시간(특정일) 조회
    @PostMapping("/getAppointment")
    @ResponseBody
    public String getAppointment(@AuthDoctor Doctor doctor, @RequestParam String date) {
        JsonObject obj = new JsonObject();

        int hospitalIdx = doctor.getHospitalIdx();
        Map<String, String> data = hConfigService.getAppointment(hospitalIdx, date);
        obj.addProperty("result", false);
        if(!isNull(data.get("data1"))) {
            obj.addProperty("data1", data.get("data1")); // 지정환자
            obj.addProperty("result", true);
        }
        if(!isNull(data.get("data2"))) {
            obj.addProperty("data2", data.get("data2")); // 일반환자
            obj.addProperty("result", true);
        }

        return obj.toString();
    }

    // 진료예약시간(특정일) 저장
    @PostMapping("/saveAppointment")
    @ResponseBody
    public String saveAppointment(@AuthDoctor Doctor doctor, @RequestParam String date, @RequestParam String timeTable, @RequestParam String d_timeTable) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int chk = hConfigService.saveAppointment(doctor.getHospitalIdx(), date, timeTable, d_timeTable);
        if (chk != 0) obj.addProperty("result", true);

        return obj.toString();
    }

    // 진료예약관리 저장
    @PostMapping("saveAdmTreat")
    @ResponseBody
    public String saveAdmTreat(@AuthDoctor Doctor doctor, HttpServletRequest request) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);

        int chk = hConfigService.saveAdmTreat(doctor, request);
        if (chk != 0) obj.addProperty("result", true);

        return obj.toString();
    }
}