package com.hellodoctor.controller.hospital;

import com.google.gson.JsonObject;
import com.hellodoctor.common.AuthDoctor;
import com.hellodoctor.common.Paging;
import com.hellodoctor.model.hospital.Doctor;
import com.hellodoctor.service.hospital.HRemoteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static com.hellodoctor.common.Functions.isNull;

/**
 * 의사-원격진료 관련 controller
 */
@Controller
@RequestMapping("/hospital")
@Slf4j
public class HRemoteController {
    @Autowired
    HRemoteService hRemoteService;

    // 2.1.1 원격진료-예약신청
    @GetMapping("/remoteList")
    public String remoteList(@AuthDoctor Doctor doctor, Model model) {
        if (doctor == null) {
            model.addAttribute("isLogout", true);
        } else {
            model.addAttribute("userName", doctor.getUserName());  // header bind
        }

        return "hospital/remote_list";
    }

    // 원격진료-예약신청 목록
    @PostMapping("/ajaxRemoteList")
    public String getRemoteList(@AuthDoctor Doctor doctor, Model model, @RequestParam(value="page", required=false, defaultValue="1") int page, HttpServletRequest request) {
        if (doctor == null) {
            model.addAttribute("isLogout", true);
        } else {
            // 전체 목록 수
            Map<String, String> countMap = hRemoteService.getRemoteListCount(doctor, request);
            String pageUrl = "./remoteList/page=";
            int listTotalCnt = Integer.parseInt(String.valueOf(countMap.get("listTotalCnt")));
            Paging paging = new Paging(pageUrl, listTotalCnt, 15, page, 10);
            model.addAttribute("listCnt", countMap); // 상태별 카운트
            model.addAttribute("paging", paging); // 페이징

            // 목록
            Map<String, Object> listMap = hRemoteService.getRemoteList(doctor, request, paging.getStartRecord(), paging.getEndRecord());
            model.addAttribute("listData", listMap.get("listData"));
        }

        return "hospital/remote_list_info";
    }

    // 원격진료 예약접수
    @PostMapping("/receiptComplete")
    @ResponseBody
    public int receiptComplete(@RequestParam Integer idx, @RequestParam String medicalTime) {
        return hRemoteService.receiptComplete(idx, medicalTime);
    }

    // 원격진료 예약거부
    @PostMapping("/receiptReject")
    @ResponseBody
    public int receiptReject(@RequestParam Integer idx, @RequestParam String cancelReason) {
        return hRemoteService.receiptReject(idx, cancelReason);
    }

    // 환자 정보
    @PostMapping("/patientInfo")
    public String patientInfo(HttpServletRequest request, Model model) {
        Map<String, Object> info = hRemoteService.getPatientInfo(request);
        if(isNull(info)) model.addAttribute("err", true);
        else model.addAttribute("info", info);

        return "hospital/remote_patient_info";
    }

    // 2.1.3 원격진료-접수
   @GetMapping("/remoteReceipt")
    public String remoteReceipt(@AuthDoctor Doctor doctor, Model model) {
       if (doctor == null) {
           model.addAttribute("isLogout", true);
       }

       return "hospital/remote_receipt";
    }

    // 원격진료-접수 목록
    @PostMapping("/ajaxRemoteReceipt")
    public String ajaxRemoteReceipt(@AuthDoctor Doctor doctor, Model model, @RequestParam(value="page", required=false, defaultValue="1") int page, HttpServletRequest request) {
        if (doctor == null) {
            model.addAttribute("isLogout", true);
        } else {
            // 전체 목록 수
            Map<String, String> countMap = hRemoteService.getRemoteReceiptCount(doctor, request);
            String pageUrl = "./remoteReceipt/page=";
            int listTotalCnt = Integer.parseInt(String.valueOf(countMap.get("listTotalCnt")));
            Paging paging = new Paging(pageUrl, listTotalCnt, 15, page, 10);
            model.addAttribute("listCnt", countMap); // 상태별 카운트
            model.addAttribute("paging", paging); // 페이징

            // 목록
            Map<String, Object> listMap = hRemoteService.getRemoteReceipt(doctor, request, paging.getStartRecord(), paging.getEndRecord());
            model.addAttribute("listData", listMap.get("listData"));
        }

        return "hospital/remote_receipt_info";
    }

    // 처방전 정보
    @PostMapping("/prescriptionInfo")
    public String prescriptionInfo(HttpServletRequest request, Model model) {
        Map<String, Object> info = hRemoteService.getPrescriptionInfo(request);
        if(!isNull(info)) model.addAttribute("info", info);

        return "hospital/remote_prescription_info";
    }

    // 처방전 등록/수정
    @PostMapping("/prescriptionSave")
    @ResponseBody
    public String prescriptionSave(@AuthDoctor Doctor doctor, MultipartHttpServletRequest request) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);
        obj.addProperty("errMsg", "");

        // 등록/수정
        int idx = hRemoteService.prescriptionSave(doctor, request);
        if (idx != 0) obj.addProperty("result", true);

        return obj.toString();
    }

    // 청구금액 정보
    @PostMapping("/billAmtInfo")
    @ResponseBody
    public Map<String, Object> billAmtInfo(@AuthDoctor Doctor doctor, HttpServletRequest request) {
        return hRemoteService.getBillAmtInfo(doctor, request);
    }

    // 청구금액 저장/수정
    @PostMapping("/billAmtSave")
    @ResponseBody
    public String billAmtSave(@AuthDoctor Doctor doctor, HttpServletRequest request) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);
        obj.addProperty("errMsg", "");

        // 저장/수정
        int idx = hRemoteService.billAmtSave(doctor, request);
        if (idx != 0) obj.addProperty("result", true);

        return obj.toString();
    }

}
