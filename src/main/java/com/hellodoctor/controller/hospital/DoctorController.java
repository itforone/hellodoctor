package com.hellodoctor.controller.hospital;

import com.google.gson.JsonObject;
import com.hellodoctor.common.AuthDoctor;
import com.hellodoctor.common.Constants;
import com.hellodoctor.config.ApiKey;
import com.hellodoctor.model.app.Hospital;
import com.hellodoctor.model.hospital.Doctor;
import com.hellodoctor.service.hospital.DoctorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.util.Map;

/**
 * 의사 관련 controller 클래스
 * @Autowired: 해당 변수 및 메서드에 스프링이 관리하는 Bean을 자동으로 매핑 (의존관계를 자동으로 설정)
 */
@Controller // 컨트롤러임을 명시
@RequestMapping("/hospital") // /app
@Slf4j
public class DoctorController {
    @Autowired
    DoctorService doctorService;

    // 회원가입 - 계정정보
    @GetMapping(value = {"/certify/signUpForm1", "/certify/signUpForm1/{idx}"})
    public String signUpForm(@AuthDoctor Doctor doctor, @PathVariable(required = false) Integer idx, Model model) {
        if (doctor == null) { // 로그인 X
            if(idx != null) { // 수정
                model.addAttribute("idx", idx);

                // 의사 정보
                Map<String, Object> doctorData = doctorService.getDoctorInfo(idx);
                model.addAttribute("data", doctorData);
                int hospitalIdx = Integer.parseInt(doctorData.get("hospitalIdx").toString());
                if(hospitalIdx != 0) {
                    model.addAttribute("hospitalIdx", hospitalIdx);
                }
            }
        } else {
            return "redirect:/hospital/remoteList";
        }

        return "hospital/sign_up_form1";
    }

    // 의사 회원가입 1(계정정보), 2(병원정보), 3(진료시간) 액션 (등록/수정)
    @PostMapping("/certify/signUpFormAction")
    @ResponseBody
    public String signUpFormAction(Doctor doctor, Hospital hospital, MultipartHttpServletRequest request) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);
        obj.addProperty("errMsg", "");

        // 추가/수정
        int idx = doctorService.doctorFormAction(doctor, hospital, request);
        if (idx != 0) {
            obj.addProperty("result", true);
            obj.addProperty("idx", idx);
        }

        return obj.toString();
    }

    // 회원가입 - 병원정보
    @GetMapping("/certify/signUpForm2/{idx}")
    public String signUpForm2(@AuthDoctor Doctor doctor, @PathVariable Integer idx, Model model) {
        if (doctor == null) {
            if(idx != null) {
                // TODO: 개발완료 시 운영키로 변경 필요
                model.addAttribute("kakao_js_key", ApiKey.KAKAO_JS_KEY);
                model.addAttribute("subjectCodeList", Constants.HOSPITAL_SUBJECT_CODE); // 진료과목코드
                model.addAttribute("idx", idx);
                model.addAttribute("errMsg", "");

                // 의사정보
                Map<String, Object> doctorData = doctorService.getDoctorInfo(idx);
                int hospitalIdx = Integer.parseInt(doctorData.get("hospitalIdx").toString());
                if(hospitalIdx != 0) {
                    // 병원정보
                    Map<String, Object> hospitalData = doctorService.getHospitalInfo(hospitalIdx);
                    model.addAttribute("data", hospitalData);
                    model.addAttribute("hospitalIdx", hospitalIdx);
                }
            } else {
                model.addAttribute("errMsg", "오류가 발생하였습니다.");
            }
        }
        else {
            return "redirect:/hospital/remoteList";
        }

        return "hospital/sign_up_form2";
    }

    // 회원가입 - 병원영업 및 진료시간
    @GetMapping("/certify/signUpForm3/{idx}")
    public String signUpForm3(@AuthDoctor Doctor doctor, @PathVariable Integer idx, Model model) {
        if (doctor == null) {
            if(idx != null) {
                model.addAttribute("idx", idx);
                model.addAttribute("errMsg", "");

                // 의사정보
                Map<String, Object> doctorData = doctorService.getDoctorInfo(idx);
                int hospitalIdx = Integer.parseInt(doctorData.get("hospitalIdx").toString());
                if(hospitalIdx != 0) {
                    // 병원정보
                    Map<String, Object> hospitalData = doctorService.getHospitalInfo(hospitalIdx);
                    model.addAttribute("medicalTimeList", Constants.MEDICAL_TIME); // 예약진료시간
                    model.addAttribute("data", hospitalData);
                    model.addAttribute("hospitalIdx", hospitalIdx);
                } else {
                    model.addAttribute("errMsg", "오류가 발생하였습니다.");
                }
            } else {
                model.addAttribute("errMsg", "오류가 발생하였습니다.");
            }
        }
        else {
            return "redirect:/hospital/remoteList";
        }

        return "hospital/sign_up_form3";
    }

    // 회원가입 - 정산정보
    @GetMapping("/certify/signUpForm4/{idx}")
    public String signUpForm4(@AuthDoctor Doctor doctor, @PathVariable Integer idx, Model model) {
        if (doctor == null) {
            if(idx != null) {
                model.addAttribute("bankCodeList", Constants.BANK_CODE);
                model.addAttribute("idx", idx);
                model.addAttribute("errMsg", "");

                // 의사정보
                Map<String, Object> doctorData = doctorService.getDoctorInfo(idx);
                int hospitalIdx = Integer.parseInt(doctorData.get("hospitalIdx").toString());
                if(hospitalIdx != 0) {
                    // 병원정보
                    Map<String, Object> hospitalData = doctorService.getHospitalInfo(hospitalIdx);
                    model.addAttribute("data", hospitalData);
                    model.addAttribute("hospitalIdx", hospitalIdx);
                } else {
                    model.addAttribute("errMsg", "오류가 발생하였습니다.");
                }
            } else {
                model.addAttribute("errMsg", "오류가 발생하였습니다.");
            }
        }
        else {
            return "redirect:/hospital/remoteList";
        }

        return "hospital/sign_up_form4";
    }

    // 회원가입 완료
    @PostMapping("/certify/signUpComplete")
    @ResponseBody
    public String signUpComplete(MultipartHttpServletRequest request) {
        JsonObject obj = new JsonObject();
        obj.addProperty("result", false);
        obj.addProperty("errMsg", "");

        int chk = doctorService.signUpComplete(request);
        if (chk != 0) {
            obj.addProperty("result", true);
        } else {
            obj.addProperty("errMsg", "오류가 발생하였습니다.");
        }

        return obj.toString();
    }

    // 아이디 중복체크
    @PostMapping("/certify/idCheck")
    @ResponseBody
    public int idCheck(@RequestParam("id") String id) {
        return doctorService.idCheck(id);
    }
}