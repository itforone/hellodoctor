package com.hellodoctor.controller.hospital;

import com.hellodoctor.common.AuthDoctor;
import com.hellodoctor.common.Paging;
import com.hellodoctor.model.hospital.Doctor;
import com.hellodoctor.service.hospital.HTreatService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Map;

/**
 * 의사-진료내역 관련 controller
 */

@Controller
@RequestMapping("/hospital")
@Slf4j
public class HTreatController {

    @Autowired
    HTreatService hTreatService;

    // 진료내역
    @GetMapping("/treatList")
    public String treatList(@AuthDoctor Doctor doctor, Model model) {
        if (doctor == null) {
            model.addAttribute("isLogout", true);
        } else {
            // 현재 월의 마지막날짜 구하기
            LocalDate date = LocalDate.now();
            String today = date.toString();
            String year = today.substring(0,4);
            String month = today.substring(5,7);
            Calendar cal = Calendar.getInstance();
            cal.set(Integer.parseInt(year),Integer.parseInt(month)-1,1);
            String lastDay = Integer.toString(cal.getActualMaximum(Calendar.DAY_OF_MONTH));
            model.addAttribute("stDate", year+"-"+month+"-01");
            model.addAttribute("edDate", year+"-"+month+"-"+lastDay);
        }

        return "hospital/treat_list";
    }

    // 진료내역 목록
    @PostMapping("/ajaxTreatList")
    public String getRemoteList(@AuthDoctor Doctor doctor, Model model, @RequestParam(value="page", required=false, defaultValue="1") int page, HttpServletRequest request) {
        if (doctor == null) {
            model.addAttribute("isLogout", true);
        } else {
            // 전체 목록 수
            Map<String, String> countMap = hTreatService.getTreatListCount(doctor, request);
            String pageUrl = "./treatList/page=";
            int listTotalCnt = Integer.parseInt(String.valueOf(countMap.get("listTotalCnt")));
            Paging paging = new Paging(pageUrl, listTotalCnt, 15, page, 10);
            model.addAttribute("listCnt", countMap); // 상태별 카운트
            model.addAttribute("paging", paging); // 페이징

            // 목록
            Map<String, Object> listMap = hTreatService.getTreatList(doctor, request, paging.getStartRecord(), paging.getEndRecord());
            model.addAttribute("listData", listMap.get("listData"));
        }

        return "hospital/treat_list_info";
    }
}