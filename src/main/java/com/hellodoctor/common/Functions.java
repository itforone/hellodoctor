package com.hellodoctor.common;

import com.hellodoctor.mapper.app.RemoteMapper;
import com.hellodoctor.model.app.HospitalHour;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.thymeleaf.util.ArrayUtils;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.TextStyle;
import java.util.*;

/* 공통함수 */
@Slf4j
public class Functions {

    /**
     * 날짜형식 변경
     * @param dateStr : YYYY-mm-dd HH:mm:ss
     * @param changeFormat : 변경하고 싶은 포맷 (ex. YYYY-mm-dd)
     */
    public static final String ChangeDateFormat(String dateStr, String changeFormat) {
        String str = dateStr;

        try {
            // 문자열 -> Date로 변환
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = sdf.parse(dateStr);

            // Date -> 원하는 포맷으로 변경
            SimpleDateFormat sdfChange = new SimpleDateFormat(changeFormat);
            str = sdfChange.format(date);

        } catch (ParseException e) {
            log.info("ChangeDateFormat() error=" + e.toString());
        }

        return str;
    }

    /**
     * 랜덤문자열 (대소문자 알파벳 + 숫자)
     * @param maxLength : 랜덤문자열 길이
     */
    public static String randomString(int maxLength) {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        Random random = new Random();

        String str = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(maxLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

        return str;
    }

    /**
     * 유닉스 타임스탬프
     * (2022-06-24 16:24:42 = 타임스탬프 1656055482128)
     */
    public static String getTimeStamp() {
        Timestamp tmStamp = Timestamp.valueOf(LocalDateTime.now());
        String messageID = String.format("%d" , tmStamp.getTime());

        return messageID;
    }

    /**
     * 연락처 형식 변경
     * @param hp : 01012345678
     * (01012345678 ==> 010-1234-5678)
     */
    public static String getHyphenHp(String hp) {
        String regEx = "(\\d{3})(\\d{3,4})(\\d{4})";
        return hp.replaceAll(regEx, "$1-$2-$3");
    }

    /**
     * 특정 자릿수만큼 숫자 앞 0으로 채우기
     * @param num : 자릿수 (4)
     * @param data: 포맷할 데이터 (9), 문자열이면 Integer 타입으로 변경 필요
     * 9 ==> 0009
     */
    public static String addZero(int num, int data) {
        return String.format("%0"+num+"d", data);
    }

    /**
     * 주민등록번호로 성별 조회
     * @param rrNo : 주민등록번호 (999999-9999999)
     */
    public static String getGenderByRrno(String rrNo) {
        // 성별구분
        String lastRrno = rrNo.substring(rrNo.lastIndexOf("-") + 1);
        return (lastRrno.substring(0, 1).equals("1"))? "M" : "F"; // 남:여
    }

    /**
     * 검색필터 현재일로부터 n개월까지 종료일 추출
     * @param month : 현재일로부터 n개월
     */
    public static Map<String, String> getSearchDateFilter(int month) {
        String todayDate = "";      // 2022-07-01
        String beforeDate = "";     // 2022-06-01 (ex.1개월전)
        LocalDateTime nowDate = LocalDateTime.now();
        LocalDate today = nowDate.toLocalDate();
        todayDate = today.toString() + " 23:59:59";

        // 0: '전체보기'
        if (month > 0) {
            LocalDateTime beforeDt = nowDate.minusMonths(month); // 현재일로부터 n개월 전
            LocalDate beforeDay = beforeDt.toLocalDate();
            beforeDate = beforeDay.toString();
        }

        Map<String, String> result = new HashMap<>();
        result.put("todayDate", todayDate);
        result.put("beforeDate", beforeDate);

        return result;
    }

    /**
     * 인증번호 - 문자박스
     * @param length : 자릿수 (4 = 4자리)
     */
    public static String issueCertNo(int length) {
        String certNo = ""; // 인증번호 n자리 생성
        Random random = new Random();
        for (int i=0; i<length; i++) {
            certNo += random.nextInt(9);
        }

        // TODO: 문자발송 추가


        return certNo;
    }

    /**
     * 병원 진료시간
     * 평일or요일/공휴일/점심
     * @param eachSetYn : Y=요일별 개별 설정, N=평일 공통
     */
    public static Map<String, String> getHospitalHours(List<HospitalHour> searchHours, String eachSetYn, String restStartHour, String restEndHour) {
        Map<String, String> hours = new LinkedHashMap<>();

        for (int i=0; i<searchHours.size(); i++) {
            HospitalHour obj = searchHours.get(i);

            int _startHour = obj.getStartHour();
            int _endHour = obj.getEndHour();
            String _startHourFmt = _startHour<1200? Functions.addZero(4, _startHour) : String.valueOf(_startHour);
            String _endHourFmt = _startHour<1200? Functions.addZero(4, _endHour) : String.valueOf(_endHour);
            // 09:00~18:00
            String _hourStr = _startHourFmt.substring(0,2) + ":" + _startHourFmt.substring(2,4);
            _hourStr += " - " + _endHourFmt.substring(0,2) + ":" + _endHourFmt.substring(2,4);

            String yoilNum = obj.getYoil();
            String yoilStr = "";
            if (eachSetYn.equals("N")) { // 요일별 시간설정 아님 (평일, 토요일, 일요일, 공휴일 로만 설정)
                String[] weekdayCheck = {"2","3","4","5"};
                if (ArrayUtils.contains(weekdayCheck, yoilNum)) continue; // 화~금까진 패스
                else if (yoilNum.equals("1")) yoilNum = "0"; // 월 = 평일
            }

            switch (yoilNum) {
                case "0" : yoilStr = "평　일"; break;
                case "H" : yoilStr = "공휴일"; break;
                default: yoilStr = Constants.COMMON_YOIL.get(yoilNum) + "요일";
            }

            hours.put(yoilStr, _hourStr);
        }
        // 점심시간
        if (!isNull(restStartHour) && !isNull(restEndHour)) { //if (!restStartHour.isEmpty() && !restEndHour.isEmpty()) {
            String _startHourFmt = restStartHour;
            String _endHourFmt = restEndHour;
            // 09:00~18:00
            String _hourStr = _startHourFmt.substring(0,2) + ":" + _startHourFmt.substring(2,4);
            _hourStr += " - " + _endHourFmt.substring(0,2) + ":" + _endHourFmt.substring(2,4);
            hours.put("점　심", _hourStr);
        }
        return hours;
    }
    /**
     * 병원 진료시간 > 오늘 종료시간
     * @return 18:00
     */
    public static String getHospitalEndHour(List<HospitalHour> searchHours, String todayNum) {
        String todayEndHour = "";

        for (int i=0; i<searchHours.size(); i++) {
            HospitalHour obj = searchHours.get(i);

            int _startHour = obj.getStartHour();
            int _endHour = obj.getEndHour();
            // String _startHourFmt = _startHour<1200? Functions.addZero(4, _startHour) : String.valueOf(_startHour);
            String _endHourFmt = _startHour<1200? Functions.addZero(4, _endHour) : String.valueOf(_endHour);
            // 09:00~18:00
            // String _hourStr = _startHourFmt.substring(0,2) + ":" + _startHourFmt.substring(2,4);
            // _hourStr += " - " + _endHourFmt.substring(0,2) + ":" + _endHourFmt.substring(2,4);

            // 오늘 진료종료시간
            if(obj.getYoil().equals(todayNum)) {
                // todayEndHour = _endHourFmt;
                // map.put("todayEndHour", _hourStr.split(" - ")[1]);
                todayEndHour = _endHourFmt.substring(0,2) + ":" + _endHourFmt.substring(2,4);
            }
        }

        return todayEndHour;
    }



    /**
     * 병원 비급여
     */
    public static Map<String, String> getHospitalNonReimbursement(String nonReimbStr) {
        Map<String, String> map = new HashMap<>();
        if (!isNull(nonReimbStr)) {
            try {
                JSONArray jsonArray = new JSONArray(nonReimbStr);
                DecimalFormat decFormat = new DecimalFormat("###,###");

                for (int i=0; i<jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    map.put(jsonObject.get("subject").toString(), decFormat.format(jsonObject.get("price")));
                }

            } catch (Exception e) {
                log.info("JSONParser Exception" + e);
            }
        }
        return map;
    }

    /**
     * 병원진료과목 string 출력
     * (출력포맷 : 산부인과, 신경외과, 성형외과)
     */
    public static String getHospitalSubjectNames(String hospitalSubjCode) {
        String subjectNames = "";
        String[] subjectCodeSplit = hospitalSubjCode.split(",");
        for (String code : subjectCodeSplit) {
            int cd = Integer.parseInt(code);
            String name = Constants.HOSPITAL_SUBJECT_CODE.get(cd);
            if (subjectNames.isEmpty()) subjectNames = name;
            else { // 중복제거
                if (subjectNames.indexOf(name) == -1) subjectNames += "," + name;
            }
        }
        return subjectNames;
    }

    /**
     * 환자질환 ↔ 병원진료과목 매칭하여 질환에 해당하는 진료과목 추출
     * @param medicalSubjCode : 환자 질환 코드
     * @param hospitalSubjCode : 병원진료과목 (,구분)
     *
     * ex. 병원진료과목 :내과(A-1), 피부과(A-2) / 환자질환 : 피부(A-2)
     *     => 출력포맷 : 피부과
     */
    public static String getSubjectCodeMatching(String medicalSubjCode, String hospitalSubjCode) {
        String subjectNames = "";
        String[] subjectCodeSplit = hospitalSubjCode.split(",");
        for (String code : subjectCodeSplit) {
            int cd = Integer.parseInt(code);
            // 환자질환에 속하는 진료과목인지?
            if (Constants.HOSPITAL_MATCHING_CODE.get(cd).contains(medicalSubjCode)) {
                String name = Constants.HOSPITAL_SUBJECT_CODE.get(cd);
                if (subjectNames.isEmpty()) subjectNames = name;
                else { // 중복제거
                    if (subjectNames.indexOf(name) == -1) subjectNames += "," + name;
                }
            }
        }

        return subjectNames;
    }

    /**
     * 이노페이 카드등록시 고유아이디 생성
     */
    public static String createIPayUserId(int mbIdx) {
        return "helloDoctor" + addZero(6, mbIdx);
    }

    /**
     * 이노페이 승인일시
     * 220725142104 -> 2022.07.25(월) 14:21:04
     */
    public static String paymentAuthDate(String authDate) {
        if (authDate == null) return "";

        String date = "20" + authDate;
        String dateFormat = "";
        dateFormat += date.substring(0,4) + "." + date.substring(4,6) + "." + date.substring(6,8);

        LocalDate ymd = LocalDate.of(Integer.parseInt(date.substring(0,4)), Integer.parseInt(date.substring(4,6)), Integer.parseInt(date.substring(6,8)));
        DayOfWeek dayOfWeek = ymd.getDayOfWeek();
        dateFormat += "(" + dayOfWeek.getDisplayName(TextStyle.NARROW, Locale.KOREAN) + ") ";
        dateFormat += date.substring(8,10) + ":" + date.substring(10,12) + ":" + date.substring(12,14);

        return dateFormat;
    }
    /**
     * 이노페이 결제구분
     * 신한(일시불)
     */
    public static String paymentAuthType(String cardName, String cardQuota) {
        String authType = (cardName != null)? cardName : "";
        if (cardQuota != null && !cardQuota.equals(""))
            authType += (cardQuota.equals("00"))? "(일시불)" : "("+cardQuota+")";

        return authType;
    }

    /**
     * NULL / 공백 / 리스트나 맵에 데이터가 있는지 체크
     * NULL이거나 공백이면 true
     */
    public static boolean isNull(Object obj) {
        if(obj == null) {
            return true;
        } else if((obj instanceof String) && (((String) obj).length() == 0)) {
            return true;
        } else if(obj instanceof  Map) {
            return ((Map<?,?>) obj).isEmpty();
        } else if(obj instanceof  List) {
            return ((List<?>) obj).isEmpty();
        } else if(obj instanceof Object[]) {
            return (((Object[]) obj).length == 0);
        }

        return false;
    }

    /**
     * MySql varchar 크기만큼 문자열 자르기 (한글 3byte)
     * @param msg : 문자열
     * @param maxSize : 크기
     * @return
     */
    public static String subVarcharStr(String msg, int maxSize) {
        String rtnVal = "";

        if (ObjectUtils.isEmpty(msg))
            return rtnVal;

        try {
            StringBuilder sbStr = new StringBuilder(maxSize);
            int nCnt = 0;
            for(char ch: msg.toCharArray()){
                if(++nCnt > maxSize) break;
                sbStr.append(ch);
            }
            rtnVal = sbStr.toString();
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
            return null;
        }

        return rtnVal;
    }

    /**
     * 만나이 계산
     * @param birthDay : 2022-01-01 또는 20220101
     * @return int nn
     */
    public static int getAgeByBirthday(String birthDay) {
        String birth = birthDay.replaceAll("-", "");

        // 년,월,일 자르기
        int year = Integer.parseInt(birth.substring(0, 4));
        int month = Integer.parseInt(birth.substring(4, 6));
        int day = Integer.parseInt(birth.substring(6, 8));
        Calendar current = Calendar.getInstance();

        // 현재년, 월, 일 get
        int currentYear = current.get(Calendar.YEAR);
        int currentMonth = current.get(Calendar.MONTH) + 1;
        int currentDay = current.get(Calendar.DAY_OF_MONTH);
        int age = currentYear - year;

        // 만나이
        if (month * 100 + day > currentMonth * 100 + currentDay) {
            age--;
        }

        return age;
    }

    /**
     * 회원번호 생성
     * hospital + 날짜(년(yy)월(mm)) + 난수6자리
     * Ex) hospital + 2022.07.08 + 난수 = h220722123456
     */
    public static String createMemberNo() {
        String number = "h";

        LocalDate now = LocalDate.now();
        String today = now.toString();
        String todayFormat = today.substring(2,10).replace("-", ""); // 220101
        number += todayFormat;

        Random random = new Random();
        for (int i=0; i<6; i++) {
            number += random.nextInt(9); // 난수6자리
        }

        return number;
    }
}
