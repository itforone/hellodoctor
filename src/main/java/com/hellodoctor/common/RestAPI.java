package com.hellodoctor.common;

import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Slf4j
public class RestAPI {
    /**
     * POST 요청
     * Content-Type : json
     * @param url : api server
     * @param header : jsonObject
     * @param body : jsonObject
     * @return
     */
    public static Map<String, Object> requestPost(String url, JSONObject header, JSONObject body) {
        Map<String, Object> resultMap = new HashMap<>();

        try {
            RestTemplate restTemplate = new RestTemplate();

            // header set (host, content-type)
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            // 헤더 추가 시
            if(header.size() != 0) {
                Iterator<String> iter = header.keySet().iterator();
                while(iter.hasNext()) {
                    String key = iter.next();
                    httpHeaders.add(key, String.valueOf(header.get(key)));
                }
            }

            // body set
            // -- map 사용시 400 오류
            // MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
            // body.add("mid", Constants.IPAY_REG_CARD_MID);
            // -- json body 예시
            // JSONObject body = new JSONObject();
            // body.put("mid", Constants.IPAY_REG_CARD_MID);
            // body.put("arsUseYn", "N");

            // request message
            HttpEntity<?> requestMessage = new HttpEntity<>(body.toString(), httpHeaders);

            // api 요청
            HttpEntity<String> response = restTemplate.postForEntity(url, requestMessage, String.class);

            // api 응답 파싱
            String responseBody = response.getBody();
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(responseBody.toString()); // string to json

            resultMap.put("connectResult", true); // 서버통신 여부
            resultMap.put("responseJson", jsonObject); // 응답결과 json

        } catch (Exception e) {
            resultMap.put("connectResult", false);
            resultMap.put("errMsg", e.toString());
        }

        return resultMap;
    }
}
