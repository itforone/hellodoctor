package com.hellodoctor.common;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/* 상수 */
public class Constants {
    // 기본 위도경도
    public static final String EMPTY_LAT = "37.4985723";
    public static final String EMPTY_LNG = "127.0280428";

    // 요일 (병원)
    public static final Map<String, String> COMMON_YOIL = new HashMap<>();
    static {
        COMMON_YOIL.put("1", "월");
        COMMON_YOIL.put("2", "화");
        COMMON_YOIL.put("3", "수");
        COMMON_YOIL.put("4", "목");
        COMMON_YOIL.put("5", "금");
        COMMON_YOIL.put("6", "토");
        COMMON_YOIL.put("7", "일");
        COMMON_YOIL.put("H", "공휴일");
    }

    // 로그인 에러메시지
    public static final Map<Integer, String> MEMBER_LOGIN_ERR_MSG = new HashMap<>();
    static {
        MEMBER_LOGIN_ERR_MSG.put(1, "아이디 또는 비밀번호가 올바르지 않습니다.");
        MEMBER_LOGIN_ERR_MSG.put(2, "시스템 문제로 인증처리에 실패했습니다. 잠시 후 다시 시도해 주세요.");
        MEMBER_LOGIN_ERR_MSG.put(3, "인증 요청이 거부되었습니다. 관리자에게 문의하세요."); //계정 없음
    }

    // 나의가족관계
    public static final Map<Integer, String> MEMBER_FAMILY_REL = new HashMap<>();
    static {
        MEMBER_FAMILY_REL.put(1, "부모");
        MEMBER_FAMILY_REL.put(2, "배우자");
        MEMBER_FAMILY_REL.put(3, "자녀");
    }

    // 진료신청 질환명
    public static final Map<Integer, String> MEDICAL_REQUEST_SUBJECT = new HashMap<>();
    static {
        MEDICAL_REQUEST_SUBJECT.put(1, "일반");   // A-1 : 내과,신경과,외과,정형외과,신경외과,마취통증의학과,산부인과,소아청소년과,이비인후과,재활의학과,가정의학과
        MEDICAL_REQUEST_SUBJECT.put(2, "피부");   // A-2 : 피부과,비뇨기과,비뇨의학과
        MEDICAL_REQUEST_SUBJECT.put(3, "호흡기");  // A-3 : 내과,소아청소년과,이비인후과,가정의학과
        MEDICAL_REQUEST_SUBJECT.put(4, "여성");   // A-4 : 산부인과,가정의학과
        MEDICAL_REQUEST_SUBJECT.put(5, "비뇨기");  // A-5 : 내과,비뇨기과,비뇨의학과
        MEDICAL_REQUEST_SUBJECT.put(6, "만성질환"); // A-6 : 내과,신경과,외과,신경외과,흉부외과,재활의학과,가정의학과
    }

    // 진료신청 질환상세
    public static final Map<Integer, String> MEDICAL_REQUEST_SUBJECT_DESC = new HashMap<>();
    static {
        MEDICAL_REQUEST_SUBJECT_DESC.put(1, "두통 / 복통 / 몸살 / 열");
        MEDICAL_REQUEST_SUBJECT_DESC.put(2, "여드름 / 알러지 / 탈모");
        MEDICAL_REQUEST_SUBJECT_DESC.put(3, "천식 / 비염 / 감기");
        MEDICAL_REQUEST_SUBJECT_DESC.put(4, "사후피임 / 생리통 / 질염");
        MEDICAL_REQUEST_SUBJECT_DESC.put(5, "비뇨기염증질환 / 전립선염 / 방광염");
        MEDICAL_REQUEST_SUBJECT_DESC.put(6, "고혈압 / 당뇨 / 고지혈증");
    }

    // 진료신청 질환코드
    public static final Map<Integer, String> MEDICAL_REQUEST_SUBJECT_CODE = new HashMap<>();
    static {
        MEDICAL_REQUEST_SUBJECT_CODE.put(1, "G");
        MEDICAL_REQUEST_SUBJECT_CODE.put(2, "D");
        MEDICAL_REQUEST_SUBJECT_CODE.put(3, "R");
        MEDICAL_REQUEST_SUBJECT_CODE.put(4, "O");
        MEDICAL_REQUEST_SUBJECT_CODE.put(5, "U");
        MEDICAL_REQUEST_SUBJECT_CODE.put(6, "C");
    }

    // 병원 진료과목
    public static final Map<Integer, String> HOSPITAL_SUBJECT_CODE = new HashMap<>();
    static {
        HOSPITAL_SUBJECT_CODE.put(1, "응급");           // 매칭질환없음
        HOSPITAL_SUBJECT_CODE.put(2, "가정의학과");      // A-1,A-3,A-4,A-6
        HOSPITAL_SUBJECT_CODE.put(3, "정신과");         // 매칭질환없음
        HOSPITAL_SUBJECT_CODE.put(4, "피부과");         // A-2
        HOSPITAL_SUBJECT_CODE.put(5, "산부인과");       // A-1,A-4
        HOSPITAL_SUBJECT_CODE.put(6, "정형외과");       // A-1
        HOSPITAL_SUBJECT_CODE.put(7, "치과");          // 매칭질환없음
        HOSPITAL_SUBJECT_CODE.put(8, "내과");          // A-1,A-3,A-5,A-6
        HOSPITAL_SUBJECT_CODE.put(9, "비뇨기과");        // A-5,A-2
        HOSPITAL_SUBJECT_CODE.put(10, "재활의학과");     // A-1,A-6
        HOSPITAL_SUBJECT_CODE.put(11, "신경외과");       // A-1,A-6
        HOSPITAL_SUBJECT_CODE.put(12, "통증의학과");     // A-1 (마취통증의학과)
        HOSPITAL_SUBJECT_CODE.put(13, "소아청소년과");    // A-1,A-3
        HOSPITAL_SUBJECT_CODE.put(14, "성형외과");       // 매칭질환없음
        HOSPITAL_SUBJECT_CODE.put(15, "영상의학과");     // 매칭질환없음
        HOSPITAL_SUBJECT_CODE.put(16, "마취통증의학과");   // A-1
        HOSPITAL_SUBJECT_CODE.put(17, "이비인후과");      // A-1,A-3
        HOSPITAL_SUBJECT_CODE.put(18, "한방의학과");     // 매칭질환없음
        HOSPITAL_SUBJECT_CODE.put(19, "안과");          // 매칭질환없음
    }

    // 병원진료과목 - 환자질환 매칭
    public static final Map<Integer, String> HOSPITAL_MATCHING_CODE = new HashMap<>();
    static {
        HOSPITAL_MATCHING_CODE.put(1, "7");           // 응급
        HOSPITAL_MATCHING_CODE.put(2, "1,3,4,6");      // 가정의학과
        HOSPITAL_MATCHING_CODE.put(3, "7");         // 정신과
        HOSPITAL_MATCHING_CODE.put(4, "2");         // 피부과
        HOSPITAL_MATCHING_CODE.put(5, "1,4");       // 산부인과
        HOSPITAL_MATCHING_CODE.put(6, "1");       // 정형외과
        HOSPITAL_MATCHING_CODE.put(7, "7");          // 치과
        HOSPITAL_MATCHING_CODE.put(8, "1,3,5,6");          // 내과
        HOSPITAL_MATCHING_CODE.put(9, "5,2");        // 비뇨기과
        HOSPITAL_MATCHING_CODE.put(10, "1,6");     // 재활의학과
        HOSPITAL_MATCHING_CODE.put(11, "1,6");       // 신경외과
        HOSPITAL_MATCHING_CODE.put(12, "1");     // 통증의학과
        HOSPITAL_MATCHING_CODE.put(13, "1,3");    // 소아청소년과
        HOSPITAL_MATCHING_CODE.put(14, "7");       // 성형외과
        HOSPITAL_MATCHING_CODE.put(15, "7");     // 영상의학과
        HOSPITAL_MATCHING_CODE.put(16, "1");   // 마취통증의학과
        HOSPITAL_MATCHING_CODE.put(17, "1,3");      // 이비인후과
        HOSPITAL_MATCHING_CODE.put(18, "7");     // 한방의학과
        HOSPITAL_MATCHING_CODE.put(19, "7");          // 안과
    }

    // 앱 서비스문의 - 문의유형
    public static final Map<Integer, String> CS_CATEGORY = new HashMap<>();
    static {
        CS_CATEGORY.put(1, "진료문의");
        CS_CATEGORY.put(2, "앱 불편사항");
        CS_CATEGORY.put(3, "기타문의");
    }

    // 웹 서비스문의 - 문의유형
    public static final Map<Integer, String> CS_WEB_CATEGORY = new HashMap<>();
    static {
        CS_WEB_CATEGORY.put(1, "환자CS");
        CS_WEB_CATEGORY.put(2, "서비스");
        CS_WEB_CATEGORY.put(3, "결제/정산");
        CS_WEB_CATEGORY.put(4, "오류/버그");
        CS_WEB_CATEGORY.put(5, "회원탈퇴");
        CS_WEB_CATEGORY.put(6, "기타문의");
    }


    // 원격진료 리뷰 만족도 체크 내용
    public static final Map<Integer, String> REVIEW_CHECK_ITEM_CODE = new HashMap<>();
    static {
        REVIEW_CHECK_ITEM_CODE.put(1, "증상을 쉽게 설명해 주셨어요.");
        REVIEW_CHECK_ITEM_CODE.put(2, "차수에 대해 걱정해 주셨어요.");
        REVIEW_CHECK_ITEM_CODE.put(3, "정확하게 처방해 주셨어요.");
        REVIEW_CHECK_ITEM_CODE.put(4, "꼼꼼하게 진단해 주셨어요.");
        REVIEW_CHECK_ITEM_CODE.put(5, "친절하게 알려주셨어요.");
        REVIEW_CHECK_ITEM_CODE.put(6, "진료 후 문의도 잘 받아주셨어요.");
    }

    // 카드사 코드 (이노페이 부록 9p)
    public static final Map<Integer, String> CARD_CODE = new HashMap<>();
    static {
        CARD_CODE.put(1, "비씨");
        CARD_CODE.put(2, "국민");
        CARD_CODE.put(3, "외환");
        CARD_CODE.put(4, "삼성");
        CARD_CODE.put(6, "신한");
        CARD_CODE.put(7, "현대");
        CARD_CODE.put(8, "롯데");
        CARD_CODE.put(9, "한미");
        CARD_CODE.put(10, "신세계한미");
        CARD_CODE.put(11, "시티");
        CARD_CODE.put(12, "농협");
        CARD_CODE.put(13, "수협");
        CARD_CODE.put(14, "평화");
        CARD_CODE.put(15, "우리");
        CARD_CODE.put(16, "하나");
        CARD_CODE.put(17, "동남(주택)");
        CARD_CODE.put(18, "주택");
        CARD_CODE.put(19, "조흥(강원)");
        CARD_CODE.put(20, "축협(농협)");
        CARD_CODE.put(21, "광주");
        CARD_CODE.put(22, "전북");
        CARD_CODE.put(23, "제주");
        CARD_CODE.put(24, "산은");
        CARD_CODE.put(25, "해외비자");
        CARD_CODE.put(26, "해외마스터");
        CARD_CODE.put(27, "해외다이너스");
        CARD_CODE.put(28, "해외AMX");
        CARD_CODE.put(29, "해외JCB");
    }


    // 은행 코드
    public static final Map<String, String> BANK_CODE = new LinkedHashMap<>();
    static {
        BANK_CODE.put("023", "SC제일은행");
        BANK_CODE.put("039", "경남은행");
        BANK_CODE.put("034", "광주은행");
        BANK_CODE.put("004", "국민은행");
        BANK_CODE.put("003", "기업은행");
        BANK_CODE.put("011", "농협");
        BANK_CODE.put("031", "대구은행");
        BANK_CODE.put("032", "부산은행");
        BANK_CODE.put("064", "산림조합중앙회");
        BANK_CODE.put("002", "산업은행");
        BANK_CODE.put("045", "새마을금고");
        BANK_CODE.put("007", "수협");
        BANK_CODE.put("088", "신한은행");
        BANK_CODE.put("048", "신협중앙회");
        BANK_CODE.put("020", "우리은행");
        BANK_CODE.put("071", "우체국");
        BANK_CODE.put("137", "전북은행");
        BANK_CODE.put("035", "제주은행");
        BANK_CODE.put("090", "카카오뱅크");
        BANK_CODE.put("089", "케이뱅크");
        BANK_CODE.put("092", "토스뱅크");
        BANK_CODE.put("081", "하나은행");
        BANK_CODE.put("027", "한국씨티은행");
    }

    // 진료소견 과거병력
    public static final Map<Integer, String> MEDICAL_OPINION_HISTORY_CHECK = new HashMap<>();
    static {
        MEDICAL_OPINION_HISTORY_CHECK.put(1, "고지혈증");
        MEDICAL_OPINION_HISTORY_CHECK.put(2, "간질환");
        MEDICAL_OPINION_HISTORY_CHECK.put(3, "당뇨병");
        MEDICAL_OPINION_HISTORY_CHECK.put(4, "심장질환");
        MEDICAL_OPINION_HISTORY_CHECK.put(5, "고혈압");
        MEDICAL_OPINION_HISTORY_CHECK.put(0, "직접입력");
    }

    // 병원-예약진료시간
    public static final Map<Integer, String> MEDICAL_TIME = new LinkedHashMap<>();
    static {
        MEDICAL_TIME.put(0, "00:00");
        MEDICAL_TIME.put(15, "00:15");
        MEDICAL_TIME.put(30, "00:30");
        MEDICAL_TIME.put(45, "00:45");
        MEDICAL_TIME.put(100, "01:00");
        MEDICAL_TIME.put(115, "01:15");
        MEDICAL_TIME.put(130, "01:30");
        MEDICAL_TIME.put(145, "01:45");
        MEDICAL_TIME.put(200, "02:00");
        MEDICAL_TIME.put(215, "02:15");
        MEDICAL_TIME.put(230, "02:30");
        MEDICAL_TIME.put(245, "02:45");
        MEDICAL_TIME.put(300, "03:00");
        MEDICAL_TIME.put(315, "03:15");
        MEDICAL_TIME.put(330, "03:30");
        MEDICAL_TIME.put(345, "03:45");
        MEDICAL_TIME.put(400, "04:00");
        MEDICAL_TIME.put(415, "04:15");
        MEDICAL_TIME.put(430, "04:30");
        MEDICAL_TIME.put(445, "04:45");
        MEDICAL_TIME.put(500, "05:00");
        MEDICAL_TIME.put(515, "05:15");
        MEDICAL_TIME.put(530, "05:30");
        MEDICAL_TIME.put(545, "05:45");
        MEDICAL_TIME.put(600, "05:00");
        MEDICAL_TIME.put(615, "05:15");
        MEDICAL_TIME.put(630, "05:30");
        MEDICAL_TIME.put(645, "05:45");
        MEDICAL_TIME.put(700, "07:00");
        MEDICAL_TIME.put(715, "07:15");
        MEDICAL_TIME.put(730, "07:30");
        MEDICAL_TIME.put(745, "07:45");
        MEDICAL_TIME.put(800, "08:00");
        MEDICAL_TIME.put(815, "08:15");
        MEDICAL_TIME.put(830, "08:30");
        MEDICAL_TIME.put(845, "08:45");
        MEDICAL_TIME.put(900, "09:00");
        MEDICAL_TIME.put(915, "09:15");
        MEDICAL_TIME.put(930, "09:30");
        MEDICAL_TIME.put(945, "09:45");
        MEDICAL_TIME.put(1000, "10:00");
        MEDICAL_TIME.put(1015, "10:15");
        MEDICAL_TIME.put(1030, "10:30");
        MEDICAL_TIME.put(1045, "10:45");
        MEDICAL_TIME.put(1100, "11:00");
        MEDICAL_TIME.put(1115, "11:15");
        MEDICAL_TIME.put(1130, "11:30");
        MEDICAL_TIME.put(1145, "11:45");
        MEDICAL_TIME.put(1200, "12:00");
        MEDICAL_TIME.put(1215, "12:15");
        MEDICAL_TIME.put(1230, "12:30");
        MEDICAL_TIME.put(1245, "12:45");
        MEDICAL_TIME.put(1300, "13:00");
        MEDICAL_TIME.put(1315, "13:15");
        MEDICAL_TIME.put(1330, "13:30");
        MEDICAL_TIME.put(1345, "13:45");
        MEDICAL_TIME.put(1400, "14:00");
        MEDICAL_TIME.put(1415, "14:15");
        MEDICAL_TIME.put(1430, "14:30");
        MEDICAL_TIME.put(1445, "14:45");
        MEDICAL_TIME.put(1500, "15:00");
        MEDICAL_TIME.put(1515, "15:15");
        MEDICAL_TIME.put(1530, "15:30");
        MEDICAL_TIME.put(1545, "15:45");
        MEDICAL_TIME.put(1600, "16:00");
        MEDICAL_TIME.put(1615, "16:15");
        MEDICAL_TIME.put(1630, "16:30");
        MEDICAL_TIME.put(1645, "16:45");
        MEDICAL_TIME.put(1700, "17:00");
        MEDICAL_TIME.put(1715, "17:15");
        MEDICAL_TIME.put(1730, "17:30");
        MEDICAL_TIME.put(1745, "17:45");
        MEDICAL_TIME.put(1800, "18:00");
        MEDICAL_TIME.put(1815, "18:15");
        MEDICAL_TIME.put(1830, "18:30");
        MEDICAL_TIME.put(1845, "18:45");
        MEDICAL_TIME.put(1900, "19:00");
        MEDICAL_TIME.put(1915, "19:15");
        MEDICAL_TIME.put(1930, "19:30");
        MEDICAL_TIME.put(2000, "20:00");
        MEDICAL_TIME.put(2015, "20:15");
        MEDICAL_TIME.put(2030, "20:30");
        MEDICAL_TIME.put(2045, "20:45");
        MEDICAL_TIME.put(2100, "21:00");
        MEDICAL_TIME.put(2115, "21:15");
        MEDICAL_TIME.put(2130, "21:30");
        MEDICAL_TIME.put(2145, "21:45");
        MEDICAL_TIME.put(2200, "22:00");
        MEDICAL_TIME.put(2215, "22:15");
        MEDICAL_TIME.put(2230, "22:30");
        MEDICAL_TIME.put(2245, "22:45");
        MEDICAL_TIME.put(2300, "23:00");
    }

    // 기본이미지(테스트이미지)
    public final static String NO_IMAGE = "/img/noimg.svg";

    // 공지사항 열람대상
    public static final Map<String, String> ADM_NOTICE_TARGET = new LinkedHashMap<>();
    static {
        ADM_NOTICE_TARGET.put("A", "회원");
        ADM_NOTICE_TARGET.put("H", "병원");
        ADM_NOTICE_TARGET.put("P", "약국");
    }

    // 공지사항 유형
    // TBC: 공지사항 정의 필요
    public static final Map<Integer, String> ADM_NOTICE_CATEGORY = new LinkedHashMap<>();
    static {
        ADM_NOTICE_CATEGORY.put(1, "일반");
        ADM_NOTICE_CATEGORY.put(2, "기타");
    }

    // 관리자 약관관리 목록
    public static final Map<String, String> SITE_TERMS_LIST = new LinkedHashMap<>();
    static {
        SITE_TERMS_LIST.put("6", "위치기반서비스 이용약관");
        SITE_TERMS_LIST.put("5", "마케팅 정보 활용 동의");
        SITE_TERMS_LIST.put("4", "고유식별정보수집 및 처리방침");
        SITE_TERMS_LIST.put("3", "개인민감정보(건강정보) 처리방침");
        SITE_TERMS_LIST.put("2", "개인정보 처리방침");
        SITE_TERMS_LIST.put("1", "서비스 이용약관");
    }

}