package com.hellodoctor.common;

import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * S3버킷에 object를 업로드하는 서비스
 *
 * 폴더생성
 * nCloudStorageUtils.createFolder("test");
 * 폴더조회
 * nCloudStorageUtils.searchObjectList("test");
 * 폴더삭제
 * nCloudStorageUtils.deleteObject("test/");
 * 파일삭제
 * nCloudStorageUtils.deleteObject("test/testFile.jpg");
 */

@Slf4j
@Service
@RequiredArgsConstructor
public class NCloudStorageUtils {
    @Value("${ncloud.bucketName}")
    private String bucketName;

    private final AmazonS3 amazonS3;

    /**
     * 폴더 생성
     * @param folderName : 폴더명
     */
    public void createFolder(String folderName) {
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentLength(0L);
        objectMetadata.setContentType("application/x-directory");
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, folderName+"/", new ByteArrayInputStream(new byte[0]), objectMetadata);

        try {
            amazonS3.putObject(putObjectRequest);
            log.info("폴더생성성공=" + folderName);
        } catch (Exception e) {
            log.info("폴더생성실패=" + e.toString());
        }
    }

    /**
     * Object 조회
     * @param folderName : 폴더명
     */
    public void searchObjectList(String folderName) { //String fileName
        // 폴더 경로
        String prefix = (!folderName.isEmpty())? folderName + "/" : "";
        //List<String> list = new ArrayList<>();

        try {
            ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
                    .withBucketName(bucketName)
                    .withPrefix(prefix)
                    .withDelimiter("/")
                    .withMaxKeys(300);

            ObjectListing objectListing = amazonS3.listObjects(listObjectsRequest);

            log.info("Folder List:");
            for (String commonPrefixes : objectListing.getCommonPrefixes()) {
                log.info("  -폴더명=" + commonPrefixes);
                // list.add
            }

            log.info("File List:");
            for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
                String uploadUrl = amazonS3.getUrl(bucketName, objectSummary.getKey()).toString();
                log.info("  -URL=" + uploadUrl);
                log.info("  -파일정보=" + objectSummary.toString());
            }
        } catch (AmazonS3Exception e) {
            log.info("AmazonS3Exception()=" + e.toString());
        } catch(SdkClientException e) {
            log.info("SdkClientException()=" + e.toString());
        }

        // return list;
    }

    /**
     * 파일 또는 폴더 삭제
     *
     * @param fileUrl : 전체경로/파일명
     * - 폴더삭제 : {도메인}/{폴더}/{폴더}/.../
     * - 파일삭제 : {도메인}/{폴더}/{폴더}/.../{파일명}
     */
    public int deleteObject(String fileUrl) {
        int result = 0;

        // String url = "https://hellodoctor-dev.kr.object.ncloudstorage.com/myFamily/1656056794278file1.jpg";
        String splitPath = fileUrl.substring(fileUrl.indexOf(".com/")+5);

        try {
            amazonS3.deleteObject(bucketName, splitPath);
            log.info("deleteObject=" + splitPath);
            result = 1;
        } catch(AmazonS3Exception e) {
            log.info("AmazonS3Exception=" + e.getErrorMessage());
        }

        return result;
    }

    /**
     * 파일 업로드
     *
     * @param folder : 폴더명
     * @param fileName : 파일명
     * @param file : input type=file
     */
    public String uploadFile(String folder, String fileName, MultipartFile file) {
        String path = folder + "/" + fileName;
        String uploadUrl = "";

        try {
            amazonS3.putObject(new PutObjectRequest(bucketName, path, file.getInputStream(), null)
                    .withCannedAcl(CannedAccessControlList.PublicRead));
            uploadUrl = amazonS3.getUrl(bucketName, path).toString();
            log.info("uploadFile()=성공/" + uploadUrl);
        } catch (Exception e) {
            log.info("uploadFile()=실패");
        }

        return uploadUrl; // 전체 url
    }


//
//    // 파일 삭제
//    public void deleteFile(String fileName) {
//        DeleteObjectRequest request = new DeleteObjectRequest(bucketName, fileName);
//        s3.deleteObject(request);
//    }
//
//    // 폴더 삭제 (폴더 내 모든파일 삭제됨)
//    public void removeFolder(String folderName){
//        ListObjectsV2Request listObjectsV2Request = new ListObjectsV2Request().withBucketName(bucketName).withPrefix(folderName+"/");
//        ListObjectsV2Result listObjectsV2Result = s3.listObjectsV2(listObjectsV2Request);
//        ListIterator<S3ObjectSummary> listIterator = listObjectsV2Result.getObjectSummaries().listIterator();
//
//        while (listIterator.hasNext()){
//            S3ObjectSummary objectSummary = listIterator.next();
//            DeleteObjectRequest request = new DeleteObjectRequest(bucketName,objectSummary.getKey());
//            s3.deleteObject(request);
//
//            log.info("Deleted " + objectSummary.getKey());
//        }
//    }
//
//    private String upload(File uploadFile, String folderName){
//        String fileName = folderName + "/" + uploadFile.getName();
//        String uploadImageUrl = putS3(uploadFile, fileName);
//
//        removeNewFile(uploadFile);
//        return uploadImageUrl;
//    }
//
//    // S3에 업로드
//    private String putS3(File uploadFile, String fileName){
//        s3.putObject(new PutObjectRequest(bucketName, fileName, uploadFile).withCannedAcl(CannedAccessControlList.PublicRead));
//        return s3.getUrl(bucketName, fileName).toString();
//    }
//
//    //임시로 생성된 new file을 삭제
//    private void removeNewFile(File targetFile){
//        if(targetFile.delete()){
//            log.info("File deleted.");
//        }else{
//            log.info("File doesn't deleted.");
//        }
//    }
//
//
//    //multipartFile을 File타입으로 변환해준다. (변환된 파일을 가지고 put해줄 것이다)
//    private Optional<File> convert(MultipartFile file) throws IOException {
//        // 파일명 겹치치 않기위해 currentTimeMillis() 추가하여 생성
//        File convertFile = new File(System.currentTimeMillis() + StringUtils.cleanPath(file.getOriginalFilename()));
//
//        if(convertFile.createNewFile()){
//            try(FileOutputStream fos = new FileOutputStream(convertFile)){
//                fos.write(file.getBytes());
//            }
//            return Optional.of(convertFile);
//        }
//        return Optional.empty();
//    }

}

