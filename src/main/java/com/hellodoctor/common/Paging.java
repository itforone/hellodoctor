package com.hellodoctor.common;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Paging {
    // 이동URL
    private String pageUrl;

    // 전체 글 개수
    private int totalCount;
    // 한페이지 노출 개수
    private int listRows;
    // 현재 페이지
    private int currentPage;
    // 전체 페이지
    private int totalPages;

    // 하단 페이징 블록 개수 ex. 10 = [1][2][3]...[10]
    private int listPageRows;
    // 전체블록 개수
    private int totalBlocks;
    // 현재블록
    private int currentBlock;

    // 현재블록 시작 페이지
    private int startPage;
    // 현재블록 끝 페이지
    private int endPage;

    // 쿼리용
    // limit 시작행
    private int startRecord;
    // limit 끝행
    private int endRecord;
    // 게시판 시작 글번호 (내림차순)
    private int listNo;


    // 생성자 (이동URL, 총글수, 한페이지 글수=기본15, 현재페이지, 하단블록수=기본10)
    public Paging(String pageUrl, int totalCount, int listRows, int page, int listPageRows) {
        this.pageUrl = pageUrl;

        this.totalCount = totalCount;
        this.listRows = listRows;
        this.currentPage = page;
        this.listPageRows = listPageRows;

        pageCalculator();
    }

    // 페이지 계산
    public void pageCalculator() {
        int _totalPages = 1;
        int _totalBlocks = 1;
        int _currentBlock = 1;
        int _startPage = 1;
        int _endPage = 1;
        int _listNo = 1;

        if (this.totalCount > 0) {
            // 페이지 계산
            // 1. 전체페이지
            _totalPages = (int) Math.ceil((double) this.totalCount/ (double) this.listRows);
            if (this.currentPage > _totalPages) this.currentPage = _totalPages;
            // 2. 전체블록
            _totalBlocks = (int) Math.ceil((double) _totalPages/ (double) this.listPageRows);
            // 3. 현재블록
            _currentBlock = (int) Math.ceil((double) this.currentPage/ (double) this.listPageRows);
            // 4. 현재블록 시작 페이지
            _startPage = (_currentBlock * this.listPageRows) - (this.listPageRows - 1);
            // 5. 현재블록 끝 페이지
            _endPage = (_currentBlock * this.listPageRows);
            if (_totalPages <= _endPage) _endPage = _totalPages;

            // 게시판 시작 글번호 (내름차순)
            _listNo = this.totalCount - (this.listRows * (this.currentPage - 1));
        }

        this.totalPages = _totalPages;
        this.totalBlocks = _totalBlocks;
        this.currentBlock = _currentBlock;
        this.startPage = _startPage;
        this.endPage = _endPage;
        this.listNo = _listNo;

        // ex. LIMIT 0, 15
        this.startRecord = (this.currentPage-1) * this.listRows;
        this.endRecord = this.listRows;
    }

}
