package com.hellodoctor.mapper.api;

import com.hellodoctor.model.api.PaymentCancelResult;
import com.hellodoctor.model.api.PaymentResult;
import com.hellodoctor.model.app.Member;
import com.hellodoctor.model.app.MemberCreditCard;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 이노페이 Mapper 인터페이스
 */

@Mapper
public interface IPayMapper {
    int insertMemberCreditCard(MemberCreditCard memberCreditCard); // 회원 결제카드 등록
    int inertPaymentResult(PaymentResult paymentResult); // 결제결과 등록
    int inertPaymentCancelResult(PaymentCancelResult paymentCancelResult); // 결제취소결과 등록
    int updateToCancelPaymentResult(String tid); // 등록된 결제결과 취소로 변경
    Member getMemberByIdx(int mbIdx); // 인덱스로 회원정보
    String getMemberBillKey(int cardIdx); // 진료비 결제전 회원 빌키 조회
    int completeMedical(int idx, int paymentIdx); // 진료비결제후 진료완료 변경

    // 진료비 결제실패 관련
    int updateFailedMedical(int idx, String msg); // 진료비 결제실패 변경
    int failedPushSendCheck(int mbIdx, int idx); // 진료비 결제실패 푸시DB 중복확인
}