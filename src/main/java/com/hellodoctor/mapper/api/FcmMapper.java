package com.hellodoctor.mapper.api;

import com.hellodoctor.model.api.PaymentCancelResult;
import com.hellodoctor.model.api.PaymentResult;
import com.hellodoctor.model.api.Push;
import com.hellodoctor.model.app.Member;
import com.hellodoctor.model.app.MemberCreditCard;
import org.apache.ibatis.annotations.Mapper;

/**
 * Fcm Mapper 인터페이스
 */

@Mapper
public interface FcmMapper {
    int insertPush(Push push); // 푸시 INSERT

}