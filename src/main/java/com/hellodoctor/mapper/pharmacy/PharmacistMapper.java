package com.hellodoctor.mapper.pharmacy;

import com.hellodoctor.model.app.Pharmacy;
import com.hellodoctor.model.app.PharmacyTmp;
import com.hellodoctor.model.pharmacy.Pharmacist;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * 약사 Mapper 인터페이스
 */

@Mapper
public interface PharmacistMapper {
    Map<String, String> hpCheck(String hp, String email); // 아이디찾기 연락처 비교 or 비밀번호찾기 이메일,연락처 비교
    int resetPassword(String email, String password); // 비밀번호 재설정

    // 약사
    int insertPharmacist(Pharmacist pharmacist); // 약사 등록
    int idCheck(String id); // 아이디 중복체크
    int updatePharmacist(Pharmacist pharmacist, String columnValue); // 약사 수정
    int deletePharmacist(int idx); // 약사 삭제
    Pharmacist getPharmacistInfo(int idx); // 약사 정보

    // 약국
    int insertPharmacy(Pharmacy pharmacy); // 약국 정보 등록
    int updatePharmacy(Pharmacy pharmacy, String columnValue); // 약국 정보 수정
    Pharmacy getPharmacyInfo(int idx); // 약국 정보
    int updatePharmacyTmp(Pharmacy pharmacy, int pharmacyIdx); // 약국 임시테이블 정보 저장 (관리자 승인 전까지 임시테이블에 저장)
    PharmacyTmp getPharmacyTmpInfo(int pharmacyIdx); // 약국 임시테이블 정보
    int deletePharmacyTmp(int pharmacyIdx); // 약국 임시테이블 정보 삭제
}
