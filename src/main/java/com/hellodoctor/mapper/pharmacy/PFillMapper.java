package com.hellodoctor.mapper.pharmacy;

import com.hellodoctor.model.app.PharmacyRequest;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 약사 약 조제관리 mapper
 */
@Mapper
public interface PFillMapper {
    // 약 조제관리 - 조제요청
    List<Map<String, String>> getFillRequestCount(Integer pharmacyIdx, String sqlSearch); // 약 조제관리 - 조제요청 목록 수
    List<Map<String, String>> getFillRequest(Integer pharmacyIdx, String sqlSearch, int startRecord, int endRecord); // 약 조제관리 - 조제요청 목록
    int statusComplete(Integer idx, Integer billAmt); // 약 조제관리 - 조제요청 상태 - 접수
    int statusReject(Integer idx, String cancelDate, String cancelReason); // 약 조제관리 - 조제요청 상태 - 거부
    Map<String, String> getAddrInfo(Integer medicalIdx); // 배송지 정보
    int receiveMethodChange(Integer medicalIdx, String receiveMethod); // 약 수령방식 변경
    PharmacyRequest getPharmacyRequestInfo(Integer idx); // 약 조제요청 정보 (단건)

    // 약 조제관리 - 조제접수
    List<Map<String, String>> getFillReceiveCount(Integer pharmacyIdx, String sqlSearch); // 약 조제관리 - 조제접수 목록 수
    List<Map<String, String>> getFillReceive(Integer pharmacyIdx, String sqlSearch, int startRecord, int endRecord); // 약 조제관리 - 조제접수 목록
    int fillComplete(Integer idx, String completeDate); // 약 조제관리 - 조제접수 상태 - 완료
    List<Map<String, String>> mediAutocomplete(Integer pharmacyIdx, String search); // 의약품 검색 (자동완성)
    int saveGuide(Integer medicalIdx, List<Map<String, String>> guideList); // 복약지도 저장 (등록/수정)
    int deleteGuide(Integer medicalIdx); // 복약지도 삭제
    List<Map<String, String>> mediGuideInfo(Integer medicalIdx); // 복약지도 정보

    // 약 조제내역
    List<Map<String, String>> getFillListCount(int pharmacyIdx, String sqlSearch); // 약 조제내역 목록 수
    List<Map<String, String>> getFillList(int pharmacyIdx, String sqlSearch, int startRecord, int endRecord); // 약 조제내역 목록
}