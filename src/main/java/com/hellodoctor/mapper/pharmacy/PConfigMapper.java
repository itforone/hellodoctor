package com.hellodoctor.mapper.pharmacy;

import com.hellodoctor.model.app.PharmacyTmp;
import com.hellodoctor.model.hospital.DesignatePatientTemplate;
import com.hellodoctor.model.pharmacy.Medicine;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 관리 Mapper 인터페이스
 */
@Mapper
public interface PConfigMapper {
    int getAdmSubAccountCount(int pharmacyIdx, String sqlSearch); // 서브계정관리 목록 수
    List<Map<String, String>> getAdmSubAccount(int pharmacyIdx, String sqlSearch, int startRecord, int endRecord); // 서브계정관리 목록
    int getAdmHospitalTotalCount(int pharmacyIdx); // 지정병원 전체등록수
    List<Map<String, String>> getAdmHospitalList(int pharmacyIdx, int startRecord, int endRecord); // 지정병원 목록
    int insertMedicine(Medicine medicine); // 의약품 등록
    int updateMedicine(Medicine medicine); // 의약품 수정
    int deleteMedicine(int idx); // 의약품 삭제
    Medicine getMedicineOne(int idx); // 의약품 1건 조회
    int getMedicineTotalCount(int pharmacyIdx, String columnValue); // 의약품 전체등록수
    List<Medicine> getMedicineList(int pharmacyIdx, int startRecord, int endRecord, String columnValue); // 의약품 목록

}