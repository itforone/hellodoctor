package com.hellodoctor.mapper.app;

import com.hellodoctor.model.app.Medical;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 우리동네주치의 관련 Mapper 인터페이스
 */
@Mapper
public interface MyDoctorMapper {
    List<Map<String, String>> requestMyDoctorList(int mbIdx, String mbHp); // 메인 > 우리동네주치의 요청내역 조회

    int requestMyDoctorAction(int idx, String status, int mbIdx); // 메인 > 우리동네주치의 요청 수락/거절

    int myDoctorTotalCount(int mbIdx); // 우리동네주치의 전체수

    List<Map<String, Object>> getMyDoctorList(int mbIdx, String lat, String lng, String today, String todayNum); // 우리동네주치의 목록 | @param: 회원 idx, 위도, 경도, 오늘일자, 오늘요일

    Map<String, Object> getMyDoctorOne(int mbIdx, String lat, String lng, String today, String todayNum, int myDoctorIdx); // 우리동네주치의 병원1건 정보

    String getMemberLastRrNo(int mbIdx); //환자 마지막 진료기록 (주민번호)

    int insertMyDoctorForm(Medical params); // 진료신청 등록 (임시저장)
    int updateMyDoctorForm(Medical params); // 진료신청 업데이트
}
