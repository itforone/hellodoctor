package com.hellodoctor.mapper.app;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 약국 관련 mapper
 */

@Mapper
public interface PharmacyMapper {
    List<Map<String, Object>> getPharmacyList(String gu, String dong, String lat, String lng); // 약국 목록 | @param: 지역(구), 지역(동), 위도, 경도
    List<Map<String, Object>> getLatLng(String todayNum); // 약국 위치 좌표 목록
    Map<String, Object> getPharmacyInfo(Integer idx, String lat, String lng); // 선택 약국 정보 | @param: 약국idx, 위도, 경도
}
