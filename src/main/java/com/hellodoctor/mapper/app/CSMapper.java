package com.hellodoctor.mapper.app;

import com.hellodoctor.model.app.CSApp;
import com.hellodoctor.model.app.Medical;
import com.hellodoctor.model.app.MemberFamily;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 서비스문의 Mapper 인터페이스
 */
@Mapper
public interface CSMapper {
    int insertContactForm(CSApp params); // 서비스문의 등록

    List<Map<String, String>> getContactList(int tab, int mbIdx, String todayDate, String beforeDate); // 서비스문의 목록

}