package com.hellodoctor.mapper.app;

import com.hellodoctor.model.app.MemberFamily;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 나의가족관리 Mapper 인터페이스
 */
@Mapper
public interface MemberFamilyMapper {
    int insertFamily(MemberFamily params); // 가족추가

    List<MemberFamily> getFamilyList(int mbIdx); // 가족목록

    int deleteMyFamily(int idx); // 가족삭제

    MemberFamily getFamilyDetail(int idx); // 가족수정정보

    int updateFamily(MemberFamily params); // 가족수정

    Map<String, String> getFamilyLastMedical(int mbIdx, int patientIdx); // 진료환자(본인+나의가족) 마지막병원정보


}