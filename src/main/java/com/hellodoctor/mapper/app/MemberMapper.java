package com.hellodoctor.mapper.app;

import com.hellodoctor.model.app.Hospital;
import com.hellodoctor.model.app.MedicineGuideDetail;
import com.hellodoctor.model.app.MemberCreditCard;
import org.apache.ibatis.annotations.Mapper;

import com.hellodoctor.model.app.Member;

import java.util.List;
import java.util.Map;

/**
 * 회원 관련 Mapper 인터페이스
 */
@Mapper
public interface MemberMapper {
    int signUpMember(Member member); // 회원가입
    int idCheck(String id); // 아이디 중복체크
    String getMemberInfo(String column, String id); // 회원정보
    Member isMember(String id, String name, String birth); // 회원인지 확인
    int resetPassword(String id, String pw); // 비밀번호 재설정
    int resetHp(String id, String hp); // 연락처 재설정
    int updatePushSetting(String mbIdx, String pushInfo, String pushMk); // 앱푸시설정변경
    int updateMemberInfo(int mbIdx, String columnValue); // 회원정보변경 | @param: 회원idx, 변경할 컬럼&값
    List<MemberCreditCard> getMemberCreditCardList(int mbIdx); // 회원 카드목록

    int deleteMemberCreditCard(Map<String, Object> map); // 회원 카드삭제
    int beforePaymentMedicalCount(int mbIdx, int cardIdx); // 회원 카드삭제 전 결제예정 진료수 확인
    List<Map<String, Object>> getPaymentHistory(int mbIdx, String todayDate, String beforeDate, int startRecord, int endRecord); // 결제내역
}
