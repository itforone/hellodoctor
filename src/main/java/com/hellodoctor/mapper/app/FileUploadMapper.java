package com.hellodoctor.mapper.app;

import com.hellodoctor.model.app.FileUpload;
import com.hellodoctor.model.app.Member;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 파일업로드 Mapper 인터페이스
 */
@Mapper
public interface FileUploadMapper {
    // 파일업로드
    int insertUploadFiles(Map<String, Object> map);
    // 업로드된 파일 조회
    List<FileUpload> getUploadFiles(String code, String tblName, int limit);
    // 파일삭제
    int deleteUploadFiles(List<String> fileUrl);

}