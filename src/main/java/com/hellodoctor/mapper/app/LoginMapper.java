package com.hellodoctor.mapper.app;

import com.hellodoctor.model.admin.Admin;
import com.hellodoctor.model.app.Member;
import com.hellodoctor.model.hospital.Doctor;
import com.hellodoctor.model.pharmacy.Pharmacist;
import org.apache.ibatis.annotations.Mapper;

/**
 * 회원DB를 가져오기 위한 Mapper 인터페이스
 */
@Mapper
public interface LoginMapper {
    Member getMemberAccount(String id); // 회원 로그인
    Doctor getDoctorAccount(String userId); // 의사 로그인
    Pharmacist getPharmacistAccount(String id); // 약사 로그인
    Admin getAdminAccount(String id); // 관리자 로그인
}