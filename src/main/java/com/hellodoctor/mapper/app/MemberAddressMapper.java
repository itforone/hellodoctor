package com.hellodoctor.mapper.app;

import com.hellodoctor.model.app.MemberAddress;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 회원 배송지 Mapper 인터페이스
 */

@Mapper
public interface MemberAddressMapper {
    int insertMemberAddress(MemberAddress memberAddress); // 회원 배송지 추가
    int updateMemberAddress(MemberAddress memberAddress); // 회원 배송지 수정
    List<MemberAddress> getAddressList(int mbIdx); // 회원 배송지 목록
    int deleteMemberAddress(int idx); // 회원 배송지 삭제
    MemberAddress getAddressOne(int addrIdx); // 회원 배송지 선택 정보
}
