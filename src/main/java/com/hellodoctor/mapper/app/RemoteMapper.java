package com.hellodoctor.mapper.app;

import com.hellodoctor.model.app.*;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 진료신청 Mapper 인터페이스
 */
@Mapper
public interface RemoteMapper {
    int insertRemoteForm(Medical params); // 진료신청 등록 (임시저장)
    Medical getRemoteForm(int idx, String status); // 진료신청 상세정보
    int changeRemoteFormHopeHour(String hour, String idx); // 병원선택시 진료시간 변경
    // 병원선택시 진료가능병원 목록
    List<Map<String, Object>> getAllHospitalList(String lat, String lng, String todayNum, String today, String[] matchingCodeArr);
    // 병원선택시 주치전담병원 목록
    List<Map<String, Object>> getMyHospitalList(String lat, String lng, String todayNum, String today, String[] matchingCodeArr, int mbIdx);
    MedicalReceiptOrder searchReceiptOrder(String date); // 진료접수번호 순번조회 int hospitalIdx,
    int insertReceiptOrder(MedicalReceiptOrder params); // 진료접수번호 순번생성 등록
    int updateRemoteForm(String hospitalIdx, String medicalIdx, String receiptNo, String appStartHour, String appEndHour, String cardIdx);    // 진료신청 완료
    //Hospital getRemoteStatusHospitalData(int hospitalIdx); // 진료신청완료 후 선택한 병원정보
    Map<String, Object> getRemoteStatusData(int medicalIdx, String lat, String lng); // 원격진료 상세보기 > 진료신청완료 후 진료&병원 정보
    List<HospitalHour> getHospitalHours(int hospitalIdx); // 병원 전체 진료시간
    int cancelRemoteForm(int medicalIdx); // 환자 진료신청취소
    int getHospitalVisitCount(int medicalIdx, int hospitalIdx, String rcptDate); // 환자 초진/재진 확인
    int reviewWriteAction(MedicalReview medicalReview); // 원격진료 리뷰작성

    // 원격진료 목록
    List<Map<String, Object>> getRemoteListAppointment(int mbIdx, String todayDate, String beforeDate, int startRecord, int endRecord); // 진료접수
    List<Map<String, Object>> getRemoteListIng(int mbIdx, String todayDate, String beforeDate, int startRecord, int endRecord); // 진료진행중
    List<Map<String, Object>> getRemoteListComplete(int mbIdx, String todayDate, String beforeDate, int startRecord, int endRecord); // 진료완료
    List<Map<String, Object>> getRemoteListCancel(int mbIdx, String todayDate, String beforeDate, int startRecord, int endRecord); // 진료취소

    List<Pharmacy> getPharmacySelectList(int hospitalIdx); // 진료진행중 연계약국리스트
    int insertPharmacyRequest(PharmacyRequest request); // 약조제요청 등록
    int updatePharmacyRequestIndex(int medicalIdx, int requestIdx); // 약조제요청 등록성공시 진료DB 업데이트
    Map<String, String> getRemoteStatusIngData(int reqIdx, int medicalIdx, int mbAddrIdx); // 원격진료 상세보기 > 조제중이면 약국,조제비 정보 조회
    Map<String, String> getRemoteCompleteDetail(int pharmacyReqIdx, int mbAddrIdx); // 원격진료 목록 진료완료 > 상세보기 결제정보
    String getPrescriptionImage(int idx); // 처방전보기
    MedicalReview getMedicalReview(int idx); // 리뷰보기

}