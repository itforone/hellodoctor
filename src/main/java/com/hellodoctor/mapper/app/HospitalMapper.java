package com.hellodoctor.mapper.app;

import com.hellodoctor.model.app.Hospital;
import org.apache.ibatis.annotations.Mapper;
import software.amazon.ion.Decimal;

import java.util.List;
import java.util.Map;

/**
 * 병원 관련 mapper
 */

@Mapper
public interface HospitalMapper {
    List<Map<String, Object>> getHospitalList(String gu, String dong, String lat, String lng, String today, String todayNum); // 병원 목록 | @param: 지역(구), 지역(동), 위도, 경도, 오늘일자, 오늘요일
    List<Map<String, Object>> getLatLng(String todayNum); // 병원/ 위치 좌표 목록 | 오늘 요일
    Map<String, Object> getHospitalInfo(Integer idx, String lat, String lng, String today, String todayNum); // 선택 병원 정보 | @param: 병원idx, 위도, 경도, 오늘일자, 오늘요일
    List<Map<String, Object>> getMyHospitalList(int mbIdx, String lat, String lng,String today, String todayNum); // 주치전담병원 목록 | @param: 회원 idx, 위도, 경도, 오늘일자, 오늘요일
    Hospital getMyHospital(int hospitalIdx); // 주치전담병원 정보
    int deleteMyHospital(int mbIdx, String myHospitalIdx); // 주치 전담병원 삭제 | @param: 회원idx, 삭제할 병원idx
}
