package com.hellodoctor.mapper.app;

import com.hellodoctor.model.app.CSApp;
import com.hellodoctor.model.app.MedicineGuideDetail;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 복약지도 Mapper 인터페이스
 */
@Mapper
public interface MedicineGuideMapper {
    // int insertContactForm(CSApp params); // 서비스문의 등록

    List<Map<String, String>> getGuideList(int mbIdx, String todayDate, String beforeDate); // 복약지도 목록
    
    List<MedicineGuideDetail> getGuideDetail(int medicalIdx); // 복약지도 상세

}