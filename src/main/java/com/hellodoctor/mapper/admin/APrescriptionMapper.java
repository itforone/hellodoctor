package com.hellodoctor.mapper.admin;

import com.hellodoctor.model.hospital.MedicalPrescription;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 처방전관리 Mapper 인터페이스
 */

@Mapper
public interface APrescriptionMapper {
    List<Map<String, String>> getPxListCount(String sqlSearch); // 처방전관리 목록 수
    List<Map<String, String>> getPxList(String sqlSearch, int startRecord, int endRecord); // 처방전관리 목록
    int updatePrescription(int idx, String columnValue); // 처방전 상태 변경
    int getPrescriptionCount(int medicalIdx); // 진료별 처방전 개수
    MedicalPrescription getPrescriptionInfo(int idx); // 처방전 정보 (단건)
    int insertReissuePrescription(MedicalPrescription medicalPrescription); // 재발행 처방전 등록
}
