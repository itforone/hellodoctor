package com.hellodoctor.mapper.admin;

import com.hellodoctor.model.hospital.AdminNotice;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 커뮤니티 Mapper 인터페이스
 */
@Mapper
public interface ABoardMapper {
    int getReviewTotalCount(int hospitalIdx); // 리뷰 전체글수
    List<Map<String, String>> getReviewList(int hospitalIdx, int startRecord, int endRecord); // 리뷰 목록
    Map<String, String> getReviewDetail(int reviewIdx); // 리뷰 상세

    int getNoticeTotalCount(String columnValue); // 공지사항 전체글수
    List<AdminNotice> getNoticeList(String columnValue, int startRecord, int endRecord); // 공지사항 목록
    AdminNotice getNoticeOne(int idx); // 공지사항 상세보기
    int insertNotice(AdminNotice form); // 공지 등록
    int updateNotice(AdminNotice form); // 공지 수정
    int deleteNotice(int idx); // 공지 삭제

    int getCSTotalCount(String columnValue, String target); // CS문의 전체글수
    List<Map<String, String>> getCSList(String columnValue, int startRecord, int endRecord, String target); // CS문의 목록
    Map<String, String> getCSForm(int idx, String target); // CS문의 상세보기
    int saveCSAnswer(Map<String, String> params, String target); // CS문의 답변

}
