package com.hellodoctor.mapper.admin;

import com.hellodoctor.model.admin.Admin;
import com.hellodoctor.model.admin.Terms;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 시스템관리 Mapper 인터페이스
 */
@Mapper
public interface ASystemMapper {
    List<Terms> getTermsList(); // 약관 목록
    Terms getTerms(String key); // 약관 1개 조회
    int insertTerms(Terms form); // 약관 등록
    int updateTerms(Terms form); // 약관 수정

    int adminIdCheck(String id); // 관리자 아이디 중복체크
    int insertAdmin(Admin form); // 관리자 등록
    int updateAdmin(Admin form); // 관리자 수정
    int deleteAdmin(int idx); // 관리자 삭제
    List<Admin> getAdminList(); // 관리자 목록
}
