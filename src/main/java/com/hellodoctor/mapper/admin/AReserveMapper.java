package com.hellodoctor.mapper.admin;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 예약정보관리 Mapper 인터페이스
 */

@Mapper
public interface AReserveMapper {
    List<Map<String, String>> getReserveListCount(String sqlSearch); // 예약정보관리 목록 수
    List<Map<String, String>> getReserveList(String sqlSearch, int startRecord, int endRecord); // 예약정보관리 목록
}
