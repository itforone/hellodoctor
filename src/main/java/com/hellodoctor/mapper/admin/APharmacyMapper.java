package com.hellodoctor.mapper.admin;

import com.hellodoctor.model.app.PharmacyTmp;
import com.hellodoctor.model.pharmacy.Pharmacist;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 약국관리 Mapper 인터페이스
 */
@Mapper
public interface APharmacyMapper {
    int getPharmacyTotalCount(String columnValue); // 약국관리 전체약국수
    List<Map<String, Object>> getPharmacyList(int startRecord, int endRecord, String columnValue); // 약국관리 목록
    int changePharmacyAuth(int idx, String authYn); // 약국 승인상태 변경
    List<Pharmacist> getPharmacyAccount(int idx); // 약국 계정정보(약사)

    // 약국관리-약국정보 수정요청
    int getPharmacyUpdateTotalCount(String columnValue); // 약국정보 수정요청 전체약국수
    List<Map<String, Object>> getPharmacyUpdateList(int startRecord, int endRecord, String columnValue); // 약국정보 수정요청 목록
    int pharmacyUpdateApproval(int pharmacyIdx, PharmacyTmp pharmacyTmp); // 병원정보 수정요청 승인
}
