package com.hellodoctor.mapper.admin;

import com.hellodoctor.model.app.Hospital;
import com.hellodoctor.model.app.HospitalTmp;
import com.hellodoctor.model.app.Member;
import com.hellodoctor.model.hospital.Doctor;
import com.hellodoctor.model.hospital.HospitalSubject;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 회원관리 Mapper 인터페이스
 */
@Mapper
public interface AMemberMapper {
    int getMemberTotalCount(String columnValue); // 회원관리 전체회원수
    List<Map<String, Object>> getMemberList(int startRecord, int endRecord, String columnValue); // 회원관리 목록
    Map<String, Object> getMemberInfoByIdx(int idx); // 회원정보 (인덱스로 조회)

}
