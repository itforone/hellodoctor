package com.hellodoctor.mapper.admin;

import com.hellodoctor.model.app.HospitalTmp;
import com.hellodoctor.model.hospital.Doctor;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 병원관리 Mapper 인터페이스
 */
@Mapper
public interface AHospitalMapper {
    // 병원관리-병원관리
    int getHospitalTotalCount(String columnValue); // 병원관리 전체병원수
    List<Map<String, Object>> getHospitalList(int startRecord, int endRecord, String columnValue); // 병원관리 목록
    Map<String, Object> getHospitalInfoByIdx(int idx); // 병원정보 (인덱스로 조회)
    int changeHospitalAuth(int idx, String authYn); // 병원 승인상태 변경
    List<Doctor> getHospitalAccount(int idx); // 병원 계정정보(의사)

    // 병원관리-병원정보 수정요청
    int getHospitalUpdateTotalCount(String columnValue); // 병원정보 수정요청 전체병원수
    List<Map<String, Object>> getHospitalUpdateList(int startRecord, int endRecord, String columnValue); // 병원정보 수정요청 목록
    int hospitalUpdateApproval(int hospitalIdx, HospitalTmp hospitalTmp); // 병원정보 수정요청 승인
}