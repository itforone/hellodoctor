package com.hellodoctor.mapper.hospital;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 처방전재발행 Mapper 인터페이스
 */

@Mapper
public interface HPrescriptionMapper {
    List<Map<String, String>> getPxReissueCount(int hospitalIdx, String sqlSearch); // 처방전 재발행 목록 수
    List<Map<String, String>> getPxReissue(int hospitalIdx, String sqlSearch, int startRecord, int endRecord); // 처방전 재발행 목록
    Map<String, Object> reissuePrescriptionInfo(int presIdx); // 재발행 처방전 정보
    int savePrescription(int presIdx, String columnValue); // 재발행 처방전 저장
    Map<String, String> getIdxInfo(int presIdx); // 진료 idx 및 약국 idx
}