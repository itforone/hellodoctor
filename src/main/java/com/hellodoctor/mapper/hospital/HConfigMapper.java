package com.hellodoctor.mapper.hospital;

import com.hellodoctor.model.app.HospitalTmp;
import com.hellodoctor.model.app.Pharmacy;
import com.hellodoctor.model.hospital.DesignatePatient;
import com.hellodoctor.model.hospital.DesignatePatientTemplate;
import com.hellodoctor.model.hospital.Doctor;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 관리 Mapper 인터페이스
 */
@Mapper
public interface HConfigMapper {
    List<Pharmacy> getSearchPharmacyList(int hospitalIdx, String lat, String lng, String columnValue); // 지정약국관리 신규등록 검색 약국목록
    int deleteDesignatePharmacy(int hospitalIdx, int idx); // 지정약국관리 해제
    int insertDesignatePharmacy(int hospitalIdx, List<String> chkIdxArr); // 지정약국관리 등록
    int deduplicationPharmacy(int hospitalIdx); // 지정약국관리 중복제거
    int getDesignatePharmacyTotalCount(int hospitalIdx, String columnValue); // 지정약국관리 전체글수
    List<Map<String, String>> getDesignatePharmacyList(int startRecord, int endRecord, int hospitalIdx, String columnValue); // 지정약국관리 목록
    int getDesignatePatientCnt(int hospitalIdx); // 병원 지정환자 수
    DesignatePatientTemplate getHospitalTemplate(int hospitalIdx); // 지정환자관리 템플릿
    int insertHospitalTemplate(DesignatePatientTemplate template); // 지정환자관리 템플릿 등록
    int updateHospitalTemplate(DesignatePatientTemplate template); // 지정환자관리 템플릿 수정
    Map<String, String> requestCheckData(int hospitalIdx, String hp); // 지정환자관리 등록요청시 회원정보조회 & 요청상태 확인
    int insertDesignatePatient(DesignatePatient data); // 지정환자 등록
    int getDesignatePatientTotalCount(int hospitalIdx, String columnValue); // 지정환자관리 전체글수
    List<Map<String, String>> getDesignatePatientList(int startRecord, int endRecord, int hospitalIdx, String columnValue); // 지정환자관리 목록
    int getAdmPatientRequestCount(int hospitalIdx); // 지정환자관리 등록요청내역 전체개수
    List<Map<String, String>> getAdmPatientRequestList(int hospitalIdx, int startRecord, int endRecord); // 지정환자관리 등록요청내역 목록
    int getAdmSubAccountCount(int hospitalIdx, String sqlSearch); // 서브계정관리 목록 수
    List<Map<String, String>> getAdmSubAccount(int hospitalIdx, String sqlSearch, int startRecord, int endRecord); // 서브계정관리 목록

    // 관리-진료예약관리
    String getAppointmentList(int hospitalIdx); // 진료예약시간(특정일) 목록
    String getAppointment(int hospitalIdx, String patientType, String date); // 진료예약시간(특정일) 조회
    int insertAppointment(int hospitalIdx, String patientType, String date, String timeTable); // 진료예약시간(특정일) 등록
    int updateAppointment(int hospitalIdx, String patientType, String date, String timeTable); // 진료예약시간(특정일) 수정
    int saveAdmTreat(int hospitalIdx, List<Map<String, String>> timeList); // 진료예약시간 - 일반환자/지정환자 예약시간 등록
}