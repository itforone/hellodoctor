package com.hellodoctor.mapper.hospital;

import com.hellodoctor.model.app.Medical;
import com.hellodoctor.model.hospital.MedicalPrescription;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 의사-원격진료 Mapper 인터페이스
 */
@Mapper
public interface HRemoteMapper {
    List<Map<String, String>> getRemoteListCount(int hospitalIdx, String sqlSearch); // 원격진료 예약신청 목록 갯수
    List<Map<String, String>> getRemoteList(int hospitalIdx, String sqlSearch, int startRecord, int endRecord); // 원격진료 예약신청 목록
    int receiptComplete(Integer idx, String medicalTime); // 원격진료 예약신청 접수
    int receiptReject(Integer idx, String cancelDate, String cancelReason); // 원격진료 예약신청 거부
    Map<String, Object> getPatientInfo(Integer idx); // 환자정보
    List<Map<String, String>> getRemoteReceiptCount(int hospitalIdx, String sqlSearch); // 원격진료 접수 목록 갯수
    List<Map<String, String>> getRemoteReceipt(int hospitalIdx, String sqlSearch, int startRecord, int endRecord); // 원격진료 접수 목록
    Map<String, Object> getPrescriptionInfo(Integer idx); // 처방전 정보
    int insertPrescription(MedicalPrescription prescription); // 처방전 등록
    int updatePrescription(MedicalPrescription prescription); // 처방전 수정
    int deletePrescription(Integer idx); // 처방전 삭제
    Medical getMedicalInfo(Integer idx); // 원격진료 정보
    int updateMedical(Integer idx, String columnValue); // 원격진료 처방전 idx 수정
}