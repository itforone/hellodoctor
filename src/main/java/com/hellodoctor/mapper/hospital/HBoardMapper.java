package com.hellodoctor.mapper.hospital;

import com.hellodoctor.model.app.MedicalReview;
import com.hellodoctor.model.hospital.AdminNotice;
import com.hellodoctor.model.hospital.CSSite;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 게시판 Mapper 인터페이스
 */
@Mapper
public interface HBoardMapper {
    int getNoticeTotalCount(String target); // 공지사항 전체글수
    List<AdminNotice> getNoticeList(String target, int startRecord, int endRecord); // 공지사항 목록

    int getReviewTotalCount(int hospitalIdx); // 리뷰 전체글수
    List<Map<String, String>> getReviewList(int hospitalIdx, int startRecord, int endRecord); // 리뷰 목록
    Map<String, String> getReviewDetail(int reviewIdx, int hospitalIdx); // 리뷰 상세
    int updateReviewReply(MedicalReview review); // 리뷰 답변등록

    int insertCSForm(CSSite params); // CS문의 등록
    int updateCSForm(CSSite params); // CS문의 수정
    CSSite getCSFormOne(int idx, int userIdx); // CS문의 상세/수정폼
    int getCSTotalCount(String target, int hospitalIdx); // CS문의 전체글수 (병원기준)
    List<CSSite> getCSList(String target, int hospitalIdx, int startRecord, int endRecord); // CS문의 목록
    int deleteCSForm(int idx); // CS문의 삭제
}