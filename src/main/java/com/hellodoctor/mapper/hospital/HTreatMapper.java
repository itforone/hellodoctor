package com.hellodoctor.mapper.hospital;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 의사-진료내역격진료 Mapper 인터페이스
 */
@Mapper
public interface HTreatMapper {
    List<Map<String, String>> getTreatListCount(int hospitalIdx, String sqlSearch); // 진료내역 목록 수
    List<Map<String, String>> getTreatList(int hospitalIdx, String sqlSearch, int startRecord, int endRecord); // 진료내역 목록
}