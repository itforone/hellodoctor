package com.hellodoctor.mapper.hospital;

import com.hellodoctor.model.app.Medical;
import com.hellodoctor.model.hospital.MedicalOpinion;
import com.hellodoctor.model.hospital.MedicalPrescription;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 원격진료 WebRTC 및 진료소견 Mapper 인터페이스
 */
@Mapper
public interface HWebRTCMapper {
    Map<String, String> getPatientInfoOne(int medicalIdx, int hospitalIdx); // 환자정보
    MedicalOpinion getRemoteMedicalOpinion(int medicalIdx); // 진료소견 조회
    int insertMedicalOpinion(MedicalOpinion medicalOpinion); // 진료소견 등록
    int updateMedicalOpinion(MedicalOpinion medicalOpinion); // 진료소견 수정

}