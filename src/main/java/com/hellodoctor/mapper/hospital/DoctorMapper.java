package com.hellodoctor.mapper.hospital;

import com.hellodoctor.model.app.Hospital;
import com.hellodoctor.model.app.HospitalTmp;
import com.hellodoctor.model.hospital.Doctor;
import com.hellodoctor.model.hospital.HospitalSubject;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 의사 Mapper 인터페이스
 */
@Mapper
public interface DoctorMapper {
    Map<String, String> hpCheck(String hp, String email); // 아이디찾기 연락처 비교 or 비밀번호찾기 이메일,연락처 비교
    int resetPassword(String email, String password); // 비밀번호 재설정
    int idCheck(String id); // 아이디 중복체크

    // 의사
    int insertDoctor(Doctor doctor); // 의사 정보 등록
    int updateDoctor(Doctor doctor, String columnValue); // 의사 정보 수정
    Doctor getDoctorInfo(int idx); // 의사 정보
    int deleteDoctor(int idx); // 의사 정보 삭제

    // 병원
    int insertHospital(Hospital hospital); // 병원 정보 등록
    int updateHospital(Hospital hospital, String columnValue); // 병원 정보 수정
    int insertSubject(int hospitalIdx, String[] subjectCodeArr, String table); // 병원-진료과목 저장
    int insertHour(int hospitalIdx, List<Map<String, String>> hourList); // 병원-영업시간 저장
    int insertAppointment(int hospitalIdx, List<Map<String, String>> timeList); // 병원-진료예약시간 저장
    Hospital getHospitalInfo(int idx); // 병원 정보
    HospitalSubject getSubjectInfo(int hospitalIdx, String table); // 병원-진료과목 정보
    List<Map<String, Object>> getHourInfo(int hospitalIdx); // 병원-영업시간 정보
    List<Map<String, Object>> getAppointmentInfo(int hospitalIdx); // 병원-진료예약시간 정보
    int updateHospitalTmp(Hospital hospital, int hospitalIdx); // 병원 임시테이블 정보 저장 (관리자 승인 전까지 임시테이블에 저장)
    HospitalTmp getHospitalTmpInfo(int hospitalIdx); // 병원 임시테이블 정보
    int deleteHospitalTmp(int hospitalIdx); // 병원 임시테이블 정보 삭제
}
