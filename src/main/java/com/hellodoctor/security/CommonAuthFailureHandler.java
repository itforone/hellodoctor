package com.hellodoctor.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.*;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@Component
public class CommonAuthFailureHandler extends SimpleUrlAuthenticationFailureHandler {
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        Integer errorCode = 1;

        /*if (exception instanceof AuthenticationServiceException) {
            //"존재하지 않는 사용자입니다."
        } else if(exception instanceof BadCredentialsException) {
            //"아이디 또는 비밀번호가 틀립니다."
        } else if(exception instanceof LockedException) {
            //"잠긴 계정입니다."
        } else if(exception instanceof DisabledException) {
            //"비활성화된 계정입니다."
        } else if(exception instanceof AccountExpiredException) {
            //"만료된 계정입니다."
        } else if(exception instanceof CredentialsExpiredException) {
            //"비밀번호가 만료되었습니다."
        } else if (exception instanceof InternalAuthenticationServiceException) {
            //"시스템 문제로 인증처리에 실패했습니다"
        } else if (exception instanceof UsernameNotFoundException) {
            //"이름없음"
        } else if (exception instanceof AuthenticationCredentialsNotFoundException) {
            //인증 요청이 거부되었습니다.
        }*/
        if (exception instanceof InternalAuthenticationServiceException) {
            errorCode = 2;
        } else if (exception instanceof AuthenticationCredentialsNotFoundException) {
            errorCode = 3;
        } else if (exception instanceof UsernameNotFoundException) {
            //"이름없음"
            log.info("에러!2222");
        }

        // log.info(exception.toString());

        // login form 파라미터
        // Enumeration eParam = request.getParameterNames();
        // while (eParam.hasMoreElements()) {
        //     String pName = (String)eParam.nextElement();
        //     String pValue = request.getParameter(pName);
        //     log.info(pName + "=" + pValue);
        // }

        //errorMessage = URLEncoder.encode(errorMessage, "UTF-8"); // String

        // 리턴 url
        switch (request.getParameter("target")) {
            case "hospital" :   // 의사
                setDefaultFailureUrl("/hospital/login?error=true&code=" + errorCode);
                break;
            case "pharmacy" :   // 약사
                setDefaultFailureUrl("/pharmacy/login?error=true&code=" + errorCode);
                break;
            case "admin" :      // 관리자
                setDefaultFailureUrl("/admin/login?error=true&code=" + errorCode);
                break;
            default:
                setDefaultFailureUrl("/app/emailLogin?error=true&code=" + errorCode);
        }

        super.onAuthenticationFailure(request, response, exception);
    }
}