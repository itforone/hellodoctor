package com.hellodoctor.service.admin;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hellodoctor.common.Functions;
import com.hellodoctor.mapper.admin.APharmacyMapper;
import com.hellodoctor.mapper.pharmacy.PharmacistMapper;
import com.hellodoctor.model.app.Pharmacy;
import com.hellodoctor.model.app.PharmacyTmp;
import com.hellodoctor.model.pharmacy.Pharmacist;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.hellodoctor.common.Functions.isNull;

/**
 * 약국관리 service 클래스
 */
@Service
@Slf4j
public class APharmacyService {
    @Autowired
    APharmacyMapper aPharmacyMapper;
    @Autowired
    PharmacistMapper pharmacistMapper;

    // 약국관리 전체약국수
    public int getPharmacyTotalCount(Map<String, String> params) {
        String columnValue = commonWhereQuery(params);
        return aPharmacyMapper.getPharmacyTotalCount(columnValue);
    }

    // 약국관리 목록
    public List<Map<String, Object>> getPharmacyList(int startRecord, int endRecord, Map<String, String> params) {
        String columnValue = commonWhereQuery(params);
        List<Map<String, Object>> hospitalList = aPharmacyMapper.getPharmacyList(startRecord, endRecord, columnValue);

        for (Map<String, Object> hospital : hospitalList) {
            // 승인일자 포맷변경 (yyyy.mm.dd)
            if (!isNull(hospital.get("authDate"))) {
                String _authDate = String.valueOf(hospital.get("authDate")).substring(0, 10);
                _authDate = _authDate.replace("-", ".");
                hospital.put("authDate", _authDate);
            }

            // null check
            if (hospital.get("authYn") == null) hospital.put("authYn", "N");
            if (hospital.get("telNo") == null) hospital.put("telNo", "");
            if (hospital.get("brNo") == null) hospital.put("brNo", "");
            if (hospital.get("authDate") == null) hospital.put("authDate", "");
        }
        return hospitalList;
    }

    // 약국관리 전체약국수, 목록 쿼리조건
    public String commonWhereQuery(Map<String, String> params) {
        List<String> columnValueArr = new ArrayList<>();
        String schTxt = params.get("stx"); // 키워드
        // log.info("params" + params);

        // 키워드검색
        if (!schTxt.equals("")) {
            switch (params.get("sfl")) {
                case "name" :   // 약국명
                    columnValueArr.add("A.pharmacy_name LIKE '%"+ schTxt +"%'");
                    break;
                case "tel" :   // 연락처
                    columnValueArr.add("REPLACE(A.tel_no, '-', '') LIKE '%"+ schTxt +"%'");
                    break;
                case "addr" :    // 주소
                    String str = "(";
                    str += "A.main_addr LIKE '%"+ schTxt +"%'";
                    str += " OR A.detail_addr LIKE '%"+ schTxt + "%'";
                    str += " OR A.area_dong LIKE '%"+ schTxt + "%'";
                    str += ")";
                    columnValueArr.add(str);
                    break;
            }
        }

        // 서비스종류
        switch (String.valueOf(params.get("svc"))) {
            case "1" : // 비대면진료
                columnValueArr.add("A.nonface_svc_yn = 'Y'");
                break;
            case "2" : // 지정약국진료
                columnValueArr.add("A.designate_svc_yn = 'Y'");
                break;
            default : // 전체
        }

        // 가입승인
        switch (String.valueOf(params.get("auth"))) {
            case "Y" : // 승인완료
                columnValueArr.add("A.auth_yn = 'Y'");
                break;
            case "N" : // 미승인
                columnValueArr.add("A.auth_yn = 'N'");
                break;
            default : // 전체
        }

        return String.join(" AND ", columnValueArr);
    }


    // 약국 승인상태 변경
    public int changePharmacyAuth(int idx, String authYn) {
        return aPharmacyMapper.changePharmacyAuth(idx, authYn);
    }

    // 약국 계정정보
    public List<Pharmacist> getPharmacyAccount(int idx) {
        return aPharmacyMapper.getPharmacyAccount(idx);
    }

    // 약국 정보 상세
    public Pharmacy getPharmacyInfo(int idx) {
        Pharmacy pharmacyInfo = pharmacistMapper.getPharmacyInfo(idx);
        // dto to map
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, String> info = objectMapper.convertValue(pharmacyInfo, Map.class);
        log.info("약국정보" + info);

        return pharmacyInfo;
    }

    // 병원정보상세 첨부파일
    public Map<String, String> ajaxPharmacyImgFiles(Pharmacy info) {
        Map<String, String> files = new HashMap<>();

        // 사업자등록증
        files.put("brNoImg", info.getBrNoImg());
        files.put("brNoImgName", info.getBrNoImgName());
        if (info.getBrNoImg() != null && info.getBrNoImgName() == null) {
            int _lastIndex = info.getBrNoImg().lastIndexOf(".");
            String _ext = info.getBrNoImg().substring(_lastIndex + 1);
            files.put("brNoImgName", "사업자등록증." + _ext);
        }

        // 통신서비스 이용증명원
        files.put("svcCertImg", info.getSvcCertImg());
        files.put("svcCertImgName", info.getSvcCertImgName());
        if (info.getSvcCertImg() != null && info.getSvcCertImgName() == null) {
            int _lastIndex = info.getSvcCertImg().lastIndexOf(".");
            String _ext = info.getSvcCertImg().substring(_lastIndex + 1);
            files.put("svcCertImgName", "통신서비스이용증명원." + _ext);
        }

        // 대표이미지
        files.put("mainImg", info.getMainImg());
        files.put("mainImgName", null);
        if (info.getMainImg() != null) {
            int _lastIndex = info.getMainImg().lastIndexOf(".");
            String _ext = info.getMainImg().substring(_lastIndex + 1);
            files.put("mainImgName", "대표이미지." + _ext);
        }

        return files;
    }

    // 약국관리 - 약국정보 수정요청
    // 약국정보 수정요청 전체약국수
    public int getPharmacyUpdateTotalCount(Map<String, String> params) {
        String columnValue = commonWhereQuery(params);

        return aPharmacyMapper.getPharmacyUpdateTotalCount(columnValue);
    }

    // 약국정보 수정요청 목록
    public List<Map<String, Object>> getPharmacyUpdateList(int startRecord, int endRecord, Map<String, String> params) {
        String columnValue = commonWhereQuery(params);
        List<Map<String, Object>> pharmacyList = aPharmacyMapper.getPharmacyUpdateList(startRecord, endRecord, columnValue);

        for (Map<String, Object> pharmacy : pharmacyList) {
            // 수정요청일
            String requestDate = "";
            if(!isNull(pharmacy.get("regdate"))) {
                requestDate = String.valueOf(pharmacy.get("regdate")).substring(0, 10);
                requestDate = requestDate.replace("-", ".");
            }
            pharmacy.put("requestDate", requestDate);

            // null check
            if (pharmacy.get("telNo") == null) pharmacy.put("telNo", "");
            if (pharmacy.get("brNo") == null) pharmacy.put("brNo", "");
        }

        return pharmacyList;
    }

    // 약국정보 수정요청 상세정보
    public Map<String, Object> getPharmacyUpdateInfo(Integer idx) {
        PharmacyTmp pharmacy = pharmacistMapper.getPharmacyTmpInfo(idx); // 약국 정보
        Map<String, Object> result = new HashMap<>(); // returnData

        if (!Functions.isNull(pharmacy)) {
            result.put("idx", pharmacy.getIdx());
            result.put("nonfaceSvcYn", pharmacy.getNonfaceSvcYn());
            result.put("designateSvcYn", pharmacy.getDesignateSvcYn());
            result.put("pharmacyName", pharmacy.getPharmacyName());
            result.put("zipCode", pharmacy.getZipCode());
            result.put("mainAddr", pharmacy.getMainAddr());
            result.put("detailAddr", pharmacy.getDetailAddr());
            result.put("areaSi", pharmacy.getAreaSi());
            result.put("areaGu", pharmacy.getAreaGu());
            result.put("areaDong", pharmacy.getAreaDong());
            result.put("lat", pharmacy.getLat());
            result.put("lng", pharmacy.getLng());
            for(int i=0; i<3; i++) {
                result.put("telNo0"+(i+1), "");
                result.put("faxNo0"+(i+1), "");
            }
            if(!pharmacy.getTelNo().isEmpty()) {
                String[] telNo = pharmacy.getTelNo().split("-");
                for(int i=0; i<telNo.length; i++) {
                    result.put("telNo0"+(i+1), telNo[i]);
                }
            }
            if(!Functions.isNull(pharmacy.getFaxNo())) {
                String[] faxNo = pharmacy.getFaxNo().split("-");
                for(int i=0; i<faxNo.length; i++) {
                    result.put("faxNo0"+(i+1), faxNo[i]);
                }
            }
            result.put("brNo", pharmacy.getBrNo());
            result.put("file2", pharmacy.getBrNoImg());
            result.put("file2Name", pharmacy.getBrNoImgName());
            result.put("file3", pharmacy.getSvcCertImg());
            result.put("file3Name", pharmacy.getSvcCertImgName());
            result.put("file1", pharmacy.getMainImg());
        }

        return result;
    }

    // 약국정보 수정요청 승인
    public int pharmacyUpdateApproval(Integer pharmacyIdx) {
        int result = 0;

        // 승인 시 약국 임시테이블에 있는 정보를 약국 테이블에 UPDATE
        PharmacyTmp pharmacyTmp = pharmacistMapper.getPharmacyTmpInfo(pharmacyIdx); // 약국 임시테이블 정보
        if(!isNull(pharmacyIdx)) {
            result = aPharmacyMapper.pharmacyUpdateApproval(pharmacyIdx, pharmacyTmp); // 관리자 승인 시 약국 정보 UPDATE
            if(result != 0) {
                pharmacistMapper.deletePharmacyTmp(pharmacyIdx); // 약국 임시테이블 정보 삭제
            }
        }

        return result;
    }
}