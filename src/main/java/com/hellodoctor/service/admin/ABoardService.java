package com.hellodoctor.service.admin;

import com.hellodoctor.common.Constants;
import com.hellodoctor.common.Functions;
import com.hellodoctor.common.NCloudStorageUtils;
import com.hellodoctor.mapper.admin.ABoardMapper;
import com.hellodoctor.mapper.app.FileUploadMapper;
import com.hellodoctor.model.app.CSApp;
import com.hellodoctor.model.app.FileUpload;
import com.hellodoctor.model.hospital.AdminNotice;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.util.*;

/**
 * 커뮤니티 service 클래스
 */
@Service
@Slf4j
public class ABoardService {
    @Autowired
    FileUploadMapper fileUploadMapper;
    @Autowired
    NCloudStorageUtils nCloudStorageUtils;
    @Autowired
    ABoardMapper aBoardMapper;

    // 파일업로드 폴더명
    private String folderName = "csSite"; // cs문의

    // 리뷰 전체글수
    public int getReviewTotalCount(int hospitalIdx) {
        return aBoardMapper.getReviewTotalCount(hospitalIdx);
    }

    // 리뷰 목록
    public List<Map<String, String>> getReviewList(int hospitalIdx, int startRecord, int endRecord) {
        return aBoardMapper.getReviewList(hospitalIdx, startRecord, endRecord);
    }

    // 리뷰 상세
    public Map<String, String> getReviewDetail(int reviewIdx) {
        Map<String, String> review = aBoardMapper.getReviewDetail(reviewIdx);

        if (review != null && !review.isEmpty()) {
            // 환자정보
            // 1. 이름 (본인?가족?)
            String patientName = review.get("memberName");
            if (!String.valueOf(review.get("patientIdx")).equals("0")) {
                if (review.get("famName") != null) patientName = review.get("famName");
            }
            review.put("patientName", patientName);
            // 2. 질환정보 (질환명, 주요증상)
            String subjectCode = String.valueOf(review.get("subjectCode"));
            String subjectStr = Constants.MEDICAL_REQUEST_SUBJECT.get(Integer.parseInt(subjectCode));
            review.put("subjectStr", subjectStr);
            // 3. 질환정보 (첨부파일)
            if (review.get("reqImageList") == null) review.put("reqImageList", "");


            // 진료만족도
            String checkItemHtml = "";
            if (review.get("checkItem") != null) {
                String[] checkItems = String.valueOf(review.get("checkItem")).split(",");
                for (String item : checkItems) {
                    int code = Integer.parseInt(item);
                    checkItemHtml += "<div>" + Constants.REVIEW_CHECK_ITEM_CODE.get(code) + "</div>";
                }
            }
            review.put("checkItemHtml", checkItemHtml);

            // 답변
            if (review.get("answer") == null) review.put("answer", "");
            // 처방전이미지
            if (review.get("presImg") == null) review.put("presImg", "");
        }

        return review;
    }

    // 공지사항 전체글수
    public int getNoticeTotalCount(Map<String, String> params) {
        String columnValue = commonWhereQuery(params, "notice");
        return aBoardMapper.getNoticeTotalCount(columnValue);
    }

    // 공지사항 목록
    public List<AdminNotice> getNoticeList(int startRecord, int endRecord, Map<String, String> params) {
        String columnValue = commonWhereQuery(params, "notice");
        return aBoardMapper.getNoticeList(columnValue, startRecord, endRecord);
    }

    // 검색필터 공통쿼리조건
    public String commonWhereQuery(Map<String, String> params, String pageName) {
        List<String> columnValueArr = new ArrayList<>();
        String schTxt = params.get("stx"); // 키워드
        // log.info("commonWhereQuery = " + params);

        switch (pageName) {
            case "notice" :     // 공지사항
                if (!schTxt.equals("")) {
                    switch (params.get("sfl")) {
                        case "title":   // 제목
                            columnValueArr.add("title LIKE '%"+ schTxt +"%'");
                            break;
                        case "content": // 내용
                            columnValueArr.add("content LIKE '%"+ schTxt +"%'");
                            break;
                        default:        // 전체
                            columnValueArr.add("(title LIKE '%"+ schTxt +"%' OR content LIKE '%"+ schTxt +"%')");
                    }
                }
                // 열람대상
                if (!Functions.isNull(params.get("tgt"))) columnValueArr.add("read_target = '"+ params.get("tgt") +"'");

                break;

            case "APP" :        // CS문의 - 앱
                if (!schTxt.equals("")) {
                    switch (params.get("sfl")) {
                        case "name":    // 작성자
                            columnValueArr.add("B.mb_name LIKE '%"+ schTxt +"%'");
                            break;
                        case "tel":     // 연락처
                            columnValueArr.add("REPLACE(B.mb_hp, '-', '') LIKE '%"+ schTxt +"%'");
                            break;
                        case "content" :    // 내용
                            columnValueArr.add("A.content LIKE '%"+ schTxt +"%'");
                            break;
                    }
                }
                // 문의유형
                if (!Functions.isNull(params.get("svc"))) {
                    // if 배송문의 else 일반문의
                    if (params.get("svc").equals("0")) columnValueArr.add("service_type = '0'");
                    else columnValueArr.add("(service_type = '1' AND category = '"+ params.get("svc") +"')");
                }
                break;

            case "SITE" :       // CS문의 - 의/약
                if (!schTxt.equals("")) {
                    switch (params.get("sfl")) {
                        case "gname":    // 병원/약국명
                            columnValueArr.add("(B.hospital_name LIKE '%"+ schTxt +"%' OR C.pharmacy_name LIKE '%"+ schTxt +"%')");
                            break;
                        case "name":    // 작성자
                            String subQuery = "(";
                            subQuery += "B.idx IN (SELECT GROUP_CONCAT(hospital_idx) FROM hd_doctor WHERE user_name LIKE '%"+ schTxt +"%')";
                            subQuery += " OR ";
                            subQuery += "C.idx IN (SELECT GROUP_CONCAT(pharmacy_idx) FROM hd_pharmacist WHERE user_name LIKE '%"+ schTxt +"%')";
                            subQuery += ")";
                            columnValueArr.add(subQuery);
                            break;
                        case "tel":     // 연락처
                            columnValueArr.add("(REPLACE(B.tel_no, '-', '') LIKE '%"+ schTxt +"%' OR REPLACE(C.tel_no, '-', '') LIKE '%"+ schTxt +"%')");
                            break;
                        case "title" :    // 제목
                            columnValueArr.add("A.subject LIKE '%"+ schTxt +"%'");
                            break;
                        case "content" :    // 내용
                            columnValueArr.add("A.content LIKE '%"+ schTxt +"%'");
                            break;
                    }
                }
                // 문의유형
                if (!Functions.isNull(params.get("svc"))) {
                    columnValueArr.add("category = '"+ params.get("svc") +"'");
                }
                break;
        }

        return String.join(" AND ", columnValueArr);
    }

    // 공지사항 등록/수정 폼
    public AdminNotice getNoticeForm(int idx) {
        return aBoardMapper.getNoticeOne(idx);
    }

    // 공지사항 등록/수정 처리
    public int saveNoticeForm(AdminNotice form) {
        int idx = form.getIdx();
        // 등록:수정
        return (idx==0)? aBoardMapper.insertNotice(form) : aBoardMapper.updateNotice(form);
    }

    // 공지사항 삭제
    public int deleteNotice(int idx) {
        return aBoardMapper.deleteNotice(idx);
    }

    // CS문의 전체글수
    public int getCSTotalCount(Map<String, String> params) {
        String target = params.get("pageTarget");
        String columnValue = commonWhereQuery(params, target);
        return aBoardMapper.getCSTotalCount(columnValue, target);
    }

    // CS문의 목록
    public List<Map<String, String>> getCSList(int startRecord, int endRecord, Map<String, String> params) {
        String target = params.get("pageTarget");
        String columnValue = commonWhereQuery(params, target);
        List<Map<String, String>> csList = aBoardMapper.getCSList(columnValue, startRecord, endRecord, target);

        for (Map<String, String> cs : csList) {
            if (target.equals("APP")) { // 1. 앱
                // null check
                String[] nullCheckArr = {"content", "category", "answerDate"};
                for (String col : nullCheckArr) {
                    if (Functions.isNull(cs.get(col))) cs.put(col, "");
                }

                // 문의유형
                String categoryStr = "배송문의";
                if (cs.get("serviceType").equals("1")) {
                    if (cs.get("category").equals("")) categoryStr = "일반문의";
                    else categoryStr = Constants.CS_CATEGORY.get(Integer.parseInt(cs.get("category")));
                }
                cs.put("categoryStr", categoryStr);

            } else { // 2. 의/약
                // null check
                String[] nullCheckArr = {"groupType", "category", "subject", "answerDate", "hName", "hTelNo", "hUserName", "pName", "pTelNo", "pUserName"};
                for (String col : nullCheckArr) {
                    if (Functions.isNull(cs.get(col))) cs.put(col, "");
                }

                // 문의유형
                int cateKey = (!cs.get("category").equals(""))? Integer.parseInt(cs.get("category")) : 0;
                String categoryStr = (Constants.CS_WEB_CATEGORY.containsKey(cateKey))? Constants.CS_WEB_CATEGORY.get(cateKey) : "";
                cs.put("categoryStr", categoryStr);

                // 병원/약국 구분
                String groupName = cs.get("hName"); // 병원/약국명
                String writerName = cs.get("hUserName"); // 작성자명
                String telNo = cs.get("hTelNo"); // 연락처
                if (cs.get("groupType").equals("P")) {
                    groupName = cs.get("pName");
                    writerName = cs.get("pUserName");
                    telNo = cs.get("pTelNo");
                }
                cs.put("groupName", groupName);
                cs.put("writerName", writerName);
                cs.put("telNo", telNo);
            }

            /** 공통 **/
            // 답변여부
            String isAnswer = "Y";
            if (cs.get("answerDate").equals("")) isAnswer = "N";
            cs.put("isAnswer", isAnswer);
        }

        return csList;
    }

    // CS문의 상세보기
    public Map<String, String> getCSForm(int idx, String target) {
        Map<String, String> cs = aBoardMapper.getCSForm(idx, target);

        if (cs != null) {
            if (target.equals("APP")) { // 1. 앱
                // null check
                String[] nullCheckArr = {"content", "loginType", "imageList", "answer"};
                for (String col : nullCheckArr) {
                    if (Functions.isNull(cs.get(col))) cs.put(col, "");
                }

                // 문의유형
                String categoryStr = "배송문의";
                if (cs.get("serviceType").equals("1")) {
                    categoryStr = "일반문의";
                    if (!cs.get("category").equals(""))
                        categoryStr += " > " + Constants.CS_CATEGORY.get(Integer.parseInt(cs.get("category")));
                }
                cs.put("categoryStr", categoryStr);

                // 아이디 (로그인구분)
                String mbIdStr = cs.get("mbId");
                switch (cs.get("loginType")) {
                    case "K" : mbIdStr = "카카오 로그인"; break;
                    case "N" : mbIdStr = "네이버 로그인"; break;
                }
                cs.put("mbIdStr", mbIdStr);

            } else { // 의/약
                // null check
                String[] nullCheckArr = {"groupType", "category", "subject", "content", "answerDate", "hName", "hTelNo", "hUserName", "pName", "pTelNo", "pUserName", "hMainAddr", "pMainAddr", "answer", "answerFileCode"};
                for (String col : nullCheckArr) {
                    if (Functions.isNull(cs.get(col))) cs.put(col, "");
                }

                // 문의유형
                int cateKey = (!cs.get("category").equals(""))? Integer.parseInt(cs.get("category")) : 0;
                String categoryStr = (Constants.CS_WEB_CATEGORY.containsKey(cateKey))? Constants.CS_WEB_CATEGORY.get(cateKey) : "";
                cs.put("categoryStr", categoryStr);

                // 병원/약국 구분
                String groupName = cs.get("hName"); // 병원/약국명
                String writerName = cs.get("hUserName"); // 작성자명
                String telNo = cs.get("hTelNo"); // 연락처
                String mainAddr = cs.get("hMainAddr"); // 주소
                if (cs.get("groupType").equals("P")) {
                    groupName = cs.get("pName");
                    writerName = cs.get("pUserName");
                    telNo = cs.get("pTelNo");
                    mainAddr = cs.get("pMainAddr");
                }
                cs.put("groupName", groupName);
                cs.put("writerName", writerName);
                cs.put("telNo", telNo);
                cs.put("mainAddr", mainAddr);
            }
        }

        return cs;
    }

    // CS문의 답변등록 - 앱
    public int saveAnswerApp(Map<String, String> params) {
        return aBoardMapper.saveCSAnswer(params, "APP");
    }

    // CS문의 답변등록 - 의/약 (이미지 포함)
    public int saveAnswerSite(MultipartHttpServletRequest request) {
        Map<String, String> params = new HashMap<>();
        // 관리자이미지코드
        String answerFileCode = "";
        if (!Functions.isNull(request.getParameter("answerFileCode"))){
            answerFileCode = request.getParameter("answerFileCode");
        } else {
            // answerFileCode = Functions.addZero(5, doctorIdx);
            answerFileCode += Functions.randomString(5);
        }

        params.put("idx", request.getParameter("csIdx"));
        params.put("answer", request.getParameter("answerBox"));
        params.put("answerFileCode", request.getParameter("answerFileCode"));
        params.put("admIdx", request.getParameter("admIdx"));

        /**
         * 파일업로드 START
         * - 등록만 하는 경우
         * - multiple 구현 O
         * **/
        // 폴더생성
        nCloudStorageUtils.createFolder(folderName);
        int order = 1;
        List<FileUpload> uploadFiles = new ArrayList<>();

        // 업로드파일
        MultiValueMap<String, MultipartFile> multiFiles = request.getMultiFileMap();
        // 파일 요소 읽음
        Iterator<Map.Entry<String, List<MultipartFile>>> iterator = multiFiles.entrySet().iterator();
        List<MultipartFile> multipartFileList = new ArrayList<>();
        // 다중업로드 (multiple) MultipartFile 로 분리
        while (iterator.hasNext()) {
            Map.Entry<String, List<MultipartFile>> entry = iterator.next();
            List<MultipartFile> fileList = entry.getValue();
            for (MultipartFile file : fileList) {
                if (file.isEmpty()) continue;
                multipartFileList.add(file);
            }
        }
        // 첨부파일 체크
        if (multipartFileList.size() > 0) {
            for (MultipartFile file : multipartFileList) {
                //String elementName = file.getName(); // input name (imageFile[0], imageFile[1], ...)
                long fileSize = file.getSize();
                String fileOriginName = file.getOriginalFilename(); // input 원본파일명
                String fileExt = fileOriginName.substring(fileOriginName.lastIndexOf(".")+1); // 확장자

                if (fileSize > 0) { // 첨부파일이 존재하면
                    String _fileName = Functions.getTimeStamp() + order + "." + fileExt; // 파일명 생성
                    String url = nCloudStorageUtils.uploadFile(folderName, _fileName, file); // 업로드 처리

                    FileUpload fileUpload = new FileUpload();
                    fileUpload.setMatchCode(answerFileCode);
                    fileUpload.setTblName("csSite");
                    fileUpload.setFileUrl(url);
                    fileUpload.setOriginFileName(fileOriginName);
                    fileUpload.setSort(order);

                    uploadFiles.add(fileUpload);
                    order++;
                }
            }
        }
        /** 파일업로드 END **/

        // 답변등록
        int chk = 0;
        try {
            chk = aBoardMapper.saveCSAnswer(params, "SITE");
            if (chk > 0) {
                // 파일업로드
                if (uploadFiles.size() > 0) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("list", uploadFiles);
                    fileUploadMapper.insertUploadFiles(map);
                }

                // (수정시) 파일삭제
                String[] deleteImages = request.getParameterValues("delImg[]");
                // log.info("파일삭제확인" + String.valueOf(deleteImages));
                if (deleteImages != null && deleteImages.length > 0) {
                    for (String imgUrl : deleteImages) {
                        nCloudStorageUtils.deleteObject(imgUrl); // 서버삭제
                    }
                    List<String> delList = Arrays.asList(deleteImages);
                    fileUploadMapper.deleteUploadFiles(delList); // DB삭제
                }
            }
        } catch (Exception e) {
            log.info("saveAnswerSite() Exception=" + e);
        }

        return chk;
    }


}