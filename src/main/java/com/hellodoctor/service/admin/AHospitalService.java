package com.hellodoctor.service.admin;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hellodoctor.common.Functions;
import com.hellodoctor.mapper.admin.AHospitalMapper;
import com.hellodoctor.mapper.hospital.DoctorMapper;
import com.hellodoctor.mapper.hospital.HConfigMapper;
import com.hellodoctor.model.app.Hospital;
import com.hellodoctor.model.app.HospitalTmp;
import com.hellodoctor.model.hospital.Doctor;
import com.hellodoctor.model.hospital.HospitalSubject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.hellodoctor.common.Functions.isNull;

/**
 * 병원관리 service 클래스
 */
@Service
@Slf4j
public class AHospitalService {
    @Autowired
    AHospitalMapper aHospitalMapper;
    @Autowired
    DoctorMapper doctorMapper;
    @Autowired
    HConfigMapper hConfigMapper;

    // 병원관리 전체병원수
    public int getHospitalTotalCount(Map<String, String> params) {
        String columnValue = commonWhereQuery(params);
        return aHospitalMapper.getHospitalTotalCount(columnValue);
    }

    // 병원관리 목록
    public List<Map<String, Object>> getHospitalList(int startRecord, int endRecord, Map<String, String> params) {
        String columnValue = commonWhereQuery(params);
        List<Map<String, Object>> hospitalList = aHospitalMapper.getHospitalList(startRecord, endRecord, columnValue);

        for (Map<String, Object> hospital : hospitalList) {
            // 병원 진료 과목
            String subjectNames = "";
            if (!isNull(hospital.get("subjectCode"))) {
                String _subjectNames = Functions.getHospitalSubjectNames(String.valueOf(hospital.get("subjectCode")));
                String[] _split = _subjectNames.split(",");
                subjectNames = _split[0];
                if (_split.length > 1) subjectNames += "<br>외 " + (_split.length-1);
            }
            hospital.put("subjectNames", subjectNames); // ex. 피부과 외1

            // 승인일자 포맷변경 (yyyy.mm.dd)
            if (!isNull(hospital.get("authDate"))) {
                String _authDate = String.valueOf(hospital.get("authDate")).substring(0, 10);
                _authDate = _authDate.replace("-", ".");
                hospital.put("authDate", _authDate);
            }

            // null check
            if (hospital.get("authYn") == null) hospital.put("authYn", "N");
            if (hospital.get("telNo") == null) hospital.put("telNo", "");
            if (hospital.get("brNo") == null) hospital.put("brNo", "");
            if (hospital.get("authDate") == null) hospital.put("authDate", "");
        }
        return hospitalList;
    }

    // 병원관리 전체병원수, 목록 쿼리조건
    public String commonWhereQuery(Map<String, String> params) {
        List<String> columnValueArr = new ArrayList<>();
        String schTxt = params.get("stx"); // 키워드
        log.info("params" + params);

        // 키워드검색
        if (!schTxt.equals("")) {
            switch (params.get("sfl")) {
                case "name" :   // 병원명
                    columnValueArr.add("A.hospital_name LIKE '%"+ schTxt +"%'");
                    break;
                case "tel" :   // 연락처
                    columnValueArr.add("REPLACE(A.tel_no, '-', '') LIKE '%"+ schTxt +"%'");
                    break;
                case "addr" :    // 주소
                    String str = "(";
                    str += "A.main_addr LIKE '%"+ schTxt +"%'";
                    str += " OR A.detail_addr LIKE '%"+ schTxt + "%'";
                    str += " OR A.area_dong LIKE '%"+ schTxt + "%'";
                    str += ")";
                    columnValueArr.add(str);
                    break;
            }
        }

        // 서비스종류
        switch (String.valueOf(params.get("svc"))) {
            case "1" : // 비대면진료
                columnValueArr.add("A.nonface_svc_yn = 'Y'");
                break;
            case "2" : // 지정환자 비대면진료
                columnValueArr.add("A.designate_svc_yn = 'Y'");
                break;
            default : // 전체
        }

        // 가입승인
        switch (String.valueOf(params.get("auth"))) {
            case "Y" : // 승인완료
                columnValueArr.add("A.auth_yn = 'Y'");
                break;
            case "N" : // 미승인
                columnValueArr.add("A.auth_yn = 'N'");
                break;
            default : // 전체
        }

        return String.join(" AND ", columnValueArr);
    }

    // 병원 승인상태 변경
    public int changeHospitalAuth(int idx, String authYn) {
        return aHospitalMapper.changeHospitalAuth(idx, authYn);
    }

    // 병원 계정정보
    public List<Doctor> getHospitalAccount(int idx) {
        return aHospitalMapper.getHospitalAccount(idx);
    }

    // 병원정보상세
    public Hospital getHospitalInfo(int idx) {
        Hospital hospitalInfo = doctorMapper.getHospitalInfo(idx);
        // dto to map
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, String> info = objectMapper.convertValue(hospitalInfo, Map.class);
        log.info("병원정보" + info);

        return hospitalInfo;
    }

    // 병원정보상세 첨부파일
    public Map<String, String> getHospitalImgFiles(Hospital info) {
        Map<String, String> files = new HashMap<>();

        // 사업자등록증
        files.put("brNoImg", info.getBrNoImg());
        files.put("brNoImgName", info.getBrNoImgName());
        if (info.getBrNoImg() != null && info.getBrNoImgName() == null) {
            int _lastIndex = info.getBrNoImg().lastIndexOf(".");
            String _ext = info.getBrNoImg().substring(_lastIndex + 1);
            files.put("brNoImgName", "사업자등록증." + _ext);
        }

        // 통신서비스 이용증명원
        files.put("svcCertImg", info.getSvcCertImg());
        files.put("svcCertImgName", info.getSvcCertImgName());
        if (info.getSvcCertImg() != null && info.getSvcCertImgName() == null) {
            int _lastIndex = info.getSvcCertImg().lastIndexOf(".");
            String _ext = info.getSvcCertImg().substring(_lastIndex + 1);
            files.put("svcCertImgName", "통신서비스이용증명원." + _ext);
        }

        // 대표이미지
        files.put("mainImg", info.getMainImg());
        files.put("mainImgName", null);
        if (info.getMainImg() != null) {
            int _lastIndex = info.getMainImg().lastIndexOf(".");
            String _ext = info.getMainImg().substring(_lastIndex + 1);
            files.put("mainImgName", "대표이미지." + _ext);
        }

        return files;
    }

    // 병원관리 - 병원정보 수정요청
    // 병원정보 수정요청 전체병원수
    public int getHospitalUpdateTotalCount(Map<String, String> params) {
        String columnValue = commonWhereQuery(params);

        return aHospitalMapper.getHospitalUpdateTotalCount(columnValue);
    }

    // 병원정보 수정요청 목록
    public List<Map<String, Object>> getHospitalUpdateList(int startRecord, int endRecord, Map<String, String> params) {
        String columnValue = commonWhereQuery(params);
        List<Map<String, Object>> hospitalList = aHospitalMapper.getHospitalUpdateList(startRecord, endRecord, columnValue);

        for (Map<String, Object> hospital : hospitalList) {
            log.info("hospital: "+hospital);
            // 병원 진료 과목
            String subjectNames = "";
            if (!isNull(hospital.get("subjectCode"))) {
                String _subjectNames = Functions.getHospitalSubjectNames(String.valueOf(hospital.get("subjectCode")));
                String[] _split = _subjectNames.split(",");
                subjectNames = _split[0];
                if (_split.length > 1) subjectNames += "<br>외 " + (_split.length-1);
            }
            hospital.put("subjectNames", subjectNames); // ex. 피부과 외1

            // 수정요청일
            String requestDate = "";
            if(!isNull(hospital.get("regdate"))) {
                requestDate = String.valueOf(hospital.get("regdate")).substring(0, 10);
                requestDate = requestDate.replace("-", ".");
            }
            hospital.put("requestDate", requestDate);

            // null check
            if (hospital.get("telNo") == null) hospital.put("telNo", "");
            if (hospital.get("brNo") == null) hospital.put("brNo", "");
        }

        return hospitalList;
    }

    // 병원정보 수정요청 상세정보
    public Map<String, Object> getHospitalUpdateInfo(Integer idx) {
        HospitalTmp hospital = doctorMapper.getHospitalTmpInfo(idx); // 병원 정보
        HospitalSubject subject = doctorMapper.getSubjectInfo(idx, "hd_hospital_subject_tmp"); // 병원-진료과목 정보
        Map<String, Object> result = new HashMap<>(); // returnData

        if (!Functions.isNull(hospital)) {
            result.put("idx", hospital.getIdx());
            result.put("nonfaceSvcYn", hospital.getNonfaceSvcYn());
            result.put("designateSvcYn", hospital.getDesignateSvcYn());
            result.put("hospitalName", hospital.getHospitalName());
            result.put("zipCode", hospital.getZipCode());
            result.put("mainAddr", hospital.getMainAddr());
            result.put("detailAddr", hospital.getDetailAddr());
            result.put("areaSi", hospital.getAreaSi());
            result.put("areaGu", hospital.getAreaGu());
            result.put("areaDong", hospital.getAreaDong());
            result.put("lat", hospital.getLat());
            result.put("lng", hospital.getLng());
            for(int i=0; i<3; i++) {
                result.put("telNo0"+(i+1), "");
                result.put("faxNo0"+(i+1), "");
            }
            if(!hospital.getTelNo().isEmpty()) {
                String[] telNo = hospital.getTelNo().split("-");
                for(int i=0; i<telNo.length; i++) {
                    result.put("telNo0"+(i+1), telNo[i]);
                }
            }
            if(!Functions.isNull(hospital.getFaxNo())) {
                String[] faxNo = hospital.getFaxNo().split("-");
                for(int i=0; i<faxNo.length; i++) {
                    result.put("faxNo0"+(i+1), faxNo[i]);
                }
            }
            result.put("brNo", hospital.getBrNo());
            result.put("file2", hospital.getBrNoImg());
            result.put("file2Name", hospital.getBrNoImgName());
            result.put("file3", hospital.getSvcCertImg());
            result.put("file3Name", hospital.getSvcCertImgName());
            result.put("file1", hospital.getMainImg());
            result.put("introduce", hospital.getIntroduce());
            result.put("nonReimbJson", hospital.getNonReimbJson());
            String subjectCode = "";
            if(!Functions.isNull(subject)) subjectCode = subject.getSubjectCode();
            result.put("subjectCode", subjectCode); // 병원-진료과목
        }

        return result;
    }

    // 병원정보 수정요청 승인
    public int hospitalUpdateApproval(Integer hospitalIdx) {
        int result = 0;

        // 승인 시 병원 임시테이블에 있는 정보를 병원 테이블에 UPDATE
        HospitalTmp hospitalTmp = doctorMapper.getHospitalTmpInfo(hospitalIdx); // 병원 임시테이블 정보
        if(!isNull(hospitalIdx)) {
            result = aHospitalMapper.hospitalUpdateApproval(hospitalIdx, hospitalTmp); // 관리자 승인 시 병원 정보 UPDATE
            if(result != 0) {
                HospitalSubject subject = doctorMapper.getSubjectInfo(hospitalIdx, "hd_hospital_subject_tmp"); // 관리자 승인 시 병원-진료과목 정보 UPDATE
                String[] subjectCodeArr = new String[0];
                if(!Functions.isNull(subject)) subjectCodeArr = subject.getSubjectCode().split(",");
                doctorMapper.insertSubject(hospitalIdx, subjectCodeArr, "hd_hospital_subject");
                doctorMapper.deleteHospitalTmp(hospitalIdx); // 병원 임시테이블 정보 삭제
            }
        }

        return result;
    }
}