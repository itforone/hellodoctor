package com.hellodoctor.service.admin;

import com.hellodoctor.common.Constants;
import com.hellodoctor.common.Functions;
import com.hellodoctor.mapper.admin.ASystemMapper;
import com.hellodoctor.mapper.app.LoginMapper;
import com.hellodoctor.model.admin.Admin;
import com.hellodoctor.model.admin.Terms;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 시스템관리 service 클래스
 */
@Service
@Slf4j
public class ASystemService {
    @Autowired
    ASystemMapper aSystemMapper;
    @Autowired
    LoginMapper loginMapper;
    @Autowired
    PasswordEncoder passwordEncoder;

    // 약관 목록
    public List<Map<String, String>> getTermsList() {
        List<Terms> getTerms = aSystemMapper.getTermsList();
        // Map<String, String> terms = new HashMap<>();
        List<Map<String, String>> termsList = new ArrayList<>();

        for (String key : Constants.SITE_TERMS_LIST.keySet()) {
            /*String content = "";
            for (Terms t : getTerms) {
                if (t.getTermsType().equals(key) && t.getTermsType() != null) content = t.getTermsType();
            }
            terms.put(key, content);*/

            Map<String, String> map = new HashMap<>();
            map.put("key", key); // 조회 키
            map.put("name", Constants.SITE_TERMS_LIST.get(key)); // 약관명
            map.put("required", "N"); // 필수여부 (Y,N)
            map.put("content", ""); // 내용

            for (Terms t : getTerms) {
                if (t.getTermsType().equals(key) && t.getTermsType() != null) {
                    // 내용 등록됨
                    map.put("content", t.getContent());
                    map.put("required", t.getRequiredYn());
                }
            }

            termsList.add(map);
        }

        return termsList;
    }

    // 약관 1개 조회
    public Terms getTerms(String key) {
        return aSystemMapper.getTerms(key);
    }

    // 약관 등록/수정
    public int saveTerms(Terms form) {
        return (form.getIdx()==0)? aSystemMapper.insertTerms(form) : aSystemMapper.updateTerms(form);
    }

    // 관리자정보 조회
    public Admin getAdminAccount(String id) {
        return loginMapper.getAdminAccount(id);
    }

    // 아이디 중복체크
    public int adminIdCheck(String id) {
        return aSystemMapper.adminIdCheck(id);
    }

    // 관리자 등록/수정
    public int saveAdmin(Admin form, HttpServletRequest request) {
        // 클라이언트 IP
        // form.setLoginIp(request.getRemoteAddr());
        // 비밀번호 인코딩
        if (!Functions.isNull(request.getParameter("password"))) {
            form.setUserPassword(passwordEncoder.encode(request.getParameter("password")));
        }

        return (form.getIdx()==0)? aSystemMapper.insertAdmin(form) : aSystemMapper.updateAdmin(form);
    }

    // 관리자 삭제
    public int deleteAdmin(int idx) {
        return aSystemMapper.deleteAdmin(idx);
    }

    // 관리자 목록
    public List<Admin> getAdminList() {
        return aSystemMapper.getAdminList();
    }



}