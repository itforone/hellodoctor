package com.hellodoctor.service.admin;

import com.hellodoctor.mapper.admin.AReserveMapper;
import com.hellodoctor.model.admin.Admin;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.hellodoctor.common.Constants.MEDICAL_REQUEST_SUBJECT;
import static com.hellodoctor.common.Functions.ChangeDateFormat;
import static com.hellodoctor.common.Functions.isNull;

/**
 * 예약정보관리 service 클래스
 */
@Service
@Slf4j
public class AReserveService {
    @Autowired
    AReserveMapper aReserveMapper;

    // 예약접수관리 목록 수
    public Map<String, String> getReserveListCount(Admin admin, HttpServletRequest request) {
        // 검색
        String sqlSearch = getReserveListSearch(request);

        // 접수 목록
        List<Map<String, String>> list = aReserveMapper.getReserveListCount(sqlSearch); // (검색조건)

        int statusR = 0;
        int statusY = 0;
        int statusN = 0;
        for (Map<String, String> data : list) {
            // 접수상태 카운트 R:대기 Y:접수 N:거부
            if(data.get("medicalStatus").equals("C") || data.get("hospitalRcptYn").equals("N")) ++statusN;
            else if(data.get("hospitalRcptYn").equals("Y")) ++statusY;
            else if(data.get("hospitalRcptYn").equals("R")) ++statusR;
        }

        Map<String, String> returnList = new HashMap<>();

        // 접수상태 카운트 R:대기 Y:접수 N:거부
        returnList.put("statusR", String.valueOf(statusR)); // 대기
        returnList.put("statusY", String.valueOf(statusY)); // 접수
        returnList.put("statusN", String.valueOf(statusN)); // 완료

        // 총접수
        int total = statusR + statusY + statusN;
        returnList.put("listTotalCnt", String.valueOf(total));

        return returnList;
    }

    // 예약접수관리 목록
    public Map<String, Object> getReserveList(Admin admin, HttpServletRequest request, int startRecord, int endRecord) {
        Map<String, Object> returnList = new HashMap<>();

        // 검색
        String sqlSearch = getReserveListSearch(request);

        // limit
        if(isNull(request.getParameter("limit"))) endRecord = 15;
        else endRecord = Integer.parseInt(request.getParameter("limit"));

        // 진료 예약 목록
        List<Map<String, String>> list = aReserveMapper.getReserveList(sqlSearch, startRecord, endRecord); // (검색조건, limit start, limit end)

        List<Object> tempList = new LinkedList<>();
        for (Map<String, String> data : list) {
            // log.info("data: "+data.toString());

            // 접수시간
            String regdate = String.valueOf(data.get("regdate")).replace("T", " ");
            String receiptTime = ChangeDateFormat(regdate, "HH:mm");
            data.put("receiptTime", receiptTime);

            // 질환
            String subjectName = "";
            if(!isNull(data.get("subjectCode"))) subjectName = MEDICAL_REQUEST_SUBJECT.get(Integer.parseInt(String.valueOf(data.get("subjectCode")))) + " 질환";
            data.put("subjectName", subjectName);

            // 환자이름/관계/연락처/주민등록번호
            // 관계가 본인이면 hd_medical(진료) 데이터 본인 외이면 hd_member_family(가족) 데이터
            String relationship = "본인";
            String patientName = data.get("patientName");
            String patientHp = data.get("patientHp");
            String patientRrNo = data.get("patientRrNo");
            if(Integer.parseInt(String.valueOf(data.get("patientIdx"))) != 0) {
                int famRel = Integer.parseInt(data.get("famRel"));
                if(famRel == 1) relationship = "부모";
                else if(famRel == 2) relationship = "배우자";
                else if(famRel == 3) relationship = "자녀";
            }
            data.put("patientName", patientName); // 환자이름
            data.put("relationship", relationship); // 관계
            data.put("patientHp", patientHp); // 연락처
            data.put("patientRrNo", patientRrNo); // 주민등록번호

            // 진료구분
            // 접수번호에서 추출 - F: 초진 S: 재진 (3개월 내 같은 질환으로 2회 이상 진료 예약한 환자)
            String rcptNoArr = data.get("rcptNo").substring(7, 8);
            String medicalType = rcptNoArr.equals("F") ? "초진" : "재진";
            data.put("medicalType", medicalType);

            // 예약희망시간 15분 단위로 추출
            String hopeTime = data.get("appStartHour") + "~" + data.get("appEndHour");
            String[] hopeTimeSpilt = hopeTime.split("~");
            String[] hopeHour = hopeTimeSpilt[0].split(":");
            String[] hospitalAvailTime = new String[3];
            hospitalAvailTime[0] = hopeTimeSpilt[0];
            hospitalAvailTime[2] = hopeTimeSpilt[1];
            // 10:30~10:30 체크 ? 10:15 : 10:45
            hospitalAvailTime[1] = (hopeTimeSpilt[0].contains(":00"))? hopeHour[0] + ":" + "15" : hopeHour[0] + ":" + "45";
            data.put("hopeHourArr", String.join(",", hospitalAvailTime));

            // 상태 - 접수 시
            String medicalTime = "";
            if(!isNull(data.get("medicalTime"))) medicalTime = data.get("medicalTime");
            data.put("medicalTime", medicalTime); // 예약완료시간 = 진료시간

            // 상태 - 거부 시
            String cancelDate = "";
            String cancelReason = "";
            if(!isNull(data.get("cancelDate"))) {
                cancelDate = ChangeDateFormat(String.valueOf(data.get("cancelDate")).replace("T", " "), "yyyy-MM-dd");
                cancelReason = data.get("cancelReason");
            }
            data.put("cancelDate", cancelDate); // 취소일
            data.put("cancelReason", cancelReason); // 취소사유

            tempList.add(data);
        }
        returnList.put("listData", tempList);

        // log.info("returnList: "+returnList);
        return returnList;
    }

    // 예약접수관리 검색
    public String getReserveListSearch(HttpServletRequest request) {
        String _search = "";

        // 접수일
        String _receiptDate = "";
        if(!isNull(request.getParameter("schDate"))) {
            _receiptDate = request.getParameter("schDate");
            _search += " AND A.regdate LIKE '%"+_receiptDate+"%'";
        }

        // 예약현황
        String _schStatus = "";
        if(!isNull(request.getParameter("schStatus"))) {
            _schStatus = request.getParameter("schStatus");
            // 전체/공통조건 : 회원 신청 !=T AND 당일
            // 진료요청 : 병원 접수=R AND 회원 신청=R
            // 예약완료 : 병원 접수=Y AND 회원 신청=R
            // 진료취소 : 병원 접수=N OR 회원 신청=C
            if(_schStatus.equals("진료요청")) {
                _search += " AND (A.hospital_rcpt_yn = 'R' AND A.medical_status = 'R')";
            } else if(_schStatus.equals("예약완료")) {
                _search += " AND (A.hospital_rcpt_yn = 'Y' AND A.medical_status != 'C')";
            } else if(_schStatus.equals("진료취소")) {
                _search += " AND (A.hospital_rcpt_yn = 'N' OR A.medical_status = 'C')";
            }
        }

        // 검색조건
        String _item = "";
        String _keyword = "";
        if(!isNull(request.getParameter("schKeyword"))) {
            _item = request.getParameter("schItem");
            _keyword = request.getParameter("schKeyword");
            _search += " HAVING "+_item+" LIKE '%"+_keyword+"%'";
        }

        return _search;
    }
}