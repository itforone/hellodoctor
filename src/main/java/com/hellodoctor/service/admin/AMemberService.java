package com.hellodoctor.service.admin;

import com.hellodoctor.common.Functions;
import com.hellodoctor.mapper.admin.AMemberMapper;
import com.hellodoctor.model.app.MemberFamily;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 회원관리 service 클래스
 */
@Service
@Slf4j
public class AMemberService {
    @Autowired
    AMemberMapper aMemberMapper;

    // 회원관리 전체회원수
    public int getMemberTotalCount(Map<String, String> params) {
        String columnValue = commonWhereQuery(params);
        return aMemberMapper.getMemberTotalCount(columnValue);
    }

    // 회원관리 목록
    public List<Map<String, Object>> getMemberList(int startRecord, int endRecord, Map<String, String> params) {
        String columnValue = commonWhereQuery(params);
        List<Map<String, Object>> memberList = aMemberMapper.getMemberList(startRecord, endRecord, columnValue);
        // null check
        for (Map<String, Object> member : memberList) {
            if (member.get("mbNo")==null) member.put("mbNo", "");
            if (member.get("mbCertify")==null) member.put("mbCertify", "");
            if (member.get("mbHp")==null) member.put("mbHp", "");
            if (member.get("hospitalName")==null) member.put("hospitalName", "");
        }
        return memberList;
    }

    // 회원관리 전체회원수, 목록 쿼리조건
    public String commonWhereQuery(Map<String, String> params) {
        List<String> columnValueArr = new ArrayList<>();
        String schTxt = params.get("stx"); // 키워드
        // sfl, stx, listRow

        if (!schTxt.equals("")) {
            switch (params.get("sfl")) {
                case "num" :   // 회원번호
                    columnValueArr.add("A.mb_no LIKE '%"+ schTxt +"%'");
                    break;
                case "name" :   // 회원명
                    columnValueArr.add("A.mb_name LIKE '%"+ schTxt +"%'");
                    break;
                case "email" :   // 이메일(아이디)
                    columnValueArr.add("A.mb_id LIKE '%"+ schTxt +"%'");
                    break;
                case "hp" :    // 연락처
                    columnValueArr.add("REPLACE(A.mb_hp, '-', '') LIKE '%"+ schTxt +"%'");
                    break;
            }
        }

        return String.join(" AND ", columnValueArr);
    }

    // 회원관리 상세보기
    public Map<String, Object> getMemberInfo(int idx) {
        Map<String, Object> member = aMemberMapper.getMemberInfoByIdx(idx);

        if (member != null) {
            String[] mbHpSplit = (!Functions.isNull(member.get("mbHp"))) ? String.valueOf(member.get("mbHp")).split("-") : new String[3];
            String[] mbHpArr = new String[3];
            for (int i = 0; i < 3; i++) {
                mbHpArr[i] = (!Functions.isNull(mbHpSplit[i])) ? mbHpSplit[i] : "";
            }
            member.put("mbHpArr", mbHpArr);

            if (member.get("mbNo") == null) member.put("mbNo", "");
            if (member.get("mbCertify") == null) member.put("mbCertify", "");
            if (member.get("hospitalName") == null) member.put("hospitalName", "");
        }

        return member;
    }


}