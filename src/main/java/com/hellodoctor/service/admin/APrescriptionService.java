package com.hellodoctor.service.admin;

import com.hellodoctor.mapper.admin.APrescriptionMapper;
import com.hellodoctor.mapper.hospital.HRemoteMapper;
import com.hellodoctor.model.admin.Admin;
import com.hellodoctor.model.app.Medical;
import com.hellodoctor.model.hospital.MedicalPrescription;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.hellodoctor.common.Functions.isNull;

/**
 * 처방전관리 service 클래스
 */

@Service
@Slf4j
public class APrescriptionService {
    @Autowired
    APrescriptionMapper aPrescriptionMapper;
    @Autowired
    HRemoteMapper hRemoteMapper;

    // 처방전관리 목록 수
    public Map<String, String> getPxListCount(Admin admin, HttpServletRequest request) {
        // 검색
        String sqlSearch = getPxListSearch(request);

        List<Map<String, String>> list = aPrescriptionMapper.getPxListCount(sqlSearch); // (검색조건)

        int total = 0;
        int requestCnt = 0;
        int completeCnt = 0;
        for (Map<String, String> data : list) {
            total++;
            // 처방전 상태 카운트 requestCnt: 요청 completeCnt: 완료
            requestCnt += Integer.parseInt(String.valueOf(data.get("requestCnt")));
            completeCnt += Integer.parseInt(String.valueOf(data.get("completeCnt")));
        }

        Map<String, String> returnList = new HashMap<>();
        returnList.put("requestCnt", String.valueOf(requestCnt)); // 요청
        returnList.put("completeCnt", String.valueOf(completeCnt)); // 완료

        // 총 접수 (처방전 수)
        returnList.put("listTotalCnt", String.valueOf(total));

        return returnList;
    }

    // 처방전관리 목록
    public Map<String, Object> getPxList(Admin admin, HttpServletRequest request, int startRecord, int endRecord) {
        Map<String, Object> returnList = new HashMap<>();

        // 검색
        String sqlSearch = getPxListSearch(request);

        // limit
        if(isNull(request.getParameter("limit"))) endRecord = 15;
        else endRecord = Integer.parseInt(request.getParameter("limit"));

        List<Map<String, String>> list = aPrescriptionMapper.getPxList(sqlSearch, startRecord, endRecord); // (검색조건, limit start, limit end)

        List<Object> tempList = new LinkedList<>();
        for (Map<String, String> data : list) {
            // 접수일
            String receiptDate = String.valueOf(data.get("regdate")).substring(0, 10);
            data.put("receiptDate", receiptDate);

            // 환자이름/연락처/주민등록번호
            String patientName = data.get("patientName");
            String patientHp = data.get("patientHp");
            String patientRrNo = data.get("patientRrNo");
            data.put("patientName", patientName); // 환자이름
            data.put("patientHp", patientHp); // 연락처
            data.put("patientRrNo", patientRrNo); // 주민등록번호

            // 처방전 재발행 요청일
            String reRequestDate = "";
            if(!isNull(data.get("reRequestDate"))) reRequestDate = String.valueOf(data.get("reRequestDate")).substring(0, 16);
            data.put("reRequestDate", reRequestDate);

            // 재발행접수번호
            String reRcptNo = "";
            if(!isNull(data.get("reRcptNo"))) reRcptNo = data.get("reRcptNo");
            data.put("reRcptNo", reRcptNo);

            if (isNull(data.get("rcptNo"))) data.put("rcptNo", "");
            if (isNull(data.get("regdate"))) data.put("receiptDate", "");
            if (isNull(data.get("patientName"))) data.put("patientName", "");
            if (isNull(data.get("patientHp"))) data.put("patientHp", "");
            if (isNull(data.get("patientRrNo"))) data.put("patientRrNo", "");
            if (isNull(data.get("presStatus"))) data.put("presStatus", "");

            tempList.add(data);
        }
        returnList.put("listData", tempList);

        return returnList;
    }

    // 처방전관리 검색 조건 (쿼리)
    public String getPxListSearch(HttpServletRequest request) {
        String _search = "";

        // 접수일
        String _searchDate = "";
        if(!isNull(request.getParameter("schDate"))) {
            _searchDate = request.getParameter("schDate");
            _search += " AND A.regdate LIKE '%"+_searchDate+"%'";
        }

        // 검색조건
        String _item = "";
        String _keyword = "";
        if(!isNull(request.getParameter("schKeyword"))) {
            _item = request.getParameter("schItem");
            _keyword = request.getParameter("schKeyword");
            _search += " HAVING "+_item+" LIKE '%"+_keyword+"%'";
        }

        return _search;
    }

    // 처방전 재발행 요청
    public int rePublishRequest(int presIdx) {
        /**
         * TBC: 프로세스 재확인 필요
         * 1. 진료가 완료된 전체 처방전 목록 출력 (진료 완료 전에는 처방전 수정할 수 있기 때문)
         * 2. 처방전 재발행 요청 시 재발생 접수번호 생성
         *    hd_medical_prescription(처방전) INSERT
         *    hd_medical - 초기 pres_idx(처방전idx) UPDATE
         * 3. 처방전 상태 재발행 요청(pres_status = 1)으로 변경
         * 4. 의사-처방전 재발행 메뉴에서 재발행 상태가 요청 및 완료 (pres_status != 0) 처방전 목록 출력
         * 5. 의사-처방전 재등록 후 발행 시
         *    hd_medical - pres_idx(처방전idx) UPDATE
         *    hd_medical_prescription - re_complete_date(재발행완료일) UPDATE
         *    hd_pharmacy_request(약조제요청) 접수완료/결제완료 상태로 INSERT
         */

        // 1. 처방전 정보 확인
        MedicalPrescription medicalPrescription = aPrescriptionMapper.getPrescriptionInfo(presIdx);
        int medicalIdx = medicalPrescription.getMedicalIdx(); // 진료idx

        // 2. (원)접수번호 확인
        Medical medical = hRemoteMapper.getMedicalInfo(medicalIdx);
        String rcptNo = medical.getRcptNo();

        // 3. 재발행 처방전 등록
        // 3-1. 몇번째 재발행인지 확인하여 접수번호 생성
        int count = aPrescriptionMapper.getPrescriptionCount(medicalIdx);
        String reRcptNo = "";
        if(!isNull(rcptNo)) reRcptNo = rcptNo + "R" + count;
        // 3-2. 재발행 처방전 DB 등록
        medicalPrescription.setReRcptNo(reRcptNo);
        medicalPrescription.setBeforePresIdx(presIdx);
        int result = aPrescriptionMapper.insertReissuePrescription(medicalPrescription);
        if(result != 0) {
            // 4. 원 처방전 재발행 여부 상태 변경
            String columnValue = "is_reissue = 'Y'";
            result = aPrescriptionMapper.updatePrescription(presIdx, columnValue);

            // 5. 진료정보 초기 처방전 idx update
            if(count == 1) { // 처음 재발행 시작 시에만 update 하면 됨
                int firstPresIdx = medical.getPresIdx();
                String columnValue2 = "before_pres_idx = " + firstPresIdx;
                hRemoteMapper.updateMedical(medicalIdx, columnValue2);
            }
        }

        return result;
    }
}