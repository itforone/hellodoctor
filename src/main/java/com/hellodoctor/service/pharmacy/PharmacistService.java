package com.hellodoctor.service.pharmacy;

import com.hellodoctor.common.Functions;
import com.hellodoctor.common.NCloudStorageUtils;
import com.hellodoctor.mapper.pharmacy.PharmacistMapper;
import com.hellodoctor.model.app.Pharmacy;
import com.hellodoctor.model.pharmacy.Pharmacist;
import com.hellodoctor.service.hospital.DoctorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * 약사 관련 service 클래스
 */

@Service
@Slf4j
public class PharmacistService {
    @Autowired
    PharmacistMapper pharmacistMapper;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    NCloudStorageUtils nCloudStorageUtils;
    @Autowired
    DoctorService doctorService;

    // 파일업로드 폴더명
    private String folderName = "pharmacist"; // 약사
    private String folderName2 = "pharmacy"; // 약국

    // 아이디찾기 - 연락처비교
    public Map<String, String> hpCheck(String hp, String email) {
        Map<String, String> result = new HashMap<>();

        Map<String, String> userData = pharmacistMapper.hpCheck(hp, email);
        if (userData == null) result.put("flag", "N");
        else {
            result.put("flag", "Y");
            result.put("email", userData.get("email"));
            result.put("regdate", String.valueOf(userData.get("regdate")));
        }

        // TODO: 문자발송 추가
        String certNo = Functions.issueCertNo(4);
        result.put("certNo", certNo);


        return result;
    }

    // 비밀번호 변경
    public int resetPassword(String email, String password) {
        password = passwordEncoder.encode(password);
        return pharmacistMapper.resetPassword(email, password);
    }

    // 약사 정보
    public Pharmacist getPharmacistInfo(Integer idx) {
        return pharmacistMapper.getPharmacistInfo(idx);
    }

    // 아이디 중복체크
    public int idCheck(String id) {
        return pharmacistMapper.idCheck(id);
    }
    
    // 회원가입 1(계정정보), 2(약국정보) 액션 (등록/수정)
    public int signUpFormAction(Pharmacist pharmacist, Pharmacy pharmacy, MultipartHttpServletRequest request) {
        String step = request.getParameter("step"); // 회원가입 페이지 구분 (계정정보, 약국정보 ...)
        // (수정시) 인덱스
        String idx = request.getParameter("idx"); // 약사idx
        String pharmacyIdx = request.getParameter("pharmacyIdx"); // 약국idx
        // idx 0으로 받아오면 빈문자열로 변경
        idx = Integer.parseInt(idx) == 0 ? "" : idx;
        pharmacyIdx = Integer.parseInt(pharmacyIdx) == 0 ? "" : pharmacyIdx;

        int result = 0;
        if(step.equals("step1")) {
            // TBC: 가입 전 중복된 아이디 있는지 한번 더 확인

            // 주계정인지 부계정인지 ==> 주계정
            pharmacist.setOriginYn("Y");
            // 비밀번호 암호화
            pharmacist.setUserPassword(passwordEncoder.encode(pharmacist.getUserPassword()));
            // 파일업로드
            Map<String, String> uploadFileName = doctorService.uploadFiles(idx, folderName, request);
            // 프로필
            pharmacist.setProfileImg(uploadFileName.get("file1"));
            // 약사면허번호
            pharmacist.setLicenseNoImg(uploadFileName.get("file2"));
            String file2Name = request.getParameter("originFileName2");
            if(!Functions.isNull(uploadFileName.get("file2OriginName"))) file2Name = uploadFileName.get("file2OriginName");
            pharmacist.setLicenseNoImgName(file2Name);

            if (idx.isEmpty()) {
                result = pharmacistMapper.insertPharmacist(pharmacist); // 저장
            } else {
                pharmacist.setIdx(Integer.parseInt(idx));
                if(!pharmacyIdx.isEmpty()) pharmacist.setPharmacyIdx(Integer.parseInt(pharmacyIdx));
                result = pharmacistMapper.updatePharmacist(pharmacist, ""); // 수정
            }
        }
        else if(step.equals("step2")) {
            // 전화번호
            String[] _telNoArr = request.getParameterValues("telNo");
            String telNo = "";
            for(int i=0; i<_telNoArr.length; i++) {
                if(!Functions.isNull(_telNoArr[i])) telNo = telNo.concat(_telNoArr[i] + "-");
            }
            telNo = telNo.substring(0, telNo.length() - 1); // 마지막문자(-) 제거
            pharmacy.setTelNo(telNo);

            // 팩스번호
            String[] _faxNoArr = request.getParameterValues("faxNo");
            String faxNo = "";
            for(int i=0; i<_faxNoArr.length; i++) {
                if(!Functions.isNull(_faxNoArr[i])) faxNo = faxNo.concat(_faxNoArr[i] + "-");
            }
            faxNo = faxNo.substring(0, faxNo.length() - 1); // 마지막문자(-) 제거
            pharmacy.setFaxNo(faxNo);

            // 파일업로드
            Map<String, String> uploadFileName = doctorService.uploadFiles(pharmacyIdx, folderName2, request);
            // 대표이미지
            pharmacy.setMainImg(uploadFileName.get("file1"));
            // 사업자등록증
            pharmacy.setBrNoImg(uploadFileName.get("file2"));
            String file2Name = request.getParameter("originFileName2");
            if(!Functions.isNull(uploadFileName.get("file2OriginName"))) file2Name = uploadFileName.get("file2OriginName");
            pharmacy.setBrNoImgName(file2Name);
            // 통신서비스이용증명원
            pharmacy.setSvcCertImg(uploadFileName.get("file3"));
            String file3Name = request.getParameter("originFileName3");
            if(!Functions.isNull(uploadFileName.get("file3OriginName"))) file3Name = uploadFileName.get("file3OriginName");
            pharmacy.setSvcCertImgName(file3Name);

            if(pharmacyIdx.isEmpty()) {
                result = pharmacistMapper.insertPharmacy(pharmacy); // 약국 정보 저장
                if(result != 0) { // 저장 성공
                    int chkIdx = pharmacy.getIdx();

                    // 약사 - 약국 idx UPDATE
                    pharmacist.setIdx(Integer.parseInt(idx)); // 약사 idx
                    String columnValue = "pharmacy_idx = " + chkIdx; // 약국 idx
                    pharmacistMapper.updatePharmacist(pharmacist, columnValue);
                }
            } else {
                Pharmacy dbPharmacy = pharmacistMapper.getPharmacyInfo(Integer.parseInt(pharmacyIdx)); // DB에 저장된 약국 정보 (현재 페이지 수정 시 나머지 컬럼의 데이터 보존하기 위해 불러와서 같이 저장)
                pharmacy.setIdx(Integer.parseInt(pharmacyIdx));
                pharmacy.setBankCode(dbPharmacy.getBankCode());
                pharmacy.setAccountName(dbPharmacy.getAccountName());
                pharmacy.setAccountNum(dbPharmacy.getAccountNum());
                pharmacy.setBankImg(dbPharmacy.getBankImg());
                pharmacy.setBankImgName(dbPharmacy.getBankImgName());

                result = pharmacistMapper.updatePharmacy(pharmacy, ""); // 약국 정보 업데이트
            }
        }
        else if(step.equals("step3")) {
            if(!pharmacyIdx.isEmpty()) {
                pharmacy = pharmacistMapper.getPharmacyInfo(Integer.parseInt(pharmacyIdx));
                pharmacy.setIdx(Integer.parseInt(pharmacyIdx));
                pharmacy.setBankCode(request.getParameter("bankCode"));
                pharmacy.setAccountNum(request.getParameter("accountNum"));
                pharmacy.setAccountName(request.getParameter("accountName"));
                pharmacy.setAccountName(request.getParameter("accountName"));
                // 파일업로드
                Map<String, String> uploadFileName = doctorService.uploadFiles(pharmacyIdx, folderName2, request);
                pharmacy.setBankImg(uploadFileName.get("file2")); // 통장사본
                String file2Name = request.getParameter("originFileName2");
                if(!Functions.isNull(uploadFileName.get("file2OriginName"))) file2Name = uploadFileName.get("file2OriginName");
                pharmacy.setBankImgName(file2Name);

                result = pharmacistMapper.updatePharmacy(pharmacy, ""); // 병원 정보 업데이트
            }

            return result;
        }

        if (result != 0) result = pharmacist.getIdx(); // 저장/수정 성공

        return result;
    }

    // 약국정보
    public Map<String, Object> getPharmacyInfo(Integer idx) {
        Pharmacy pharmacy = pharmacistMapper.getPharmacyInfo(idx); // 병원 정보
        Map<String, Object> result = new HashMap<>(); // returnData

        if (!Functions.isNull(pharmacy)) {
            result.put("idx", pharmacy.getIdx());
            result.put("nonfaceSvcYn", pharmacy.getNonfaceSvcYn());
            result.put("designateSvcYn", pharmacy.getDesignateSvcYn());
            result.put("pharmacyName", pharmacy.getPharmacyName());
            result.put("zipCode", pharmacy.getZipCode());
            result.put("mainAddr", pharmacy.getMainAddr());
            result.put("detailAddr", pharmacy.getDetailAddr());
            result.put("areaSi", pharmacy.getAreaSi());
            result.put("areaGu", pharmacy.getAreaGu());
            result.put("areaDong", pharmacy.getAreaDong());
            result.put("lat", pharmacy.getLat());
            result.put("lng", pharmacy.getLng());
            for(int i=0; i<3; i++) {
                result.put("telNo0"+(i+1), "");
                result.put("faxNo0"+(i+1), "");
            }
            if(!Functions.isNull(pharmacy.getTelNo())) {
                String[] telNo = pharmacy.getTelNo().split("-");
                for(int i=0; i<telNo.length; i++) {
                    result.put("telNo0"+(i+1), telNo[i]);
                }
            }
            if(!Functions.isNull(pharmacy.getFaxNo())) {
                String[] faxNo = pharmacy.getFaxNo().split("-");
                for(int i=0; i<faxNo.length; i++) {
                    result.put("faxNo0"+(i+1), faxNo[i]);
                }
            }
            result.put("brNo", pharmacy.getBrNo());
            result.put("file2", pharmacy.getBrNoImg());
            result.put("file2Name", pharmacy.getBrNoImgName());
            result.put("file3", pharmacy.getSvcCertImg());
            result.put("file3Name", pharmacy.getSvcCertImgName());
            result.put("file1", pharmacy.getMainImg());
            result.put("bankCode", pharmacy.getBankCode());
            result.put("accountName", pharmacy.getAccountName());
            result.put("accountNum", pharmacy.getAccountNum());
            result.put("bankImg", pharmacy.getBankImg());
            result.put("bankImgName", pharmacy.getBankImgName());
            result.put("isUpdate", pharmacy.getIsUpdate());
        }
        
        return result;
    }
}
