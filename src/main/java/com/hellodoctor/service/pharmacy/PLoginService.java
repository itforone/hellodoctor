package com.hellodoctor.service.pharmacy;

import com.hellodoctor.mapper.app.LoginMapper;
import com.hellodoctor.model.pharmacy.Pharmacist;
import com.hellodoctor.model.pharmacy.PharmacistAccount;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class PLoginService implements UserDetailsService {
    @Autowired
    private LoginMapper mapper;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        /**
         * 스프링이 로그인 요청을 가로챌때 username, password변수 2개를 가로채는데
         * password 부분 처리는 알아서 처리,
         * username이 DB에 있는지 확인해줘야함
         */
        Pharmacist user = mapper.getPharmacistAccount(email);

        if (user == null) {
            log.info("에러!");
            throw new UsernameNotFoundException("회원정보 조회 실패");
        }

        log.info("loadUserByUsername=" + email + "/mbId=" + user.getUserId() + "/mbPassword=" + user.getUserPassword());

        // 세션에 등록되는 Type (UserDetail)
        // UserDetail을 구현하고 있는 User객체 반환; 생성자로 아이디, 비번, Role
        /*return User.builder()
                .username(member.getMbId())
                .password(member.getMbPassword())
                .roles("MEMBER")
                .build();*/
        /*Member authMember = Member.builder()
                .mbId(member.getMbId())
                .mbPassword(member.getPassword())
                .role("MEMBER")
                .build();

        return authMember;*/

        return new PharmacistAccount(user);
    }
}
