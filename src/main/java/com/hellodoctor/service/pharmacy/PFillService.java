package com.hellodoctor.service.pharmacy;

import com.hellodoctor.common.Functions;
import com.hellodoctor.common.NCloudStorageUtils;
import com.hellodoctor.mapper.app.MemberAddressMapper;
import com.hellodoctor.mapper.hospital.HRemoteMapper;
import com.hellodoctor.mapper.pharmacy.PFillMapper;
import com.hellodoctor.model.app.PharmacyRequest;
import com.hellodoctor.model.pharmacy.Pharmacist;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.thymeleaf.util.ArrayUtils;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static com.hellodoctor.common.Functions.ChangeDateFormat;
import static com.hellodoctor.common.Functions.isNull;

/**
 * 약사 약 조제관리 service
 */

@Service
@Slf4j
public class PFillService {
    @Autowired
    PFillMapper pFillMapper;
    @Autowired
    HRemoteMapper hRemoteMapper;
    @Autowired
    MemberAddressMapper memberAddressMapper;
    @Autowired
    NCloudStorageUtils nCloudStorageUtils;

    // 파일업로드 폴더명
    private String folderName = "medicineGuide"; // 복약지도

    // 약 조제관리 - 조제요청 목록 수
    public Map<String, String> getFillRequestCount(Pharmacist pharmacist, HttpServletRequest request) {
        // 검색
        String sqlSearch = getFillRequestSearch(request);

        // 약 조제요청 목록
        int pharmacyIdx = pharmacist.getPharmacyIdx();
        List<Map<String, String>> list = pFillMapper.getFillRequestCount(pharmacyIdx, sqlSearch); // (약국idx, 검색조건)

        int statusR = 0;
        int statusY = 0;
        int statusN = 0; // 거부 or 취소
        for (Map<String, String> data : list) {
            // 접수상태 카운트 R:대기 Y:접수 N:거부
            if(data.get("reqStatus").equals("N")) ++statusN;
            else if(data.get("reqStatus").equals("Y")) ++statusY;
            else if(data.get("reqStatus").equals("R")) ++statusR;
        }

        Map<String, String> returnList = new HashMap<>();

        // 접수상태 카운트 R:대기 Y:접수 N:거부
        returnList.put("statusR", String.valueOf(statusR)); // 대기
        returnList.put("statusY", String.valueOf(statusY)); // 접수
        returnList.put("statusN", String.valueOf(statusN)); // 완료

        // 총접수
        int total = statusR + statusY + statusN;
        returnList.put("listTotalCnt", String.valueOf(total));

        return returnList;
    }

    // 약 조제관리 - 조제요청 목록
    public Map<String, Object> getFillRequest(Pharmacist pharmacist, HttpServletRequest request, int startRecord, int endRecord) {
        Map<String, Object> returnList = new HashMap<>();

        // 검색
        String sqlSearch = getFillRequestSearch(request);

        // limit
        if(isNull(request.getParameter("limit"))) endRecord = 15;
        else endRecord = Integer.parseInt(request.getParameter("limit"));

        // 진료 예약 목록
        int pharmacyIdx = pharmacist.getPharmacyIdx();
        List<Map<String, String>> list = pFillMapper.getFillRequest(pharmacyIdx, sqlSearch, startRecord, endRecord); // (약국idx, 검색조건, limit start, limit end)

        List<Object> tempList = new LinkedList<>();
        for (Map<String, String> data : list) {
            // log.info("data: "+data.toString());

            // 조제요청시간
            String regdate = String.valueOf(data.get("regdate")).replace("T", " ");
            String requestTime = ChangeDateFormat(regdate, "HH:mm:ss");
            data.put("requestTime", requestTime);

            // 환자이름/연락처
            // 관계가 본인이면 hd_medical(진료) 데이터 본인 외이면 hd_member_family(가족) 데이터
            String patientName = data.get("patientName");
            String patientHp = data.get("patientHp");
            data.put("patientName", patientName); // 환자이름
            data.put("patientHp", patientHp); // 연락처

            // 상태 - 거부 시
            String cancelDate = "";
            String cancelReason = "";
            if(!isNull(data.get("cancelDate"))) {
                cancelDate = ChangeDateFormat(String.valueOf(data.get("cancelDate")).replace("T", " "), "yyyy-MM-dd");
                cancelReason = data.get("cancelReason");
            }
            data.put("cancelDate", cancelDate); // 취소일
            data.put("cancelReason", cancelReason); // 취소사유

            tempList.add(data);
        }
        returnList.put("listData", tempList);

        // log.info("returnList: "+returnList);
        return returnList;
    }

    // 약 조제관리 - 조제요청 검색 조건 (쿼리)
    public String getFillRequestSearch(HttpServletRequest request) {
        String _search = "";

        // 검색일
        String _schDate = "";
        if(!isNull(request.getParameter("schDate"))) {
            _schDate = request.getParameter("schDate");
            _search += " AND A.regdate LIKE '%"+_schDate+"%'";
        }

        // 상태옵션
        String _schStatus = "";
        if(!isNull(request.getParameter("schStatus"))) {
            _schStatus = request.getParameter("schStatus");
            if(_schStatus.equals("조제요청")) {
                _search += " AND (A.req_status = 'R')";
            } else if(_schStatus.equals("조제접수")) {
                _search += " AND (A.req_status = 'Y')";
            } else if(_schStatus.equals("요청취소")) {
                _search += " AND (A.req_status = 'N')";
            }
        }

        // 검색조건
        String _item = "";
        String _keyword = "";
        if(!isNull(request.getParameter("schKeyword"))) {
            _item = request.getParameter("schItem");
            _keyword = request.getParameter("schKeyword");
            _search += " HAVING "+_item+" LIKE '%"+_keyword+"%'";
        }

        return _search;
    }

    // 배송지 정보
    public Map<String, String> getAddrInfo(Integer medicalIdx) {
        Map<String, String> data = new HashMap<>(); // returnData

        Map<String, String> addr = pFillMapper.getAddrInfo(medicalIdx);
        data.put("patientName", addr.get("patientName"));
        data.put("patientHp", addr.get("patientHp"));
        data.put("mainAddr", addr.get("mainAddr"));
        data.put("detailAddr", addr.get("detailAddr"));
        String shippingMsg = "";
        data.put("shippingMsg", shippingMsg);

        return data;
    }

    // 약 조제관리 - 조제요청 상태 - 접수
    public int statusComplete(Integer idx, Integer billAmt) {
        return pFillMapper.statusComplete(idx, billAmt);
    }

    // 약 조제관리 - 조제요청 상태 - 거부
    public int statusReject(Integer idx, String cancelReason) {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String cancelDate = now.format(formatter); // 오늘날짜

        int result = pFillMapper.statusReject(idx, cancelDate, cancelReason); // 약 조제요청 상태 UPDATE
        if(result != 0) {
            PharmacyRequest pharmacyRequest = pFillMapper.getPharmacyRequestInfo(idx); // 약 조제요청 정보 (단건)
            int medicalIdx = pharmacyRequest.getMedicalIdx(); // 원격진료idx
            String columnValue = "pharmacy_req_idx = 0";
            result = hRemoteMapper.updateMedical(medicalIdx, columnValue); // 진료DB에 약조제요청idx UPDATE (초기화)
        }

        return result;
    }

    // 약 수령방식 변경
    public int receiveMethodChange(@RequestParam Integer medicalIdx, @RequestParam String receiveMethod) {
        return pFillMapper.receiveMethodChange(medicalIdx, receiveMethod);
    }

    // 약 조제관리 - 조제접수 목록 수
    public Map<String, String> getFillReceiveCount(Pharmacist pharmacist, HttpServletRequest request) {
        // 검색
        String sqlSearch = getFillReceiveSearch(request);

        // 약 조제요청 목록
        int pharmacyIdx = pharmacist.getPharmacyIdx();
        List<Map<String, String>> list = pFillMapper.getFillReceiveCount(pharmacyIdx, sqlSearch); // (약국idx, 검색조건)

        int statusR = 0;
        int statusY = 0;
        int statusN = 0;
        for (Map<String, String> data : list) {
            // 접수상태 카운트 R:진행 Y:완료 N:취소
            if(data.get("reqStatus").equals("N")) ++statusN;
            else if(data.get("reqStatus").equals("Y") && Integer.parseInt(String.valueOf(data.get("billStlmIdx"))) > 0 && !isNull(data.get("completeDate"))) ++statusY;
            else if(data.get("reqStatus").equals("Y") && Integer.parseInt(String.valueOf(data.get("billStlmIdx"))) > 0 && isNull(data.get("completeDate"))) ++statusR;
        }

        Map<String, String> returnList = new HashMap<>();

        // 접수상태 카운트 R:진행 Y:완료 N:취소
        returnList.put("statusR", String.valueOf(statusR));
        returnList.put("statusY", String.valueOf(statusY));
        returnList.put("statusN", String.valueOf(statusN));

        // 총접수
        int total = statusR + statusY + statusN;
        returnList.put("listTotalCnt", String.valueOf(total));

        return returnList;
    }

    // 약 조제관리 - 조제접수 목록
    public Map<String, Object> getFillReceive(Pharmacist pharmacist, HttpServletRequest request, int startRecord, int endRecord) {
        Map<String, Object> returnList = new HashMap<>();

        // 검색
        String sqlSearch = getFillReceiveSearch(request);

        // limit
        if(isNull(request.getParameter("limit"))) endRecord = 15;
        else endRecord = Integer.parseInt(request.getParameter("limit"));

        // 진료 예약 목록
        int pharmacyIdx = pharmacist.getPharmacyIdx();
        List<Map<String, String>> list = pFillMapper.getFillReceive(pharmacyIdx, sqlSearch, startRecord, endRecord); // (약국idx, 검색조건, limit start, limit end)

        List<Object> tempList = new LinkedList<>();
        for (Map<String, String> data : list) {
            // log.info("data: "+data.toString());

            // 조제요청시간
            String regdate = String.valueOf(data.get("regdate")).replace("T", " ");
            String requestTime = ChangeDateFormat(regdate, "HH:mm:ss");
            data.put("requestTime", requestTime);

            // 환자이름/연락처
            // 관계가 본인이면 hd_medical(진료) 데이터 본인 외이면 hd_member_family(가족) 데이터
            String patientName = data.get("patientName");
            String patientHp = data.get("patientHp");
            data.put("patientName", patientName); // 환자이름
            data.put("patientHp", patientHp); // 연락처

            // 조제완료시간
            String completeDate = "";
            if(!isNull(data.get("completeDate"))) {
                completeDate = String.valueOf(data.get("completeDate").split(" ")[1]);
            }
            data.put("completeDate", completeDate);

            // 상태 - 거부 시
            String cancelDate = "";
            String cancelReason = "";
            if(!isNull(data.get("cancelDate"))) {
                cancelDate = ChangeDateFormat(String.valueOf(data.get("cancelDate")).replace("T", " "), "yyyy-MM-dd");
                cancelReason = data.get("cancelReason");
            }
            data.put("cancelDate", cancelDate); // 취소일
            data.put("cancelReason", cancelReason); // 취소사유

            tempList.add(data);
        }
        returnList.put("listData", tempList);

        // log.info("returnList: "+returnList);
        return returnList;
    }

    // 약 조제관리 - 조제접수 검색 조건 (쿼리)
    public String getFillReceiveSearch(HttpServletRequest request) {
        String _search = "AND A.req_status != 'R' AND A.bill_amt > 0"; // 청구금액이 있는 건만 조회 (청구금액이 있으면 접수완료 상태)

        // 검색일
        String _schDate = "";
        if(!isNull(request.getParameter("schDate"))) {
            _schDate = request.getParameter("schDate");
            _search += " AND A.regdate LIKE '%"+_schDate+"%'";
        }

        // 상태옵션
        String _schStatus = "";
        if(!isNull(request.getParameter("schStatus"))) {
            _schStatus = request.getParameter("schStatus");
            if(_schStatus.equals("조제접수")) {
                _search += " AND (A.req_status = 'Y' AND A.bill_stlm_idx = 0)";
            } else if(_schStatus.equals("조제진행")) {
                _search += " AND (A.req_status = 'Y' AND A.bill_stlm_idx > 0 AND complete_date IS NULL)";
            } else if(_schStatus.equals("조제완료")) {
                _search += " AND (A.req_status = 'Y' AND A.bill_stlm_idx > 0 AND complete_date IS NOT NULL)";
            } else if(_schStatus.equals("배송처리")) {
                _search += " AND (A.req_status = 'Y' AND A.bill_stlm_idx > 0 AND complete_date IS NOT NULL AND B.receive_method != '1')"; // 직접수령은 배송이 필요없기 때문에 != '1' 처리
            } else if(_schStatus.equals("배송완료")) {
                // TBC: 택배/퀵 API 확인 후 프로세스 변경
                _search += "";
            } else if(_schStatus.equals("조제취소")) {
                _search += " AND (A.req_status = 'N')";
            }
        }

        // 검색조건
        String _item = "";
        String _keyword = "";
        if(!isNull(request.getParameter("schKeyword"))) {
            _item = request.getParameter("schItem");
            _keyword = request.getParameter("schKeyword");
            _search += " HAVING "+_item+" LIKE '%"+_keyword+"%'";
        }

        return _search;
    }

    // 약 조제관리 - 조제접수 완료
    public int fillComplete(Integer idx) {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String completeDate = now.format(formatter);

        return pFillMapper.fillComplete(idx, completeDate);
    }

    // 의약품 검색 (자동완성)
    public List<Map<String, String>> mediAutocomplete(Integer pharmacyIdx, String keyword, String gubun) {
        List<Map<String, String>> returnData = new LinkedList<>();

        String search = "AND item_name LIKE '%"+keyword+"%'";
        if(gubun.equals("code")) search = "AND item_code LIKE '%"+keyword+"%'";
        List<Map<String, String>> data = pFillMapper.mediAutocomplete(pharmacyIdx, search);
        for (Map<String, String> map : data) {
            Map<String, String> tmp = new LinkedHashMap<>();
            tmp.put("idx", String.valueOf(map.get("idx")));
            tmp.put("itemName", String.valueOf(map.get("itemName")));
            tmp.put("itemCode", String.valueOf(map.get("itemCode")));
            tmp.put("howTake", String.valueOf(map.get("howTake")));

            returnData.add(tmp);
        }

        return returnData;
    }

    // 복약지도 저장 (등록/수정)
    public int saveGuide(MultipartHttpServletRequest request) {
        int result = 0;

        /**
         * 파일업로드 START
         * - 동적으로 파일 추가/삭제하여 등록하는 경우
         * - multiple 구현 X, multiple 소스 참고
         * **/
        List<String> uploadFileName = new LinkedList<>();

        // 폴더생성
        nCloudStorageUtils.createFolder(folderName);
        // 업로드파일
        MultiValueMap<String, MultipartFile> multiFiles = request.getMultiFileMap();
        // 파일 요소 읽음
        Iterator<Map.Entry<String, List<MultipartFile>>> iterator = multiFiles.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, List<MultipartFile>> entry = iterator.next();
            List<MultipartFile> fileList = entry.getValue();
            for (MultipartFile file : fileList) {
                String elementName = file.getName(); // input name (file1, file2, file3)
                String fileOriginName = file.getOriginalFilename(); // input 원본파일명
                String fileExt = fileOriginName.substring(fileOriginName.lastIndexOf(".") + 1); // 확장자
                long fileSize = file.getSize();
                if (fileSize > 0) { // 첨부파일이 존재하면
                    String _fileName = Functions.getTimeStamp() + elementName + "." + fileExt; // 파일명 생성
                    String url = nCloudStorageUtils.uploadFile(folderName, _fileName, file); // 업로드 처리

                    uploadFileName.add(url);
                }
            }
        }

        // 파일 삭제 시
        String[] fileDelArr = request.getParameterValues("fileDel"); // 삭제이미지
        if(!isNull(fileDelArr)) {
            for(int i=0; i<fileDelArr.length; i++) {
                nCloudStorageUtils.deleteObject(fileDelArr[i]);
            }
        }
        /** 파일업로드 END **/

        List<Map<String, String>> guideList = new LinkedList<>(); // returnData

        String medicalIdx = request.getParameter("medicalIdx"); // 원격진료idx
        String[] idxArr = request.getParameterValues("medicineIdx"); // 의약품idx
        String[] nameArr = request.getParameterValues("itemName"); // 의약품명
        String[] codeArr = request.getParameterValues("itemCode"); // 코드
        String[] howArr = request.getParameterValues("howTake"); // 복약방법
        String[] imgArr = request.getParameterValues("itemImg"); // 업로드된 이미지경로
        String[] modeArr = request.getParameterValues("mode"); // 등록(r) or 수정(u)

        int fileIndex = 0;
        for(int i=0; i<nameArr.length; i++) {
            Map<String, String> map = new HashMap<>();

            // 의약품idx - idx 있으면 의약품 정보 불러옴, idx 없으면 직접 입력
            if(isNull(idxArr[i])) idxArr[i] = "0";
            map.put("medicineIdx", isNull(idxArr[i]) ? "0" : idxArr[i]);
            // 의약품명
            map.put("itemName", isNull(nameArr[i]) ? "" : nameArr[i]);
            // 코드
            map.put("itemCode", isNull(codeArr[i]) ? "" : codeArr[i]);
            // 복용방법
            map.put("howTake", isNull(howArr[i]) ? "" : howArr[i]);
            // 이미지
            if(modeArr[i].equals("r")) { // mode value "r" 등록
                map.put("itemImg", String.valueOf(uploadFileName.get(fileIndex)));
                fileIndex++;
            } else { // mode value "u" 수정
                map.put("itemImg", imgArr[i]);
            }

            guideList.add(map);
        }

        log.info("guideList" + guideList);


        if(!isNull(medicalIdx) && !isNull(guideList)) {
            // 수정 시 복약지도 전체 삭제 후 저장
            if (ArrayUtils.contains(modeArr, "u")) pFillMapper.deleteGuide(Integer.parseInt(medicalIdx));
            result = pFillMapper.saveGuide(Integer.parseInt(medicalIdx), guideList);
        }

        return result;
    }

    // 복약지도 정보
    public List<Map<String, String>> mediGuideInfo(Integer medicalIdx) {
        return pFillMapper.mediGuideInfo(medicalIdx);
    }

    // 약 조제내역 목록 수
    public Map<String, String> getFillListCount(Pharmacist pharmacist, HttpServletRequest request) {
        // 검색
        String sqlSearch = getFillListSearch(request);

        // 진료 예약 목록
        int pharmacyIdx = pharmacist.getPharmacyIdx();
        List<Map<String, String>> list = pFillMapper.getFillListCount(pharmacyIdx, sqlSearch); // (병원idx, 검색조건)

        int count = 0;
        int billAmt = 0;
        for (Map<String, String> data : list) {
            count++;
            billAmt += Integer.parseInt(String.valueOf(data.get("billAmt")));
        }

        Map<String, String> returnList = new HashMap<>();
        returnList.put("listTotalCnt", String.valueOf(count));
        returnList.put("billAmt", String.valueOf(billAmt)); // 총청구금액

        return returnList;
    }

    // 약 조제내역 목록
    public Map<String, Object> getFillList(Pharmacist pharmacist, HttpServletRequest request, int startRecord, int endRecord) {
        Map<String, Object> returnList = new HashMap<>();

        // 검색
        String sqlSearch = getFillListSearch(request);

        // limit
        if(isNull(request.getParameter("limit"))) endRecord = 15;
        else endRecord = Integer.parseInt(request.getParameter("limit"));

        // 약 조제내역 목록
        int pharmacyIdx = pharmacist.getPharmacyIdx();
        List<Map<String, String>> list = pFillMapper.getFillList(pharmacyIdx, sqlSearch, startRecord, endRecord); // (약국idx, 검색조건, limit start, limit end)

        List<Object> tempList = new LinkedList<>();
        for (Map<String, String> data : list) {
            // log.info("data: "+data.toString());

            // 조제요청일
            String regdate = String.valueOf(data.get("regdate")).replace("T", " ");
            data.put("regdate", regdate);

            // 조제완료일
            String compDate = String.valueOf(data.get("completeDate")).replace("T", " ");
            data.put("compDate", compDate);

            // 환자이름/관계/연락처
            // 관계가 본인이면 hd_medical(진료) 데이터 본인 외이면 hd_member_family(가족) 데이터
            String patientName = data.get("patientName");
            String patientHp = data.get("patientHp");
            data.put("patientName", patientName); // 환자이름
            data.put("patientHp", patientHp); // 연락처

            // 수령방식
            String receiveMethod = "";
            if(data.get("receiveMethod").equals("1")) receiveMethod = "직접 수령";
            else if(data.get("receiveMethod").equals("2")) receiveMethod = "택배";
            else if(data.get("receiveMethod").equals("3")) receiveMethod = "퀵";
            data.put("receiveMethod", receiveMethod);

            // 상태
            String fillStatus = "정산대기";
            // 결제 인덱스 있으면 정산완료
            if(Integer.parseInt(String.valueOf(data.get("billStlmIdx"))) > 0) fillStatus = "정산완료";
            data.put("fillStatus", fillStatus);

            tempList.add(data);
        }
        returnList.put("listData", tempList);

        // log.info("returnList: "+returnList);
        return returnList;
    }

    // 약 조제내역 검색 조건 (쿼리)
    public String getFillListSearch(HttpServletRequest request) {
        String _search = "";

        // 검색일 (요청일 or 완료일)
        String _schDate = request.getParameter("schDate");
        String stDate = request.getParameter("schDateSt"); // 시작
        String edDate = request.getParameter("schDateEd"); // 종료
        String column = "A.regdate";
        if(_schDate.equals("완료일")) column = "A.complete_date";
        if(!isNull(stDate) && !isNull(edDate)) {
            _search += " AND (DATE_FORMAT("+column+", '%Y-%m-%d') >= '"+stDate+"' AND DATE_FORMAT("+column+", '%Y-%m-%d') <= '"+edDate+"')";
        } else if(!isNull(stDate) && isNull(edDate)) {
            _search += " AND (DATE_FORMAT("+column+", '%Y-%m-%d') >= '"+stDate+"')";
        } else if(isNull(stDate) && !isNull(edDate)) {
            _search += " AND (DATE_FORMAT("+column+", '%Y-%m-%d') <= '"+edDate+"')";
        }

        // 검색조건
        String _item = "";
        String _keyword = "";
        if(!isNull(request.getParameter("schKeyword"))) {
            _item = request.getParameter("schItem");
            _keyword = request.getParameter("schKeyword");
            _search += " HAVING "+_item+" LIKE '%"+_keyword+"%'";
        }

        return _search;
    }
}