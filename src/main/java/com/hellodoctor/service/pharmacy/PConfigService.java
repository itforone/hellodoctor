package com.hellodoctor.service.pharmacy;

import com.hellodoctor.common.Functions;
import com.hellodoctor.common.NCloudStorageUtils;
import com.hellodoctor.mapper.app.FileUploadMapper;
import com.hellodoctor.mapper.pharmacy.PConfigMapper;
import com.hellodoctor.mapper.pharmacy.PharmacistMapper;
import com.hellodoctor.model.app.Pharmacy;
import com.hellodoctor.model.app.PharmacyTmp;
import com.hellodoctor.model.pharmacy.Medicine;
import com.hellodoctor.model.pharmacy.Pharmacist;
import com.hellodoctor.service.hospital.DoctorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static com.hellodoctor.common.Functions.isNull;

/**
 * 관리 관련 service 클래스
 */
@Service
@Slf4j
public class PConfigService {
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    FileUploadMapper fileUploadMapper;
    @Autowired
    NCloudStorageUtils nCloudStorageUtils;
    @Autowired
    PharmacistService pharmacistService;
    @Autowired
    PharmacistMapper pharmacistMapper;
    @Autowired
    DoctorService doctorService;
    @Autowired
    PConfigMapper pConfigMapper;

    // 파일업로드 폴더명
    private String folderName = "pharmacist"; // 약사
    private String folderName2 = "pharmacy"; // 약국
    private String folderName3 = "medicine"; // 의약품

    // 정산계정관리 - 저장
    public int saveAdmAccount(Pharmacist pharmacist, MultipartHttpServletRequest request) {
        int result = 0;
        String idx = request.getParameter("idx");
        String userName = request.getParameter("userName");
        String userHp = request.getParameter("userHp");
        String userPassword = request.getParameter("userPassword");
        String licenseNo = request.getParameter("licenseNo");
        String history = request.getParameter("history");
        String introduce = request.getParameter("introduce");

        if(!Functions.isNull(idx)) {
            pharmacist.setIdx(Integer.parseInt(idx));
            pharmacist.setUserName(userName);
            pharmacist.setUserHp(userHp);
            pharmacist.setLicenseNo(licenseNo);
            pharmacist.setHistory(history);
            pharmacist.setIntroduce(introduce);
            // 비밀번호 입력 시
            if(!Functions.isNull(userPassword)) {
                // 비밀번호 암호화
                pharmacist.setUserPassword(passwordEncoder.encode(userPassword));
            }
            // 파일업로드
            Map<String, String> uploadFileName = doctorService.uploadFiles(idx, folderName, request);
            // 프로필
            pharmacist.setProfileImg(uploadFileName.get("file1"));
            // 약사면허번호
            pharmacist.setLicenseNoImg(uploadFileName.get("file2"));
            if(!Functions.isNull(uploadFileName.get("file2OriginName"))) {
                pharmacist.setLicenseNoImgName(uploadFileName.get("file2OriginName"));
            }

            result = pharmacistMapper.updatePharmacist(pharmacist, "");
        }

        return result;
    }

    // 서브계정관리 - 목록 수
    public int getAdmSubAccountCount(int pharmacyIdx, HttpServletRequest request) {
        // 검색
        String sqlSearch = "";
        String _item = "";
        String _keyword = "";
        if(!isNull(request.getParameter("schKeyword"))) {
            _item = request.getParameter("schItem");
            _keyword = request.getParameter("schKeyword");
            sqlSearch += " AND "+_item+" LIKE '%"+_keyword+"%'";
        }

        return pConfigMapper.getAdmSubAccountCount(pharmacyIdx, sqlSearch);
    }

    // 서브계정관리 - 목록
    public Map<String, Object> getAdmSubAccount(int pharmacyIdx, HttpServletRequest request, int startRecord, int endRecord) {
        Map<String, Object> returnList = new HashMap<>();

        // 검색
        String sqlSearch = "";
        String _item = "";
        String _keyword = "";
        if(!isNull(request.getParameter("schKeyword"))) {
            _item = request.getParameter("schItem");
            _keyword = request.getParameter("schKeyword");
            sqlSearch += " AND "+_item+" LIKE '%"+_keyword+"%'";
        }

        // limit
        if(isNull(request.getParameter("limit"))) endRecord = 15;
        else endRecord = Integer.parseInt(request.getParameter("limit"));

        List<Map<String, String>> accountList = pConfigMapper.getAdmSubAccount(pharmacyIdx, sqlSearch, startRecord, endRecord); // (병원idx, 검색조건, limit start, limit end)

        List<Object> tempList = new LinkedList<>();
        for (Map<String, String> map : accountList) {
            String regdate = map.get("regdate").replace("-", "."); // 생성일
            map.put("regdate", regdate);

            tempList.add(map);
        }
        returnList.put("listData", tempList);

        return returnList;
    }

    // 서브계정관리 - 저장
    public int saveAdmSubAccount(Pharmacist pharmacist, MultipartHttpServletRequest request) {
        // (수정시) 인덱스
        String idx = request.getParameter("idx"); // 의사idx
        // idx 0으로 받아오면 빈문자열로 변경
        idx = Integer.parseInt(idx) == 0 ? "" : idx;

        Pharmacist pharmacist2 = new Pharmacist();
        pharmacist2.setOriginYn(request.getParameter("originYn")); // 주계정인지 부계정인지 ==> 부계정
        pharmacist2.setPharmacyIdx(pharmacist.getPharmacyIdx()); // 약국idx
        pharmacist2.setUserId(request.getParameter("userId")); // 아이디
        pharmacist2.setUserName(request.getParameter("userName")); // 약사명
        if(!Functions.isNull(request.getParameter("userPassword"))) { // 비밀번호 입력 시
            pharmacist2.setUserPassword(passwordEncoder.encode(request.getParameter("userPassword"))); // 비밀번호 암호화
        } else {
            // 수정 시 비밀번호 미입력이면 기존 비밀번호 그대로
            Map<String, Object> subDoctor = doctorService.getDoctorInfo(Integer.parseInt(idx));
            pharmacist2.setUserPassword(String.valueOf(subDoctor.get("userPassword")));
        }
        pharmacist2.setUserHp(request.getParameter("userHp")); // 전화번호
        // 파일업로드
        Map<String, String> uploadFileName = doctorService.uploadFiles(idx, folderName, request);
        // 약사면허번호
        pharmacist2.setLicenseNo(request.getParameter("licenseNo"));
        pharmacist2.setLicenseNoImg(uploadFileName.get("file2"));
        String file2Name = request.getParameter("originFileName2");
        if(!Functions.isNull(uploadFileName.get("file2OriginName"))) file2Name = uploadFileName.get("file2OriginName");
        pharmacist2.setLicenseNoImgName(file2Name);

        int result = 0;
        if (isNull(idx)) {
            result = pharmacistMapper.insertPharmacist(pharmacist2); // 약사 저장
        } else {
            pharmacist2.setIdx(Integer.parseInt(idx));
            result = pharmacistMapper.updatePharmacist(pharmacist2, ""); // 약사 수정
        }

        return result;
    }

    // 서브계정관리 - 삭제
    public int deletePharmacist(int idx) {
        return pharmacistMapper.deletePharmacist(idx);
    }

    // 약국정보관리 - 저장
    public int saveAdmPharmacy(Pharmacist pharmacist, Pharmacy pharmacy, MultipartHttpServletRequest request) {
        int result = 0;
        String pharmacyIdx = request.getParameter("pharmacyIdx");

        if(!Functions.isNull(pharmacyIdx)) {
            pharmacy.setIdx(Integer.parseInt(pharmacyIdx));

            // 전화번호
            String[] _telNoArr = request.getParameterValues("telNo");
            String telNo = "";
            for(int i=0; i<_telNoArr.length; i++) {
                if(!Functions.isNull(_telNoArr[i])) telNo = telNo.concat(_telNoArr[i] + "-");
            }
            telNo = telNo.substring(0, telNo.length() - 1); // 마지막문자(-) 제거
            pharmacy.setTelNo(telNo);

            // 팩스번호
            String[] _faxNoArr = request.getParameterValues("faxNo");
            String faxNo = "";
            for(int i=0; i<_faxNoArr.length; i++) {
                if(!Functions.isNull(_faxNoArr[i])) faxNo = faxNo.concat(_faxNoArr[i] + "-");
            }
            faxNo = faxNo.substring(0, faxNo.length() - 1); // 마지막문자(-) 제거
            pharmacy.setFaxNo(faxNo);

            // 파일업로드
            Map<String, String> uploadFileName = doctorService.uploadFiles(pharmacyIdx, folderName2, request);
            // 대표이미지
            pharmacy.setMainImg(uploadFileName.get("file1"));
            // 사업자등록증
            pharmacy.setBrNoImg(uploadFileName.get("file2"));
            String file2Name = request.getParameter("originFileName2");
            if(!Functions.isNull(uploadFileName.get("file2OriginName"))) file2Name = uploadFileName.get("file2OriginName");
            pharmacy.setBrNoImgName(file2Name);
            // 통신서비스이용증명원
            pharmacy.setSvcCertImg(uploadFileName.get("file3"));
            String file3Name = request.getParameter("originFileName3");
            if(!Functions.isNull(uploadFileName.get("file3OriginName"))) file3Name = uploadFileName.get("file3OriginName");
            pharmacy.setSvcCertImgName(file3Name);

            String columnValue = "is_update = 'Y'";
            pharmacistMapper.updatePharmacy(pharmacy, columnValue); // 약국 정보 업데이트
            result = pharmacistMapper.updatePharmacyTmp(pharmacy, Integer.parseInt(pharmacyIdx)); // 약국 정보 임시테이블 저장 (관리자 승인 전까지 임시테이블에 저장)
        }

        return result;
    }

    // 정산계좌관리 - 정산정보
    public Pharmacy getPharmacyInfo(int pharmacyIdx) {
        return pharmacistMapper.getPharmacyInfo(pharmacyIdx);
    }

    // 정산계좌관리 - 저장
    public int saveAccount(MultipartHttpServletRequest request) {
        String pharmacyIdx = request.getParameter("pharmacyIdx");
        int result = 0;

        if (!pharmacyIdx.isEmpty()) {
            Pharmacy pharmacy = new Pharmacy();
            pharmacy.setIdx(Integer.parseInt(pharmacyIdx));
            String bankCode = request.getParameter("bankCode");
            String AccountName = request.getParameter("accountName");
            String AccountNum = request.getParameter("accountNum");

            // 약국 정보 업데이트
            String columnValue = "bank_code = '"+ bankCode +"', account_name = '"+ AccountName +"', account_num = '"+ AccountNum +"'";

            MultipartFile file = request.getFile("bankImg");
            if (file != null && file.getSize() > 0) {
                // 파일업로드
                Map<String, String> uploadFileName = doctorService.uploadFiles(pharmacyIdx, folderName2, request);
                columnValue += ", bank_img = '"+ uploadFileName.get("bankImg") +"', bank_img_name = '"+ uploadFileName.get("bankImgOriginName") +"'";
            }

            result = pharmacistMapper.updatePharmacy(pharmacy, columnValue);
        }

        return result;
    }

    // 지정병원 전체등록수
    public int getAdmHospitalTotalCount(int pharmacyIdx) {
        return pConfigMapper.getAdmHospitalTotalCount(pharmacyIdx);
    }

    // 지정병원 목록
    public List<Map<String, String>> getAdmHospitalList(int pharmacyIdx, int startRecord, int endRecord) {
        return pConfigMapper.getAdmHospitalList(pharmacyIdx, startRecord, endRecord);
    }

    // 의약품 조회
    public Medicine getMedicineOne(int idx) {
        return pConfigMapper.getMedicineOne(idx);
    }

    // 의약품 등록/수정
    public int registerMedicine(MultipartHttpServletRequest request, Medicine medicine) {
        /**
         * 파일업로드 START
         * - 등록/수정을 모두 하는 경우
         * - multiple 구현 X
         * **/
        // 폴더생성
        nCloudStorageUtils.createFolder(folderName3);

        MultipartFile file = request.getFile("tempFile");
        boolean fileDeleteChk = (String.valueOf(request.getParameter("tempImgDel")).equals("1"))? true:false;
        String tempImg = request.getParameter("tempImgUrl");
        String tempImgName = request.getParameter("tempImgName");

        if (file != null && file.getSize() > 0) {
            String fileOriginName = file.getOriginalFilename(); // input 원본파일명
            String fileExt = fileOriginName.substring(fileOriginName.lastIndexOf(".")+1); // 확장자
            String fileName = Functions.getTimeStamp() + medicine.getPharmacyIdx() + "." + fileExt; // 파일명 생성
            String url = nCloudStorageUtils.uploadFile(folderName, fileName, file); // 업로드 처리
            log.info("url="+url);

            tempImg = url;
            tempImgName = fileOriginName;
            fileDeleteChk = true;

        } else if (fileDeleteChk) {
            tempImg = "";
            tempImgName = "";
        }

        // 기존 파일 삭제 (1.파일교체, 2.파일삭제 체크)
        if (fileDeleteChk && !request.getParameter("tempImgUrl").equals("")) {
            nCloudStorageUtils.deleteObject(request.getParameter("tempImgUrl"));
        }

        medicine.setItemImg(tempImg);
        medicine.setItemImgName(tempImgName);
        /** 파일업로드 END **/

        return (medicine.getIdx()==0)? pConfigMapper.insertMedicine(medicine) : pConfigMapper.updateMedicine(medicine);
    }

    // 의약품 삭제
    public int deleteMedicine(int idx) {
        return pConfigMapper.deleteMedicine(idx);
    }

    // 의약품 전체등록수
    public int getMedicineTotalCount(int pharmacyIdx, String sfl, String stx) {
        String columnValue = "";
        if (!stx.equals("")) {
            switch (sfl) {
                case "name" : columnValue = "item_name LIKE '%"+ stx +"%'"; break;
                case "code" : columnValue = "item_code LIKE '%"+ stx +"%'"; break;
            }
        }
        return pConfigMapper.getMedicineTotalCount(pharmacyIdx, columnValue);
    }

    // 의약품 목록
    public List<Medicine> getMedicineList(int pharmacyIdx, int startRecord, int endRecord, String sfl, String stx) {
        String columnValue = "";
        if (!stx.equals("")) {
            switch (sfl) {
                case "name" : columnValue = "item_name LIKE '%"+ stx +"%'"; break;
                case "code" : columnValue = "item_code LIKE '%"+ stx +"%'"; break;
            }
        }
        return pConfigMapper.getMedicineList(pharmacyIdx, startRecord, endRecord, columnValue);
    }


}