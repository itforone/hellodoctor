package com.hellodoctor.service.hospital;

import com.hellodoctor.common.Functions;
import com.hellodoctor.common.NCloudStorageUtils;
import com.hellodoctor.mapper.hospital.HRemoteMapper;
import com.hellodoctor.model.app.Medical;
import com.hellodoctor.model.hospital.Doctor;
import com.hellodoctor.model.hospital.MedicalPrescription;
import com.hellodoctor.service.app.RemoteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static com.hellodoctor.common.Constants.MEDICAL_REQUEST_SUBJECT;
import static com.hellodoctor.common.Functions.ChangeDateFormat;
import static com.hellodoctor.common.Functions.isNull;

/**
 * 의사-원격진료 service 클래스
 */
@Service
@Slf4j
public class HRemoteService {
    @Autowired
    HRemoteMapper hRemoteMapper;
    @Autowired
    RemoteService remoteService;

    @Autowired
    NCloudStorageUtils nCloudStorageUtils;

    // 파일업로드 폴더명
    private String folderName = "prescription"; // 처방전

    // 원격진료 예약신청 목록 수
    public Map<String, String> getRemoteListCount(Doctor doctor, HttpServletRequest request) {
        // 검색
        String sqlSearch = getRemoteListSearch(request);

        // 진료 예약 목록
        int hospitalIdx = doctor.getHospitalIdx();
        List<Map<String, String>> list = hRemoteMapper.getRemoteListCount(hospitalIdx, sqlSearch); // (병원idx, 검색조건)

        int statusR = 0;
        int statusY = 0;
        int statusN = 0;
        for (Map<String, String> data : list) {
            // 접수상태 카운트 R:대기 Y:접수 N:거부
            if(data.get("medicalStatus").equals("C") || data.get("hospitalRcptYn").equals("N")) ++statusN;
            else if(data.get("hospitalRcptYn").equals("Y")) ++statusY;
            else if(data.get("hospitalRcptYn").equals("R")) ++statusR;
        }

        Map<String, String> returnList = new HashMap<>();

        // 접수상태 카운트 R:대기 Y:접수 N:거부
        returnList.put("statusR", String.valueOf(statusR)); // 대기
        returnList.put("statusY", String.valueOf(statusY)); // 접수
        returnList.put("statusN", String.valueOf(statusN)); // 완료

        // 총접수
        int total = statusR + statusY + statusN;
        returnList.put("listTotalCnt", String.valueOf(total));

        return returnList;
    }

    // 원격진료 예약신청 목록
    public Map<String, Object> getRemoteList(Doctor doctor, HttpServletRequest request, int startRecord, int endRecord) {
        Map<String, Object> returnList = new HashMap<>();

        // 검색
        String sqlSearch = getRemoteListSearch(request);

        // limit
        if(isNull(request.getParameter("limit"))) endRecord = 15;
        else endRecord = Integer.parseInt(request.getParameter("limit"));

        // 진료 예약 목록
        int hospitalIdx = doctor.getHospitalIdx();
        List<Map<String, String>> list = hRemoteMapper.getRemoteList(hospitalIdx, sqlSearch, startRecord, endRecord); // (병원idx, 검색조건, limit start, limit end)

        List<Object> tempList = new LinkedList<>();
        for (Map<String, String> data : list) {
            // log.info("data: "+data.toString());

            // 접수시간
            String regdate = String.valueOf(data.get("regdate")).replace("T", " ");
            String receiptTime = ChangeDateFormat(regdate, "HH:mm");
            data.put("receiptTime", receiptTime);

            // 질환
            String subjectName = "";
            if(!isNull(data.get("subjectCode"))) subjectName = MEDICAL_REQUEST_SUBJECT.get(Integer.parseInt(String.valueOf(data.get("subjectCode")))) + " 질환";
            data.put("subjectName", subjectName);

            // 환자이름/관계/연락처/주민등록번호
            // 관계가 본인이면 hd_medical(진료) 데이터 본인 외이면 hd_member_family(가족) 데이터
            String relationship = "본인";
            String patientName = data.get("patientName");
            String patientHp = data.get("patientHp");
            String patientRrNo = data.get("patientRrNo");
            if(Integer.parseInt(String.valueOf(data.get("patientIdx"))) != 0) {
                int famRel = Integer.parseInt(data.get("famRel"));
                if(famRel == 1) relationship = "부모";
                else if(famRel == 2) relationship = "배우자";
                else if(famRel == 3) relationship = "자녀";
            }
            data.put("patientName", patientName); // 환자이름
            data.put("relationship", relationship); // 관계
            data.put("patientHp", patientHp); // 연락처
            data.put("patientRrNo", patientRrNo); // 주민등록번호

            // 진료구분
            // 접수번호에서 추출 - F: 초진 S: 재진 (3개월 내 같은 질환으로 2회 이상 진료 예약한 환자)
            String rcptNoArr = data.get("rcptNo").substring(7, 8);
            String medicalType = rcptNoArr.equals("F") ? "초진" : "재진";
            data.put("medicalType", medicalType);

            // 예약희망시간 15분 단위로 추출
            String hopeTime = data.get("appStartHour") + "~" + data.get("appEndHour");
            String[] hopeTimeSpilt = hopeTime.split("~");
            String[] hopeHour = hopeTimeSpilt[0].split(":");
            String[] hospitalAvailTime = new String[3];
            hospitalAvailTime[0] = hopeTimeSpilt[0];
            hospitalAvailTime[2] = hopeTimeSpilt[1];
            // 10:30~10:30 체크 ? 10:15 : 10:45
            hospitalAvailTime[1] = (hopeTimeSpilt[0].contains(":00"))? hopeHour[0] + ":" + "15" : hopeHour[0] + ":" + "45";
            data.put("hopeHourArr", String.join(",", hospitalAvailTime));

            // 상태 - 접수 시
            String medicalTime = "";
            if(!isNull(data.get("medicalTime"))) medicalTime = data.get("medicalTime");
            data.put("medicalTime", medicalTime); // 예약완료시간 = 진료시간

            // 상태 - 거부 시
            String cancelDate = "";
            String cancelReason = "";
            if(!isNull(data.get("cancelDate"))) {
                cancelDate = ChangeDateFormat(String.valueOf(data.get("cancelDate")).replace("T", " "), "yyyy-MM-dd");
                cancelReason = data.get("cancelReason");
            }
            data.put("cancelDate", cancelDate); // 취소일
            data.put("cancelReason", cancelReason); // 취소사유

            tempList.add(data);
        }
        returnList.put("listData", tempList);

        // log.info("returnList: "+returnList);
        return returnList;
    }

    // 원격진료 예약신청 검색 조건 (쿼리)
    public String getRemoteListSearch(HttpServletRequest request) {
        String _search = "";

        // 접수일
        String _receiptDate = "";
        if(!isNull(request.getParameter("schDate"))) {
            _receiptDate = request.getParameter("schDate");
            _search += " AND A.regdate LIKE '%"+_receiptDate+"%'";
        }

        // 예약현황
        String _schStatus = "";
        if(!isNull(request.getParameter("schStatus"))) {
            _schStatus = request.getParameter("schStatus");
            // 전체/공통조건 : 회원 신청 !=T AND 당일
            // 진료요청 : 병원 접수=R AND 회원 신청=R
            // 예약완료 : 병원 접수=Y AND 회원 신청=R
            // 진료취소 : 병원 접수=N OR 회원 신청=C
            if(_schStatus.equals("진료요청")) {
                _search += " AND (A.hospital_rcpt_yn = 'R' AND A.medical_status = 'R')";
            } else if(_schStatus.equals("예약완료")) {
                _search += " AND (A.hospital_rcpt_yn = 'Y' AND A.medical_status != 'C')";
            } else if(_schStatus.equals("진료취소")) {
                _search += " AND (A.hospital_rcpt_yn = 'N' OR A.medical_status = 'C')";
            }
        }

        // 검색조건
        String _item = "";
        String _keyword = "";
        if(!isNull(request.getParameter("schKeyword"))) {
            _item = request.getParameter("schItem");
            _keyword = request.getParameter("schKeyword");
            _search += " HAVING "+_item+" LIKE '%"+_keyword+"%'";
        }

        return _search;
    }

    // 원격진료 예약신청 접수
    public int receiptComplete(Integer idx, String medicalTime) {
        return hRemoteMapper.receiptComplete(idx, medicalTime);
    }

    // 원격진료 예약신청 거부
    public int receiptReject(Integer idx, String cancelReason) {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String cancelDate = now.format(formatter); // 오늘날짜

        return hRemoteMapper.receiptReject(idx, cancelDate, cancelReason);
    }

    // 환자 정보
    public Map<String, Object> getPatientInfo(HttpServletRequest request) {
        Map<String, Object> data = hRemoteMapper.getPatientInfo(Integer.parseInt(request.getParameter("idx")));

        // 질환
        String subjectName = "";
        if(!isNull(data.get("subjectCode"))) subjectName = MEDICAL_REQUEST_SUBJECT.get(Integer.parseInt(String.valueOf(data.get("subjectCode")))) + " 질환";
        data.put("subjectName", subjectName);

        // 증상 내용
        String symptom = "";
        if(!isNull(data.get("symptom"))) symptom = String.valueOf(data.get("symptom"));
        data.put("symptom", symptom);

        // 증상 첨부파일
        List<String> fileList = new LinkedList<>();
        if(!isNull(data.get("symptomImg"))) {
            String[] fileArr = String.valueOf(data.get("symptomImg")).split(",");
            fileList.addAll(Arrays.asList(fileArr));
        }
        data.put("fileList", fileList);

        // 이전 진료 예약 내역 (진료 완료 건 조회)
        Medical medical =  hRemoteMapper.getMedicalInfo(Integer.parseInt(request.getParameter("idx"))); // 진료정보
        int hospitalIdx = medical.getHospitalIdx();
        String compDate = medical.getRegdate().replace("T", " ");
        String sqlSearch = " AND A.regdate < '"+compDate+"'  AND A.medical_status = 'Y' AND A.patient_idx = "+data.get("patientIdx"); // 선택 진료정보의 접수일 이전 AND 진료 상태 완료 AND 환자idx = 선택 환자의 환자idx

        List<Map<String, String>> list = hRemoteMapper.getRemoteList(hospitalIdx, sqlSearch, 0, 0); // (병원idx, 검색조건, limit start, limit end)
        List<Map<String, String>> prevList = new LinkedList<>();
        if(!isNull(list)) {
            for(int i=0; i<list.size(); i++) {
                Map<String, String> map = new HashMap<>();

                // 진료일자
                String regdate = String.valueOf(list.get(i).get("regdate")).replace("T", " ");
                String medicalDate = ChangeDateFormat(regdate, "yyyy.MM.dd");
                map.put("medicalDate", medicalDate);

                // 진료병원
                String hospitalName = String.valueOf(data.get("hospitalName"));
                map.put("hospitalName", hospitalName);

                // 질환
                String subjectName2 = "";
                if(!isNull(list.get(i).get("subjectCode"))) subjectName2 = MEDICAL_REQUEST_SUBJECT.get(Integer.parseInt(String.valueOf(list.get(i).get("subjectCode")))) + " 질환";
                map.put("subjectName", subjectName2);

                // 처방전 idx
                map.put("presIdx", String.valueOf(list.get(i).get("presIdx")));

                prevList.add(map);
            }
        }
        data.put("prevList", prevList);

        return data;
    }

    // 원격진료 접수 목록 수
    public Map<String, String> getRemoteReceiptCount(Doctor doctor, HttpServletRequest request) {
        // 검색
        String sqlSearch = getRemoteReceiptSearch(request);

        // 접수 목록
        int hospitalIdx = doctor.getHospitalIdx();
        List<Map<String, String>> list = hRemoteMapper.getRemoteReceiptCount(hospitalIdx, sqlSearch); // (병원idx, 검색조건)

        int statusR = 0;
        int statusY = 0;
        int statusN = 0;
        for (Map<String, String> data : list) {
            // 접수상태 카운트 R:대기 Y:접수 N:거부
            if(data.get("medicalStatus").equals("C") || data.get("hospitalRcptYn").equals("N")) ++statusN;
            else if(data.get("hospitalRcptYn").equals("Y")) ++statusY;
            else if(data.get("hospitalRcptYn").equals("R")) ++statusR;
        }

        Map<String, String> returnList = new HashMap<>();

        // 접수상태 카운트 R:대기 Y:접수 N:거부
        returnList.put("statusR", String.valueOf(statusR)); // 대기
        returnList.put("statusY", String.valueOf(statusY)); // 접수
        returnList.put("statusN", String.valueOf(statusN)); // 완료

        // 총접수
        int total = statusR + statusY + statusN;
        returnList.put("listTotalCnt", String.valueOf(total));

        return returnList;
    }

    // 원격진료 접수 목록
    public Map<String, Object> getRemoteReceipt(Doctor doctor, HttpServletRequest request, int startRecord, int endRecord) {
        Map<String, Object> returnList = new HashMap<>();

        // 검색
        String sqlSearch = getRemoteReceiptSearch(request);

        // limit
        if(isNull(request.getParameter("limit"))) endRecord = 15;
        else endRecord = Integer.parseInt(request.getParameter("limit"));

        // 접수 목록
        int hospitalIdx = doctor.getHospitalIdx();
        List<Map<String, String>> list = hRemoteMapper.getRemoteReceipt(hospitalIdx, sqlSearch, startRecord, endRecord); // (병원idx, 검색조건, limit start, limit end)

        List<Object> tempList = new LinkedList<>();
        for (Map<String, String> data : list) {
            // log.info("data: "+data.toString());

            // 진료시간
            String medicalTime = "";
            if(!isNull(data.get("medicalTime"))) medicalTime = data.get("medicalTime");
            data.put("medicalTime", medicalTime);

            // 질환
            String subjectName = "";
            if(!isNull(data.get("subjectCode"))) subjectName = MEDICAL_REQUEST_SUBJECT.get(Integer.parseInt(String.valueOf(data.get("subjectCode")))) + " 질환";
            data.put("subjectName", subjectName);

            // 환자이름/관계/연락처/주민등록번호
            // 관계가 본인이면 hd_medical(진료) 데이터 본인 외이면 hd_member_family(가족) 데이터
            String relationship = "본인";
            String patientName = data.get("patientName");
            String patientHp = data.get("patientHp");
            String patientRrNo = data.get("patientRrNo");
            if(Integer.parseInt(String.valueOf(data.get("patientIdx"))) != 0) {
                int famRel = Integer.parseInt(data.get("famRel"));
                if(famRel == 1) relationship = "부모";
                else if(famRel == 2) relationship = "배우자";
                else if(famRel == 3) relationship = "자녀";
            }
            data.put("patientName", patientName); // 환자이름
            data.put("relationship", relationship); // 관계
            data.put("patientHp", patientHp); // 연락처
            data.put("patientRrNo", patientRrNo); // 주민등록번호

            // 진료구분
            // 접수번호에서 추출 - F: 초진 S: 재진 (3개월 내 같은 질환으로 2회 이상 진료 예약한 환자)
            String rcptNoArr = data.get("rcptNo").substring(7, 8);
            String medicalType = rcptNoArr.equals("F") ? "초진" : "재진";
            data.put("medicalType", medicalType);

            // 상태 - 취소 시
            String cancelDate = "";
            String cancelReason = "";
            if(!isNull(data.get("cancelDate"))) {
                cancelDate = ChangeDateFormat(String.valueOf(data.get("cancelDate")).replace("T", " "), "yyyy-MM-dd");
                cancelReason = data.get("cancelReason");
            }
            data.put("cancelDate", cancelDate); // 취소일
            data.put("cancelReason", cancelReason); // 취소사유

            tempList.add(data);
        }
        returnList.put("listData", tempList);

        // log.info("returnList: "+returnList);
        return returnList;
    }

    // 원격진료 접수 검색 조건 (쿼리)
    public String getRemoteReceiptSearch(HttpServletRequest request) {
        String _search = "";

        // 진료일
        String _medicalDate = "";
        if(!isNull(request.getParameter("schDate"))) {
            _medicalDate = request.getParameter("schDate");
            _search += " AND A.regdate LIKE '%"+_medicalDate+"%'";
        }

        // 예약현황
        String _schStatus = "";
        if(!isNull(request.getParameter("schStatus"))) {
            _schStatus = request.getParameter("schStatus");
            log.info("schStatus: "+_schStatus);
            // 전체: 회원상태!=T AND 병원상태 IN (Y,N) AND 진료시간!=NULL
            // 진료대기: 병원상태=Y AND 회원상태=R AND (진료소견인덱스=NULL OR (처방전인덱스=0 AND 처방전有) OR (청구금액=0 AND 청구금액有))
            // 진료취소: 병원접수=N OR 회원상태=C
            // 진료진행중(비용청구): 병원상태=Y AND 회원상태!=T AND 진료소견인덱스!=NULL AND (처방전인덱스>0 OR 처방전無) AND (청구금액>0 OR 청구금액無) AND 비용청구(자동결제시간)=NULL
            // 진료완료(결제대기): 병원상태=Y AND 회원상태!=T AND 비용청구(자동결제시간)!=NULL AND 결제인덱스=0
            // 진료완료(결제완료): 병원상태=Y AND 회원상태!=T AND 비용청구(자동결제시간)!=NULL AND 결제인덱스>0
            _search += " AND A.medical_time IS NOT NULL"; // 취소의 경우 접수 완료 후 취소된 건만 보여주기 위해 medical_time(진료시간) 조건 추가
            if(_schStatus.equals("진료대기")) {
                _search += " AND (A.hospital_rcpt_yn = 'Y' AND A.medical_status = 'R' AND (D.idx IS NULL OR (A.pres_idx = 0 AND A.is_pres = 'Y') OR (A.total_bill_amt = 0 AND A.is_bill_amt = 'Y')))";
            } else if(_schStatus.equals("진료취소")) {
                _search += " AND (A.hospital_rcpt_yn = 'N' OR A.medical_status = 'C')";
            } else if(_schStatus.equals("비용청구")) {
                _search += " AND (A.hospital_rcpt_yn = 'Y' AND A.medical_status != 'T' AND A.idx IS NOT NULL AND (A.pres_idx > 0 OR A.is_pres = 'N') AND (A.total_bill_amt > 0 OR A.is_bill_amt = 'N') AND A.bill_date IS NULL)";
            } else if(_schStatus.equals("결제대기")) {
                _search += " AND (A.hospital_rcpt_yn = 'Y' AND A.medical_status != 'T' AND A.bill_date IS NOT NULL AND A.bill_stlm_idx = 0)";
            } else if(_schStatus.equals("결제완료")) {
                _search += " AND (A.hospital_rcpt_yn = 'Y' AND A.medical_status != 'T' AND A.bill_date IS NOT NULL AND A.bill_stlm_idx > 0)";
            } else {
                _search += " AND A.hospital_rcpt_yn IN ('Y','N')";
            }
        }

        // 검색조건
        String _item = "";
        String _keyword = "";
        if(!isNull(request.getParameter("schKeyword"))) {
            _item = request.getParameter("schItem");
            _keyword = request.getParameter("schKeyword");
            _search += " HAVING "+_item+" LIKE '%"+_keyword+"%'";
        }

        return _search;
    }

    // 처방전 정보
    public Map<String, Object> getPrescriptionInfo(HttpServletRequest request) {
        int presIdx = Integer.parseInt(request.getParameter("presIdx")); // 처방전idx
        Map<String, Object> data = hRemoteMapper.getPrescriptionInfo(presIdx);

        // 처방전 이미지
        String presImg = "";
        String presImgName = "";
        if(!isNull(data)) {
            if(!isNull(data.get("presImg"))) {
                presImg = String.valueOf(data.get("presImg"));
                presImgName = String.valueOf(data.get("presImgName"));
            }
            data.put("presImg", presImg);
            data.put("presImgName", presImgName);
        }

        return data;
    }

    // 처방전 등록/수정
    @Transactional
    public int prescriptionSave(Doctor doctor, MultipartHttpServletRequest request) {
        // 진료idx
        String medicalIdx = request.getParameter("idx");
        // 처방전idx
        int presIdx = Integer.parseInt(request.getParameter("presIdx"));
        // 처방전이 없습니다.
        String noPres = "";
        if(!isNull(request.getParameter("noPres"))) noPres = request.getParameter("noPres");

        int result = 0;
        if(!isNull(medicalIdx) && Integer.parseInt(medicalIdx) != 0) {
            // 처방전 없음
            if(noPres.equals("Y")) {
                if(presIdx != 0) hRemoteMapper.deletePrescription(presIdx); // 처방전 삭제
                String columnValue = "pres_idx = 0, is_pres = 'N'";
                result = hRemoteMapper.updateMedical(Integer.parseInt(medicalIdx), columnValue); // 진료DB에 처방전idx UPDATE
            }
            // 처방전 있음
            else {
                // 의사idx
                int doctorIdx = doctor.getIdx();
                // 진료정보
                Medical medical = hRemoteMapper.getMedicalInfo(Integer.parseInt(medicalIdx));
                // 병원idx
                int hospitalIdx = medical.getHospitalIdx();
                // 처방전 이미지
                // 파일업로드
                Map<String, String> uploadFileName = uploadFiles(medicalIdx, folderName, request);
                String presImg = null;
                String presImgName = null;
                if(!isNull(uploadFileName)){
                    presImg = uploadFileName.get("file1");
                    presImgName = uploadFileName.get("file1OriginName");
                }

                MedicalPrescription prescription = new MedicalPrescription();
                prescription.setIdx(presIdx);
                prescription.setMedicalIdx(Integer.parseInt(medicalIdx));
                prescription.setHospitalIdx(hospitalIdx);
                prescription.setPresImg(presImg);
                prescription.setPresImgName(presImgName);
                prescription.setDoctorIdx(doctorIdx);

                if(presIdx == 0) {
                    result = hRemoteMapper.insertPrescription(prescription); // 처방전 등록
                    if(result == 1) {
                        String columnValue = "pres_idx = " + prescription.getIdx() + ", is_pres = 'Y'";
                        hRemoteMapper.updateMedical(Integer.parseInt(medicalIdx), columnValue); // 진료DB에 처방전idx UPDATE

                        // 우리동네주치의 진료이면 처방전등록 후 연계약국으로 약조제요청 자동 등록
                        if (request.getParameter("myDrYn").equals("Y")) {
                            int myDrPidx = (request.getParameter("myDrPidx")!=null)? Integer.parseInt(request.getParameter("myDrPidx")):0;
                            remoteService.selectPharmacyAction(Integer.parseInt(medicalIdx), myDrPidx);
                        }
                    }
                } else {
                    result = hRemoteMapper.updatePrescription(prescription); // 처방전 수정
                }
            }
        }

        return result;
    }

    // 파일업로드
    public Map<String, String> uploadFiles(String idx, String folderName, MultipartHttpServletRequest request) {
        /**
         * 파일업로드 START
         * - 등록/수정을 모두 하는 경우
         * - multiple 구현 X
         **/
        nCloudStorageUtils.createFolder(folderName);

        // 파일명 ( 수정시 체크 originFile1 = 업로드된 파일명 )
        Map<String, String> uploadFileName = new HashMap<>();
        uploadFileName.put("file1", request.getParameter("originFile1")); // 처방전이미지
        log.info("fileUpload: "+request.getParameter("originFile1"));

        // 업로드파일
        Map<String, MultipartFile> files = request.getFileMap();
        // 파일 요소 읽음
        Iterator<Map.Entry<String, MultipartFile>> iterator = files.entrySet().iterator();
        // 첨부파일 체크
        while (iterator.hasNext()) {
            Map.Entry<String, MultipartFile> entry = iterator.next(); // file = entry.getValue()

            String elementName = entry.getValue().getName(); // input name (file1, file2, file3)
            long fileSize = entry.getValue().getSize();
            String fileOriginName = entry.getValue().getOriginalFilename(); // input 원본파일명
            String fileExt = fileOriginName.substring(fileOriginName.lastIndexOf(".") + 1); // 확장자
            //log.info(elementName + "/" + fileSize + "/" + file.getValue());

            if (fileSize > 0) { // 첨부파일이 존재하면
                String _fileName = Functions.getTimeStamp() + elementName + "." + fileExt; // 파일명 생성
                String url = nCloudStorageUtils.uploadFile(folderName, _fileName, entry.getValue()); // 업로드 처리
                //log.info("url="+url);

                // 수정시 첨부파일 새로등록하면 서버 파일삭제
                if (!idx.isEmpty() && uploadFileName.containsValue(elementName)) { // originFile3이 존재하면 삭제
                    nCloudStorageUtils.deleteObject(uploadFileName.get(elementName));
                }

                uploadFileName.put(elementName, url);
                uploadFileName.put(elementName + "OriginName", fileOriginName); // ex. file1OriginName
            }
        }
        /** 파일업로드 END **/

        return uploadFileName;
    }

    // 청구금액 정보
    public Map<String, Object> getBillAmtInfo(Doctor doctor, HttpServletRequest request) {
        Medical medical = hRemoteMapper.getMedicalInfo(Integer.parseInt(request.getParameter("idx")));
        Map<String, Object> map = new HashMap<>();

        map.put("medicalIdx", medical.getIdx());
        map.put("hospitalIdx", medical.getHospitalIdx());
        map.put("nonReimbBillAmt", medical.getNonReimbBillAmt());
        map.put("patientBillAmt", medical.getPatientBillAmt());
        map.put("totalBillAmt", medical.getTotalBillAmt());
        map.put("isBillAmt", medical.getIsBillAmt());

        return map;
    }

    // 청구금액 저장/수정
    public int billAmtSave(Doctor doctor, HttpServletRequest request) {
        // 진료idx
        String medicalIdx = request.getParameter("idx");
        // 청구 금액이 없습니다.
        String noAmt = "";
        if(!isNull(request.getParameter("noAmt"))) noAmt = request.getParameter("noAmt");

        int result = 0;
        if(!isNull(medicalIdx) && Integer.parseInt(medicalIdx) != 0) {
            String columnValue = "";
            if(noAmt.equals("Y")) { // 청구금액 없음 ==> 진료 상태 완료로 변경
                columnValue = " medical_status = 'Y', non_reimb_bill_amt = 0, patient_bill_amt = 0, total_bill_amt = 0, is_bill_amt = 'N'";
            } else {
                // 비급여액
                int noReimb = 0;
                if(!isNull(request.getParameter("nonReimb"))) noReimb = Integer.parseInt(request.getParameter("nonReimb").replaceAll("\\,", ""));
                // 환자부담금
                int patient = 0;
                if(!isNull(request.getParameter("patient"))) patient = Integer.parseInt(request.getParameter("patient").replaceAll("\\,", ""));
                // 총금액
                int total = noReimb + patient;

                columnValue = " non_reimb_bill_amt = "+noReimb+", patient_bill_amt = "+patient+", total_bill_amt = "+total+", is_bill_amt = 'Y'";
            }
            result = hRemoteMapper.updateMedical(Integer.parseInt(medicalIdx), columnValue); // 진료DB에 청구금액정보 UPDATE
        }

        return result;
    }

}