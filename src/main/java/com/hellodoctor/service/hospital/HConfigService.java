package com.hellodoctor.service.hospital;

import com.google.gson.Gson;
import com.hellodoctor.common.Constants;
import com.hellodoctor.common.Functions;
import com.hellodoctor.common.NCloudStorageUtils;
import com.hellodoctor.mapper.app.FileUploadMapper;
import com.hellodoctor.mapper.hospital.DoctorMapper;
import com.hellodoctor.mapper.hospital.HConfigMapper;
import com.hellodoctor.model.app.Hospital;
import com.hellodoctor.model.app.Pharmacy;
import com.hellodoctor.model.hospital.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static com.hellodoctor.common.Functions.isNull;

import static com.hellodoctor.common.Functions.ChangeDateFormat;

/**
 * 관리 관련 service 클래스
 */
@Service
@Slf4j
public class HConfigService {
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    FileUploadMapper fileUploadMapper;
    @Autowired
    NCloudStorageUtils nCloudStorageUtils;
    @Autowired
    DoctorService doctorService;
    @Autowired
    DoctorMapper doctorMapper;
    @Autowired
    HConfigMapper hConfigMapper;

    // 파일업로드 폴더명
    private String folderName = "doctor"; // 의사
    private String folderName2 = "hospital"; // 병원
    private String folderName3 = "template"; // 템플릿

    // 정산계정관리 - 저장
    public int saveAdmAccount(Doctor doctor, MultipartHttpServletRequest request) {
        int result = 0;
        String idx = request.getParameter("idx");
        String userName = request.getParameter("userName");
        String userHp = request.getParameter("userHp");
        String userPassword = request.getParameter("userPassword");
        String licenseNo = request.getParameter("licenseNo");
        String history = request.getParameter("history");
        String introduce = request.getParameter("introduce");

        if(!Functions.isNull(idx)) {
            doctor.setIdx(Integer.parseInt(idx));
            doctor.setUserName(userName);
            doctor.setUserHp(userHp);
            doctor.setLicenseNo(licenseNo);
            doctor.setHistory(history);
            doctor.setIntroduce(introduce);

            // 비밀번호 입력 시
            if(!Functions.isNull(userPassword)) {
                // 비밀번호 암호화
                doctor.setUserPassword(passwordEncoder.encode(userPassword));
            }

            // 파일업로드
            Map<String, String> uploadFileName = doctorService.uploadFiles(idx, folderName, request);

            // 의사-프로필
            doctor.setProfileImg(uploadFileName.get("file1"));
            // 의사-의사면허번호
            doctor.setLicenseNoImg(uploadFileName.get("file2"));
            if(!Functions.isNull(uploadFileName.get("file2OriginName"))) {
                doctor.setLicenseNoImgName(uploadFileName.get("file2OriginName"));
            }
            // 의사-전문의자격증
            doctor.setDoctorCertImg(uploadFileName.get("file3"));
            if(!Functions.isNull(uploadFileName.get("file3OriginName"))) {
                doctor.setDoctorCertImgName(uploadFileName.get("file3OriginName"));
            }

            result = doctorMapper.updateDoctor(doctor, "");
        }

        return result;
    }

    // 서브계정관리 - 목록 수
    public int getAdmSubAccountCount(int hospitalIdx, HttpServletRequest request) {
        // 검색
        String sqlSearch = "";
        String _item = "";
        String _keyword = "";
        if(!isNull(request.getParameter("schKeyword"))) {
            _item = request.getParameter("schItem");
            _keyword = request.getParameter("schKeyword");
            sqlSearch += " AND "+_item+" LIKE '%"+_keyword+"%'";
        }

        return hConfigMapper.getAdmSubAccountCount(hospitalIdx, sqlSearch);
    }

    // 서브계정관리 - 목록
    public Map<String, Object> getAdmSubAccount(int hospitalIdx, HttpServletRequest request, int startRecord, int endRecord) {
        Map<String, Object> returnList = new HashMap<>();

        // 검색
        String sqlSearch = "";
        String _item = "";
        String _keyword = "";
        if(!isNull(request.getParameter("schKeyword"))) {
            _item = request.getParameter("schItem");
            _keyword = request.getParameter("schKeyword");
            sqlSearch += " AND "+_item+" LIKE '%"+_keyword+"%'";
        }

        // limit
        if(isNull(request.getParameter("limit"))) endRecord = 15;
        else endRecord = Integer.parseInt(request.getParameter("limit"));

        List<Map<String, String>> accountList = hConfigMapper.getAdmSubAccount(hospitalIdx, sqlSearch, startRecord, endRecord); // (병원idx, 검색조건, limit start, limit end)

        List<Object> tempList = new LinkedList<>();
        for (Map<String, String> map : accountList) {
            int subjectCode = Integer.parseInt(map.get("subSubjectCode"));
            String subjectName = Constants.HOSPITAL_SUBJECT_CODE.get(subjectCode); // 진료과
            map.put("subjectName", subjectName);

            String regdate = map.get("regdate").replace("-", "."); // 생성일
            map.put("regdate", regdate);

            tempList.add(map);
        }
        returnList.put("listData", tempList);

        return returnList;
    }

    // 서브계정관리 - 등록 - 진료과 정보
    public Map<Integer, String> getSubjectCodeInfo(int hospitalIdx) {
        // 현재 병원의 진료과목 정보
        HospitalSubject hospitalSubject = doctorMapper.getSubjectInfo(hospitalIdx, "hd_hospital_subject");
        String[] subjectCodeArr = hospitalSubject.getSubjectCode().split(",");

        Map<Integer, String> subjectCode = new LinkedHashMap<>();
        for(int i=0; i<subjectCodeArr.length; i++) {
            int code = Integer.parseInt(subjectCodeArr[i]);
            subjectCode.put(code, Constants.HOSPITAL_SUBJECT_CODE.get(code));
        }

        return subjectCode;
    }

    // 서브계정관리 - 저장
    public int saveAdmSubAccount(Doctor doctor, MultipartHttpServletRequest request) {
        // (수정시) 인덱스
        String idx = request.getParameter("idx"); // 의사idx
        // idx 0으로 받아오면 빈문자열로 변경
        idx = Integer.parseInt(idx) == 0 ? "" : idx;

        Doctor doctor2 = new Doctor();
        doctor2.setOriginYn(request.getParameter("originYn")); // 주계정인지 부계정인지 ==> 부계정
        doctor2.setHospitalIdx(doctor.getHospitalIdx()); // 병원idx
        doctor2.setUserId(request.getParameter("userId")); // 아이디
        doctor2.setUserName(request.getParameter("userName")); // 의사명
        if(!Functions.isNull(request.getParameter("userPassword"))) { // 비밀번호 입력 시
            doctor2.setUserPassword(passwordEncoder.encode(request.getParameter("userPassword"))); // 비밀번호 암호화
        } else {
            // 수정 시 비밀번호 미입력이면 기존 비밀번호 그대로
            Map<String, Object> subDoctor = doctorService.getDoctorInfo(Integer.parseInt(idx));
            doctor2.setUserPassword(String.valueOf(subDoctor.get("userPassword")));
        }
        doctor2.setUserHp(request.getParameter("userHp")); // 전화번호
        doctor2.setSubPosition(request.getParameter("subPosition")); // 직급
        doctor2.setSubSubjectCode(request.getParameter("subSubjectCode")); // 진료과(코드)
        // 파일업로드
        Map<String, String> uploadFileName = doctorService.uploadFiles(idx, folderName, request);
        // 의사면허번호
        doctor2.setLicenseNo(request.getParameter("licenseNo"));
        doctor2.setLicenseNoImg(uploadFileName.get("file2"));
        String file2Name = request.getParameter("originFileName2");
        if(!Functions.isNull(uploadFileName.get("file2OriginName"))) file2Name = uploadFileName.get("file2OriginName");
        doctor2.setLicenseNoImgName(file2Name);

        int result = 0;
        if (isNull(idx)) {
            result = doctorMapper.insertDoctor(doctor2); // 의사 저장
        } else {
            doctor2.setIdx(Integer.parseInt(idx));
            result = doctorMapper.updateDoctor(doctor2, ""); // 의사 수정
        }

        return result;
    }

    // 서브계정관리 - 삭제
    public int deleteDoctor(int idx) {
        return doctorMapper.deleteDoctor(idx);
    }

    // 병원정보관리 - 저장
    public int saveAdmHospital(Doctor doctor, Hospital hospital, MultipartHttpServletRequest request) {
        int result = 0;
        String hospitalIdx = request.getParameter("hospitalIdx");

        if(!Functions.isNull(hospitalIdx)) {
            hospital.setIdx(Integer.parseInt(hospitalIdx));

            // 전화번호
            String[] _telNoArr = request.getParameterValues("telNo");
            String telNo = "";
            for(int i=0; i<_telNoArr.length; i++) {
                if(!Functions.isNull(_telNoArr[i])) telNo = telNo.concat(_telNoArr[i] + "-");
            }
            telNo = telNo.substring(0, telNo.length() - 1); // 마지막문자(-) 제거
            hospital.setTelNo(telNo);

            // 팩스번호
            String[] _faxNoArr = request.getParameterValues("faxNo");
            String faxNo = "";
            for(int i=0; i<_faxNoArr.length; i++) {
                if(!Functions.isNull(_faxNoArr[i])) faxNo = faxNo.concat(_faxNoArr[i] + "-");
            }
            faxNo = faxNo.substring(0, faxNo.length() - 1); // 마지막문자(-) 제거
            hospital.setFaxNo(faxNo);

            // 파일업로드
            Map<String, String> uploadFileName = doctorService.uploadFiles(hospitalIdx, folderName2, request);
            // 병원-대표이미지
            hospital.setMainImg(uploadFileName.get("file1"));
            // 병원-사업자등록증
            hospital.setBrNoImg(uploadFileName.get("file2"));
            String file2Name = request.getParameter("originFileName2");
            if(!Functions.isNull(uploadFileName.get("file2OriginName"))) file2Name = uploadFileName.get("file2OriginName");
            hospital.setBrNoImgName(file2Name);
            // 병원-통신서비스이용증명원
            hospital.setSvcCertImg(uploadFileName.get("file3"));
            String file3Name = request.getParameter("originFileName3");
            if(!Functions.isNull(uploadFileName.get("file3OriginName"))) file3Name = uploadFileName.get("file3OriginName");
            hospital.setSvcCertImgName(file3Name);
            // 비급여진료비 json
            hospital.setNonReimbJson(hospital.getNonReimbJson());
            // 진료과목 저장 (병원idx, 진료과목코드배열)
            String[] subjectCodeArr = request.getParameter("subjectCode").split(",");

            String columnValue = "is_update = 'Y'";
            doctorMapper.updateHospital(hospital, columnValue); // 병원 정보 업데이트
            result = doctorMapper.updateHospitalTmp(hospital, Integer.parseInt(hospitalIdx)); // 병원 정보 임시테이블 저장 (관리자 승인 전까지 임시테이블에 저장)
            if(result != 0) doctorMapper.insertSubject(Integer.parseInt(hospitalIdx), subjectCodeArr, "hd_hospital_subject_tmp");
        }

        return result;
    }

    // 정산계좌관리 - 정산정보
    public Hospital getHospitalInfo(int hospitalIdx) {
        return doctorMapper.getHospitalInfo(hospitalIdx);
    }

    // 정산계좌관리 - 저장
    public int saveAccount(MultipartHttpServletRequest request) {
        String hospitalIdx = request.getParameter("hospitalIdx");
        int result = 0;

        if (!hospitalIdx.isEmpty()) {
            Hospital hospital = new Hospital();
            hospital.setIdx(Integer.parseInt(hospitalIdx));
            // hospital.setBankCode(request.getParameter("bankCode"));
            // hospital.setAccountName(request.getParameter("accountName"));
            // hospital.setAccountNum(request.getParameter("accountNum"));
            String bankCode = request.getParameter("bankCode");
            String AccountName = request.getParameter("accountName");
            String AccountNum = request.getParameter("accountNum");

            // 병원 정보 업데이트
            String columnValue = "bank_code = '"+ bankCode +"', account_name = '"+ AccountName +"', account_num = '"+ AccountNum +"'";

            MultipartFile file = request.getFile("bankImg");
            if (file != null && file.getSize() > 0) {
                // 파일업로드
                Map<String, String> uploadFileName = doctorService.uploadFiles(hospitalIdx, folderName2, request);
                // log.info(uploadFileName.toString());
                columnValue += ", bank_img = '"+ uploadFileName.get("bankImg") +"', bank_img_name = '"+ uploadFileName.get("bankImgOriginName") +"'";
            }

            result = doctorMapper.updateHospital(hospital, columnValue);
        }

        return result;
    }

    // 지정약국관리 - 신규등록 목록
    // 검색어 없으면 병원주소기준 500m 이내 약국 노출
    public List<Pharmacy> getSearchPharmacyList(int hospitalIdx, String lat, String lng, String sfl, String stx) {
        String columnValue = "";
        String schTxt = (stx!=null)? stx.replaceAll(" ", "") : "";
        if (!schTxt.equals("")) {
            switch (sfl) {
                case "name" :   // 약국이름
                    columnValue = "REPLACE(A.pharmacy_name, ' ', '') LIKE '%"+ schTxt +"%'";
                    break;
                case "addr" :   // 주소
                    columnValue = "(A.main_addr LIKE '%"+ schTxt +"%' OR A.detail_addr LIKE '%"+ schTxt +"%' OR A.area_gu LIKE '%"+ schTxt +"%' OR A.area_dong LIKE '%"+ schTxt +"%')";
                    break;
                case "tel" :    // 연락처
                    columnValue = "REPLACE(A.tel_no, '-', '') LIKE '%"+ schTxt +"%'";
                    break;
            }
        }
        return hConfigMapper.getSearchPharmacyList(hospitalIdx, lat, lng, columnValue);
    }

    // 지정약국관리 신규등록 처리
    @Transactional
    public int saveDesignatePharmacy(int hospitalIdx, String chkIdx) {
        int result = 0;
        List<String> chkIdxArr = Arrays.asList(chkIdx.split(","));

        // 1. 신규등록
        result = hConfigMapper.insertDesignatePharmacy(hospitalIdx, chkIdxArr);
        // 2. 중복제거
        if (result > 0) hConfigMapper.deduplicationPharmacy(hospitalIdx);

        return result;
    }

    // 지정약국관리 해제 처리
    public int deleteDesignatePharmacy(int hospitalIdx, int idx) {
        return hConfigMapper.deleteDesignatePharmacy(hospitalIdx, idx);
    }


    // 지정약국관리 전체글수
    public int getDesignatePharmacyTotalCount(int hospitalIdx, Map<String, String> params) {
        String columnValue = commonDesignatePharmacyQuery(params);
        return hConfigMapper.getDesignatePharmacyTotalCount(hospitalIdx, columnValue);
    }

    // 지정약국관리 목록
    public List<Map<String, String>> getDesignatePharmacyList(int startRecord, int endRecord, int hospitalIdx, Map<String, String> params) {
        String columnValue = commonDesignatePharmacyQuery(params);
        return hConfigMapper.getDesignatePharmacyList(startRecord, endRecord, hospitalIdx, columnValue);
    }

    // 지정약국관리 목록, 전체글수 쿼리조건
    public String commonDesignatePharmacyQuery(Map<String, String> params) {
        List<String> columnValueArr = new ArrayList<>();
        String schTxt = params.get("tstx"); // 키워드
        if (!schTxt.equals("")) {
            switch (params.get("tsfl")) {
                case "name" :   // 약국이름
                    columnValueArr.add("REPLACE(B.pharmacy_name, ' ', '') LIKE '%"+ schTxt +"%'");
                    break;
                case "addr" :   // 주소
                    columnValueArr.add("(B.main_addr LIKE '%"+ schTxt +"%' OR B.detail_addr LIKE '%"+ schTxt +"%' OR B.area_gu LIKE '%"+ schTxt +"%' OR B.area_dong LIKE '%"+ schTxt +"%')");
                    break;
                case "tel" :    // 연락처
                    columnValueArr.add("REPLACE(B.tel_no, '-', ' ') LIKE '%"+ schTxt +"%'");
                    break;
            }
        }

        // 등록일 시작
        if (!params.get("sdate").equals("")) columnValueArr.add("A.regdate >= '" + params.get("sdate") + " 00:00:00'");
        // 등록일 종료
        if (!params.get("edate").equals("")) columnValueArr.add("A.regdate <= '" + params.get("edate") + " 23:59:59'");

        return String.join(" AND ", columnValueArr);
    }

    // 병원 지정환자 수 - 지정약국 해지시 1명 이상이면 불가처리
    public int getDesignatePatientCnt(int hospitalIdx) {
        return hConfigMapper.getDesignatePatientCnt(hospitalIdx);
    }

    // 지정환자관리 템플릿 조회
    public DesignatePatientTemplate getHospitalTemplate(int hospitalIdx) {
        return hConfigMapper.getHospitalTemplate(hospitalIdx);
    }

    // 지정환자관리 템플릿 저장
    public int saveHospitalTemplate(MultipartHttpServletRequest request, DesignatePatientTemplate template) {
        /**
         * 파일업로드 START
         * - 등록/수정을 모두 하는 경우
         * - multiple 구현 X
         * **/
        // 폴더생성
        nCloudStorageUtils.createFolder(folderName3);

        MultipartFile file = request.getFile("tempFile");
        boolean fileDeleteChk = (String.valueOf(request.getParameter("tempImgDel")).equals("1"))? true:false;
        String tempImg = request.getParameter("tempImgUrl");
        String tempImgName = request.getParameter("tempImgName");

        if (file != null && file.getSize() > 0) {
            String fileOriginName = file.getOriginalFilename(); // input 원본파일명
            String fileExt = fileOriginName.substring(fileOriginName.lastIndexOf(".")+1); // 확장자
            String fileName = Functions.getTimeStamp() + template.getHospitalIdx() + "." + fileExt; // 파일명 생성
            String url = nCloudStorageUtils.uploadFile(folderName, fileName, file); // 업로드 처리
            log.info("url="+url);

            tempImg = url;
            tempImgName = fileOriginName;
            fileDeleteChk = true;

        } else if (fileDeleteChk) {
            tempImg = "";
            tempImgName = "";
        }

        // 기존 파일 삭제 (1.파일교체, 2.파일삭제 체크)
        if (fileDeleteChk && !request.getParameter("tempImgUrl").equals("")) {
            nCloudStorageUtils.deleteObject(request.getParameter("tempImgUrl"));
        }

        template.setTempImg(tempImg);
        template.setTempImgName(tempImgName);
        /** 파일업로드 END **/

        if (!Functions.isNull(template.getTitle())) {
            template.setTitle(Functions.subVarcharStr(template.getTitle(),200)); // varchar(200)
        }

        return (template.getIdx()==0)? hConfigMapper.insertHospitalTemplate(template) : hConfigMapper.updateHospitalTemplate(template);
    }

    // 지정환자관리 등록요청
    public Map<String, Object> reqeustDesignatePatient(Map<String, String> params) {
        String hp = params.get("hp");
        int hospitalIdx = Integer.parseInt(params.get("hospitalIdx"));
        int doctorIdx = Integer.parseInt(params.get("doctorIdx"));

        Map<String, Object> result = new HashMap<>();
        result.put("result", false);
        result.put("errMsg", "");

        /**
         * 등록요청 진행과정
         * 1. 휴대폰번호로 회원정보 조회 & 이미 등록요청한 상태인지 확인
         * 2. 병원지정환자DB insert
         * 3. 회원에게 템플릿 문자메시지 발송
         */
        DesignatePatient designatePatient = new DesignatePatient();
        designatePatient.setHospitalIdx(hospitalIdx);
        designatePatient.setDoctorIdx(doctorIdx);
        designatePatient.setMbIdx(0);
        designatePatient.setStatus("R");
        designatePatient.setMbHp(hp);

        // 1. 휴대폰번호로 회원정보 조회 & 이미 등록요청한 상태인지 확인
        Map<String, String> checkData = hConfigMapper.requestCheckData(hospitalIdx, hp);
        log.info(String.valueOf(checkData));

        if (!Functions.isNull(checkData)) {
            int readyCnt = Integer.parseInt(String.valueOf(checkData.get("readyCnt")));
            int appectCnt = Integer.parseInt(String.valueOf(checkData.get("appectCnt")));

            if (readyCnt > 0) {
                // 이미 등록요청 됨
                result.put("errMsg", "이미 요청 된 휴대폰번호 입니다.");
                return result;
            } else if (appectCnt > 0) {
                // 수락완료한 회원
                result.put("errMsg", "지정환자로 등록된 휴대폰번호 입니다.");
                return result;
            }

            if (checkData.get("mbIdx") != null) {
                // 회원정보 조회 성공 (null = 가입하지 않은 전화번호)
                int mbIdx = Integer.parseInt(String.valueOf(checkData.get("mbIdx")));
                designatePatient.setMbIdx(mbIdx);
            }
        }

        // 2. 병원지정환자DB insert
        int chk = hConfigMapper.insertDesignatePatient(designatePatient);
        if (chk == 0) {
            // 2.1 실패
            result.put("errMsg", "요청에 실패했습니다.<br>잠시 후 다시 시도해 주세요.");
            return result;
        }

        // 3. 회원에게 템플릿 문자메시지 발송
        // TBC: 문자발송 추가 - 템플릿 하단 필수 :: 구글 플레이스토어 : URL (고정) 애플 앱스토어 : URL (고정)
        DesignatePatientTemplate template = hConfigMapper.getHospitalTemplate(hospitalIdx);
        log.info("template=" + template);

        result.put("result", true);

        return result;
    }

    // 지정환자관리 전체글수
    public int getDesignatePatientTotalCount(int hospitalIdx, Map<String, String> params) {
        String columnValue = commonDesignatePatientQuery(params);
        return hConfigMapper.getDesignatePatientTotalCount(hospitalIdx, columnValue);
    }

    // 지정환자관리 목록
    public List<Map<String, String>> getDesignatePatientList(int startRecord, int endRecord, int hospitalIdx, Map<String, String> params) {
        String columnValue = commonDesignatePatientQuery(params);
        return hConfigMapper.getDesignatePatientList(startRecord, endRecord, hospitalIdx, columnValue);
    }

    // 지정환자관리 목록, 전체글수 쿼리조건
    public String commonDesignatePatientQuery(Map<String, String> params) {
        List<String> columnValueArr = new ArrayList<>();
        String schTxt = params.get("stx"); // 키워드
        if (!schTxt.equals("")) {
            switch (params.get("sfl")) {
                case "name" :   // 환자이름
                    columnValueArr.add("REPLACE(B.mb_name, ' ', '') LIKE '%"+ schTxt +"%'");
                    break;
                case "tel" :    // 연락처
                    columnValueArr.add("REPLACE(B.mb_hp, '-', '') LIKE '%"+ schTxt +"%'");
                    break;
            }
        }

        // 등록일 시작
        if (!params.get("sdate").equals("")) columnValueArr.add("A.regdate >= '" + params.get("sdate") + " 00:00:00'");
        // 등록일 종료
        if (!params.get("edate").equals("")) columnValueArr.add("A.regdate <= '" + params.get("edate") + " 23:59:59'");

        return String.join(" AND ", columnValueArr);
    }

    // 지정환자관리 등록요청내역 전체개수
    public int getAdmPatientRequestCount(int hospitalIdx) {
        return hConfigMapper.getAdmPatientRequestCount(hospitalIdx);
    }

    // 지정환자관리 등록요청내역 목록
    public List<String> getAdmPatientRequestList(int hospitalIdx, int startRecord, int endRecord) {
        List<Map<String, String>> requestList =hConfigMapper.getAdmPatientRequestList(hospitalIdx, startRecord, endRecord);
        List<String> jsonArray = new ArrayList<>();

        for (Map<String, String> map : requestList) {
            String _regdate = String.valueOf(map.get("regdate")).replace("T", " ");
            String _formatter = ChangeDateFormat(_regdate, "yyyy.MM.dd HH:mm");
            map.put("regdate", _formatter);

            String json = new Gson().toJson(map);
            jsonArray.add(json);
        }

        return jsonArray;
    }

    // 지정약국 수
    public int getPharmacyCount(Integer hospitalIdx) {
        return hConfigMapper.getDesignatePharmacyTotalCount(hospitalIdx, "");
    }

    // 진료예약시간(특정일) 목록
    public String getAppointmentList(int hospitalIdx) {
        String data = hConfigMapper.getAppointmentList(hospitalIdx);
        String returnData = "";
        if(!isNull(data)) returnData = data;

        return returnData;
    }

    // 진료예약시간(특정일) 조회
    public Map<String, String> getAppointment(int hospitalIdx, String date) {
        Map<String, String> returnData = new LinkedHashMap<>();
        for(int i=1; i<=2; i++) {
            String data = hConfigMapper.getAppointment(hospitalIdx, String.valueOf(i), date);
            if(!isNull(data)) returnData.put("data"+i, data);
        }

        return returnData;
    }

    // 진료예약시간(특정일) 저장
    public int saveAppointment(int hospitalIdx, String date, String timeTable, String d_timeTable) {
        int result = 0;

        // 일반환자시간
        String patientType = "2";
        String data = hConfigMapper.getAppointment(hospitalIdx, patientType, date);
        if(!isNull(data)) result = hConfigMapper.updateAppointment(hospitalIdx, patientType, date, timeTable); // 수정
        else result = hConfigMapper.insertAppointment(hospitalIdx, patientType, date, timeTable); // 등록

        // 지정환자시간
        if(!isNull(d_timeTable)) {
            patientType = "1";
            data = hConfigMapper.getAppointment(hospitalIdx, patientType, date);
            if(!isNull(data)) result = hConfigMapper.updateAppointment(hospitalIdx, patientType, date, d_timeTable); // 수정
            else result = hConfigMapper.insertAppointment(hospitalIdx, patientType, date, d_timeTable); // 등록
        }

        return result;
    }

    // 진료예약관리 저장
    public int saveAdmTreat(Doctor doctor, HttpServletRequest request) {
        int result = 0;

        // 병원영업시간
        String[] yoilArr = request.getParameter("yoil").split(",");
        String[] startHourArr = request.getParameter("startHour").split(",");
        String[] endHourArr = request.getParameter("endHour").split(",");
        // 예약진료시간 - 일반환자시간
        String weekdayTimeArr2 = request.getParameter("weekdayTimeArr"); // 평일
        String weekendTimeArr2 = request.getParameter("weekendTimeArr"); // 주말
        String holidayTimeArr2 = request.getParameter("holidayTimeArr"); // 일요일/공휴일
        // 예약진료시간 - 지정환자시간
        String weekdayTimeArr1 = request.getParameter("d_weekdayTimeArr"); // 평일
        String weekendTimeArr1 = request.getParameter("d_weekendTimeArr"); // 주말
        String holidayTimeArr1 = request.getParameter("d_holidayTimeArr"); // 일요일/공휴일

        int hospitalIdx = doctor.getHospitalIdx();
        List<Map<String, String>> hourList = new ArrayList<>();
        List<Map<String, String>> timeList = new ArrayList<>();
        if(!isNull(hospitalIdx)) {
            for(int i=0; i<yoilArr.length; i++) {
                String yoil = yoilArr[i];

                // 병원영업시간
                Map<String, String> hourMap;
                if(yoil.equals("0")) { // 평일 (평일이면 1~5로 입력)
                    for(int j=0; j<=5; j++) { // 월화수목금
                        hourMap = new HashMap<>();
                        hourMap.put("yoil", String.valueOf(j));
                        hourMap.put("startHour", startHourArr[i]);
                        hourMap.put("endHour", endHourArr[i]);
                        hourList.add(hourMap);
                    }
                } else {
                    hourMap = new HashMap<>();
                    hourMap.put("yoil", yoil);
                    hourMap.put("startHour", startHourArr[i]);
                    hourMap.put("endHour", endHourArr[i]);
                    hourList.add(hourMap);
                }

                // 예약진료시간
                Map<String, String> timeMap;
                if(yoil.equals("6")) { // 주말
                    if(weekendTimeArr1.length() != 0) {
                        timeMap = new HashMap<>();
                        timeMap.put("patientType", "1");
                        timeMap.put("yoilType", yoil); // 토요일
                        timeMap.put("timeTable", weekendTimeArr1);
                        timeList.add(timeMap);
                    }
                    if(weekendTimeArr2.length() != 0) {
                        timeMap = new HashMap<>();
                        timeMap.put("patientType", "2");
                        timeMap.put("yoilType", yoil); // 토요일
                        timeMap.put("timeTable", weekendTimeArr2);
                        timeList.add(timeMap);
                    }
                }
                else if(yoil.equals("7")) { // 일요일
                    if(holidayTimeArr1.length() != 0) {
                        timeMap = new HashMap<>();
                        timeMap.put("patientType", "1");
                        timeMap.put("yoilType", yoil); // 일요일
                        timeMap.put("timeTable", holidayTimeArr1);
                        timeList.add(timeMap);
                    }
                    if(holidayTimeArr2.length() != 0) {
                        timeMap = new HashMap<>();
                        timeMap.put("patientType", "2");
                        timeMap.put("yoilType", yoil); // 일요일
                        timeMap.put("timeTable", holidayTimeArr2);
                        timeList.add(timeMap);
                    }
                }
                else if(yoil.equals("0")) { // 평일 (평일이면 1~5로 입력)
                    if(weekdayTimeArr1.length() != 0) {
                        for(int j=1; j<=5; j++) { // 월화수목금
                            timeMap = new HashMap<>();
                            timeMap.put("patientType", "1");
                            timeMap.put("yoilType", String.valueOf(j));
                            timeMap.put("timeTable", weekdayTimeArr1);
                            timeList.add(timeMap);
                        }
                    }
                    if(weekdayTimeArr2.length() != 0) {
                        for(int j=1; j<=5; j++) { // 월화수목금
                            timeMap = new HashMap<>();
                            timeMap.put("patientType", "2");
                            timeMap.put("yoilType", String.valueOf(j));
                            timeMap.put("timeTable", weekdayTimeArr2);
                            timeList.add(timeMap);
                        }
                    }
                }
                else { // 월화수목금
                    if(weekdayTimeArr1.length() != 0) {
                        timeMap = new HashMap<>();
                        timeMap.put("patientType", "1");
                        timeMap.put("yoilType", yoil);
                        timeMap.put("timeTable", weekdayTimeArr1);
                        timeList.add(timeMap);
                    }
                    if(weekdayTimeArr2.length() != 0) {
                        timeMap = new HashMap<>();
                        timeMap.put("patientType", "2");
                        timeMap.put("yoilType", yoil);
                        timeMap.put("timeTable", weekdayTimeArr2);
                        timeList.add(timeMap);
                    }
                }
            }
            // log.info("hourList: "+hourList);
            // log.info("timeList: "+timeList);

            // 병원 정보 업데이트
            String columnValue = "each_set_yn = '" + request.getParameter("eachSetYn") + "', rest_start_hour = " + request.getParameter("restStartHour")+", rest_end_hour = " + request.getParameter("restEndHour");
            Hospital hospital = new Hospital();
            hospital.setIdx(hospitalIdx);
            result = doctorMapper.updateHospital(hospital, columnValue);
            if(result != 0) {
                // 병원영업시간 저장 (병원idx, 영업시간정보)
                if(hourList.toArray().length != 0) {
                    doctorMapper.insertHour(hospitalIdx, hourList);
                }
                // 진료예약가능시간 저장 (병원idx, 예약시간정보)
                if(timeList.toArray().length != 0) {
                    hConfigMapper.saveAdmTreat(hospitalIdx, timeList);
                }
            }
        }

        return result;
    }

    // 진료예약시간 전체 조회
    public Map<String, Object> getAppointmentInfo(int hospitalIdx) {
        List<Map<String, Object>> list1 = new ArrayList<>();
        List<Map<String, Object>> list2 = new ArrayList<>();
        Map<String, Object> returnData = new LinkedHashMap<>();
        List<Map<String, Object>> appointment = doctorMapper.getAppointmentInfo(hospitalIdx);

        for (Map<String, Object> map : appointment) {
            if(map.get("patientType").equals("1")) { // 지정환자
                list1.add(map);
            } else { // 일반환자
                list2.add(map);
            }
        }
        returnData.put("list1", list1);
        returnData.put("list2", list2);

        return returnData;
    }
}