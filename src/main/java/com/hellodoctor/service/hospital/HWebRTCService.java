package com.hellodoctor.service.hospital;

import com.hellodoctor.common.Functions;
import com.hellodoctor.common.NCloudStorageUtils;
import com.hellodoctor.mapper.app.FileUploadMapper;
import com.hellodoctor.mapper.hospital.HRemoteMapper;
import com.hellodoctor.mapper.hospital.HWebRTCMapper;
import com.hellodoctor.model.app.FileUpload;
import com.hellodoctor.model.app.Medical;
import com.hellodoctor.model.hospital.Doctor;
import com.hellodoctor.model.hospital.MedicalOpinion;
import com.hellodoctor.model.hospital.MedicalPrescription;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.*;

import static com.hellodoctor.common.Constants.MEDICAL_REQUEST_SUBJECT;
import static com.hellodoctor.common.Functions.isNull;

/**
 * 원격진료 WebRTC 및 진료소견 service 클래스
 */
@Service
@Slf4j
public class HWebRTCService {
    @Autowired
    HWebRTCMapper hWebRTCMapper;
    @Autowired
    FileUploadMapper fileUploadMapper;

    // 진료실 환자정보 & 진료소견 & 기타첨부파일(증상이미지) 불러오기
    @Transactional
    public Map<String, Object> getRemoteMedicalInfo(int medicalIdx, int hospitalIdx) {
        Map<String, Object> result = new HashMap<>();

        // 1. 환자정보
        Map<String, String> patientInfo = hWebRTCMapper.getPatientInfoOne(medicalIdx, hospitalIdx);

        if (patientInfo != null) { // 내 환자 아님
            String patientName = "";
            int age = 0;
            String birth = "";
            String gender = "";

            if (String.valueOf(patientInfo.get("ptIdx")).equals("0")) { // 환자본인
                patientName = patientInfo.get("memberName");
                age = Functions.getAgeByBirthday(patientInfo.get("memberBirth"));
                gender = patientInfo.get("memberGender");
                birth = patientInfo.get("memberBirth").substring(0,4) + "-" + patientInfo.get("memberBirth").substring(4,6) + "-" + patientInfo.get("memberBirth").substring(6,8);

            } else { // 가족
                patientName = patientInfo.get("famName");

                String[] rrno = String.valueOf(patientInfo.get("famRrNo")).split("-");
                String rrnoBackFirstNum = rrno[1].substring(0, 1); // 뒷자리 첫번호
                String _birth = (rrnoBackFirstNum.equals("1") || rrnoBackFirstNum.equals("2")) ? "19" : "20";
                _birth += rrno[0];
                age = Functions.getAgeByBirthday(_birth);

                birth = _birth.substring(0,4) + "-" + _birth.substring(4,6) + "-" + _birth.substring(6,8);
                gender = (rrnoBackFirstNum.equals("1") || rrnoBackFirstNum.equals("3")) ? "남" : "여";
            }
            patientInfo.put("ptName", patientName);
            patientInfo.put("ptAge", String.valueOf(age));
            patientInfo.put("ptBirth", birth);
            patientInfo.put("ptGender", gender);
        }

        // 2. 진료소견
        MedicalOpinion medicalOpinion = hWebRTCMapper.getRemoteMedicalOpinion(medicalIdx);

        // 3. 기타첨부파일(증상이미지)
        String fileCode = patientInfo.get("ptFileCode");
        result.put("etcFiles", "");
        if (fileCode != null) {
            List<FileUpload> files = fileUploadMapper.getUploadFiles(fileCode, "medical", 0);
            result.put("etcFiles", files);
        }

        result.put("patientInfo", patientInfo);
        result.put("medicalOpinion", medicalOpinion);
        log.info("result=" + result);

        return result;
    }

    // 진료실 진료소견 저장
    public int saveMedicalOpinion(MedicalOpinion medicalOpinion) {
        int idx = medicalOpinion.getIdx();
        // 신규등록:수정
        return (idx==0)? hWebRTCMapper.insertMedicalOpinion(medicalOpinion) : hWebRTCMapper.updateMedicalOpinion(medicalOpinion);
    }
}
