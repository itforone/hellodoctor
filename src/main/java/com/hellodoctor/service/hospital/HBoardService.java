package com.hellodoctor.service.hospital;

import com.hellodoctor.common.Constants;
import com.hellodoctor.common.Functions;
import com.hellodoctor.common.NCloudStorageUtils;
import com.hellodoctor.mapper.app.FileUploadMapper;
import com.hellodoctor.mapper.hospital.HBoardMapper;
import com.hellodoctor.model.app.FileUpload;
import com.hellodoctor.model.app.MedicalReview;
import com.hellodoctor.model.hospital.AdminNotice;
import com.hellodoctor.model.hospital.CSSite;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.util.*;

/**
 * 게시판 관련 service 클래스
 */
@Service
@Slf4j
public class HBoardService {
    @Autowired
    FileUploadMapper fileUploadMapper;

    @Autowired
    NCloudStorageUtils nCloudStorageUtils;

    @Autowired
    HBoardMapper HBoardMapper;

    // 파일업로드 폴더명
    private String folderName = "csSite"; // cs문의

    // 공지사항 전체글수
    public int getNoticeTotalCount(String target) {
        return HBoardMapper.getNoticeTotalCount(target);
    }

    // 공지사항 목록
    public List<AdminNotice> getNotice(String target, int startRecord, int endRecord) {
        return HBoardMapper.getNoticeList(target, startRecord, endRecord);
    }

    // 리뷰 전체글수
    public int getReviewTotalCount(int hospitalIdx) {
        return HBoardMapper.getReviewTotalCount(hospitalIdx);
    }

    // 리뷰 목록
    public List<Map<String, String>> getReviewList(int hospitalIdx, int startRecord, int endRecord) {
        return HBoardMapper.getReviewList(hospitalIdx, startRecord, endRecord);
    }

    // 리뷰 상세
    public Map<String, String> getReviewDetail(int reviewIdx, int hospitalIdx) {
        Map<String, String> review = HBoardMapper.getReviewDetail(reviewIdx, hospitalIdx);

        if (review != null && !review.isEmpty()) {
            // 환자정보
            // 1. 이름 (본인?가족?)
            String patientName = review.get("memberName");
            if (!String.valueOf(review.get("patientIdx")).equals("0")) {
                if (review.get("famName") != null) patientName = review.get("famName");
            }
            review.put("patientName", patientName);
            // 2. 질환정보 (질환명, 주요증상)
            String subjectCode = String.valueOf(review.get("subjectCode"));
            String subjectStr = Constants.MEDICAL_REQUEST_SUBJECT.get(Integer.parseInt(subjectCode));
            review.put("subjectStr", subjectStr);
            // 3. 질환정보 (첨부파일)
            if (review.get("reqImageList") == null) review.put("reqImageList", "");


            // 진료만족도
            String checkItemHtml = "";
            if (review.get("checkItem") != null) {
                String[] checkItems = String.valueOf(review.get("checkItem")).split(",");
                for (String item : checkItems) {
                    int code = Integer.parseInt(item);
                    checkItemHtml += "<div>" + Constants.REVIEW_CHECK_ITEM_CODE.get(code) + "</div>";
                }
            }
            review.put("checkItemHtml", checkItemHtml);

            // 답변
            if (review.get("answer") == null) review.put("answer", "");
            // 처방전이미지
            if (review.get("presImg") == null) review.put("presImg", "");
        }

        return review;
    }

    // 리뷰 답변등록
    public int updateReviewReply(String answer, int reviewIdx, int doctorIdx) {
        MedicalReview reply = new MedicalReview();
        reply.setAnswer(answer);
        reply.setIdx(reviewIdx);
        reply.setDoctorIdx(doctorIdx);

        int update = HBoardMapper.updateReviewReply(reply);
        return update;
    }

    // CS문의 등록/수정
    public int registerCSForm(MultipartHttpServletRequest request, int doctorIdx, int hospitalIdx, String gubun) {
        // 이미지코드
        String fileCode = "";
        if (!Functions.isNull(request.getParameter("fileCode"))){
            fileCode = request.getParameter("fileCode");
        } else {
            fileCode = Functions.addZero(5, doctorIdx);
            fileCode += Functions.randomString(5);
        }

        CSSite cs = new CSSite();
        int idx = Integer.parseInt(request.getParameter("idx"));

        if (idx == 0) {
            // 등록
            cs.setGroupIdx(hospitalIdx);
            cs.setUserIdx(doctorIdx);
            cs.setGroupType(gubun);
            cs.setFileCode(fileCode);
        } else {
            // 수정
            cs.setIdx(idx);
        }
        // 등록,수정 공통
        cs.setCategory(Integer.parseInt(request.getParameter("category").toString()));
        cs.setSubject(Functions.subVarcharStr(String.valueOf(request.getParameter("subject")), 180));
        cs.setContent(request.getParameter("content"));

        /**
         * 파일업로드 START
         * - 등록만 하는 경우
         * - multiple 구현 O
         * **/
        // 폴더생성
        nCloudStorageUtils.createFolder(folderName);
        int order = 1;
        List<FileUpload> uploadFiles = new ArrayList<>();

        // 업로드파일
        MultiValueMap<String, MultipartFile> multiFiles = request.getMultiFileMap();
        // 파일 요소 읽음
        Iterator<Map.Entry<String, List<MultipartFile>>> iterator = multiFiles.entrySet().iterator();
        List<MultipartFile> multipartFileList = new ArrayList<>();
        // 다중업로드 (multiple) MultipartFile 로 분리
        while (iterator.hasNext()) {
            Map.Entry<String, List<MultipartFile>> entry = iterator.next();
            List<MultipartFile> fileList = entry.getValue();
            for (MultipartFile file : fileList) {
                if (file.isEmpty()) continue;
                multipartFileList.add(file);
            }
        }
        // 첨부파일 체크
        if (multipartFileList.size() > 0) {
            for (MultipartFile file : multipartFileList) {
                //String elementName = file.getName(); // input name (imageFile[0], imageFile[1], ...)
                long fileSize = file.getSize();
                String fileOriginName = file.getOriginalFilename(); // input 원본파일명
                String fileExt = fileOriginName.substring(fileOriginName.lastIndexOf(".")+1); // 확장자

                if (fileSize > 0) { // 첨부파일이 존재하면
                    String _fileName = Functions.getTimeStamp() + doctorIdx + order + "." + fileExt; // 파일명 생성
                    String url = nCloudStorageUtils.uploadFile(folderName, _fileName, file); // 업로드 처리

                    FileUpload fileUpload = new FileUpload();
                    fileUpload.setMatchCode(fileCode);
                    fileUpload.setTblName("csSite");
                    fileUpload.setFileUrl(url);
                    fileUpload.setOriginFileName(fileOriginName);
                    fileUpload.setSort(order);

                    uploadFiles.add(fileUpload);
                    order++;
                }
            }
        }
        /** 파일업로드 END **/

        int insertIdx = 0;

        // 문의등록/수정
        try {
            // 등록:수정
            int cnt = (idx == 0)? HBoardMapper.insertCSForm(cs) : HBoardMapper.updateCSForm(cs);
            log.info("HBoardMapperUpdate" + String.valueOf(cnt));
            if (cnt == 1) {
                insertIdx = cs.getIdx();
                // 파일업로드
                if (uploadFiles.size() > 0) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("list", uploadFiles);
                    fileUploadMapper.insertUploadFiles(map);
                }

                // (수정시) 파일삭제
                String[] deleteImages = request.getParameterValues("delImg[]");
                log.info("파일삭제확인" + String.valueOf(deleteImages));
                if (deleteImages != null && deleteImages.length > 0) {
                    for (String imgUrl : deleteImages) {
                        nCloudStorageUtils.deleteObject(imgUrl); // 서버삭제
                    }
                    List<String> delList = Arrays.asList(deleteImages);
                    fileUploadMapper.deleteUploadFiles(delList); // DB삭제
                }
            }

        } catch (Exception e) {
            log.info("registerCSForm() Exception=" + e);
        }

        return insertIdx;
    }

    // CS문의 상세보기, 수정폼
    public CSSite getCSForm(int idx, int doctorIdx) {
        CSSite cs = HBoardMapper.getCSFormOne(idx, doctorIdx);
        return cs;
    }

    // CS문의 전체글수
    public int getCSTotalCount(String target, int hospitalIdx) {
        return HBoardMapper.getCSTotalCount(target, hospitalIdx);
    }

    // CS문의 목록
    public List<CSSite> getCSList(String target, int hospitalIdx, int startRecord, int endRecord) {
        return HBoardMapper.getCSList(target, hospitalIdx, startRecord, endRecord);
    }

    // CS문의 삭제
    public int deleteCSForm(int idx) {
        return HBoardMapper.deleteCSForm(idx);

    }
}
