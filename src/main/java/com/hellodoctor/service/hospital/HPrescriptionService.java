package com.hellodoctor.service.hospital;

import com.hellodoctor.mapper.hospital.HPrescriptionMapper;
import com.hellodoctor.mapper.hospital.HRemoteMapper;
import com.hellodoctor.model.app.Medical;
import com.hellodoctor.model.hospital.Doctor;
import com.hellodoctor.service.app.RemoteService;
import com.hellodoctor.service.pharmacy.PFillService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.hellodoctor.common.Functions.isNull;

/**
 * 처방전 재발행 service 클래스
 */

@Service
@Slf4j
public class HPrescriptionService {
    @Autowired
    HPrescriptionMapper hPrescriptionMapper;
    @Autowired
    HRemoteMapper hRemoteMapper;
    @Autowired
    HRemoteService hRemoteService;
    @Autowired
    RemoteService remoteService;
    @Autowired
    PFillService pFillService;

    // 파일업로드 폴더명
    private String folderName = "prescription"; // 처방전

    // 처방전 재발행 목록 수
    public Map<String, String> getPxReissueCount(Doctor doctor, HttpServletRequest request) {
        // 검색
        String sqlSearch = getPxReissueSearch(request);

        int hospitalIdx = doctor.getHospitalIdx();
        List<Map<String, String>> list = hPrescriptionMapper.getPxReissueCount(hospitalIdx, sqlSearch); // (병원idx, 검색조건)

        int status1 = 0;
        int status2 = 0;
        int totalBillAmt = 0;
        for (Map<String, String> data : list) {
            // 처방전상태 카운트 1:재발행대기 2:재발행완료
            if(data.get("presStatus").equals("1")) ++status1;
            else if(data.get("presStatus").equals("2")) ++status2;
            // 본인부담금
            totalBillAmt += isNull(data.get("billAmt")) ? 0 : Integer.parseInt(String.valueOf(data.get("billAmt")));
        }

        Map<String, String> returnList = new HashMap<>();
        returnList.put("status1", String.valueOf(status1)); // 대기
        returnList.put("status2", String.valueOf(status2)); // 완료
        returnList.put("totalBillAmt", String.valueOf(totalBillAmt)); // 총 본인부담금

        // 총 접수
        int total = status1 + status2;
        returnList.put("listTotalCnt", String.valueOf(total));

        return returnList;
    }

    // 처방전 재발행 목록
    public Map<String, Object> getPxReissue(Doctor doctor, HttpServletRequest request, int startRecord, int endRecord) {
        Map<String, Object> returnList = new HashMap<>();

        // 검색
        String sqlSearch = getPxReissueSearch(request);

        // limit
        if(isNull(request.getParameter("limit"))) endRecord = 15;
        else endRecord = Integer.parseInt(request.getParameter("limit"));

        int hospitalIdx = doctor.getHospitalIdx();
        List<Map<String, String>> list = hPrescriptionMapper.getPxReissue(hospitalIdx, sqlSearch, startRecord, endRecord); // (병원idx, 검색조건, limit start, limit end)

        List<Object> tempList = new LinkedList<>();
        for (Map<String, String> data : list) {
            // 환자이름/연락처/주민등록번호
            String patientName = data.get("patientName");
            String patientHp = data.get("patientHp");
            String patientRrNo = data.get("patientRrNo");
            data.put("patientName", patientName); // 환자이름
            data.put("patientHp", patientHp); // 연락처
            data.put("patientRrNo", patientRrNo); // 주민등록번호

            // 처방전 재발행 요청일
            String reRequestDate = "";
            if(!isNull(data.get("reRequestDate"))) reRequestDate = String.valueOf(data.get("reRequestDate")).substring(0, 16);
            data.put("reRequestDate", reRequestDate);

            // 처방전 재발행 완료일
            String reCompleteDate = "";
            if(!isNull(data.get("reCompleteDate"))) reCompleteDate = String.valueOf(data.get("reCompleteDate")).substring(0, 16);
            data.put("reCompleteDate", reCompleteDate);

            if (isNull(data.get("rcptNo"))) data.put("rcptNo", "");
            if (isNull(data.get("reRcptNo"))) data.put("reRcptNo", "");
            if (isNull(data.get("regdate"))) data.put("receiptDate", "");
            if (isNull(data.get("patientName"))) data.put("patientName", "");
            if (isNull(data.get("patientHp"))) data.put("patientHp", "");
            if (isNull(data.get("patientRrNo"))) data.put("patientRrNo", "");
            if (isNull(data.get("presStatus"))) data.put("presStatus", "");
            if (isNull(data.get("billAmt"))) data.put("billAmt", "");
            if (isNull(data.get("presImg"))) data.put("presImg", ""); // 처방전 이미지
            if (isNull(data.get("presImgName"))) data.put("presImgName", "");

            tempList.add(data);
        }
        returnList.put("listData", tempList);

        return returnList;
    }

    // 처방전 재발행 검색 조건 (쿼리)
    public String getPxReissueSearch(HttpServletRequest request) {
        String _search = "";

        // 발행일
        String stDate = request.getParameter("schDateSt"); // 시작
        String edDate = request.getParameter("schDateEd"); // 종료
        if(!isNull(stDate) && !isNull(edDate)) {
            _search += " AND (DATE_FORMAT(re_request_date, '%Y-%m-%d') >= '"+stDate+"' AND DATE_FORMAT(re_request_date, '%Y-%m-%d') <= '"+edDate+"')";
        } else if(!isNull(stDate) && isNull(edDate)) {
            _search += " AND (DATE_FORMAT(re_request_date, '%Y-%m-%d') >= '"+stDate+"')";
        } else if(isNull(stDate) && !isNull(edDate)) {
            _search += " AND (DATE_FORMAT(re_request_date, '%Y-%m-%d') <= '"+edDate+"')";
        }

        // 재발행 현황
        String _status = "";
        if(!isNull(request.getParameter("schStatus"))) {
            _status = request.getParameter("schStatus");
            _search += " AND pres_status = '"+_status+"'";
        }

        // 검색조건
        String _item = "";
        String _keyword = "";
        if(!isNull(request.getParameter("schKeyword"))) {
            _item = request.getParameter("schItem");
            _keyword = request.getParameter("schKeyword");
            _search += " HAVING "+_item+" LIKE '%"+_keyword+"%'";
        }

        return _search;
    }

    // 재발행 처방전 정보
    public Map<String, Object> reissuePrescriptionInfo(HttpServletRequest request) {
        int presIdx = Integer.parseInt(request.getParameter("presIdx")); // 처방전idx
        Map<String, Object> data = hPrescriptionMapper.reissuePrescriptionInfo(presIdx);

        // 처방전 이미지
        if (isNull(data.get("presImg"))) data.put("presImg", "");
        if (isNull(data.get("presImgName"))) data.put("presImgName", "");

        return data;
    }

    // 재발행 처방전 저장
    public int saveReissuePrescription(MultipartHttpServletRequest request) {
        // 진료idx
        String medicalIdx = request.getParameter("idx");
        // 처방전idx
        int presIdx = Integer.parseInt(request.getParameter("presIdx"));

        // 처방전 이미지
        // 파일업로드
        Map<String, String> uploadFileName = hRemoteService.uploadFiles(medicalIdx, folderName, request);
        String presImg = null;
        String presImgName = null;
        if(!isNull(uploadFileName)){
            presImg = uploadFileName.get("file1");
            presImgName = uploadFileName.get("file1OriginName");
        }

        String columnValue = "pres_img = '"+presImg+"', pres_img_name = '"+presImgName+"', pres_status = '0'"; // 처방전 저장
        return hPrescriptionMapper.savePrescription(presIdx, columnValue);
    }

    // 재발행 처방전 - 발행
    public int prescriptionPublish(HttpServletRequest request) {
        // 처방전 idx
        int presIdx = Integer.parseInt(request.getParameter("presIdx"));
        // 본인 부담금
        int billAmt = Integer.parseInt(request.getParameter("billAmt"));

        // 진료 idx 및 약국 idx
        Map<String, String> map = hPrescriptionMapper.getIdxInfo(presIdx);
        int medicalIdx = Integer.parseInt(String.valueOf(map.get("medicalIdx"))); // 진료 idx
        log.info("medicalIdx: "+medicalIdx);
        int pharmacyIdx = Integer.parseInt(String.valueOf(map.get("pharmacyIdx"))); // 약국 idx
        log.info("pharmacyIdx: "+pharmacyIdx);

        // 처방전 저장
        String columnValue = "pres_status = '2', bill_amt = '"+billAmt+"', re_complete_date = now()";
        int result = hPrescriptionMapper.savePrescription(presIdx, columnValue);
        if(result != 0) {
            // 진료 DB에 처방전 idx UPDATE
            String columnValue2 = "pres_idx = " + presIdx;
            result = hRemoteMapper.updateMedical(medicalIdx, columnValue2);

            // 약 조제요청 INSERT
            remoteService.selectPharmacyAction(medicalIdx, pharmacyIdx);
            // 조제 접수 완료 상태로 UPDATE
            Medical medical = hRemoteMapper.getMedicalInfo(medicalIdx);
            int pharmaryReqIdx = medical.getPharmacyReqIdx();
            pFillService.statusComplete(pharmaryReqIdx, billAmt); // 약 조제접수 요청
            pFillService.fillComplete(pharmaryReqIdx); // 약 조제 접수 완료
        }

        return result;
    }
}