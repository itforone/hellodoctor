package com.hellodoctor.service.hospital;

import com.hellodoctor.common.Functions;
import com.hellodoctor.common.NCloudStorageUtils;
import com.hellodoctor.mapper.hospital.DoctorMapper;
import com.hellodoctor.model.app.Hospital;
import com.hellodoctor.model.hospital.Doctor;
import com.hellodoctor.model.hospital.HospitalSubject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.util.*;

/**
 * 의사 관련 service 클래스
 * @Transactional: 트랜잭션이 보장된 메소드로 설정
 */
@Service
@Slf4j
public class DoctorService {
    @Autowired
    DoctorMapper doctorMapper;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    NCloudStorageUtils nCloudStorageUtils;

    // 파일업로드 폴더명
    private String folderName = "doctor"; // 의사
    private String folderName2 = "hospital"; // 병원

    // 아이디찾기 - 연락처비교
    public Map<String, String> hpCheck(String hp, String email) {
        Map<String, String> result = new HashMap<>();

        Map<String, String> userData = doctorMapper.hpCheck(hp, email);
        if (userData == null) result.put("flag", "N");
        else {
            result.put("flag", "Y");
            result.put("email", userData.get("email"));
            result.put("regdate", String.valueOf(userData.get("regdate")));
        }

        // TODO: 문자발송 추가
        String certNo = Functions.issueCertNo(4);
        result.put("certNo", certNo);


        return result;
    }

    // 비밀번호 변경
    public int resetPassword(String email, String password) {
        password = passwordEncoder.encode(password);
        return doctorMapper.resetPassword(email, password);
    }

    // 아이디 중복체크
    public int idCheck(String id) {
        return doctorMapper.idCheck(id);
    }

    // 의사 회원가입 1(계정정보), 2(병원정보), 3(진료시간) 액션 (등록/수정)
    @Transactional
    public int doctorFormAction(Doctor doctor, Hospital hospital, MultipartHttpServletRequest request) {
        String step = request.getParameter("step"); // 회원가입 페이지 구분 (계정정보, 병원정보/비급여진료비 ...)
        // (수정시) 인덱스
        String idx = request.getParameter("idx"); // 의사idx
        String hospitalIdx = request.getParameter("hospitalIdx"); // 병원idx
        // idx 0으로 받아오면 빈문자열로 변경
        idx = Integer.parseInt(idx) == 0 ? "" : idx;
        hospitalIdx = Integer.parseInt(hospitalIdx) == 0 ? "" : hospitalIdx;

        int result = 0;
        if(step.equals("step1")) {
            // TBC: 가입 전 중복된 아이디 있는지 한번 더 확인

            // 주계정인지 부계정인지 ==> 주계정
            doctor.setOriginYn("Y");
            // 비밀번호 암호화
            doctor.setUserPassword(passwordEncoder.encode(doctor.getUserPassword()));
            // 파일업로드
            Map<String, String> uploadFileName = uploadFiles(idx, folderName, request);
            // 의사-프로필
            doctor.setProfileImg(uploadFileName.get("file1"));
            // 의사-의사면허번호
            doctor.setLicenseNoImg(uploadFileName.get("file2"));
            String file2Name = request.getParameter("originFileName2");
            if(!Functions.isNull(uploadFileName.get("file2OriginName"))) file2Name = uploadFileName.get("file2OriginName");
            doctor.setLicenseNoImgName(file2Name);
            // 의사-전문의자격증
            doctor.setDoctorCertImg(uploadFileName.get("file3"));
            String file3Name = request.getParameter("originFileName3");
            if(!Functions.isNull(uploadFileName.get("file3OriginName"))) file3Name = uploadFileName.get("file3OriginName");
            doctor.setDoctorCertImgName(file3Name);

            if (idx.isEmpty()) {
                result = doctorMapper.insertDoctor(doctor); // 의사 저장
            } else {
                doctor.setIdx(Integer.parseInt(idx));
                if(!hospitalIdx.isEmpty()) doctor.setHospitalIdx(Integer.parseInt(hospitalIdx));
                result = doctorMapper.updateDoctor(doctor, ""); // 의사 수정
            }
        }
        else if(step.equals("step2")) {
            // 전화번호
            String[] _telNoArr = request.getParameterValues("telNo");
            String telNo = "";
            for(int i=0; i<_telNoArr.length; i++) {
                if(!Functions.isNull(_telNoArr[i])) telNo = telNo.concat(_telNoArr[i] + "-");
            }
            telNo = telNo.substring(0, telNo.length() - 1); // 마지막문자(-) 제거
            hospital.setTelNo(telNo);

            // 팩스번호
            String[] _faxNoArr = request.getParameterValues("faxNo");
            String faxNo = "";
            for(int i=0; i<_faxNoArr.length; i++) {
                if(!Functions.isNull(_faxNoArr[i])) faxNo = faxNo.concat(_faxNoArr[i] + "-");
            }
            faxNo = faxNo.substring(0, faxNo.length() - 1); // 마지막문자(-) 제거
            hospital.setFaxNo(faxNo);

            // 파일업로드
            Map<String, String> uploadFileName = uploadFiles(hospitalIdx, folderName2, request);
            // 병원-대표이미지
            hospital.setMainImg(uploadFileName.get("file1"));
            // 병원-사업자등록증
            hospital.setBrNoImg(uploadFileName.get("file2"));
            String file2Name = request.getParameter("originFileName2");
            if(!Functions.isNull(uploadFileName.get("file2OriginName"))) file2Name = uploadFileName.get("file2OriginName");
            hospital.setBrNoImgName(file2Name);
            // 병원-통신서비스이용증명원
            hospital.setSvcCertImg(uploadFileName.get("file3"));
            String file3Name = request.getParameter("originFileName3");
            if(!Functions.isNull(uploadFileName.get("file3OriginName"))) file3Name = uploadFileName.get("file3OriginName");
            hospital.setSvcCertImgName(file3Name);
            // 비급여진료비 json
            hospital.setNonReimbJson(hospital.getNonReimbJson());
            // 진료과목
            String[] subjectCodeArr = request.getParameter("subjectCode").split(",");

            if(hospitalIdx.isEmpty()) {
                result = doctorMapper.insertHospital(hospital); // 병원 정보 저장
                if(result != 0) { // 저장 성공
                    int chkIdx = hospital.getIdx();

                    // 진료과목 저장 (병원idx, 진료과목코드배열, db table)
                    doctorMapper.insertSubject(chkIdx, subjectCodeArr, "hd_hospital_subject");

                    // 의사 - 병원 idx UPDATE
                    doctor.setIdx(Integer.parseInt(idx));
                    String columnValue = "hospital_idx = " + chkIdx;
                    doctorMapper.updateDoctor(doctor, columnValue);
                }
            } else {
                Hospital dbHospital = doctorMapper.getHospitalInfo(Integer.parseInt(hospitalIdx)); // DB에 저장된 병원 정보 (현재 페이지 수정 시 나머지 컬럼의 데이터 보존하기 위해 불러와서 같이 저장)
                hospital.setIdx(Integer.parseInt(hospitalIdx));
                hospital.setBankCode(dbHospital.getBankCode());
                hospital.setAccountName(dbHospital.getAccountName());
                hospital.setAccountNum(dbHospital.getAccountNum());
                hospital.setBankImg(dbHospital.getBankImg());
                hospital.setBankImgName(dbHospital.getBankImgName());
                hospital.setEachSetYn(dbHospital.getEachSetYn());
                hospital.setRestStartHour(dbHospital.getRestStartHour());
                hospital.setRestEndHour(dbHospital.getRestEndHour());

                result = doctorMapper.updateHospital(hospital, ""); // 병원 정보 업데이트
                if(result != 0) {
                    HospitalSubject hospitalSubject = doctorMapper.getSubjectInfo(Integer.parseInt(hospitalIdx), "hd_hospital_subject"); // 진료과목 정보
                    if(!request.getParameter("subjectCode").equals(hospitalSubject.getSubjectCode())) { // db에 저장된 진료과목과 다르면 진료과목 삭제 후 재저장
                        // 진료과목 저장 (병원idx, 진료과목코드배열, db table)
                        doctorMapper.insertSubject(Integer.parseInt(hospitalIdx), subjectCodeArr, "hd_hospital_subject");
                    }
                }
            }
        }
        else if(step.equals("step3")) { // 병원 영업 시간 / 예약 진료 시간
            // 병원영업시간
            String[] yoilArr = request.getParameter("yoil").split(",");
            String[] startHourArr = request.getParameter("startHour").split(",");
            String[] endHourArr = request.getParameter("endHour").split(",");
            // 예약진료시간
            String weekdayTimeArr = request.getParameter("weekdayTimeArr"); // 평일
            String weekendTimeArr = request.getParameter("weekendTimeArr"); // 주말
            String holidayTimeArr = request.getParameter("holidayTimeArr"); // 일요일/공휴일

            List<Map<String, String>> hourList = new ArrayList<>();
            List<Map<String, String>> timeList = new ArrayList<>();
            if(!hospitalIdx.isEmpty()) {
                for(int i=0; i<yoilArr.length; i++) {
                    String yoil = yoilArr[i];

                    // 병원영업시간
                    Map<String, String> hourMap;
                    if(yoil.equals("0")) { // 평일 (평일이면 1~5로 입력)
                        for(int j=0; j<=5; j++) { // 월화수목금
                            hourMap = new HashMap<>();
                            hourMap.put("yoil", String.valueOf(j));
                            hourMap.put("startHour", startHourArr[i]);
                            hourMap.put("endHour", endHourArr[i]);
                            hourList.add(hourMap);
                        }
                    } else {
                        hourMap = new HashMap<>();
                        hourMap.put("yoil", yoil);
                        hourMap.put("startHour", startHourArr[i]);
                        hourMap.put("endHour", endHourArr[i]);
                        hourList.add(hourMap);
                    }

                    // 예약진료시간
                    Map<String, String> timeMap;
                    if(yoil.equals("6")) { // 주말
                        if(weekendTimeArr.length() != 0) {
                            timeMap = new HashMap<>();
                            timeMap.put("yoilType", yoil); // 토요일
                            timeMap.put("timeTable", weekendTimeArr);
                            timeList.add(timeMap);
                        }
                    }
                    else if(yoil.equals("7")) { // 일요일
                        if(holidayTimeArr.length() != 0) {
                            timeMap = new HashMap<>();
                            timeMap.put("yoilType", yoil); // 일요일
                            timeMap.put("timeTable", holidayTimeArr);
                            timeList.add(timeMap);
                        }
                    }
                    else if(yoil.equals("0")) { // 평일 (평일이면 1~5로 입력)
                        if(weekdayTimeArr.length() != 0) {
                            for(int j=1; j<=5; j++) { // 월화수목금
                                timeMap = new HashMap<>();
                                timeMap.put("yoilType", String.valueOf(j));
                                timeMap.put("timeTable", weekdayTimeArr);
                                timeList.add(timeMap);
                            }
                        }
                    }
                    else { // 월화수목금
                        if(weekdayTimeArr.length() != 0) {
                            timeMap = new HashMap<>();
                            timeMap.put("yoilType", yoil);
                            timeMap.put("timeTable", weekdayTimeArr);
                            timeList.add(timeMap);
                        }
                    }
                }
                // log.info("hourList: "+hourList);
                // log.info("timeList: "+timeList);

                // 병원 정보 업데이트
                String columnValue = "each_set_yn = '" + request.getParameter("eachSetYn") + "', rest_start_hour = " + request.getParameter("restStartHour")+", rest_end_hour = " + request.getParameter("restEndHour");
                hospital.setIdx(Integer.parseInt(hospitalIdx));
                result = doctorMapper.updateHospital(hospital, columnValue);
                if(result != 0) {
                    // 병원영업시간 저장 (병원idx, 영업시간정보)
                    if(hourList.toArray().length != 0) {
                        doctorMapper.insertHour(Integer.parseInt(hospitalIdx), hourList);
                    }
                    // 진료예약가능시간 저장 (병원idx, 예약시간정보)
                    if(timeList.toArray().length != 0) {
                        doctorMapper.insertAppointment(Integer.parseInt(hospitalIdx), timeList);
                    }
                }
            }
        }

        if (result != 0) result = doctor.getIdx(); // 저장/수정 성공

        return result; // idx return
    }

    // 파일업로드
    public Map<String, String> uploadFiles(String idx, String folderName, MultipartHttpServletRequest request) {
        /**
         * 파일업로드 START
         * - 등록/수정을 모두 하는 경우
         * - multiple 구현 X
         **/
        nCloudStorageUtils.createFolder(folderName);

        // 파일명 ( 수정시 체크 originFile1 = 업로드된 파일명 )
        Map<String, String> uploadFileName = new HashMap<>();
        uploadFileName.put("file1", request.getParameter("originFile1")); // 의사-프로필 / 병원-대표이미지
        uploadFileName.put("file2", request.getParameter("originFile2")); // 의사-의사면허번호 / 병원-사업자등록증
        uploadFileName.put("file3", request.getParameter("originFile3")); // 의사-전문의자격증 / 병원-통신서비스이용증명원

        // 업로드파일
        Map<String, MultipartFile> files = request.getFileMap();
        // 파일 요소 읽음
        Iterator<Map.Entry<String, MultipartFile>> iterator = files.entrySet().iterator();
        // 첨부파일 체크
        while (iterator.hasNext()) {
            Map.Entry<String, MultipartFile> entry = iterator.next(); // file = entry.getValue()

            String elementName = entry.getValue().getName(); // input name (file1, file2, file3)
            long fileSize = entry.getValue().getSize();
            String fileOriginName = entry.getValue().getOriginalFilename(); // input 원본파일명
            String fileExt = fileOriginName.substring(fileOriginName.lastIndexOf(".") + 1); // 확장자
            //log.info(elementName + "/" + fileSize + "/" + file.getValue());

            if (fileSize > 0) { // 첨부파일이 존재하면
                String _fileName = Functions.getTimeStamp() + elementName + "." + fileExt; // 파일명 생성
                String url = nCloudStorageUtils.uploadFile(folderName, _fileName, entry.getValue()); // 업로드 처리
                //log.info("url="+url);

                // 수정시 첨부파일 새로등록하면 서버 파일삭제
                if (!idx.isEmpty() && uploadFileName.containsValue(elementName)) { // originFile3이 존재하면 삭제
                    nCloudStorageUtils.deleteObject(uploadFileName.get(elementName));
                }

                uploadFileName.put(elementName, url);
                uploadFileName.put(elementName + "OriginName", fileOriginName); // ex. file1OriginName
            }
        }
        /** 파일업로드 END **/

        return uploadFileName;
    }

    // 의사 회원가입 완료
    public int signUpComplete(MultipartHttpServletRequest request) {
        String hospitalIdx = request.getParameter("hospitalIdx");

        int result = 0;
        if(!hospitalIdx.isEmpty()) {
            // 파일업로드
            Map<String, String> uploadFileName = uploadFiles(hospitalIdx, folderName2, request);

            Hospital hospital = doctorMapper.getHospitalInfo(Integer.parseInt(hospitalIdx));
            hospital.setIdx(Integer.parseInt(hospitalIdx));
            hospital.setBankCode(request.getParameter("bankCode"));
            hospital.setAccountNum(request.getParameter("accountNum"));
            hospital.setAccountName(request.getParameter("accountName"));
            hospital.setAccountName(request.getParameter("accountName"));
            hospital.setBankImg(uploadFileName.get("file2")); // 병원-통장사본
            String file2Name = request.getParameter("originFileName2");
            if(!Functions.isNull(uploadFileName.get("file2OriginName"))) file2Name = uploadFileName.get("file2OriginName");
            hospital.setBankImgName(file2Name);

            result = doctorMapper.updateHospital(hospital, ""); // 병원 정보 업데이트
            if(result != 0) {
                String columnValue = "temp_yn = 'N'";
                result = doctorMapper.updateHospital(hospital, columnValue); // 병원 정보 업데이트
            }
        }

        return result;
    }

    // 의사 정보
    public Map<String, Object> getDoctorInfo(Integer idx) {
        Doctor data = doctorMapper.getDoctorInfo(idx);
        Map<String, Object> result = new HashMap<>();

        if (data != null) {
            result.put("idx", data.getIdx());
            result.put("hospitalIdx", data.getHospitalIdx());
            result.put("userName", data.getUserName());
            result.put("userId", data.getUserId());
            result.put("userHp", data.getUserHp());
            result.put("userPassword", data.getUserPassword());
            result.put("licenseNo", data.getLicenseNo());
            result.put("file1", data.getProfileImg());
            result.put("file2", data.getLicenseNoImg());
            result.put("file2Name", data.getLicenseNoImgName());
            result.put("file3", data.getDoctorCertImg());
            result.put("file3Name", data.getDoctorCertImgName());
            result.put("subPosition", data.getSubPosition()); // 서브계정관리-직급
            int subSubjectCode = 0;
            if(!Functions.isNull(data.getSubSubjectCode())) {
                subSubjectCode = Integer.parseInt(data.getSubSubjectCode());
            }
            result.put("subSubjectCode", subSubjectCode); // 서브계정관리-진료과
            result.put("lastDate", data.getLastDate()); // 마지막로그인일자
        }

        return result;
    }

    // 병원 정보
    public Map<String, Object> getHospitalInfo(Integer idx) {
        Hospital hospital = doctorMapper.getHospitalInfo(idx); // 병원 정보
        HospitalSubject subject = doctorMapper.getSubjectInfo(idx, "hd_hospital_subject"); // 병원-진료과목 정보
        List<Map<String, Object>> appointment = doctorMapper.getAppointmentInfo(idx); // 병원-진료예약시간 정보
        List<Map<String, Object>> hour = doctorMapper.getHourInfo(idx); // 병원-영업시간 정보
        Map<String, Object> result = new HashMap<>(); // returnData

        if (!Functions.isNull(hospital)) {
            result.put("idx", hospital.getIdx());
            result.put("nonfaceSvcYn", hospital.getNonfaceSvcYn());
            result.put("designateSvcYn", hospital.getDesignateSvcYn());
            result.put("hospitalName", hospital.getHospitalName());
            result.put("zipCode", hospital.getZipCode());
            result.put("mainAddr", hospital.getMainAddr());
            result.put("detailAddr", hospital.getDetailAddr());
            result.put("areaSi", hospital.getAreaSi());
            result.put("areaGu", hospital.getAreaGu());
            result.put("areaDong", hospital.getAreaDong());
            result.put("lat", hospital.getLat());
            result.put("lng", hospital.getLng());
            for(int i=0; i<3; i++) {
                result.put("telNo0"+(i+1), "");
                result.put("faxNo0"+(i+1), "");
            }
            if(!hospital.getTelNo().isEmpty()) {
                String[] telNo = hospital.getTelNo().split("-");
                for(int i=0; i<telNo.length; i++) {
                    result.put("telNo0"+(i+1), telNo[i]);
                }
            }
            if(!Functions.isNull(hospital.getFaxNo())) {
                String[] faxNo = hospital.getFaxNo().split("-");
                for(int i=0; i<faxNo.length; i++) {
                    result.put("faxNo0"+(i+1), faxNo[i]);
                }
            }
            result.put("brNo", hospital.getBrNo());
            result.put("file2", hospital.getBrNoImg());
            result.put("file2Name", hospital.getBrNoImgName());
            result.put("file3", hospital.getSvcCertImg());
            result.put("file3Name", hospital.getSvcCertImgName());
            result.put("file1", hospital.getMainImg());
            result.put("introduce", hospital.getIntroduce());
            result.put("nonReimbJson", hospital.getNonReimbJson());
            result.put("bankCode", hospital.getBankCode());
            result.put("accountName", hospital.getAccountName());
            result.put("accountNum", hospital.getAccountNum());
            result.put("bankImg", hospital.getBankImg());
            result.put("bankImgName", hospital.getBankImgName());
            result.put("eachSetYn", hospital.getEachSetYn());
            result.put("restStartHour", hospital.getRestStartHour());
            result.put("restEndHour", hospital.getRestEndHour());
            String subjectCode = "";
            if(!Functions.isNull(subject)) subjectCode = subject.getSubjectCode();
            result.put("subjectCode", subjectCode); // 병원-진료과목
            result.put("hospitalAppointment", appointment); // 병원-진료예약시간
            result.put("hospitalHour", hour); // 병원-영업시간
            result.put("hourSize", hour.size());
            result.put("isUpdate", hospital.getIsUpdate());
        }
        // log.info(String.valueOf(result));

        return result;
    }
}
