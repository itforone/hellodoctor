package com.hellodoctor.service.hospital;

import com.hellodoctor.mapper.hospital.HTreatMapper;
import com.hellodoctor.model.hospital.Doctor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.hellodoctor.common.Constants.MEDICAL_REQUEST_SUBJECT;
import static com.hellodoctor.common.Functions.*;

/**
 * 의사-진료내역 service 클래스
 */

@Service
@Slf4j
public class HTreatService {
    @Autowired
    HTreatMapper hTreatMapper;

    // 진료내역 목록 수
    public Map<String, String> getTreatListCount(Doctor doctor, HttpServletRequest request) {
        // 검색
        String sqlSearch = getRemoteListSearch(request);

        // 진료 예약 목록
        int hospitalIdx = doctor.getHospitalIdx();
        List<Map<String, String>> list = hTreatMapper.getTreatListCount(hospitalIdx, sqlSearch); // (병원idx, 검색조건)

        int count = 0;
        int totalAmt = 0;
        int nonReimbAmt = 0;
        int patientAmt = 0;
        for (Map<String, String> data : list) {
            count++;
            totalAmt += Integer.parseInt(String.valueOf(data.get("totalBillAmt")));
            nonReimbAmt += Integer.parseInt(String.valueOf(data.get("nonReimbBillAmt")));
            patientAmt += Integer.parseInt(String.valueOf(data.get("patientBillAmt")));
        }

        Map<String, String> returnList = new HashMap<>();
        returnList.put("listTotalCnt", String.valueOf(count));
        returnList.put("totalBillAmt", String.valueOf(totalAmt)); // 총청구금액
        returnList.put("nonReimbBillAmt", String.valueOf(nonReimbAmt)); // 비급여액
        returnList.put("patientBillAmt", String.valueOf(patientAmt)); // 환자부담금

        return returnList;
    }

    // 진료내역 목록
    public Map<String, Object> getTreatList(Doctor doctor, HttpServletRequest request, int startRecord, int endRecord) {
        Map<String, Object> returnList = new HashMap<>();

        // 검색
        String sqlSearch = getRemoteListSearch(request);

        // limit
        if(isNull(request.getParameter("limit"))) endRecord = 15;
        else endRecord = Integer.parseInt(request.getParameter("limit"));

        // 진료내역 목록
        int hospitalIdx = doctor.getHospitalIdx();
        List<Map<String, String>> list = hTreatMapper.getTreatList(hospitalIdx, sqlSearch, startRecord, endRecord); // (병원idx, 검색조건, limit start, limit end)

        List<Object> tempList = new LinkedList<>();
        for (Map<String, String> data : list) {
            // log.info("data: "+data.toString());

            // 접수번호
            if (isNull(data.get("rcptNo"))) data.put("rcptNo", "");

            // 진료신청일
            String regdate = String.valueOf(data.get("regdate")).replace("T", " ");
            data.put("regdate", regdate);

            // 진료완료일 TBC: 비용청구시간으로 입력
            String compDate = String.valueOf(data.get("billDate")).replace("T", " ");
            data.put("compDate", compDate);

            // 환자이름/관계/연락처/주민등록번호
            // 관계가 본인이면 hd_medical(진료) 데이터 본인 외이면 hd_member_family(가족) 데이터
            String patientName = data.get("patientName");
            String patientHp = data.get("patientHp");
            String patientRrNo = data.get("patientRrNo");
            data.put("patientName", patientName); // 환자이름
            data.put("patientHp", patientHp); // 연락처
            data.put("patientRrNo", patientRrNo); // 주민등록번호

            // 상태
            String treatStatus = "정산대기";
            // 청구금액이 없거나 결제 인덱스 있으면 정산완료
            if(Integer.parseInt(String.valueOf(data.get("totalBillAmt"))) == 0 && data.get("isBillAmt").equals("N") || Integer.parseInt(String.valueOf(data.get("billStlmIdx"))) > 0) treatStatus = "정산완료";
            data.put("treatStatus", treatStatus);

            tempList.add(data);
        }
        returnList.put("listData", tempList);

        // log.info("returnList: "+returnList);
        return returnList;
    }

    // 진료내역 검색 조건 (쿼리)
    public String getRemoteListSearch(HttpServletRequest request) {
        String _search = "";

        // 검색일 (신청일 or 완료일)
        String _schDate = request.getParameter("schDate");
        String stDate = request.getParameter("schDateSt"); // 시작
        String edDate = request.getParameter("schDateEd"); // 종료
        String column = "A.regdate";
        if(_schDate.equals("완료일")) column = "A.bill_date";
        if(!isNull(stDate) && !isNull(edDate)) {
            _search += " AND (DATE_FORMAT("+column+", '%Y-%m-%d') >= '"+stDate+"' AND DATE_FORMAT("+column+", '%Y-%m-%d') <= '"+edDate+"')";
        } else if(!isNull(stDate) && isNull(edDate)) {
            _search += " AND (DATE_FORMAT("+column+", '%Y-%m-%d') >= '"+stDate+"')";
        } else if(isNull(stDate) && !isNull(edDate)) {
            _search += " AND (DATE_FORMAT("+column+", '%Y-%m-%d') <= '"+edDate+"')";
        }

        // 검색조건
        String _item = "";
        String _keyword = "";
        if(!isNull(request.getParameter("schKeyword"))) {
            _item = request.getParameter("schItem");
            _keyword = request.getParameter("schKeyword");
            _search += " HAVING "+_item+" LIKE '%"+_keyword+"%'";
        }

        return _search;
    }
}
