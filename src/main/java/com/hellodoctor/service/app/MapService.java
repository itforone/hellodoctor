package com.hellodoctor.service.app;

import com.hellodoctor.common.Constants;
import com.hellodoctor.common.Functions;
import com.hellodoctor.mapper.app.HospitalMapper;
import com.hellodoctor.mapper.app.PharmacyMapper;
import com.hellodoctor.mapper.app.RemoteMapper;
import com.hellodoctor.model.app.HospitalHour;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.ArrayUtils;

import java.text.DecimalFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

import static com.hellodoctor.common.Constants.NO_IMAGE;
import static com.hellodoctor.common.Functions.addZero;

@Service
@Slf4j
public class MapService {
    @Autowired
    HospitalMapper hospitalMapper;
    @Autowired
    PharmacyMapper pharmacyMapper;
    @Autowired
    RemoteMapper remoteMapper;

    // 병원 목록 | @param: 지역(구), 지역(동), 위도, 경도
    public List<Map<String, Object>> getHospitalList(String gu, String dong, String lat, String lng) {
        // 오늘 요일
        LocalDate date = LocalDate.now();
        String today = date.toString();
        DayOfWeek dayOfWeek = date.getDayOfWeek();
        String todayNum = String.valueOf(dayOfWeek.getValue());

        // 현재시간
        LocalTime time = LocalTime.now();
        int currentTime = Integer.parseInt(addZero(2, time.getHour()) + addZero(2, time.getMinute())); // format: 1700
        // log.info("getHospitalList|currentTime= "+currentTime);

        // 1. 거리순 병원 전체 목록 (내위치 또는 중심좌표 반경 1km 이내 병원 목록) | @param: (지역(구), 지역(동), 위도, 경도, 오늘일자, 오늘요일)
        List<Map<String, Object>> searchHospitalList = hospitalMapper.getHospitalList(gu, dong, lat, lng, today, todayNum);
        // log.info("getHospitalList|searchHospitalList= "+ searchHospitalList);
        List<Map<String, Object>> returnHospitalList = new ArrayList<>(); // return List

        for (Map<String, Object> listData : searchHospitalList) {
            if(listData.get("mainImg") == null) listData.put("mainImg", NO_IMAGE); // 대표이미지

            todayNum = String.valueOf(dayOfWeek.getValue()); // 오늘 요일 확인
            // (220809) 평일=0 에서 변경 1~5 입력으로 변경
            /*if (listData.get("eachSetYn").equals("N")) { // 요일별 시간설정 아님 (평일, 토요일, 일요일, 공휴일 로만 설정)
                String[] weekdayCheck = {"1","2","3","4","5"};
                if (ArrayUtils.contains(weekdayCheck, todayNum)) todayNum = "0"; // 오늘이 평일인지 확인
            }*/

            // 각 병원 진료시간
            List<HospitalHour> searchHours = remoteMapper.getHospitalHours(Integer.parseInt(listData.get("idx").toString())); // @param: 병원 idx
            // log.info("searchHours= " + searchHours.size());
            for (int i=0; i<searchHours.size(); i++) {
                HospitalHour obj = searchHours.get(i);
                // log.info("searchHours=" + obj.getYoil());
                if(obj.getYoil().equals(todayNum)) { // 오늘
                    // int _startHour = obj.getStartHour(); // 진료시작시간
                    int _endHour = obj.getEndHour(); // 진료종료시간
                    // String _startHourFmt = _startHour<1200? Functions.addZero(4, _startHour) : String.valueOf(_startHour);
                    String _endHourFmt = _endHour<1200? Functions.addZero(4, _endHour) : String.valueOf(_endHour);
                    String _hourStr = _endHourFmt.substring(0,2) + ":" + _endHourFmt.substring(2,4); // format: 17:00
                    // 진료종료시간
                    listData.put("endHour", _hourStr);

                    // 진료중 or 진료종료
                    String medicalYn = "N";
                    if(!_endHourFmt.isBlank() && currentTime < _endHour) medicalYn = "Y"; // 현재시간이 진료종료시간 보다 작음 (진료종료시간 안지남==>진료중)
                    listData.put("medicalYn", medicalYn);

                    break;
                }
            }
            if(listData.get("endHour") == null) listData.put("endHour", ""); // null 예외처리

            // 2. 예약가능시간 추출
            String availTimeTable = "";
            if (listData.get("todayTimeTable") != null) { // 오늘날짜(YYYY-mm-dd)에 예약시간이 별도로 지정됨
                availTimeTable = listData.get("todayTimeTable").toString();
            } else if (listData.get("weekTimeTable") != null) { // 요일(일~월)에 해당하는 공통시간
                availTimeTable = listData.get("weekTimeTable").toString();
            }

            // 3. 예약가능시간 조회결과 처리
            if (!availTimeTable.isEmpty()) { // 3.1 성공
                //log.info("availTimeTable= "+availTimeTable);
                String[] availTimeArr = availTimeTable.split(",");
                for (String chkTime : availTimeArr) {
                    listData.put("status", false); // 예약불가
                    int compareTime = Integer.parseInt(chkTime);
                    if(compareTime > currentTime) { // 현재시간 이후에 예약가능시간이 있으면
                        listData.put("status", true); // 예약가능
                        break;
                    }
                }
            } else {
                listData.put("status", false); // 예약불가
            }

            // 거리
            int distance = (int) (Double.parseDouble(String.valueOf(listData.get("distance"))) * 1000); // 현위치에서의 거리: km로 받아와서 m로 변환
            if(distance > 1000) { // 1000m 이상
                String km = String.format("%.1f", Double.parseDouble(String.valueOf(listData.get("distance"))));
                listData.put("distance", km+"km");
            } else {
                listData.put("distance", distance+"m");
            }

            returnHospitalList.add(listData);
        }
        return returnHospitalList;
    }

    // 위치 좌표 목록 | @param: 병원 약국 구분
    public JSONObject getLatLng(String gubun) {
        // 오늘 요일
        LocalDate date = LocalDate.now();
        DayOfWeek dayOfWeek = date.getDayOfWeek();
        String todayNum = String.valueOf(dayOfWeek.getValue());

        JSONObject obj = new JSONObject();
        List<Map<String, Object>> dataList = new ArrayList<>();
        if(gubun.equals("hospital")) {
            dataList = hospitalMapper.getLatLng(todayNum);
        } else if(gubun.equals("pharmacy")) {
            dataList = pharmacyMapper.getLatLng(todayNum);
        }
        // log.info("getLatLng|dataList= "+dataList);

        if(dataList.size() != 0) { obj.put("dataList", dataList); }
        else { obj.put("dataList", ""); }

        return obj;
    }

    // 선택 병원 정보 | @param: 병원 idx, 위도, 경도)
    public Map<String, Object> getHospitalInfo(Integer idx, String lat, String lng) {
        // 오늘 요일
        LocalDate date = LocalDate.now();
        String today = date.toString();
        DayOfWeek dayOfWeek = date.getDayOfWeek();
        String todayNum = String.valueOf(dayOfWeek.getValue());

        // 현재시간
        LocalTime time = LocalTime.now();
        int currentTime = Integer.parseInt(addZero(2, time.getHour()) + addZero(2, time.getMinute())); // format: 1700

        // 1. 선택 병원 정보 | @param: 병원 idx, 위도, 경도, 오늘일자, 오늘요일
        Map<String, Object> map = hospitalMapper.getHospitalInfo(idx, lat, lng, today, todayNum);
        if(!map.isEmpty()) {
            // null 예외처리
            if (map.get("todayEndHour") == null) map.put("todayEndHour", ""); // 오늘 진료종료시간
            if (map.get("nonReimbJson") == null) map.put("nonReimbJson", ""); // 비급여
            if (map.get("introduce") == null) map.put("introduce", ""); // 병원소개
            if (map.get("telNo") == null) map.put("telNo", ""); // 전화번호
            if (map.get("mainImg") == null) map.put("mainImg", NO_IMAGE); // 대표이미지

            // 2. 예약가능시간 추출
            String availTimeTable = "";
            if (map.get("todayTimeTable") != null) { // 오늘날짜(YYYY-mm-dd)에 예약시간이 별도로 지정됨
                availTimeTable = map.get("todayTimeTable").toString();
            } else if (map.get("weekTimeTable") != null) { // 요일(일~월)에 해당하는 공통시간
                availTimeTable = map.get("weekTimeTable").toString();
            }

            // 3. 예약가능시간 조회결과 처리
            if (!availTimeTable.isEmpty()) { // 3.1 성공
                //log.info("availTimeTable= "+availTimeTable);
                String[] availTimeArr = availTimeTable.split(",");
                for (String chkTime : availTimeArr) {
                    map.put("status", false); // 예약불가
                    int compareTime = Integer.parseInt(chkTime);
                    if(compareTime > currentTime) { // 현재시간 이후에 예약가능시간이 있으면
                        map.put("status", true); // 예약가능
                        break;
                    }
                }
            } else {
                map.put("status", false); // 예약불가
            }

            // 4. 병원 진료 과목
            String subjectNames = "";
            if(map.get("subjectCode") != null) subjectNames = Functions.getHospitalSubjectNames(String.valueOf(map.get("subjectCode")));
            map.put("subjectNames", subjectNames);

            // 거리
            int distance = (int) (Double.parseDouble(String.valueOf(map.get("distance"))) * 1000); // 현위치에서의 거리: km로 받아와서 m로 변환
            if(distance > 1000) { // 1000m 이상
                String km = String.format("%.1f", Double.parseDouble(String.valueOf(map.get("distance"))));
                map.put("distance", km+"km");
            } else {
                map.put("distance", distance+"m");
            }

            // 진료시간
            List<HospitalHour> searchHours = remoteMapper.getHospitalHours(idx);
            Map<String, String> hours = Functions.getHospitalHours(searchHours, String.valueOf(map.get("eachSetYn")), String.valueOf(map.get("restStartHour")), String.valueOf(map.get("restEndHour")));
            String todayEndHour = Functions.getHospitalEndHour(searchHours, todayNum); // 오늘 진료종료시간
            map.put("todayEndHour", todayEndHour);
            map.put("hospitalHours", hours);

            // 비급여
            Map<String, String> nonReimbMap = Functions.getHospitalNonReimbursement(String.valueOf(map.get("nonReimbJson")));
            map.put("nonReimb", nonReimbMap);

            // 진료중 or 진료종료
            String medicalYn = "N";
            if(!todayEndHour.equals("") && currentTime < Integer.parseInt(todayEndHour.replace(":",""))) medicalYn = "Y"; // 현재시간이 진료종료시간 보다 작음 (진료종료시간 안지남==>진료중)
            map.put("medicalYn", medicalYn);

            // log.info("getHospitalInfo|map= "+map);
        }

        return map;
    }

    // 약국 목록
    public List<Map<String, Object>> getPharmacyList(String gu, String dong, String lat, String lng) {
        // 거리순 약국 전체 목록 (내위치 또는 중심좌표 반경 1km 이내 병원 목록) | @param: (지역(구), 지역(동), 위도, 경도)
        List<Map<String, Object>> searchPharmacyList = pharmacyMapper.getPharmacyList(gu, dong, lat, lng);
        // log.info("getHospitalList|searchPharmacyList= "+ searchPharmacyList);
        List<Map<String, Object>> returnPharmacyList = new ArrayList<>(); // return List

        for (Map<String, Object> listData : searchPharmacyList) {
            if (listData.get("mainImg") == null) listData.put("mainImg", NO_IMAGE); // 대표이미지

            // 거리
            int distance = (int) (Double.parseDouble(String.valueOf(listData.get("distance"))) * 1000); // 현위치에서의 거리: km로 받아와서 m로 변환
            if(distance > 1000) { // 1000m 이상
                String km = String.format("%.1f", Double.parseDouble(String.valueOf(listData.get("distance"))));
                listData.put("distance", km+"km");
            } else {
                listData.put("distance", distance+"m");
            }
            returnPharmacyList.add(listData);
        }

        return returnPharmacyList;
    }

    // 약국 정보
    public Map<String, Object> getPharmacyInfo(Integer idx, String lat, String lng) {
        // 선택 약국 정보 | @param: 약국 idx, 위도, 경도, 오늘일자, 오늘요일
        Map<String, Object> map = pharmacyMapper.getPharmacyInfo(idx, lat, lng);
        if(!map.isEmpty()) {
            if (map.get("mainImg") == null) map.put("mainImg", NO_IMAGE); // 대표이미지

            // 거리
            int distance = (int) (Double.parseDouble(String.valueOf(map.get("distance"))) * 1000); // 현위치에서의 거리: km로 받아와서 m로 변환
            if(distance > 1000) { // 1000m 이상
                String km = String.format("%.1f", Double.parseDouble(String.valueOf(map.get("distance"))));
                map.put("distance", km+"km");
            } else {
                map.put("distance", distance+"m");
            }
        }

        return map;
    }
}
