package com.hellodoctor.service.app;

import com.hellodoctor.common.Functions;
import com.hellodoctor.mapper.app.MyDoctorMapper;
import com.hellodoctor.mapper.app.RemoteMapper;
import com.hellodoctor.model.app.HospitalHour;
import com.hellodoctor.model.app.Medical;
import com.hellodoctor.model.app.Member;
import com.hellodoctor.model.app.Pharmacy;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.rmi.Remote;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

import static com.hellodoctor.common.Constants.NO_IMAGE;
import static com.hellodoctor.common.Functions.addZero;

/**
 * 우리동네주치의 service 클래스
 */
@Service
@Slf4j
public class MyDoctorService {
    @Autowired
    MyDoctorMapper myDoctorMapper;
    @Autowired
    RemoteMapper remoteMapper;

    // 메인 > 우리동네주치의 요청내역 조회
    public List<Map<String, String>> requestMyDoctorList(int mbIdx, String mbHp) {
        return myDoctorMapper.requestMyDoctorList(mbIdx, mbHp);
    }

    // 메인 > 우리동네주치의 수락/거절
    public int requestMyDoctorAction(int idx, String status, int mbIdx) {
        return myDoctorMapper.requestMyDoctorAction(idx, status, mbIdx);
    }

    // 우리동네주치의 전체수
    public int myDoctorTotalCount(int mbIdx) {
        return myDoctorMapper.myDoctorTotalCount(mbIdx);
    }

    // 우리동네주치의 목록
    public List<Map<String, Object>> getMyDoctorList(int mbIdx, String lat, String lng) {
        // 오늘요일 (1~7, 공휴일 H), 오늘날짜 (YYYY-mm-dd) TODO:공휴일처리!
        LocalDate date = LocalDate.now();
        String today = date.toString();
        DayOfWeek dayOfWeek = date.getDayOfWeek();
        String todayNum = String.valueOf(dayOfWeek.getValue());

        List<Map<String, Object>> dataList = myDoctorMapper.getMyDoctorList(mbIdx, lat, lng, today, todayNum);
        // log.info("dataLiat= "+dataList);
        List<Map<String, Object>> returnDataList = new ArrayList<>(); // return List

        for(Map<String, Object> data : dataList) {
            // 예약가능여부 (지정환자 예약시간이 존재하면)
            String availTimeTable = "";
            if (data.get("todayTimeTable") != null) { // 오늘날짜(YYYY-mm-dd)에 예약시간이 별도로 지정됨
                availTimeTable = data.get("todayTimeTable").toString();
            } else if (data.get("weekTimeTable") != null) { // 요일(일~월)에 해당하는 공통시간
                availTimeTable = data.get("weekTimeTable").toString();
            }

            // 진료중 or 진료종료
            String medicalYn = "N";
            if (!availTimeTable.isEmpty()) {
                medicalYn = "Y"; // (예약가능) 진료중
            } else {
                // (예약불가능) 진료불가
            }
            data.put("medicalYn", medicalYn);

            // 거리
            int distance = (int) (Double.parseDouble(String.valueOf(data.get("distance"))) * 1000); // 현위치에서의 거리: km로 받아와서 m로 변환
            if(distance > 1000) { // 1000m 이상
                String km = String.format("%.1f", Double.parseDouble(String.valueOf(data.get("distance"))));
                data.put("distance", km+"km");
            } else {
                data.put("distance", distance+"m");
            }

            // 병원 진료 과목
            String subjectNames = Functions.getHospitalSubjectNames(String.valueOf(data.get("subjectCode")));
            data.put("subjectNames", subjectNames);

            // 주말 진료
            if (data.get("weekendCloseYn") == null) data.put("weekendCloseYn", "");

            // 대표이미지
            if(data.get("mainImg") == null) data.put("mainImg", NO_IMAGE);

            returnDataList.add(data);
        }
        log.info(returnDataList.toString());
        return returnDataList;
    }

    // 우리동네주치의 병원정보
    public Map<String, Object> getMyDoctorOne(int mbIdx, String lat, String lng, int myDoctorIdx) {
        // 오늘요일 (1~7, 공휴일 H), 오늘날짜 (YYYY-mm-dd) TODO:공휴일처리!
        LocalDate date = LocalDate.now();
        String today = date.toString();
        DayOfWeek dayOfWeek = date.getDayOfWeek();
        String todayNum = String.valueOf(dayOfWeek.getValue());
        // 현재 시/분
        LocalTime time = LocalTime.now();
        int currentHour = time.getHour();
        int currentMin = time.getMinute();

        // 병원정보
        Map<String, Object> data = myDoctorMapper.getMyDoctorOne(mbIdx, lat, lng, today, todayNum, myDoctorIdx);
        int hospitalIdx = Integer.parseInt(String.valueOf(data.get("hospitalIdx")));

        /** 진료희망시간 START **/
        // 예약가능시간 추출
        String availTimeTable = "";
        if (data.get("todayTimeTable") != null) { // 오늘날짜(YYYY-mm-dd)에 예약시간이 별도로 지정됨
            availTimeTable = data.get("todayTimeTable").toString();
        } else if (data.get("weekTimeTable") != null) { // 요일(일~월)에 해당하는 공통시간
            availTimeTable = data.get("weekTimeTable").toString();
        }
        String[] availTimeTableArr = availTimeTable.split(",");

        // 진료시간 범위 설정 (30분 단위)
        int startTime = currentHour; //1;
        int endTime = 23;
        int key = 0;
        Map<Integer, String> timeTable = new HashMap<>();

        for (int i=startTime; i<endTime; i++) {
            String _hour = (i<10)? "0"+i : String.valueOf(i);
            String _nextHour = (i+1<10)? "0"+(i+1) : String.valueOf(i+1);

            // 병원에서 설정한 지정환자예약시간(15분단위)를 앱에 노출되는 30분 단위시간과 비교
            String _compareHour = String.valueOf(i);
            String _compareNextHour = String.valueOf(i+1);
            String[] _compareArr = new String[5];
            _compareArr[0] = _compareHour + "00";
            _compareArr[1] = _compareHour + "15";
            _compareArr[2] = _compareHour + "30";
            _compareArr[3] = _compareHour + "45";
            _compareArr[4] = _compareNextHour + "00";

            List<String> _list = Arrays.asList(availTimeTableArr);

            // 9:00~9:30
            if (_list.contains(_compareArr[0]) || _list.contains(_compareArr[1]) || _list.contains(_compareArr[2])) {
                if (currentMin < 30) { // 현재분이 30분 미만이면 추가
                    String[] _time1 = {_hour + ":00", _hour + ":30"};
                    timeTable.put(key, String.join("~", _time1));
                    key++;
                }
            }

            // 9:30~10:00
            if (_list.contains(_compareArr[2]) || _list.contains(_compareArr[3]) || _list.contains(_compareArr[4])) {
                String[] _time2 = {_hour + ":30", _nextHour + ":00"};
                timeTable.put(key, String.join("~", _time2));
                key++;
            }
        }
        data.put("timeTable", timeTable);
        /** 진료희망시간 END **/

        // 거리
        int distance = (int) (Double.parseDouble(String.valueOf(data.get("distance"))) * 1000); // 현위치에서의 거리: km로 받아와서 m로 변환
        if(distance > 1000) { // 1000m 이상
            String km = String.format("%.1f", Double.parseDouble(String.valueOf(data.get("distance"))));
            data.put("distance", km+"km");
        } else {
            data.put("distance", distance+"m");
        }

        // 병원 진료 과목
        String subjectNames = Functions.getHospitalSubjectNames(String.valueOf(data.get("subjectCode")));
        data.put("subjectNames", subjectNames);

        // 진료시간
        List<HospitalHour> searchHours = remoteMapper.getHospitalHours(hospitalIdx);
        Map<String, String> hours = Functions.getHospitalHours(searchHours, String.valueOf(data.get("eachSetYn")), String.valueOf(data.get("restStartHour")), String.valueOf(data.get("restEndHour")));
        data.put("hospitalHours", hours);

        // 비급여
        data.put("nonReimb", "");
        if (data.get("nonReimbJson") != null) {
            Map<String, String> nonReimbMap = Functions.getHospitalNonReimbursement(String.valueOf(data.get("nonReimbJson")));
            data.put("nonReimb", nonReimbMap);
        }

        // 대표이미지
        if(data.get("mainImg") == null) data.put("mainImg", NO_IMAGE);

        // 연계약국 리스트
        List<Pharmacy> pharmacyList = remoteMapper.getPharmacySelectList(hospitalIdx);
        data.put("pharmacyList", pharmacyList);

        log.info(data.toString());
        return data;
    }

    //환자 마지막 진료기록 (주민번호)
    public String getMemberLastRrNo(int mbIdx) {
        return myDoctorMapper.getMemberLastRrNo(mbIdx);
    }

    // 빠른진료신청 결제전 진료 DB 임시등록, 업데이트
    public int saveQuickMedical(HttpServletRequest request, Member member) {
        Enumeration eParam = request.getParameterNames();
        while (eParam.hasMoreElements()) {
            String pName = (String)eParam.nextElement();
            String pValue = request.getParameter(pName);
            log.info("파라미터 : " + pName + "=" + pValue);
        }

        // 진료환자 주민등록번호
        String rrNo = request.getParameter("rr_no[0]") + "-" + request.getParameter("rr_no[1]");
        // 배송메시지
        String receiveMethod = request.getParameter("receive");
        String deliveryMsg = ((receiveMethod.equals("2") || receiveMethod.equals("3")) && request.getParameter("deliveryMsg") != null)? request.getParameter("deliveryMsg") : "";
        // 진료희망시간
        String[] hopeHourArr = request.getParameter("hopeTime").split("~");
        // 병원 인덱스
        int hospitalIdx = Integer.parseInt(request.getParameter("hospitalIdx"));
        // 연계약국 인덱스
        int pharmacyIdx = Integer.parseInt(request.getParameter("pharmacy"));
        // 진료 인덱스 (0=임시등록, 1이상=수정)
        int medicalIdx = Integer.parseInt(request.getParameter("idx"));

        Medical medicalRequest = new Medical();
        medicalRequest.setIdx(medicalIdx);
        medicalRequest.setMbIdx(member.getIdx()); // 회원인덱스
        medicalRequest.setMedicalStatus("T"); // 진료상태 (T:임시저장)
        medicalRequest.setHospitalRcptYn("R"); // 병원접수상태 (R:대기)
        medicalRequest.setPatientIdx(0);; // 진료환자 인덱스 (0:본인, n:나의가족 인덱스)
        medicalRequest.setSubjectCode("0"); // 질환
        medicalRequest.setPatientRrNo(rrNo);
        medicalRequest.setPatientHp(member.getMbHp());
        medicalRequest.setHopeHour(hopeHourArr[0]); // 희망시간
        medicalRequest.setReceiveMethod(request.getParameter("receive")); // 약수령방법
        medicalRequest.setMbAddressIdx(Integer.parseInt(request.getParameter("addrIdx"))); // 배송지인덱스
        medicalRequest.setDeliveryMsg(deliveryMsg);
        medicalRequest.setAppStartHour(hopeHourArr[0]);
        medicalRequest.setAppEndHour(hopeHourArr[1]);
        medicalRequest.setHospitalIdx(hospitalIdx);
        medicalRequest.setMyDrYn("Y"); // 우리동네주치의?
        medicalRequest.setMyDrPharmacyIdx(pharmacyIdx); // 연계약국

        // 배달비 없음 (JLK에서 부담)
        medicalRequest.setDeliveryAmt(0);

        int resultIdx = 0;

        if (medicalIdx == 0) {
            // 임시등록
            int cnt = myDoctorMapper.insertMyDoctorForm(medicalRequest);
            if (cnt == 1) resultIdx = medicalRequest.getIdx();
        } else {
            // 수정
            int cnt = myDoctorMapper.updateMyDoctorForm(medicalRequest);
            log.info(cnt+"");
            if (cnt == 1) resultIdx = medicalRequest.getIdx();
        }

        return resultIdx;
    }

}
