package com.hellodoctor.service.app;

import com.hellodoctor.common.Functions;
import com.hellodoctor.mapper.app.MemberMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.hellodoctor.model.app.Member;
import org.springframework.transaction.annotation.Transactional;

/**
 * 회원 관련 service 클래스
 * @Transactional: 트랜잭션이 보장된 메소드로 설정
 */
@Service
@Slf4j
public class MemberService {
    @Autowired
    MemberMapper memberMapper;
    @Autowired
    PasswordEncoder passwordEncoder;

    // 회원가입
    @Transactional
    public int signUpMember(Member member) {
        //BCryptPasswordEncoder pwEncoder = new BCryptPasswordEncoder();
        member.setMbPassword(passwordEncoder.encode(member.getMbPassword())); // set/get 사용하려면 model/Member 클래스에 getter/setter 어노테이션 추가
        member.setMbNo(Functions.createMemberNo());
        int idx = 0;
        try {
            int chk = memberMapper.signUpMember(member);
            if(chk != 0) { idx = member.getIdx(); }
        } catch (Exception e) {
            log.info("signUpMember() Exception=" + e);
        }
        return idx;
    }

    // 아이디 중복체크
    public int idCheck(String id) {
        //log.info("중복체크: "+id);
        return memberMapper.idCheck(id);
    }

    // 회원정보
    public String getMemberInfo(String column, String id) {
        return memberMapper.getMemberInfo(column, id);
    }

    // 회원인지 확인
    public Member isMember(String id, String name, String birth) {
        return memberMapper.isMember(id, name, birth);
    }

    // 비밀번호 재설정
    public int resetPassword(String id, String pw) {
        pw = passwordEncoder.encode(pw);
        return memberMapper.resetPassword(id, pw);
    }

    // 연락처 재설정
    public int resetHp(String id, String hp) {
        return memberMapper.resetHp(id, hp);
    }
}
