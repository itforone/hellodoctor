package com.hellodoctor.service.app;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hellodoctor.common.AuthMember;
import com.hellodoctor.common.Constants;
import com.hellodoctor.common.Functions;
import com.hellodoctor.common.NCloudStorageUtils;
import com.hellodoctor.mapper.app.*;
import com.hellodoctor.model.app.*;
import com.hellodoctor.service.api.IPayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.thymeleaf.util.ArrayUtils;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

import static com.hellodoctor.common.Constants.NO_IMAGE;
import static com.hellodoctor.common.Functions.addZero;

/**
 * 마이페이지 service 클래스
 * @Transactional: 트랜잭션이 보장된 메소드로 설정
 */
@Service
@Slf4j
public class MyPageService {
    @Autowired
    FileUploadMapper fileUploadMapper;

    @Autowired
    NCloudStorageUtils nCloudStorageUtils;

    @Autowired
    MemberFamilyMapper memberFamilyMapper;

    @Autowired
    MemberMapper memberMapper;

    @Autowired
    CSMapper csMapper;

    @Autowired
    HospitalMapper hospitalMapper;

    @Autowired
    RemoteMapper remoteMapper;

    @Autowired
    MedicineGuideMapper medicineGuideMapper;

    @Autowired
    IPayService iPayService;

    // 파일업로드 폴더명
    private String folderName = "myFamily"; // 나의가족관리
    private String folderName2 = "csApp"; // 서비스문의

    // 가족추가/수정
    public int registerMyFamily(MultipartHttpServletRequest request, int mbIdx) {
        // 주민등록번호
        String rrno = request.getParameter("rr_no[0]").toString() +
                "-" + request.getParameter("rr_no[1]").toString();
        // 진료환자 연락처
        String famHp = request.getParameter("fam_hp[0]").toString() +
                "-" +
                request.getParameter("fam_hp[1]").toString() +
                "-" +
                request.getParameter("fam_hp[2]").toString();

        // (수정시) 인덱스
        String idx = request.getParameter("idx");

        /**
         * 파일업로드 START
         * - 등록/수정을 모두 하는 경우
         * - multiple 구현 X
         * **/
        // 폴더생성
        nCloudStorageUtils.createFolder(folderName);

        // 파일명 ( 수정시 체크 originFile1 = 업로드된 파일명 )
        Map<String, String> uploadFileName = new HashMap<>();
        uploadFileName.put("file1", request.getParameter("originFile1")); // 대리진료자 주민등록증
        uploadFileName.put("file2", request.getParameter("originFile2")); // 진료환자 주민등록증
        uploadFileName.put("file3", request.getParameter("originFile3")); // 가족관계증명서

        // 업로드파일
        Map<String, MultipartFile> files = request.getFileMap();
        // 파일 요소 읽음
        Iterator<Map.Entry<String, MultipartFile>> iterator = files.entrySet().iterator();
        // 첨부파일 체크
        while (iterator.hasNext()) {
            Map.Entry<String, MultipartFile> entry = iterator.next(); // file = entry.getValue()

            String elementName = entry.getValue().getName(); // input name (file1, file2, file3)
            long fileSize = entry.getValue().getSize();
            String fileOriginName = entry.getValue().getOriginalFilename(); // input 원본파일명
            String fileExt = fileOriginName.substring(fileOriginName.lastIndexOf(".")+1); // 확장자

            //log.info(elementName + "/" + fileSize + "/" + file.getValue());

            if (fileSize > 0) { // 첨부파일이 존재하면
                String _fileName = Functions.getTimeStamp() + elementName + "." + fileExt; // 파일명 생성
                String url = nCloudStorageUtils.uploadFile(folderName, _fileName, entry.getValue()); // 업로드 처리
                //log.info("url="+url);

                // 수정시 첨부파일 새로등록하면 서버 파일삭제
                if (!idx.isEmpty() && uploadFileName.containsValue(elementName)) { // originFile3이 존재하면 삭제
                    nCloudStorageUtils.deleteObject(uploadFileName.get(elementName));
                }

                uploadFileName.put(elementName, url);
            }
        }
        /** 파일업로드 END **/

        MemberFamily memberFamily = new MemberFamily();
        memberFamily.setMbIdx(mbIdx);
        memberFamily.setDelYn("N");
        memberFamily.setFamRel(request.getParameter("fam_rel"));
        memberFamily.setIsDelegate(request.getParameter("is_delegate"));
        memberFamily.setFamName(request.getParameter("fam_name"));
        memberFamily.setRrNo(rrno);
        memberFamily.setFamHp(famHp);
        memberFamily.setMbRrnoImg(uploadFileName.get("file1"));
        memberFamily.setFamRrnoImg(uploadFileName.get("file2"));
        memberFamily.setFamRelImg(uploadFileName.get("file3"));

        int result = 0;

        if (idx.isEmpty()) {
            // 가족추가
            result = memberFamilyMapper.insertFamily(memberFamily);
        } else {
            // 가족수정
            memberFamily.setIdx(Integer.parseInt(idx));
            //log.info(memberFamily.toString());
            result = memberFamilyMapper.updateFamily(memberFamily);
        }

        return result;
    }

    // 가족목록
    public List<Object> getFamilyList(int mbIdx) {
        List<MemberFamily> dataList = memberFamilyMapper.getFamilyList(mbIdx);
        List<Object> familyList = new ArrayList<>();

        for (MemberFamily family : dataList) {
            Map<String, String> map = new HashMap<>();

            // 성별구분
            String rrno = family.getRrNo();
            String lastRrno = rrno.substring(rrno.lastIndexOf("-") + 1);
            String famSex = (lastRrno.substring(0, 1).equals("1"))? "M" : "F";

            // 등록일
            String regdate = Functions.ChangeDateFormat(family.getRegdate(), "YYYY.MM.dd");

            map.put("idx", String.valueOf(family.getIdx())); // 인덱스
            map.put("name", family.getFamName()); // 이름
            map.put("hp", family.getFamHp());    // 휴대폰번호
            map.put("isDelegate", family.getIsDelegate());    // 대리진료여부
            map.put("famSex", famSex);    // 성별(M,F)
            map.put("regdate", regdate);
            map.put("famRel", Constants.MEMBER_FAMILY_REL.get(Integer.parseInt(family.getFamRel())));
            map.put("rrNo", String.valueOf(family.getRrNo())); // 주민번호

            familyList.add(map);
        }

        return familyList; //familyList;
    }

    // 가족삭제
    public int deleteMyFamily(int idx) {
        int result = memberFamilyMapper.deleteMyFamily(idx);
        return result;
    }

    // 가족수정
    public Map<String, Object> getFamilyDetail(int idx) {
        MemberFamily data = memberFamilyMapper.getFamilyDetail(idx);
        Map<String, Object> result = new HashMap<>();

        if (data != null) {
            // 주민등록번호, 연락처
            String[] rrno = data.getRrNo().split("-");
            String[] hp = data.getFamHp().split("-");

            result.put("idx", data.getIdx());   // 인덱스
            result.put("mbIdx", data.getMbIdx());   // 회원인덱스
            result.put("famRel", Integer.parseInt(data.getFamRel())); // 가족관계
            result.put("isDelegate", data.getIsDelegate()); // 대리진료여부
            result.put("name", data.getFamName()); // 이름
            result.put("rrno1", rrno[0]); // 주민번호 앞자리
            result.put("rrno2", rrno[1]); // 주민번호 뒷자리
            result.put("hp1", hp[0]); // 연락처
            result.put("hp2", hp[1]);
            result.put("hp3", hp[2]);

            result.put("file1", data.getMbRrnoImg());
            result.put("file2", data.getFamRrnoImg());
            result.put("file3", data.getFamRelImg());
        }
        return result;
    }

    // 앱설정 변경처리
    public int updatePushSetting(String mbIdx, String pushInfo, String pushMk) {
        return memberMapper.updatePushSetting(mbIdx, pushInfo, pushMk);
    }

    // 주치 전담병원 목록
    public List<Map<String, Object>> getMyHospitalList(int mbIdx, String lat, String lng) {
        // 오늘 요일
        LocalDate date = LocalDate.now();
        String today = date.toString();
        DayOfWeek dayOfWeek = date.getDayOfWeek();
        String todayNum = String.valueOf(dayOfWeek.getValue());
        // 현재시간
        LocalTime time = LocalTime.now();
        int currentTime = Integer.parseInt(addZero(2, time.getHour()) + addZero(2, time.getMinute())); // format: 1700

        List<Map<String, Object>> dataList = hospitalMapper.getMyHospitalList(mbIdx, lat, lng, today, todayNum);
        // log.info("dataLiat= "+dataList);
        List<Map<String, Object>> returnDataList = new ArrayList<>(); // return List

        for(Map<String, Object> data : dataList) {
            todayNum = String.valueOf(dayOfWeek.getValue()); // 오늘 요일
            // (220809) 평일=0 에서 변경 1~5 입력으로 변경
            // 각 병원의 요일별 오늘 진료시간 확인
            /*if (data.get("eachSetYn").equals("N")) { // 요일별 시간설정 아님 (평일, 토요일, 일요일, 공휴일 로만 설정)
                String[] weekdayCheck = {"1","2","3","4","5"};
                if (ArrayUtils.contains(weekdayCheck, todayNum)) todayNum = "0"; // 오늘이 평일
            }*/

            // 각 병원 진료시간
            List<HospitalHour> searchHours = remoteMapper.getHospitalHours(Integer.parseInt(String.valueOf(data.get("idx")))); // @param: 병원 idx
            // log.info("searchHours= " + searchHours.size());
            // 진료중 or 진료종료
            String medicalYn = "N";
            for (int i=0; i<searchHours.size(); i++) {
                HospitalHour obj = searchHours.get(i);
                // log.info("searchHours=" + obj.getYoil());
                if(obj.getYoil().equals(todayNum)) { // 오늘
                    // int _startHour = obj.getStartHour(); // 진료시작시간
                    int _endHour = obj.getEndHour(); // 진료종료시간
                    // String _startHourFmt = _startHour<1200? Functions.addZero(4, _startHour) : String.valueOf(_startHour);
                    String _endHourFmt = _endHour<1200? Functions.addZero(4, _endHour) : String.valueOf(_endHour);

                    // 진료중 or 진료종료
                    if(!_endHourFmt.isBlank() && currentTime < _endHour) medicalYn = "Y"; // 현재시간이 진료종료시간 보다 작음 (진료종료시간 안지남==>진료중)

                    break;
                }
            }
            data.put("medicalYn", medicalYn);

            // 거리
            int distance = (int) (Double.parseDouble(String.valueOf(data.get("distance"))) * 1000); // 현위치에서의 거리: km로 받아와서 m로 변환
            if(distance > 1000) { // 1000m 이상
                String km = String.format("%.1f", Double.parseDouble(String.valueOf(data.get("distance"))));
                data.put("distance", km+"km");
            } else {
                data.put("distance", distance+"m");
            }

            // 병원 진료 과목
            String subjectNames = Functions.getHospitalSubjectNames(String.valueOf(data.get("subjectCode")));
            data.put("subjectNames", subjectNames);

            // 주말 진료
            if (data.get("weekendCloseYn") == null) data.put("weekendCloseYn", "");

            // 대표이미지
            if(data.get("mainImg") == null) data.put("mainImg", NO_IMAGE);

            returnDataList.add(data);
        }
        log.info(returnDataList.toString());

        return returnDataList;
    }

    // 마이페이지-개인정보-주치 전담병원 정보
    public Hospital getMyHospital(int hospitalIdx) {
        return hospitalMapper.getMyHospital(hospitalIdx);
    }

    // 주치 전담병원 삭제
    public int deleteMyHospital(@AuthMember Member member, String delHospitalIdx, String profile) {
        int chk = hospitalMapper.deleteMyHospital(member.getIdx(), delHospitalIdx);
        if(chk == 1) {
            String columnValue = "";
            if(profile.equals("Y")) { // 프로필에서 주치 전담병원 삭제 시
                columnValue = "my_hospital_idx = 0";
            } else {
                // 삭제한 주치 전담병원에 내가 지정한 병원이 있으면
                String[] _arr = delHospitalIdx.split(",");
                for(int i = 0; i<_arr.length; i++){
                    if(Integer.parseInt(_arr[i]) == member.getMyHospitalIdx()) {
                        columnValue = "my_hospital_idx = 0";
                        break;
                    }
                }
            }

            int chk2 = memberMapper.updateMemberInfo(member.getIdx(), columnValue); // 회원정보변경: 주치 전담병원idx 변경
            if(chk2 == 1) {
                member.setMyHospitalIdx(0);
            }
        }
        return chk;
    }

    // 주치 전담병원 지정
    public int updateMyHospital(@AuthMember Member member, String selHospitalIdx) {
        int chk = 0;
        if(selHospitalIdx != null) {
            String columnValue = "my_hospital_idx = " + selHospitalIdx;
            chk = memberMapper.updateMemberInfo(member.getIdx(), columnValue); // 회원정보변경: 주치 전담병원idx 변경
            if(chk == 1) {
                member.setMyHospitalIdx(Integer.parseInt(selHospitalIdx));
            }
        }
        return chk;
    }

    // 마이페이지-서비스문의-문의등록
    public int registerContactForm(MultipartHttpServletRequest request, int mbIdx) {
        /*Enumeration eParam = request.getParameterNames();
        while (eParam.hasMoreElements()) {
            String pName = (String)eParam.nextElement();
            String pValue = request.getParameter(pName);
            log.info(pName + "=" + pValue);
        }*/

        // 이미지코드
        String imgCode = Functions.addZero(5, mbIdx);
        imgCode += Functions.randomString(5);

        CSApp csApp = new CSApp();
        csApp.setMbIdx(mbIdx);
        csApp.setServiceType(request.getParameter("service_type"));
        csApp.setCategory(request.getParameter("category"));
        csApp.setContent(request.getParameter("content"));
        csApp.setFileCode(imgCode);

        /**
         * 파일업로드 START
         * - 등록만 하는 경우
         * - multiple 구현 O
         * **/
        // 폴더생성
        nCloudStorageUtils.createFolder(folderName2);
        int order = 1;
        List<FileUpload> uploadFiles = new ArrayList<>();

        // 업로드파일
        MultiValueMap<String, MultipartFile> multiFiles = request.getMultiFileMap();
        // 파일 요소 읽음
        Iterator<Map.Entry<String, List<MultipartFile>>> iterator = multiFiles.entrySet().iterator();
        List<MultipartFile> multipartFileList = new ArrayList<>();
        // 다중업로드 (multiple) MultipartFile 로 분리
        while (iterator.hasNext()) {
            Map.Entry<String, List<MultipartFile>> entry = iterator.next();
            List<MultipartFile> fileList = entry.getValue();
            for (MultipartFile file : fileList) {
                if (file.isEmpty()) continue;
                multipartFileList.add(file);
            }
        }
        // 첨부파일 체크
        if (multipartFileList.size() > 0) {
            for (MultipartFile file : multipartFileList) {
                //String elementName = file.getName(); // input name (imageFile[0], imageFile[1], ...)
                long fileSize = file.getSize();
                String fileOriginName = file.getOriginalFilename(); // input 원본파일명
                String fileExt = fileOriginName.substring(fileOriginName.lastIndexOf(".")+1); // 확장자

                if (fileSize > 0) { // 첨부파일이 존재하면
                    String _fileName = Functions.getTimeStamp() + mbIdx + order + "." + fileExt; // 파일명 생성
                    String url = nCloudStorageUtils.uploadFile(folderName, _fileName, file); // 업로드 처리

                    FileUpload fileUpload = new FileUpload();
                    fileUpload.setMatchCode(imgCode);
                    fileUpload.setTblName("csApp");
                    fileUpload.setFileUrl(url);
                    fileUpload.setSort(order);

                    uploadFiles.add(fileUpload);
                    order++;
                }
            }
        }
        /** 파일업로드 END **/

        int insertIdx = 0;

        // 문의등록
        try {
            int cnt = csMapper.insertContactForm(csApp);
            if (cnt == 1) {
                insertIdx = csApp.getIdx();
                // 파일업로드
                if (uploadFiles.size() > 0) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("list", uploadFiles);
                    fileUploadMapper.insertUploadFiles(map);
                }
            }

        } catch (Exception e) {
            log.info("registerContactForm() Exception=" + e);
        }


        return insertIdx;
    }

    // 서비스문의 (배송문의, 일반문의) 목록
    public Map<Integer, Object> getContactList(int mbIdx, int tab, int month) {
        // 오늘 ~ n개월
        Map<String, String> searchDate = Functions.getSearchDateFilter(month);

        Map<Integer, Object> result = new HashMap<>();
        List contactList = csMapper.getContactList(tab, mbIdx, searchDate.get("todayDate"), searchDate.get("beforeDate"));
        List emptySafeList = new ArrayList<>();

        for (Object data : contactList) {
            Map<String, String> item = (Map<String, String>) data;
            //log.info("item=" + item);

            // 이미지 변수 null이면 공백처리
            if (item.get("imageList") == null) item.put("imageList", "");
            // 카테고리
            if (item.get("category") == null) item.put("cateName", "문의");
            else {
                if (tab == 1) item.put("cateName", Constants.CS_CATEGORY.get(Integer.parseInt(item.get("category"))));
                else item.put("cateName", "배송문의");
            }

            emptySafeList.add(item);
        }
        result.put(tab, emptySafeList);
        // log.info("result=" + result);

        return result;
    }

    // 복약지도 목록
    public List<Map<String, String>> getMedicineGuideList(int mbIdx, String mbName, int month) {
        // 오늘 ~ n개월
        Map<String, String> searchDate = Functions.getSearchDateFilter(month);

        Map<Integer, Object> result = new HashMap<>(); // 날짜별 목록 출력됨
        List guideList = medicineGuideMapper.getGuideList(mbIdx, searchDate.get("todayDate"), searchDate.get("beforeDate"));
        List emptySafeList = new ArrayList<>();

        String compareDate = "";
        for (Object data : guideList) {
            Map<String, String> item = (Map<String, String>) data;
            // log.info("item=" + item);

            // 이미지 변수 null이면 공백처리
            if (item.get("itemImage") == null) item.put("itemImage", "");
            // 약국명 TODO:약국명 추가
            if (item.get("pharmacyName") == null) item.put("pharmacyName", "드림약국");
            // 성별
            String gender = Functions.getGenderByRrno(String.valueOf(item.get("rrNo")));
            item.put("gender", gender);
            // 질환
            item.put("subject", Constants.MEDICAL_REQUEST_SUBJECT.get(Integer.parseInt(String.valueOf(item.get("subjectCode")))));
            // 본인진료?
            String isDelegate = "N";
            String targetName = mbName + "(본인)";
            if (mbIdx != Integer.parseInt(String.valueOf(item.get("patientIdx")))) {
                isDelegate = "Y";
                //targetName = item.get("famName").toString();
                targetName = (item.get("famRel") != null)? Constants.MEMBER_FAMILY_REL.get(Integer.parseInt(String.valueOf(item.get("famRel")))) : "";
                targetName += "(대리진료)";
            }
            item.put("isDelegate", isDelegate);
            item.put("targetName", targetName);
            // 등록일 비교 - 같은날은 묶어서 표현
            String listDate = String.valueOf(item.get("regdate")).substring(0, 10);
            if (!listDate.equals(compareDate)) {
                compareDate = listDate;
                item.put("dt", listDate.replaceAll("-", "."));
            } else {
                item.put("dt", "");
            }

            emptySafeList.add(item);
        }
        log.info("emptySafeList=" + emptySafeList);

        return emptySafeList;
    }

    // 복약지도 상세보기 (1개의 진료에 처방된 복약지도 정보들 가져옴)
    public List<MedicineGuideDetail> getMedicineGuideDetail(int medicalIdx) {
        // List<MedicineGuideDetail> result = medicineGuideMapper.getGuideDetail(medicalIdx);
        // for (MedicineGuideDetail data : result) {
        //     log.info("data=" + data.getMIdx());
        // }
        return medicineGuideMapper.getGuideDetail(medicalIdx);
    }

    // 결제관리 회원 카드목록
    public List<Map<String, Object>> getMemberCreditCardList(int mbIdx) {
        List<MemberCreditCard> cardList = memberMapper.getMemberCreditCardList(mbIdx);
        List<Map<String, Object>> result = new ArrayList<>();

        // convert class to map
        ObjectMapper objectMapper = new ObjectMapper();
        for (MemberCreditCard data : cardList) {
            Map convertMap = objectMapper.convertValue(data, Map.class);
            // 카드사명 추가
            int cardCode = Integer.parseInt(data.getCardCode());
            convertMap.put("cardName", Constants.CARD_CODE.get(cardCode)); // 카드사명 추가
            // 카드번호 추가
            // String[] splitNum = data.getCardNum().split("-");

            result.add(convertMap);
        }

        return result;
    }

    // 결제관리 회원 카드삭제
    public int deleteMemberCreditCard(int mbIdx, int idx) { //, String mbId
        Map<String, Object> map = new HashMap<>();
        map.put("mbIdx", mbIdx);
        map.put("idx", idx);

        // 1. 삭제 전 결제예정 진료수 확인
        int scheduleCount = memberMapper.beforePaymentMedicalCount(mbIdx, idx);
        if (scheduleCount > 0) {
            return -1;
        }

        // int chk = memberMapper.deleteMemberCreditCard(mbIdx, idx);
        int updateChk = memberMapper.deleteMemberCreditCard(map); // update후 selectKey 받기 위해
        String billKey = map.get("billKey").toString();

        if (updateChk > 0 && billKey != null) {
            iPayService.deleteCardBill(billKey, mbIdx); // 빌키삭제
        }

        return updateChk;
    }

    // 결제관리 결제내역
    public List<Map<String, Object>> ajaxPaymentHistory(int page, int month, int mbIdx, int listRow, String mbName) {
        // 오늘 ~ n개월
        Map<String, String> searchDate = Functions.getSearchDateFilter(month);

        int startRecord = (page-1) * listRow;
        int endRecord = listRow; // 한페이지 listRow만큼 가져옴

        List<Map<String, Object>> paymentHistory = memberMapper.getPaymentHistory(mbIdx, searchDate.get("todayDate"), searchDate.get("beforeDate"), startRecord, endRecord);
        List<Map<String, Object>> result = new ArrayList<>();

        for (Map<String, Object> data : paymentHistory) {
            // 결제승인정보
            String payMethod = Functions.paymentAuthType(String.valueOf(data.get("acquCardName")), String.valueOf(data.get("cardQuota")));
            String authDate = Functions.paymentAuthDate(String.valueOf(data.get("authDate")));
            data.put("payMethod", payMethod);
            data.put("authDate", authDate);

            // 환자명(관계)
            int patientIdx = Integer.parseInt(String.valueOf(data.get("patientIdx")));
            String patientName = "";
            if (patientIdx == 0) {
                patientName = mbName + "(본인)";
            } else {
                if (data.get("famName") != null) patientName = String.valueOf(data.get("famName"));
                if (data.get("famRel") != null) {
                    int famRel = Integer.parseInt(String.valueOf(data.get("famRel")));
                    patientName += "(" + Constants.MEMBER_FAMILY_REL.get(famRel) + ")";
                }
            }
            data.put("patientName", patientName);

            // 병원명/약국명
            String placeName = "";
            if (String.valueOf(data.get("payType")).equals("H")) { // 병원명
                if (data.get("hospitalName")!=null) placeName = String.valueOf(data.get("hospitalName"));
            } else { // 약국명
                if (data.get("pharmacyName")!=null) placeName = String.valueOf(data.get("pharmacyName"));
            }
            data.put("placeName", placeName);

            result.add(data);
        }

        log.info("result=" + result);
        return result;
    }



}
