package com.hellodoctor.service.app;

import com.hellodoctor.mapper.app.LoginMapper;
import com.hellodoctor.model.app.Member;
import com.hellodoctor.model.app.MemberAccount;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class LoginService implements UserDetailsService {
    @Autowired
    private LoginMapper mapper;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        /**
         * 스프링이 로그인 요청을 가로챌때 username, password변수 2개를 가로채는데
         * password 부분 처리는 알아서 처리,
         * username이 DB에 있는지 확인해줘야함
         */
        Member member = mapper.getMemberAccount(email);

        if (member == null) {
            log.info("에러!");
            throw new UsernameNotFoundException("회원정보 조회 실패");
        }

        log.info("loadUserByUsername=" + email + "/mbId=" + member.getMbId() + "/mbPassword=" + member.getMbPassword());

        // 세션에 등록되는 Type (UserDetail)
        // UserDetail을 구현하고 있는 User객체 반환; 생성자로 아이디, 비번, Role
        /*return User.builder()
                .username(member.getMbId())
                .password(member.getMbPassword())
                .roles("MEMBER")
                .build();*/
        /*Member authMember = Member.builder()
                .mbId(member.getMbId())
                .mbPassword(member.getPassword())
                .role("MEMBER")
                .build();

        return authMember;*/

        return new MemberAccount(member);
    }
}
