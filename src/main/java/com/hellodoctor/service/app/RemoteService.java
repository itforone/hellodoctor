package com.hellodoctor.service.app;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hellodoctor.common.*;
import com.hellodoctor.mapper.app.*;
import com.hellodoctor.model.app.*;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.text.DecimalFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import org.thymeleaf.util.ArrayUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.function.Function;

import static com.hellodoctor.common.Constants.NO_IMAGE;

/**
 * 원격진료 service 클래스
 */
@Service
@Slf4j
public class RemoteService {
    @Autowired
    MyPageService myPageService;

    @Autowired
    RemoteMapper remoteMapper;

    @Autowired
    FileUploadMapper fileUploadMapper;

    @Autowired
    NCloudStorageUtils nCloudStorageUtils;

    @Autowired
    MemberAddressMapper memberAddressMapper;

    @Autowired
    MemberFamilyMapper memberFamilyMapper;

    @Autowired
    MemberMapper memberMapper;

    // 병원선택 - 주치전담병원, 진료가능병원 목록 저장
    public static List<Map<String, Object>> MY_HOSPITAL_REPOSITORY;
    public static List<Map<String, Object>> MEDICAL_HOSPITAL_REPOSITORY;
    // 병원선택 - 주치전담병원, 진료가능병원 갯수

    // 파일업로드 폴더명
    private String folderName = "symptom";

    // 회원 배송지 추가/수정
    public int addressWriteAction(@AuthMember Member member, MemberAddress memberAddress, HttpServletRequest httpServletRequest) {
        int idx = 0;
        int chk = 0;
        try {
            if(httpServletRequest.getParameter("mode").equals("insert")) { // 배송지 추가
                chk = memberAddressMapper.insertMemberAddress(memberAddress);
                if (chk == 1) {
                    idx = memberAddress.getIdx(); // 추가한 배송지 idx
                }
            } else { // 배송지 수정
                if(httpServletRequest.getParameter("idx") != null) {
                    idx = Integer.parseInt(httpServletRequest.getParameter("idx"));
                    if(idx != 0) {
                        memberAddress.setIdx(idx);
                        chk = memberAddressMapper.updateMemberAddress(memberAddress); // 배송지 정보 수정
                    }
                }
            }
            if (chk == 1) { // 추가/수정 완료 시
                String columnValue = "address_idx="+idx;
                int chk2 = memberMapper.updateMemberInfo(Integer.parseInt(httpServletRequest.getParameter("mbIdx")), columnValue); // 회원 기본 배송지 수정
                if(chk2 > 0) member.setAddressIdx(idx);
            }
        } catch (Exception e) {
            log.info("addressWriteAction() Exception=" + e);
        }
        return idx;
    }

    // 회원 배송지 목록
    public List<MemberAddress> getAddressList(int mbIdx) {
        List<MemberAddress> addrList = memberAddressMapper.getAddressList(mbIdx);
        for(int i=0; i<addrList.size(); i++) {
            MemberAddress obj = addrList.get(i);
            obj.setMainAddr("<div></div>"+obj.getMainAddr()); // 라디오버튼 안보여서 <div></div>추가
        }
        return addrList;
    }

    // 회원 배송지 삭제
    public int deleteMemberAddress(@AuthMember Member member, int idx) {
        int result = memberAddressMapper.deleteMemberAddress(idx);
        if(result > 0) {
            if(member.getAddressIdx() == idx) {
                // 삭제한 배송지가 나의 기본배송지일 때 hd_member - address_idx 초기화!
                String columnValue = "address_idx = 0";
                memberMapper.updateMemberInfo(member.getIdx(), columnValue);
                member.setAddressIdx(0);
            }
        }

        return result;
    }

    // 회원 배송지 선택 정보
    public MemberAddress getAddressOne(int addrIdx) {
        return memberAddressMapper.getAddressOne(addrIdx);
    }

    // 기본배송지 설정
    public int setDefaultAddress(@AuthMember Member member, Integer idx) {
        int result = 0;
        if(idx != null) {
            String columnValue = "address_idx = "+idx;
            result = memberMapper.updateMemberInfo(member.getIdx(), columnValue);
            if(result > 0) member.setAddressIdx(idx);
        }
        return result;
    }

    // 메인/진료신청 - 진료환자(가족)목록
    public List<Object> getMedicalFamilyList(Member member) {
        List<Object> dataList = myPageService.getFamilyList(member.getIdx());

        // 내정보
        Map<String, String> myInfo = new HashMap<>();
        myInfo.put("idx", String.valueOf(member.getIdx()));
        myInfo.put("name", member.getMbName());
        myInfo.put("sex", member.getMbSex());
        myInfo.put("isMember", "Y"); // 본인
        myInfo.put("isDelegate", "Y"); // 대리진료여부 (본인이라 Y)
        // 내정보 - 마지막 병원정보
        Map<String, String> lastMedical = lastMedicalInfo(member.getIdx(), member.getIdx());
        myInfo.put("lastMedicalCnt", lastMedical.get("cnt"));
        myInfo.put("lastHospital", lastMedical.get("hospitalName"));
        myInfo.put("lastMedicalDate", lastMedical.get("date"));

        // 내정보 + 가족정보
        List<Object> familyList = new ArrayList<Object>();
        familyList.add(myInfo);

        for (int i = 0; i < dataList.size(); i++) {
            Map<String, Object> data = (Map) dataList.get(i); // 파싱
            Map<String, String> info = new HashMap<>();

            info.put("idx", data.get("idx").toString());
            info.put("name", data.get("name").toString());
            info.put("sex", data.get("famSex").toString());
            info.put("isMember", "N"); // 본인아님
            info.put("isDelegate", data.get("isDelegate").toString()); // 대리진료여부 Y,N

            // 가족 - 마지막 병원정보
            Map<String, String> _lastMedical = lastMedicalInfo(member.getIdx(), Integer.parseInt(data.get("idx").toString()));
            info.put("lastMedicalCnt", _lastMedical.get("cnt"));
            info.put("lastHospital", _lastMedical.get("hospitalName"));
            info.put("lastMedicalDate", _lastMedical.get("date"));

            familyList.add(info);
        }

        return familyList;
    }

    // 진료환자 선택 (1명)
    public Map<String, Object> getTargetDetail(Integer familyIdx, Member member) {
        Map<String, Object> result = new HashMap<>();

        if (familyIdx == null) {
            // log.info(member.toString());
            String[] hp = member.getMbHp().split("-");

            result.put("isMember", "Y"); // 본인
            result.put("idx", member.getIdx());
            result.put("name", member.getMbName());
            result.put("sex", member.getMbSex());
            result.put("rrno1", member.getMbBirth().substring(2));   // 주민번호 앞자리
            result.put("rrno2", "");
            result.put("hp1", hp[0]); // 연락처
            result.put("hp2", hp[1]);
            result.put("hp3", hp[2]);

        } else {
            Map<String, Object> data = myPageService.getFamilyDetail(familyIdx);

            if (data.size() > 0) {
                //log.info(data.toString());
                String sex = (data.get("rrno2").toString().substring(0, 1).equals("1")) ? "M" : "F";

                result.put("isMember", "N"); // 본인아님
                result.put("idx", data.get("idx"));
                result.put("name", data.get("name"));
                result.put("sex", sex);
                result.put("rrno1", data.get("rrno1")); // 주민번호 앞자리
                result.put("rrno2", data.get("rrno2")); // 주민번호 뒷자리
                result.put("hp1", data.get("hp1")); // 연락처
                result.put("hp2", data.get("hp2"));
                result.put("hp3", data.get("hp3"));
            }
        }

        // 마지막 병원정보
        Map<String, String> lastMedical = lastMedicalInfo(member.getIdx(), Integer.parseInt(String.valueOf(result.get("idx"))));
        result.put("lastMedicalCnt", lastMedical.get("cnt"));
        result.put("lastHospital", lastMedical.get("hospitalName"));
        result.put("lastMedicalDate", lastMedical.get("date"));

        // 배송지 정보
        String address = "";
        if(member.getAddressIdx() != 0) { // 기본 배송지가 있으면
            MemberAddress memberAddress = memberAddressMapper.getAddressOne(member.getAddressIdx());
            address = memberAddress.getMainAddr() + " " + memberAddress.getDetailAddr();
        }
        result.put("address", address);
        result.put("addrIdx", member.getAddressIdx());

        // 회원카드목록
        List<Map<String, Object>> cardList = myPageService.getMemberCreditCardList(member.getIdx());
        result.put("cardCnt", cardList.size());

        return result;
    }

    // 환자 마지막 병원기록
    Map<String, String> lastMedicalInfo(int mbIdx, int familyIdx) {
        int patientIdx = (mbIdx==familyIdx)? 0 : familyIdx; // 진료환자 본인이면 0
        Map<String, String> data = memberFamilyMapper.getFamilyLastMedical(mbIdx, patientIdx);
        Map<String, String> result = new HashMap<>();

        if (data != null) {
            String _d = String.valueOf(data.get("lastDate")).substring(0, 10);
            result.put("cnt", String.valueOf(data.get("cnt")));
            result.put("hospitalName", String.valueOf(data.get("hospitalName")));
            result.put("date", _d.replace("-", "."));
        } else {
            result.put("cnt", "0");
            result.put("hospitalName", "");
            result.put("date", "");
        }

        return result;
    }

    // 원격진료 신청등록 - 임시저장
    public int registerRemoteForm(MultipartHttpServletRequest request, int mbIdx) {
        // 진료환자 본인? 나의가족?
        int patientIdx = request.getParameter("isMember").equals("Y")? 0 : Integer.parseInt(request.getParameter("idx"));
        // 진료환자 주민등록번호
        String rrno = request.getParameter("rr_no[0]") + "-" + request.getParameter("rr_no[1]");
        // 연락처
        String hp = request.getParameter("hp[0]") + "-" + request.getParameter("hp[1]") + "-" + request.getParameter("hp[2]");
        // 증상이미지코드
        String imgCode = Functions.addZero(5, mbIdx);
        imgCode += Functions.randomString(5);
        // 배송메시지
        String receiveMethod = request.getParameter("receive");
        String deliveryMsg = ((receiveMethod.equals("2") || receiveMethod.equals("3")) && request.getParameter("deliveryMsg") != null)? request.getParameter("deliveryMsg") : "";


        Medical medicalRequest = new Medical();
        medicalRequest.setMbIdx(mbIdx); // 회원인덱스
        medicalRequest.setMedicalStatus("T"); // 진료상태 (T:임시저장)
        medicalRequest.setHospitalRcptYn("R"); // 병원접수상태 (R:대기)
        medicalRequest.setPatientIdx(patientIdx);; // 진료환자 인덱스 (0:본인, n:나의가족 인덱스)
        medicalRequest.setSubjectCode(request.getParameter("subjectCode")); // 질환
        medicalRequest.setPatientRrNo(rrno);
        medicalRequest.setPatientHp(hp);
        medicalRequest.setHopeHour(request.getParameter("hopeHour")); // 희망시간
        medicalRequest.setSymptom(request.getParameter("symptom")); // 증상
        medicalRequest.setSymptomImgCode(imgCode); // 증상이미지코드
        medicalRequest.setPatientHeight(Integer.parseInt(request.getParameter("height")));
        medicalRequest.setPatientWeight(Integer.parseInt(request.getParameter("weight")));
        medicalRequest.setReceiveMethod(request.getParameter("receive")); // 약수령방법
        medicalRequest.setMbAddressIdx(Integer.parseInt(request.getParameter("addrIdx"))); // 배송지인덱스
        medicalRequest.setDeliveryMsg(deliveryMsg);

        // 배달비 없음 (JLK에서 부담)
        medicalRequest.setDeliveryAmt(0);

        /**
         * 파일업로드 START
         * - 등록만 하는 경우
         * - multiple 구현 O
         * **/
        // 폴더생성
        nCloudStorageUtils.createFolder(folderName);
        int order = 1;
        List<FileUpload> uploadFiles = new ArrayList<>();

        // 업로드파일
        MultiValueMap<String, MultipartFile> multiFiles = request.getMultiFileMap();
        // 파일 요소 읽음
        Iterator<Map.Entry<String, List<MultipartFile>>> iterator = multiFiles.entrySet().iterator();
        List<MultipartFile> multipartFileList = new ArrayList<>();
        // 다중업로드 (multiple) MultipartFile 로 분리
        while (iterator.hasNext()) {
            Map.Entry<String, List<MultipartFile>> entry = iterator.next();
            List<MultipartFile> fileList = entry.getValue();
            for (MultipartFile file : fileList) {
                if (file.isEmpty()) continue;
                multipartFileList.add(file);
            }
        }
        // 첨부파일 체크
        if (multipartFileList.size() > 0) {
            for (MultipartFile file : multipartFileList) {
                //String elementName = file.getName(); // input name (imageFile[0], imageFile[1], ...)
                long fileSize = file.getSize();
                String fileOriginName = file.getOriginalFilename(); // input 원본파일명
                String fileExt = fileOriginName.substring(fileOriginName.lastIndexOf(".")+1); // 확장자

                if (fileSize > 0) { // 첨부파일이 존재하면
                    String _fileName = Functions.getTimeStamp() + mbIdx + order + "." + fileExt; // 파일명 생성
                    String url = nCloudStorageUtils.uploadFile(folderName, _fileName, file); // 업로드 처리

                    FileUpload fileUpload = new FileUpload();
                    fileUpload.setMatchCode(imgCode);
                    fileUpload.setTblName("medical");
                    fileUpload.setFileUrl(url);
                    fileUpload.setSort(order);

                    uploadFiles.add(fileUpload);
                    order++;
                }
            }
        }
        /** 파일업로드 END **/


        int insertIdx = 0;

        // 신청등록
        try {
            int cnt = remoteMapper.insertRemoteForm(medicalRequest);
            if (cnt == 1) {
                insertIdx = medicalRequest.getIdx();

                // 파일업로드
                if (uploadFiles.size() > 0) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("list", uploadFiles);
                    // for (FileUpload param : uploadFiles) {
                    //     log.info(param.getFileUrl());
                    //     fileUploadMapper.uploadFile(param);
                    // }
                    fileUploadMapper.insertUploadFiles(map);
                }


            }
        } catch (Exception e) {
            log.info("registerRemoteForm() Exception=" + e);
        }

        //log.info("insertIdx=" + insertIdx);
        return insertIdx;
    }

    // 원격진료 신청시 상세정보
    public Map<String, Object> getRemoteForm(int idx, String medicalStatus) {
        Medical data = remoteMapper.getRemoteForm(idx, medicalStatus); // 진료신청 상세정보
        Map<String, Object> result = new HashMap<>();

        if (data != null) {
            result.put("formData", data);
            result.put("requestMemberId", data.getMbIdx()); // 신청회원 인덱스
            result.put("hospitalIdx", data.getHospitalIdx()); // 진료신청한 병원 인덱스


            // 선택한 질환, 설명
            int selectSubjectCode = Integer.parseInt(data.getSubjectCode());
            result.put("matchingSubjectCode", selectSubjectCode); // 1,..,6
            result.put("selectSubjectCode", Constants.MEDICAL_REQUEST_SUBJECT_CODE.get(selectSubjectCode)); // G,..,C
            result.put("selectSubject", Constants.MEDICAL_REQUEST_SUBJECT.get(selectSubjectCode));
            result.put("selectSubjectDetail", Constants.MEDICAL_REQUEST_SUBJECT_DESC.get(selectSubjectCode));

            // 환자 희망시간
            String[] hopeHourStr = data.getHopeHour().split(":");
            int[] hopeHour = {Integer.parseInt(hopeHourStr[0]), Integer.parseInt(hopeHourStr[1])};

            // 진료시간 범위 설정 (30분 단위)
            int startTime = 9;
            int endTime = 22;
            int key = 0;
            int selectKey = 0; // 환자선택시간 순번
            Map<Integer, String> timeTable = new HashMap<>();

            for (int i=startTime; i<endTime; i++) {
                String _hour = (i<10)? "0"+i : String.valueOf(i);
                String _nextHour = String.valueOf(i+1);

                // 9:00~9:30
                String[] _time1 = {_hour + ":00", _hour + ":30"};
                timeTable.put(key, String.join("~", _time1));
                key++;

                // 9:30~10:00
                String[] _time2 = {_hour + ":30", _nextHour + ":00"};
                timeTable.put(key, String.join("~", _time2));
                key++;

                // 환자 희망시간에 맞는 key
                if (i == hopeHour[0]) {
                    selectKey = (hopeHour[1] < 30)? key-2 : key-1;
                }
            }
            // log.info("희망시간"+ data.getHopeHour() +"/희망시간key=" + selectKey + "/table=" + timeTable+"");
            result.put("timeTable", timeTable);
            result.put("selectKey", selectKey);
            result.put("selectTime", timeTable.get(selectKey));
        }

        return result;
    }

    // 원격진료 병원선택 진료시간 변경
    public int changeRemoteFormHopeHour(String hourRange, String idx) {
        String[] hour = hourRange.split("~");
        //if (hour[0].indexOf(":30") != -1) hour[0] = hour[0].replace(":30", ":31"); // 31분으로 변경

        return remoteMapper.changeRemoteFormHopeHour(hour[0], idx);
    }

    /**
     * 원격진료 병원목록 추출
     * - 전체 카운팅을 위해 최초호출시 전체 데이터 조회&가공하여 VO에 저장 -> 이후 값 불러오기..가능한가?
     *
     * page : 현재페이지
     * matchingCode : 진료접수시 질환 코드 (MEDICAL_REQUEST_SUBJECT)
     */
    public Map<Integer, Object> getHospitalSelectList(Map<String, String> params, String lat, String lng){
        int tab = Integer.parseInt(params.get("tab"));
        String hopeTime = params.get("hopeTime");
        int idx = Integer.parseInt(params.get("idx"));
        int page = Integer.parseInt(params.get("page"));
        int mbIdx = Integer.parseInt(params.get("mbIdx"));
        int matchingCode = Integer.parseInt(params.get("matchingCode"));

        /**
         * 공통조건
         * 1. 진료가능과목 조회
         * 2. 영업 요일에 해당 - 요일에 해당하지만 진료시간 끝난건 진료중/진료종료로 상태표시됨
         * 3. 진료예약시간 체크 - 병원은 15분 단위로 진료시간 설정가능
         *      if 특정일에 설정한 예약시간이 있는지?
         *      else 일반환자시간 조회
         */
        // 회원이 선택한 희망시간(범위)에서 진료예약시간 추출
        // ex. 회원이 10:00~10:30 선택시 병원 진료예약시간 10:00,10:15,10:30 추출하여 예약가능 비교
        String[] hopeTimeSpilt = hopeTime.split("~");
        String[] hopeHour = hopeTimeSpilt[0].split(":");
        String[] hospitalAvailTime = new String[3];
        hospitalAvailTime[0] = hopeTimeSpilt[0].replace(":", "");
        hospitalAvailTime[2] = hopeTimeSpilt[1].replace(":", "");
        // 10:30~10:30 체크 ? 10:15 : 10:45
        hospitalAvailTime[1] = (hopeTimeSpilt[0].indexOf(":00") > -1)? hopeHour[0] + "15" : hopeHour[0] + "45";

        // 오늘요일 (1~7, 공휴일 H), 오늘날짜 (YYYY-mm-dd) TODO:공휴일처리!
        LocalDate date = LocalDate.now();
        String today = date.toString();
        DayOfWeek dayOfWeek = date.getDayOfWeek();
        String todayNum = String.valueOf(dayOfWeek.getValue());

        // 상단 탭 카운팅 (진료가능 병원수)
        Map<Integer, Integer> medicalAvailCnt = new HashMap<>();

        // 최초 호출시 한번에 데이터 조회
        if (tab == 1 && page == 1) {
            for (int i=1; i<=2; i++) { // 1:주치전담병원, 2:진료가능병원
                List<Map<String, Object>> searchHospitalList = new ArrayList<>(); // 병원전체목록 조회
                List<Map<String, Object>> hospitalRepository = new ArrayList<>(); // 저장할 병원전체목록
                int availCnt = 0;

                // 병원진료과목-환자질환 매칭
                String medicalSubjectCode = String.valueOf(matchingCode); // 질환 int->string 으로 변환
                List<String> checkHospitalSubjCdList = new ArrayList<>();
                for (Integer hospitalSubjCd : Constants.HOSPITAL_MATCHING_CODE.keySet()) {
                    // 병원진료과목에 해당하는 질환 split
                    String[] codeSplit = Constants.HOSPITAL_MATCHING_CODE.get(hospitalSubjCd).split(",");
                    if (Arrays.stream(codeSplit).anyMatch(medicalSubjectCode::equals)) {
                        // 환자가 선택한 질환이 진료과목에 포함되면 arrayList 추가
                        checkHospitalSubjCdList.add(hospitalSubjCd.toString());
                    }
                }
                // List<> -> String[] 변환 (매칭할 진료과목 추출 완료)
                String[] matchingCodeArr = new String[checkHospitalSubjCdList.size()];
                matchingCodeArr = checkHospitalSubjCdList.toArray(matchingCodeArr);

                // 1. 거리순 병원 전체 목록
                if (i == 1) { // 주치전담병원 목록 (병원 예약시간 - 지정환자시간)
                    searchHospitalList = remoteMapper.getMyHospitalList(lat, lng, todayNum, today, matchingCodeArr, mbIdx);
                } else { // 진료가능병원 목록 (병원 예약시간 - 일반환자시간)
                    searchHospitalList = remoteMapper.getAllHospitalList(lat, lng, todayNum, today, matchingCodeArr);
                }

                for (Map listData : searchHospitalList) {
                    if(listData.get("mainImg") == null) listData.put("mainImg", NO_IMAGE); // 대표이미지

                    //log.info(listData.toString());
                    // 2. 예약가능시간 추출
                    String availTimeTable = "";
                    if (listData.get("todayTimeTable") != null) { // 오늘날짜(YYYY-mm-dd)에 예약시간이 별도로 지정됨
                        availTimeTable = listData.get("todayTimeTable").toString();
                    } else if (listData.get("weekTimeTable") != null) { // 요일(일~월)에 해당하는 공통시간
                        availTimeTable = listData.get("weekTimeTable").toString();
                    }

                    // 3. 예약가능시간 조회결과 처리
                    if (!availTimeTable.isEmpty()) { // 3.1 성공
                        log.info("3.1 조회결과성공/idx=" + listData.get("idx") + "/" + listData);
                        // 병원 진료예약가능?
                        // - 예약가능시간 비교
                        Boolean canAppointment = (availTimeTable.indexOf(hospitalAvailTime[0]) > -1 || availTimeTable.indexOf(hospitalAvailTime[1]) > -1 || availTimeTable.indexOf(hospitalAvailTime[2]) > -1);
                        if (canAppointment) {
                            // 4.1. 진료중 (예약가능시간에 해당함)
                            listData.put("status", true); // (예약가능) 진료중
                            availCnt++;

                        } else {
                            // 4.2. 진료불가 (예약가능시간 아님)
                            listData.put("status", false); // (예약불가능) 진료불가
                        }

                        // 거리 km
                        Double distance = (double) listData.get("distance");
                        String km = String.format("%.1f", distance);
                        listData.put("km", km);

                        // 병원진료과목
                        String hospitalSubjectCode = String.valueOf(listData.get("subjectCode"));
                        // 질환-병원진료과목 매칭
                        String subjectNames = Functions.getSubjectCodeMatching(medicalSubjectCode, hospitalSubjectCode);
                        listData.put("subjectNames", subjectNames);

                        hospitalRepository.add((Map<String, Object>) listData);

                    } else {    // 3.2. 실패 - 예약가능시간 조회실패 or 시간설정안함
                        log.info("3.2 조회결과실패=" + listData.get("idx") + "/" + listData);
                    }
                }

                if (i == 1) {
                    MY_HOSPITAL_REPOSITORY = hospitalRepository;
                } else {
                    MEDICAL_HOSPITAL_REPOSITORY = hospitalRepository;
                }
                medicalAvailCnt.put(i, availCnt);

            } // end for
        }

        // 페이징 후 결과값 return
        Map<Integer, Object> result = new HashMap<>();

        for (int i=1; i<=2; i++) {
            Map<String, Object> tabResult = new HashMap<>();
            int totalCount = (i==1)? MY_HOSPITAL_REPOSITORY.size() : MEDICAL_HOSPITAL_REPOSITORY.size();
            int listRows = 5; // 한페이지 노출 갯수
            int totalPage = (int) Math.ceil((double) totalCount/ (double)listRows);
            boolean isLastPage = (page==totalPage || totalPage==0); // 마지막페이지 true/false
            int startRecord = (page-1) * listRows; // 시작열
            int endRecord = startRecord+listRows; // 마지막열
            if (endRecord > totalCount) endRecord = totalCount;

            tabResult.put("listRows", listRows);
            tabResult.put("totalCount", totalCount);
            tabResult.put("totalPage", totalPage);
            tabResult.put("startRecord", startRecord);
            tabResult.put("endRecord", endRecord);
            tabResult.put("isLastPage", isLastPage);

            tabResult.put("availCnt", medicalAvailCnt.get(i));

            if (page > totalPage) {
                log.info("불러올 페이지 없음!");
                tabResult.put("canNextPage", false);

            } else {
                List<Object> record = new ArrayList<>();
                for (int j=startRecord; j<endRecord; j++) {
                    if (i == 1) {
                        record.add(MY_HOSPITAL_REPOSITORY.get(j));
                    } else {
                        record.add(MEDICAL_HOSPITAL_REPOSITORY.get(j));
                    }
                }
                tabResult.put("list", record); // 예약시간에 해당하는 병원목록
            }

            // log.info("페이징=" + tabResult);
            // log.info("총목록=" + MEDICAL_HOSPITAL_REPOSITORY);
            result.put(i, tabResult);
        }

        return result;
    }

    /**
     * 원격진료 신청완료 처리
     * - 접수번호 생성 (13자리)
     *  1. 질환정보 : 일반(G), 피부(D), 호흡기(R), 여성(O), 비뇨기(U), 만성질환(C)
     *  2. 날짜 : 년월일 6자리
     *  3. 진료구분 : 초진(F), 재진(S) *재진: 3개월 내 같은 질환으로 2회 이상 진료 예약한 환자
     *  4. 접수번호 : 5자리 순번
     *
     *  ex. G220629F00001
     */
    @Transactional
    public int updateRemoteForm(int hospitalIdx, int medicalIdx, String code, String hopeRange, String cardIdx) { // 10,7,O
        LocalDate date = LocalDate.now();
        String today = date.toString();
        int order = 1;

        // 진료 접수번호 순번 조회 & 새 순번 등록
        MedicalReceiptOrder receiptOrder = remoteMapper.searchReceiptOrder(today);//hospitalIdx
        if (receiptOrder != null) {
            order = receiptOrder.getNumber() + 1;
        }
        MedicalReceiptOrder insertReceiptOrder = new MedicalReceiptOrder(hospitalIdx, order, today);
        int insertResultCnt = remoteMapper.insertReceiptOrder(insertReceiptOrder);
        log.info("insertResultCnt=" + insertResultCnt);

        if (insertResultCnt == 0)   // 등록실패
            return 0;

        // 초진(F), 재진(S)
        int hospitalVisitCount = remoteMapper.getHospitalVisitCount(medicalIdx, hospitalIdx, today);
        String medicalType = (hospitalVisitCount >= 2)? "S" : "F";

        // 접수번호 생성
        String receiptNo = code;
        receiptNo += (today.replace("-", "")).substring(2);
        receiptNo += medicalType;
        receiptNo += String.format("%05d", order);
        log.info("receiptNo=" + receiptNo);

        // 예약시간
        String[] hopeRangeArr = hopeRange.split("~");

        // 진료 업데이트
        int resultCnt = remoteMapper.updateRemoteForm(String.valueOf(hospitalIdx), String.valueOf(medicalIdx), receiptNo, hopeRangeArr[0], hopeRangeArr[1], cardIdx);

        return resultCnt;
    }

    /**
     * 원격진료 상세화면
     * 상태 : 진료신청/진료중/조제중
     */
    public Map<String, Object> getMedicalStatus(int idx, String lat, String lng) {
        Map<String, Object> result = new HashMap<>();

        // 진료 & 병원정보
        Map<String, Object> viewData = remoteMapper.getRemoteStatusData(idx, lat, lng);
        // log.info("getMedicalStatus=" + viewData);

        if (viewData != null) {
            result = viewData;

            // null 예외처리
            if (viewData.get("hospitalIntroduce") == null) viewData.put("hospitalIntroduce", "");
            if (viewData.get("nonReimbJson") == null) viewData.put("nonReimbJson", "");
            if (viewData.get("mainImg") == null) result.put("mainImg", NO_IMAGE);

            // 병원진료과목
            String subjectNames = Functions.getHospitalSubjectNames(String.valueOf(viewData.get("subjectCode")));
            viewData.put("subjectNames", subjectNames);

            // 거리 km
            Double distance = (double) result.get("distance");
            String km = String.format("%.1f", distance);
            result.put("km", km);

            // 진료시간
            List<HospitalHour> searchHours = remoteMapper.getHospitalHours(Integer.parseInt(viewData.get("hospitalIdx").toString()));
            Map<String, String> hours = Functions.getHospitalHours(searchHours, String.valueOf(viewData.get("eachSetYn")), String.valueOf(viewData.get("restStartHour")), String.valueOf(viewData.get("restEndHour")));
            result.put("hospitalHours", hours);

            // 비급여
            Map<String, String> nonReimbMap = Functions.getHospitalNonReimbursement(String.valueOf(viewData.get("nonReimbJson")));
            result.put("nonReimb", nonReimbMap);

            // 진료상태바 표시
            // String medicalStatus = viewData.get("medicalStatus").toString();    // 진료상태
            // String hospitalReceiptYn = viewData.get("rcptYn").toString();       // 병원접수상태

        } else {
            // 데이터 null
        }

        return result;
    }

    // 원격진료 상세화면 - 조제중이면 약국,조제비 정보 조회
    public Map<String, String> getRemoteStatusIngData(int reqidx, int medicalIdx, int mbAddrIdx) {
        return remoteMapper.getRemoteStatusIngData(reqidx, medicalIdx, mbAddrIdx);
    }

    // 진료 신청취소
    public int cancelRemoteForm(int medicalIdx) {
        int result = remoteMapper.cancelRemoteForm(medicalIdx);
        return result;
    }

    // 원격진료 리뷰작성
    public int reviewWriteAction(MedicalReview medicalReview, HttpServletRequest httpServletRequest) {
        medicalReview.setMbIdx(Integer.parseInt(httpServletRequest.getParameter("mbIdx"))); // 회원 idx

        int insertIdx = 0;
        int cnt = remoteMapper.reviewWriteAction(medicalReview);
        if (cnt == 1) insertIdx = medicalReview.getIdx();

        return insertIdx;
    }

    // 원격진료 목록
    public List<Map<String, Object>> ajaxRemoteList(int page, int month, int status, int mbIdx, int listRow) {
        // 오늘 ~ n개월
        Map<String, String> searchDate = Functions.getSearchDateFilter(month);

        List<Map<String, Object>> result = new ArrayList<>();

        int startRecord = (page-1) * listRow;
        int endRecord = listRow; // 한페이지 listRow만큼 가져옴

        List<Map<String, Object>> mapperData = new ArrayList<>();

        switch (status) {
            case 0 : // 진료접수
                mapperData = remoteMapper.getRemoteListAppointment(mbIdx, searchDate.get("todayDate"), searchDate.get("beforeDate"), startRecord, endRecord);
                break;
            case 1 : // 진료진행중
                mapperData = remoteMapper.getRemoteListIng(mbIdx, searchDate.get("todayDate"), searchDate.get("beforeDate"), startRecord, endRecord);
                break;
            case 2 : // 진료완료
                mapperData = remoteMapper.getRemoteListComplete(mbIdx, searchDate.get("todayDate"), searchDate.get("beforeDate"), startRecord, endRecord);
                break;
            case 3 : // 진료취소
                mapperData = remoteMapper.getRemoteListCancel(mbIdx, searchDate.get("todayDate"), searchDate.get("beforeDate"), startRecord, endRecord);
                break;
        }

        for (Map<String, Object> data : mapperData) {
            // 환자질환
            String medicalSubjectCode = String.valueOf(data.get("medicalSubjectCode"));
            // 병원진료과목
            String hospitalSubjectCode = String.valueOf(data.get("hospitalSubjectCode"));
            // 질환-병원진료과목 매칭
            String subjectNames = Functions.getSubjectCodeMatching(medicalSubjectCode, hospitalSubjectCode);

            // 매칭 진료과목
            data.put("subjectNames", subjectNames);


            // 진료실입장 가능여부 (진료시작 10분전 활성화) + 병원에서 수락함
            String canEnter = "N";
            if (String.valueOf(data.get("hospitalRcptYn")).equals("Y")) { // 병원수락=Y
                String startDateStr = String.valueOf(data.get("regdate")).substring(0, 10) + " " + String.valueOf(data.get("appStartHour")) + ":00"; // yyyy-MM-dd HH:mm:ss
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime appStartDateTime = LocalDateTime.parse(startDateStr, formatter); // 예약시간
                LocalDateTime currentDateTime = LocalDateTime.now(); // 현재시간
                // 예약시간 10분전에 해당하는지? 입장가능 : 불가
                canEnter = appStartDateTime.minusMinutes(10).isBefore(currentDateTime) ? "Y" : "N";
                // log.info("예약시간=" + appStartDateTime + "/현재시간=" + currentDateTime);
                // log.info(canEnter + "");
            }
            data.put("canEnter", canEnter);

            // 예약취소 가능여부 (TODO: 현재 정해진 취소 정의 없어 전부 취소가능)
            String canCancel = "Y"; //Y,N
            data.put("canCancel", canCancel);

            /**
             * 상태별 데이터 추가
             */
            switch (status) {
                case 1 :    // 진료접수중
                    int presIdx = !Functions.isNull(data.get("presIdx"))? Integer.parseInt(String.valueOf(data.get("presIdx"))) : 0; // 처방전 인덱스
                    data.put("presIdx", presIdx); // null check
                    // log.info("presIdx" + presIdx);

                    // 처방전 등록됨 AND 약조제요청(약국선택) 안함
                    if (presIdx > 0 && String.valueOf(data.get("pharmacyReqIdx")).equals("0")) {
                        int hospitalIdx = Integer.parseInt(String.valueOf(data.get("hospitalIdx")));
                        List<Pharmacy> pharmacyList = remoteMapper.getPharmacySelectList(hospitalIdx);
                        data.put("pharmacyList", pharmacyList);
                    } else {
                        data.put("pharmacyList", "");
                    }
                    break;

                case 2 :    // 진료완료
                    // 가족관계
                    String familyRelStr = "본인";
                    String familyName = "";
                    if (!String.valueOf(data.get("patientIdx")).equals("0")) {
                        int familyRelCode = Integer.parseInt(String.valueOf(data.get("familyRel")));
                        familyRelStr = Constants.MEMBER_FAMILY_REL.get(familyRelCode);
                        familyName = String.valueOf(data.get("familyName"));
                    }
                    data.put("familyRelStr", familyRelStr);
                    data.put("familyName", familyName);

                    // 진료소견 가족력
                    String oldHistory = "";
                    String oldHistoryCheck = String.valueOf(data.get("opOldHistoryCheck")); // 가족력 선택
                    String opOldHistoryInput = String.valueOf(data.get("opOldHistoryInput")); // 가족력 직접입력
                    if (oldHistoryCheck == null) {
                        oldHistory = opOldHistoryInput;
                    } else {
                        String[] oldHistoryCheckSplit = oldHistoryCheck.split(",");
                        for (String check : oldHistoryCheckSplit) {
                            if (check == null || check.equals("null")) continue;
                            int code = Integer.parseInt(check);
                            oldHistory += (oldHistory.isEmpty())? "" : ", ";
                            oldHistory += (code==0)? opOldHistoryInput : Constants.MEDICAL_OPINION_HISTORY_CHECK.get(code);
                        }
                    }
                    data.put("opOldHistory", oldHistory);
                    break;
            }


            // 상세페이지 step (1: 진료신청, 2:진료중, 3:조제중)
            int viewPageStep = 1;
            if (status == 1) viewPageStep = 3;
            else {
                viewPageStep = (canEnter.equals("Y"))? 2 : 1;
            }
            data.put("step", viewPageStep);

            // 목록 추가
            result.add(data);
            log.info("data=" + data);
        }

        return result;
    }

    // 원격진료 진료진행중 - 약국선택
    @Transactional
    public int selectPharmacyAction(int medicalIdx, int pharmacyIdx) {
        // boolean result = false;
        int pharmacyRequestIdx = 0;

        // 원격진료 정보 - 처방전 idx 필요
        Medical medical = remoteMapper.getRemoteForm(medicalIdx, "");
        int presIdx = medical.getPresIdx();

        // 약조제요청 정보
        PharmacyRequest request = new PharmacyRequest();
        request.setPresIdx(presIdx); // 처방전 idx
        request.setMedicalIdx(medicalIdx);
        request.setPharmacyIdx(pharmacyIdx);
        request.setReqStatus("R");

        try {
            // 1. 약조제요청DB 등록
            int cnt = remoteMapper.insertPharmacyRequest(request);
            if (cnt == 1) {
                int insertIdx = request.getIdx();

                // 2. 진료DB 업데이트
                int cnt2 = remoteMapper.updatePharmacyRequestIndex(medicalIdx, insertIdx);
                // if (cnt2 == 1) result = true;
                if (cnt2 == 1) pharmacyRequestIdx = insertIdx;
            }

        } catch (Exception e) {
            log.error("selectPharmacyAction=" + e.toString());
        }

        return pharmacyRequestIdx;
    }

    // 원격진료 목록 진료완료 > 상세보기 결제정보
    public Map<String, String> getRemoteCompleteDetail(int pharmacyReqIdx, int mbAddrIdx) {
        Map<String, String> result = remoteMapper.getRemoteCompleteDetail(pharmacyReqIdx, mbAddrIdx);

        // 진료비 결제승인정보
        String hsPayMethod = Functions.paymentAuthType(result.get("hsAcquCardName"), result.get("hsCardQuota")); // 신한(일시불)
        String hsDate = Functions.paymentAuthDate(result.get("hsAuthDate")); // 2022.07.25(월) 16:28:00
        result.put("hsPayMethod", hsPayMethod);
        result.put("hsDate", hsDate);

        // 조제비 결제승인정보
        String phmPayMethod = Functions.paymentAuthType(result.get("phmAcquCardName"), result.get("phmCardQuota")); // 신한(일시불)
        String phmDate = Functions.paymentAuthDate(result.get("phmAuthDate")); // 2022.07.25(월) 16:28:00
        result.put("phmPayMethod", phmPayMethod);
        result.put("phmDate", phmDate);

        //return remoteMapper.getRemoteCompleteDetail(pharmacyReqIdx, mbAddrIdx);
        return result;
    }

    // 처방전보기
    public String getPrescriptionImage(int idx) {
        return remoteMapper.getPrescriptionImage(idx);
    }

    // 리뷰보기
    public Map getMedicalReview(int idx) {
        MedicalReview result = remoteMapper.getMedicalReview(idx);
        ObjectMapper objectMapper = new ObjectMapper();
        Map reviewData = objectMapper.convertValue(result, Map.class);

        // 질문 선택
        List<String> checkItemList = new ArrayList<>();
        if (result.getCheckItem() != null && !result.getCheckItem().isEmpty()) {
            String[] checkItems = result.getCheckItem().split(",");
            for (String item : checkItems) {
                int code = Integer.parseInt(item);
                checkItemList.add(Constants.REVIEW_CHECK_ITEM_CODE.get(code));
            }
        }
        reviewData.put("checkItemList", checkItemList);

        return reviewData;
    }
}

