package com.hellodoctor.service.api;

import com.hellodoctor.common.Constants;
import com.hellodoctor.common.Functions;
import com.hellodoctor.common.RestAPI;
import com.hellodoctor.config.ApiKey;
import com.hellodoctor.mapper.api.FcmMapper;
import com.hellodoctor.mapper.api.IPayMapper;
import com.hellodoctor.model.api.PaymentCancelResult;
import com.hellodoctor.model.api.PaymentResult;
import com.hellodoctor.model.api.Push;
import com.hellodoctor.model.app.Member;
import com.hellodoctor.model.app.MemberCreditCard;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

/**
 * 이노페이 service 클래스
 */
@Service
@Slf4j
public class IPayService {
    private Logger logger = LoggerFactory.getLogger("IPayService");

    @Autowired
    IPayMapper iPayMapper;
    @Autowired
    FcmMapper fcmMapper;

    // moid 생성
    public String createMoid(String prefix) {
        // 현재시간으로 moid 생성
        LocalDate nowDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyMMdd");
        String today = nowDate.format(formatter);
        LocalTime nowTime = LocalTime.now();
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("HHmmss");
        today += nowTime.format(formatter1);

        String moid = prefix + today + Functions.randomString(4);
        return moid;
    }

    // [앱] 결제관리 - 카드등록 - 이노페이 즉시카드등록
    public Map<String, Object> regAutoCardBill(HttpServletRequest form, Member member) {
        HttpSession session = form.getSession();
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("result", false);

        // 카드번호 arr
        String[] cardNums = form.getParameterValues("cardNum[]");

        int mbIdx = member.getIdx();
        String userId = Functions.createIPayUserId(mbIdx); //member.getMbId();
        String buyerHp = (member.getMbHp()).replaceAll("[^0-9]", "");
        String moid = createMoid("BK");
        String cardNum = String.join("", cardNums);
        String cardExpire = form.getParameter("year")+form.getParameter("month");
        String cardPwd = form.getParameter("pwd");
        String idNum = form.getParameter("idNum");

        // 카드등록전 중복등록 체크
        //

        // 빌키 존재하면 빌키삭제 (3.2에서 삭제 실패시 세션에 남은 빌키)
        if (session.getAttribute("saveBillKey") != null) {
            String deleteBillKey = String.valueOf(session.getAttribute("saveBillKey"));
            boolean isDelete = deleteCardBill(deleteBillKey, mbIdx);
            if (isDelete) session.setAttribute("saveBillKey", "");
        }

        // header
        JSONObject header = new JSONObject();

        // requset body 생성
        JSONObject body = new JSONObject();
        body.put("mid", ApiKey.IPAY_REG_CARD_MID);   // *이노페이발급 상점id
        body.put("arsUseYn", "N");                      // *자동결제 등록 ARS사용여부
        body.put("billKey", "");                        // 기본발급받은 빌키 (존재하면 삭제후 새로등록)
        // body.put("arsConnType", "");
        body.put("buyerHp", buyerHp);                   // 휴대폰번호(숫자)
        body.put("buyerName", member.getMbName());      // *이름
        body.put("moid", moid);                         // *가맹점 주문번호
        // body.put("payExpDate", "");
        body.put("userId", userId);                     // *고유아이디
        body.put("buyerEmail", "");
        body.put("cardNum", cardNum);                   // *카드번호(숫자)
        body.put("cardExpire", cardExpire);             // *유효기간(숫자, YYMM, 년월)
        body.put("cardPwd", cardPwd);                   // *카드 비밀번호 앞 2자리
        body.put("idNum", idNum);                       // *개인:생년월일6자리, 법인:사업자번호10자리
        logger.info("request=" + body);

        // 1. API 요청
        String url = "https://api.innopay.co.kr/api/regAutoCardBill";
        Map<String, Object> response = RestAPI.requestPost(url, header, body);
        Boolean apiConnectSuccess = Boolean.valueOf(response.get("connectResult").toString());
        logger.info("response=" + response);

        // 2. API 요청 성공
        if (apiConnectSuccess) {
            JSONObject data = (JSONObject)response.get("responseJson");
            String resultCode = String.valueOf(data.get("resultCode")); // 성공=0000

            // 2.1. 자동결제 카드등록 성공
            if (resultCode.equals("0000")) {
                String billKey = String.valueOf(data.get("billKey"));

                // DB등록전 빌키세션저장
                session.setAttribute("saveBillKey", billKey);
                logger.info("DB등록전 빌키세션저장=" + session.getAttribute("saveBillKey"));

                MemberCreditCard creditCard = new MemberCreditCard();
                creditCard.setMbIdx(member.getIdx());
                creditCard.setMoid(String.valueOf(data.get("moid")));
                creditCard.setBillKey(billKey);
                creditCard.setCardCode(String.valueOf(data.get("cardCode")));
                creditCard.setCardNum(String.join("-", cardNums));
                creditCard.setCardCode(String.valueOf(data.get("cardCode")));
                creditCard.setCardExpire(cardExpire);
                creditCard.setCardPwd(cardPwd);
                creditCard.setIdNum(idNum);

                // 3. DB등록
                int insert = iPayMapper.insertMemberCreditCard(creditCard);

                // 3.1. DB등록성공
                if (insert > 0) {
                    resultMap.put("result", true);
                    session.setAttribute("saveBillKey", ""); // 빌키세션삭제
                    logger.warn("3.1. DB등록 성공 / 빌키세션삭제");
                }
                // 3.2 DB등록실패 - 빌키삭제
                else {
                    boolean isDelete = deleteCardBill(billKey, mbIdx);
                    if (isDelete) session.setAttribute("saveBillKey", "");
                }
            }
            // 2.2 자동결제 카드등록 실패
            else {
                String errMsg = "결제카드등록에 실패했습니다.";
                if (data.get("resultMsg") != "") errMsg += "<br>("+ data.get("resultMsg") +")";
                resultMap.put("errMsg", errMsg);

                logger.warn("2.2 카드등록실패 = "+ data.get("resultMsg"));

                // 테스트 후 삭제
                // 빌키삭제
            }

            logger.info("session2=" + session.getAttribute("saveBillKey"));
        }
        // 4. API 요청 실패
        else {
            resultMap.put("errMsg", "결제카드등록에 실패했습니다. 다시 시도해 주세요.");
            logger.warn("3. API 요청 실패=" + response.get("errMsg"));
        }

        return resultMap;
    }

    // 이노페이 자동카드등록 빌키 존재하면 삭제
    public Boolean deleteCardBill(String billKey, int mbIdx) { //String userId
        String delUrl = "https://api.innopay.co.kr/api/delAutoCardBill";
        Boolean isDelete = false;

        String userId = Functions.createIPayUserId(mbIdx);

        // header
        JSONObject delHeader = new JSONObject();
        JSONObject delBody = new JSONObject();
        delBody.put("mid", ApiKey.IPAY_REG_CARD_MID);
        delBody.put("userId", userId);
        delBody.put("billKey", billKey);

        Map<String, Object> delResponse = RestAPI.requestPost(delUrl, delHeader, delBody);
        Boolean apiConnectSuccess = Boolean.valueOf(delResponse.get("connectResult").toString());
        logger.warn("빌키삭제결과=" + delResponse);

        if (apiConnectSuccess) {
            JSONObject data = (JSONObject)delResponse.get("responseJson");
            String resultCode = String.valueOf(data.get("resultCode")); // 성공=0000
            // 삭제성공
            if (resultCode.equals("0000")) isDelete = true;
        }

        return isDelete;
    }

    // [앱] 원격진료 조제비/진료비/테스트1원 카드결제
    public Map<String, Object> paymentRequest(HttpServletRequest request, Member member, String billType) {
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("result", false);
        resultMap.put("tid", "");
        resultMap.put("resultCode", "");
        resultMap.put("resultMsg", "");

        String moid = createMoid(billType);
        int amt = 0;
        String billKey = (!billType.equals("H"))? request.getParameter("billKey") : "";
        // String buyerHp = member.getMbHp().replaceAll("[^0-9]","");
        String buyerName = member.getMbName();
        String goodsName = "";
        String userId = member.getMbId();
        int medicalIdx = Integer.parseInt(String.valueOf(request.getParameter("medicalIdx"))); // 진료인덱스

        if (billType == "P") { // P: 조제비
            int billAmt = Integer.valueOf(String.valueOf(request.getParameter("billAmt")));
            int deliveryAmt = Integer.valueOf(String.valueOf(request.getParameter("deliveryAmt")));
            amt = billAmt + deliveryAmt;
            goodsName = "조제비 결제";

        } else if (billType == "T") { // T: 1원결제
            amt = 1;
            goodsName = "1원결제";

        } else { // H:진료비
            // TODO: 진료비 결제 추가
            int cardIdx = Integer.valueOf(String.valueOf(request.getParameter("cardIdx")));
            int billAmt = Integer.valueOf(String.valueOf(request.getParameter("billAmt")));

            // 결제 빌키 조회
            billKey = iPayMapper.getMemberBillKey(cardIdx);
            if (Functions.isNull(billKey)) {
                resultMap.put("resultCode", "EMPTY");
                resultMap.put("resultMsg", "결제카드가 존재하지 않습니다.");
                log.info("결제빌키 조회실패" + resultMap.toString());
                return resultMap; // 빌키조회실패 -> 결제실패처리
            }

            amt = billAmt;
            goodsName = "진료비 결제";
        }

        // TBC: 테스트 후 결제금액 변경할 것
        // amt = 1;

        // header
        JSONObject header = new JSONObject();

        // requset body 생성
        JSONObject body = new JSONObject();
        body.put("mid", ApiKey.IPAY_REG_CARD_MID);   // *이노페이발급 상점id
        body.put("moid", moid);
        body.put("amt", amt);
        body.put("billKey", billKey);
        body.put("buyerName", buyerName);
        body.put("goodsName", goodsName);
        body.put("userId", userId);
        body.put("cardQuota", "00"); // 할부개월

        /**
         * 1. 결제승인API 요청
         * 2. 결제DB 등록 (결과유무 상관없이)
         * 2.1. 결제DB 등록 성공
         *      결과 = (응답코드=0000)? 완료 : 실패
         * 2.2. 결제DB 등록 실패 & 에러
         *      결과 = (응답코드=0000)? 결제취소API 후 실패 : 실패;
         * 3. 결과 리턴
         */
        // 1. API 요청
        String url = "https://api.innopay.co.kr/api/payAutoCardBill";
        Map<String, Object> response = RestAPI.requestPost(url, header, body);
        Boolean apiConnectSuccess = Boolean.valueOf(response.get("connectResult").toString());
        logger.info("response=" + response);

        // 2. API 요청 성공 -> 결제DB 등록
        if (apiConnectSuccess) {
            JSONObject data = (JSONObject) response.get("responseJson");
            String tid = String.valueOf(data.get("tid"));
            String resultCode = String.valueOf(data.get("resultCode")); // 성공=0000
            String resultMsg = String.valueOf(data.get("resultMsg"));
            if (resultCode.equals("9986")) resultMsg = "삭제된 카드";

            PaymentResult dto = new PaymentResult();
            dto.setPayCancel("N");
            dto.setPayType(billType);
            dto.setMedicalIdx(medicalIdx);
            dto.setMbIdx(member.getIdx());

            dto.setResultCode(resultCode);
            dto.setResultMsg(resultMsg);
            dto.setTid(String.valueOf(data.get("tid")));
            dto.setMoid(String.valueOf(data.get("moid")));
            dto.setBillKey(String.valueOf(data.get("billkey")));

            dto.setUserId(String.valueOf(data.get("userId")));
            dto.setAcquCardCode(String.valueOf(data.get("acquCardCode")));
            dto.setAcquCardName(String.valueOf(data.get("acquCardName")));
            dto.setAmt(Integer.parseInt(String.valueOf(data.get("amt"))));
            dto.setAppCardName(String.valueOf(data.get("appCardName")));

            dto.setAppCardCode(String.valueOf(data.get("appCardCode")));
            dto.setAuthCode(String.valueOf(data.get("authCode")));
            dto.setAuthDate(String.valueOf(data.get("authDate")));
            dto.setBuyerEmail(String.valueOf(data.get("buyerEmail")));
            dto.setBuyerName(String.valueOf(data.get("buyerName")));

            dto.setBuyerTel(String.valueOf(data.get("buyerTel")));
            dto.setCardNum(String.valueOf(data.get("cardNum")));
            dto.setCardQuota(String.valueOf(data.get("cardQuota")));
            dto.setGoodsName(String.valueOf(data.get("goodsName")));

            resultMap.put("resultCode", resultCode);
            resultMap.put("resultMsg", resultMsg);

            // 2.1. DB등록 성공
            try {
                int insert = iPayMapper.inertPaymentResult(dto);
                if (insert > 0) {
                    logger.warn("2.1. DB등록 성공");
                    // 결제성공 -> 완료
                    if (resultCode.equals("0000")) {
                        resultMap.put("result", true);
                        resultMap.put("tid", data.get("tid"));
                        resultMap.put("paymentIdx", dto.getIdx()); // 결제인덱스
                    }
                }
                // 2.2 DB등록 실패
                else {
                    logger.warn("2.2. DB등록 실패");
                    // 결제성공 -> 결제취소API  호출
                    if (resultCode.equals("0000")) cancelPayment(tid, amt, goodsName + "취소");
                }
            } catch (Exception e) {
                logger.warn("2.3. DB등록 에러" + e.toString());
                // DB등록 에러 -> 결제취소API  호출
                if (resultCode.equals("0000")) cancelPayment(tid, amt, goodsName + "취소");
            }

        }

        return resultMap;
    }

    // 결제취소
    public void cancelPayment(String tid, int amt, String cancelMsg) { //String userId
        // header
        JSONObject header = new JSONObject();

        // requset body 생성
        JSONObject cancelBody = new JSONObject();
        cancelBody.put("mid", ApiKey.IPAY_REG_CARD_MID);   // *이노페이발급 상점id
        cancelBody.put("tid", tid);
        cancelBody.put("svcCd", "01");
        cancelBody.put("cancelAmt", amt);
        cancelBody.put("cancelMsg", cancelMsg);
        cancelBody.put("cancelPwd", ApiKey.IPAY_CANCEL_PASSWORD);

        // 결제취소 API 요청
        String cancelUrl = "https://api.innopay.co.kr/api/cancelApi";
        Map<String, Object> cancelResponse = RestAPI.requestPost(cancelUrl, header, cancelBody);
        Boolean apiConnectSuccess = Boolean.valueOf(cancelResponse.get("connectResult").toString());
        logger.warn("결제취소 cancelResponse = " + cancelResponse);

        PaymentCancelResult dto = new PaymentCancelResult();
        dto.setPgTid(tid);
        dto.setPgApprovalAmt(amt);

        if (apiConnectSuccess) {
            JSONObject data = (JSONObject) cancelResponse.get("responseJson");
            if (cancelResponse.get("responseJson") != null) {
                dto.setResultCode(String.valueOf(data.get("resultCode")));
                dto.setResultMsg(String.valueOf(data.get("resultMsg")));
                dto.setPgAppDate(String.valueOf(data.get("pgAppDate")));
                dto.setPgAppTime(String.valueOf(data.get("pgAppTime")));
            }
        }

        try {
            // 1. 결제취소 DB등록
            int chk = iPayMapper.inertPaymentCancelResult(dto);
            // 2. 기등록된 결제DB 취소처리 (pay_cancel = Y)
            int chk2 = iPayMapper.updateToCancelPaymentResult(tid);
            logger.info("cancelPayment() result=tid:"+ tid +", insert:" + chk + ", update:" + chk2);

        } catch (Exception e) {
            logger.error("cancelPayment() Exception=" + e.toString());
        }
    }

    /**
     * 진료비 결제 처리
     *
     * 1. 회원빌키 조회
     * 2. 결제DB 등록
     * 3. 진료DB 업데이트 (진료완료)
     */
    // @Transactional
    public Map<String, Object> billAmtCharge(HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("result", false);
        resultMap.put("failedSendPush", false);
        String mbIdx = String.valueOf(request.getParameter("mbIdx"));
        Member member = iPayMapper.getMemberByIdx(Integer.parseInt(mbIdx));

        if (member != null) {
            // 결제승인API
            Map<String, Object> response = paymentRequest(request, member, "H");
            boolean paymentResult = (Boolean) response.get("result");
            resultMap.put("result", paymentResult);
            resultMap.put("tid", String.valueOf(response.get("tid")));
            resultMap.put("resultCode", response.get("resultCode"));
            resultMap.put("resultMsg", response.get("resultMsg"));

            boolean paymentFailAlim = paymentResult; // 결제실패 알림발송?
            int medicalIdx = Integer.parseInt(String.valueOf(request.getParameter("medicalIdx"))); // 진료인덱스

            // 진료DB 진료완료 처리
            if (paymentResult) {
                int paymentIdx = Integer.parseInt(String.valueOf(response.get("paymentIdx"))); // 결제인덱스
                int chk = iPayMapper.completeMedical(medicalIdx, paymentIdx);
                if (chk < 1) {
                    // 진료DB 업데이트 실패 -> 결제취소 처리
                    int billAmt = Integer.parseInt(String.valueOf(request.getParameter("billAmt"))); // 결제금액
                    cancelPayment(String.valueOf(resultMap.get("tid")), billAmt, "진료비 결제취소");
                    resultMap.put("result", false);

                    paymentFailAlim = false;
                }
            }

            // 진료비 결제실패시 회원에게 알림발송 (미결제내역 생성)
            if (!paymentFailAlim) {
                String msg = (Functions.isNull(resultMap.get("resultMsg")))? "" : String.valueOf(response.get("resultMsg"));
                boolean sendPush = sendPushWithPaymentFail(medicalIdx, Integer.parseInt(mbIdx), msg);
                resultMap.put("failedSendPush", sendPush);
                log.info("회원에게 알림발송?" + sendPush);
            }
        }

        return resultMap;
    }

    /**
     * 진료비 결제실패시 회원에게 알림발송 (미결제내역 생성)
     *
     * 1. 진료DB 업데이트 (결제실패) bill_stlm_idx = -1
     * 2. 푸시알림 중복확인 후 푸시DB 등록
     * 3. 회원 푸시알림 AND 미결제내역 페이지 안내
     * 4. 의사페이지 알림창 (진료비결제에 실패하여 회원에게 미결제내역 발송)
     */
    @Transactional
    public boolean sendPushWithPaymentFail(int medicalIdx, int mbIdx, String msg) {
        boolean process = false;

        // 1. 진료DB 업데이트 (결제실패처리)
        int medicalUpdateChk = iPayMapper.updateFailedMedical(medicalIdx, msg);
        log.info("1. 진료DB 업데이트 (결제실패처리) = " + medicalUpdateChk);

        if (medicalUpdateChk > 0) {
            process = true;

            // 2. 푸시알림 중복확인 후 푸시DB 등록
            int pushChk = iPayMapper.failedPushSendCheck(mbIdx, medicalIdx);
            log.info("2. 푸시알림 중복확인 후 푸시DB 등록 = " + pushChk);

            if (pushChk == 0) {
                Push push = new Push();
                push.setMbIdx(mbIdx);
                push.setContent("미결제된 진료비가 있습니다.");
                push.setUrl("/app/repayment/" + medicalIdx);
                push.setRegdateTs(Functions.getTimeStamp());
                push.setTokenIdx(""); // TBC: 사용자 토큰
                push.setRefIdx(medicalIdx);
                push.setRefAction("medical_fail");

                // 신규푸시 등록
                int insertChk = fcmMapper.insertPush(push);
                if (insertChk == 0) process = false;
                log.info("2.1 신규푸시 등록 = " + pushChk);

                // 3. 회원 푸시알림 AND 미결제내역 페이지 안내
                // TBC: 푸시발송
            }
        }

        // 4. 의사페이지 알림창 (진료비결제에 실패하여 회원에게 미결제내역 발송)
        log.info("4. 결과리턴 = " + process);
        return process;
    }

}
