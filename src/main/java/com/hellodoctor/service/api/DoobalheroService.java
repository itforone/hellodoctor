package com.hellodoctor.service.api;

import com.hellodoctor.common.Constants;
import com.hellodoctor.common.RestAPI;
import com.hellodoctor.config.ApiKey;
import com.hellodoctor.model.app.Member;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * 두발히어로(택배) service 클래스
 */
@Service
@Slf4j
public class DoobalheroService {
    // 두발히어로 API
    public Map<String, Object> doobalheroAPI(HttpServletRequest request, Member member) {
        HttpSession session = request.getSession();
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("result", false);

        // header 생성
        org.json.simple.JSONObject header = new org.json.simple.JSONObject();
        header.put("Authorization", "Bearer "+ ApiKey.DOOBALHERO_TOKEN); // *두발히어로 token

        log.info("header: "+header);

        // requset body 생성
        org.json.simple.JSONObject body = new org.json.simple.JSONObject();
        body.put("spotCode", request.getParameter("spotCode"));
        body.put("receiverName", request.getParameter("receiverName"));
        body.put("receiverMobile", request.getParameter("receiverMobile"));
        body.put("receiverAddress", request.getParameter("receiverAddress"));
        body.put("receiverAddressDetail", request.getParameter("receiverAddressDetail"));
        body.put("receiverAddressPostalCode", request.getParameter("receiverAddressPostalCode"));

        body.put("productName", request.getParameter("productName"));
        body.put("memoFromCustomer", request.getParameter("memoFromCustomer"));
        body.put("productPrice", request.getParameter("productPrice"));
        body.put("orderIdFromCorp", request.getParameter("orderIdFromCorp"));
        body.put("print", request.getParameter("print"));

        // body.put("senderName", request.getParameter("senderName"));
        // body.put("senderMobile", request.getParameter("senderMobile"));
        // body.put("senderAddress", request.getParameter("senderAddress"));
        // body.put("senderAddressDetail", request.getParameter("senderAddressDetail"));
        // body.put("frontdoorPassword", request.getParameter("frontdoorPassword"));

        log.info("body: "+body);

        // 1. API 요청
        String url = "https://partner-api.dev.dhero.kr/deliveries"; // 단건 배송접수
        Map<String, Object> response = RestAPI.requestPost(url, header, body);
        Boolean apiConnectSuccess = Boolean.valueOf(response.get("connectResult").toString());

        // 2. API 요청 성공
        if (apiConnectSuccess) {
            org.json.simple.JSONObject data = (org.json.simple.JSONObject)response.get("responseJson");
            log.info("data: "+data);
            String result = String.valueOf(data.get("result")); // 성공=SUCCESS
            // 2.1 조회 성공
            // if(result.equals("SUCCESS")) {
            //
            // }
            // // 2.2 조회 실패
            // else {
            //     String errMsg = "배송비 조회에 실패했습니다.";
            //     if (data.get("error_message") != "") errMsg += "<br>("+ data.get("error_message") +")";
            //     resultMap.put("errMsg", errMsg);
            // }
        }
        // 4. API 요청 실패
        else {
            resultMap.put("errMsg", "조회에 실패했습니다. 다시 시도해 주세요.");
            log.warn("3. API 요청 실패=" + response.get("errMsg"));
        }

        // log.info("resultMap: "+resultMap);
        return resultMap;
    }
}
