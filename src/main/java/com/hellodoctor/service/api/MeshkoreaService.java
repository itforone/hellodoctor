package com.hellodoctor.service.api;

import com.hellodoctor.common.RestAPI;
import com.hellodoctor.config.ApiKey;
import com.hellodoctor.model.app.Member;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * 메쉬코리아(퀵) service 클래스
 */
@Service
@Slf4j
public class MeshkoreaService {
    // 메쉬코리아 API
    public Map<String, Object> meshkoreaAPI(HttpServletRequest request, Member member) {
        HttpSession session = request.getSession();
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("result", false);

        // header 생성
        org.json.simple.JSONObject header = new org.json.simple.JSONObject();
        header.put("apikey", ApiKey.MESHKOREA_API_KEY); // *메쉬코리아 API KEY
        header.put("secret", ApiKey.MESHKOREA_SECRET_KEY); // *메쉬코리아 SECRET

        // requset body 생성
        org.json.simple.JSONObject body = new org.json.simple.JSONObject();
        body.put("branch_code", request.getParameter("branch_code")); // branch_code (점포고유코드)
        // body.put("dest_address", request.getParameter("dest_address"));
        // body.put("dest_address_detail", request.getParameter("dest_address_detail"));
        // body.put("dest_address_road", request.getParameter("dest_address_road"));
        // body.put("dest_address_detail_road", request.getParameter("dest_address_detail_road"));
        // body.put("dest_lat", request.getParameter("dest_lat"));
        // body.put("dest_lng", request.getParameter("dest_lng"));

        log.info("body: "+body);

        // 1. API 요청
        // String url = "https://public-prime.qa4.meshdev.io/api/delivery/submit_fee"; // 배송비 확인
        String url = "https://public-prime.qa4.meshdev.io/api/delivery/branch_status"; // 상점 상태
        Map<String, Object> response = RestAPI.requestPost(url, header, body);
        Boolean apiConnectSuccess = Boolean.valueOf(response.get("connectResult").toString());

        // 2. API 요청 성공
        if (apiConnectSuccess) {
            org.json.simple.JSONObject data = (org.json.simple.JSONObject)response.get("responseJson");
            log.info("data: "+data);
            String result = String.valueOf(data.get("result")); // 성공=SUCCESS
            // 2.1 배송비 조회 성공
            // if(result.equals("SUCCESS")) {
            //
            // }
            // // 2.2 배송비 조회 실패
            // else {
            //     String errMsg = "배송비 조회에 실패했습니다.";
            //     if (data.get("error_message") != "") errMsg += "<br>("+ data.get("error_message") +")";
            //     resultMap.put("errMsg", errMsg);
            // }
        }
        // 4. API 요청 실패
        else {
            resultMap.put("errMsg", "조회에 실패했습니다. 다시 시도해 주세요.");
            log.warn("3. API 요청 실패=" + response.get("errMsg"));
        }

        // log.info("resultMap: "+resultMap);
        return resultMap;
    }
}
