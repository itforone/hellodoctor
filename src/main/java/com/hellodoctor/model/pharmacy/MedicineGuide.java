package com.hellodoctor.model.pharmacy;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 복약지도
 */
@Data
@AllArgsConstructor // 모든 생성자 생성
@NoArgsConstructor
public class MedicineGuide {
    private int idx;
    private int medicalIdx;
    private int medicineIdx;
    private String itemName;
    private String itemCode;
    private String howTake;
    private String itemImg;
    private String itemImgName;
    private String regdate;
}