package com.hellodoctor.model.pharmacy;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 약사 회원정보
 */
@Data
@AllArgsConstructor // 모든 생성자 생성
@NoArgsConstructor
@Builder
public class Pharmacist {
    private int idx;
    private String originYn;
    private int pharmacyIdx;
    private String userName;
    private String userId;
    private String userPassword;
    private String userHp;
    private String licenseNo;
    private String licenseNoImg;
    private String licenseNoImgName;
    private String profileImg;
    private String history;
    private String introduce;
    private String regdate;
    private String lastDate;
    private String loginIp;
    private String delYn;
    private String leaveDate;
}