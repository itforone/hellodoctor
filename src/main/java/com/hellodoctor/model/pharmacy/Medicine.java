package com.hellodoctor.model.pharmacy;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 의약품
 */
@Data
@AllArgsConstructor // 모든 생성자 생성
@NoArgsConstructor
public class Medicine {
    private int idx;
    private int pharmacyIdx;
    private String delYn;
    private String itemName;
    private String itemCode;
    private String itemColor;
    private String storageMethod;
    private String howTake;
    private String ingredient;
    private String prescription;
    private String caution;
    private String sideEffect;
    private String itemImg;
    private String itemImgName;
    private String regdate;
}
