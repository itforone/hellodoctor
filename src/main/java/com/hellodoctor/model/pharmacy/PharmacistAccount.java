package com.hellodoctor.model.pharmacy;

import com.hellodoctor.model.hospital.Doctor;
import lombok.Getter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.List;

/**
 * 약사 회원정보 어댑터 클래스
 * https://sol-devlog.tistory.com/3
 */
@Getter
public class PharmacistAccount extends User {
    private Pharmacist pharmacist;
    public PharmacistAccount(Pharmacist pharmacist) {
        // ROLE 을 정할때는 ROLE_NAME 형식으로 사용, 시큐리티에서는 NAME 만 사용
        super(pharmacist.getUserId(), pharmacist.getUserPassword(),
                List.of(new SimpleGrantedAuthority("ROLE_PHARMACIST")));
        this.pharmacist = pharmacist;
    }
}