package com.hellodoctor.model.admin;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
UserDetails를 구현한 객체
UserDetails는 Spring Security에서 UserDetailsService가 권한 정보를 알 수 있도록 설정해주는 인터페이스
UserDetailsService는 LoginService가 구현
 */

@Data
@AllArgsConstructor // 모든 생성자 생성
@NoArgsConstructor
@Builder
public class Admin {
    private int idx;
    private String delYn;
    private String userName;
    private String userId;
    private String userPassword;
    private String regdate;
    private String lastDate;
    private String loginIp;
    private String delDate;
    private String role;
}
