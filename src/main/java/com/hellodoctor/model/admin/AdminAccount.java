package com.hellodoctor.model.admin;

import lombok.Getter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.List;

/**
 * 관리자 정보 어댑터 클래스
 * https://sol-devlog.tistory.com/3
 */
@Getter
public class AdminAccount extends User {
    private Admin admin;
    public AdminAccount(Admin admin) {
        // ROLE 을 정할때는 ROLE_NAME 형식으로 사용, 시큐리티에서는 NAME 만 사용
        super(admin.getUserId(), admin.getUserPassword(),
                List.of(new SimpleGrantedAuthority("ROLE_ADMIN")));
        this.admin = admin;
    }
}
