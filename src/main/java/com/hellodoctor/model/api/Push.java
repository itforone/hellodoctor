package com.hellodoctor.model.api;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * FCM 푸시 알림
 */
@Getter
@Setter
@NoArgsConstructor // 파라미터가 없는 기본 생성자 생성
@AllArgsConstructor // 모든 필드 값을 파라미터로 받는 생성자 생성
public class Push {
    public int mbIdx;
    public String delYn;
    public String content;
    public String url;
    public String regdate;
    public String regdateTs;
    public String tokenIdx;
    public int refIdx;
    public String refAction;
}
