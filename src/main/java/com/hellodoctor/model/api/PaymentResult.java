package com.hellodoctor.model.api;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 이노페이 결제결과
 */
@Getter
@Setter
@NoArgsConstructor // 파라미터가 없는 기본 생성자 생성
@AllArgsConstructor // 모든 필드 값을 파라미터로 받는 생성자 생성
public class PaymentResult {
    private int idx;
    private String payCancel;       // 결제취소여부
    private String payType;
    private int medicalIdx;
    private int mbIdx;
    private String regdate;
    // 이노페이 리턴값
    private String resultCode;
    private String resultMsg;
    private String tid;
    private String moid;
    private String billKey;

    private String userId;
    private String acquCardCode;
    private String acquCardName;
    private int amt;
    private String appCardName;

    private String appCardCode;
    private String authCode;
    private String authDate;
    private String buyerEmail;
    private String buyerName;

    private String buyerTel;
    private String cardNum;
    private String cardQuota;
    private String goodsName;
}
