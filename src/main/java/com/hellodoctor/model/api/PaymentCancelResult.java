package com.hellodoctor.model.api;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 이노페이 결제취소 결과
 */
@Getter
@Setter
@NoArgsConstructor // 파라미터가 없는 기본 생성자 생성
@AllArgsConstructor // 모든 필드 값을 파라미터로 받는 생성자 생성
public class PaymentCancelResult {
    private int idx;
    private String regdate;
    private String pgTid;           // tid
    private String resultCode;      // 결과코드 (2001:성공, 외 실패)
    private String resultMsg;       // 결과메시지
    private int pgApprovalAmt;   // 취소금액
    private String pgAppDate;       // 취소일
    private String pgAppTime;       // 취소시간
    //private String pgApprovalNo;
    // private String cancelTaxAmt;
    // private String cancelDutyFreeAmt;
    // private String pgResultCode;
    // private String pgResultMsg;
    // private String stateCd;
    // private String currency;
    /*
    // 실제결과
    "pgApprovalAmt":"1",
    "pgTid":"arstest03m01052207221521506261",
    "resultCode":"2001",
    "pgApprovalNo":"",
    "cancelTaxAmt":"",
    "cancelDutyFreeAmt":"",
    "resultMsg":"취소 성공",
    "pgAppDate":"20220722",
    "pgResultCode":null,
    "pgResultMsg":null,
    "pgAppTime":"152152",
    "stateCd":"1",
    "currency":""
     */
}
