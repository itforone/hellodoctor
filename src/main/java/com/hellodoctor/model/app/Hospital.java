package com.hellodoctor.model.app;

import lombok.*;

import java.math.BigDecimal;

/**
 * 병원
 */
@Data
@AllArgsConstructor // 모든 생성자 생성
@NoArgsConstructor
public class Hospital {
    private int idx;
    private String nonfaceSvcYn;
    private String designateSvcYn;
    private String hospitalName;
    private String zipCode;
    private String mainAddr;
    private String detailAddr;
    private String areaSi;
    private String areaGu;
    private String areaDong;
    private BigDecimal lat;
    private BigDecimal lng;
    private String telNo;
    private String faxNo;
    private String brNo;
    private String brNoImg;
    private String brNoImgName;
    private String svcCertImg;
    private String svcCertImgName;
    private String mainImg;
    private String nonReimbJson;
    private String bankCode;
    private String accountName;
    private String accountNum;
    private String bankImg;
    private String bankImgName;
    private String eachSetYn;
    private int restStartHour;
    private int restEndHour;
    private String introduce;
    private String authYn;
    private String authDate;
    private String tempYn;
    private String isUpdate;
}
