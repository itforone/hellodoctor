package com.hellodoctor.model.app;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 서비스문의
 */
@Getter
@Setter
@NoArgsConstructor // 파라미터가 없는 기본 생성자 생성
@AllArgsConstructor // 모든 필드 값을 파라미터로 받는 생성자 생성
public class CSApp {
    private int idx;
    private int mbIdx;
    private String serviceType;
    private String category;
    private String content;
    private String fileCode;
    private String regdate;
    private String answer;
    private String answerDate;
    private int adminIdx;
}
