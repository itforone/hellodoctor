package com.hellodoctor.model.app;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 병원 임시데이터
 */
@Data
@AllArgsConstructor // 모든 생성자 생성
@NoArgsConstructor
public class HospitalTmp {
    private int idx;
    private int hospitalIdx;
    private String nonfaceSvcYn;
    private String designateSvcYn;
    private String hospitalName;
    private String zipCode;
    private String mainAddr;
    private String detailAddr;
    private String areaSi;
    private String areaGu;
    private String areaDong;
    private BigDecimal lat;
    private BigDecimal lng;
    private String telNo;
    private String faxNo;
    private String brNo;
    private String brNoImg;
    private String brNoImgName;
    private String svcCertImg;
    private String svcCertImgName;
    private String mainImg;
    private String nonReimbJson;
    private String introduce;
}
