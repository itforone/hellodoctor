package com.hellodoctor.model.app;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

/*
UserDetails를 구현한 객체
UserDetails는 Spring Security에서 UserDetailsService가 권한 정보를 알 수 있도록 설정해주는 인터페이스
UserDetailsService는 LoginService가 구현
 */

@Data
@AllArgsConstructor // 모든 생성자 생성
@NoArgsConstructor
@Builder
public class Member { //implements UserDetails {
    private int idx;
    private String loginType;
    private String mbStatus;
    private String mbNo;
    private String mbId;
    private String mbPassword;
    private String mbCertify;
    private String mbName;
    private String mbSex;
    private String mbBirth;
    private String mbHp;
    private String regdate;
    private String lastDate;
    private String leaveDate;
    private String loginIp;
    private int myHospitalIdx;
    private String pushInfoYn;
    private String pushMarketingYn;
    private BigDecimal lat;
    private BigDecimal lng;
    private String role;
    private int addressIdx;

    /*
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        for(String role : role.split(",")){
            authorities.add(new SimpleGrantedAuthority(role));
        }
        return authorities;
    }

    @Override
    public String getPassword() {
        return mbPassword;
    }

    @Override
    public String getUsername() {
        return mbId;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
    */
}
