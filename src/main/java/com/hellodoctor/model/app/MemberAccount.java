package com.hellodoctor.model.app;

import lombok.Getter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.List;

/**
 * 앱 회원정보 어댑터 클래스
 * https://sol-devlog.tistory.com/3
 */
@Getter
public class MemberAccount extends User {
    private Member member;
    public MemberAccount(Member member) {
        super(member.getMbId(), member.getMbPassword(), List.of(new SimpleGrantedAuthority("MEMBER")));
        this.member = member;
    }
}
