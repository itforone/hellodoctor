package com.hellodoctor.model.app;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 복약지도 상세보기 VO
 */
@Getter
@Setter
@NoArgsConstructor // 파라미터가 없는 기본 생성자 생성
@AllArgsConstructor // 모든 필드 값을 파라미터로 받는 생성자 생성
public class MedicineGuideDetail {
    private String gItemName;
    private String gItemCode;
    private String gHowTake;
    private String gItemImage;
    private String mItemName;
    private String mItemCode;
    private String mItemColor;
    private String mStorageMethod;
    private String mHowTake;
    private String mIngredient;
    private String mPreScription;
    private String mCaution;
    private String mSideEffect;
    private String mItemImage;
    private int mIdx;
}
