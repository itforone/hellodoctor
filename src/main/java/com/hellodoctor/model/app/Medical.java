package com.hellodoctor.model.app;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 원격진료신청
 */
@Getter
@Setter
@NoArgsConstructor // 파라미터가 없는 기본 생성자 생성
@AllArgsConstructor // 모든 필드 값을 파라미터로 받는 생성자 생성
public class Medical {
    private int idx;
    private int mbIdx;
    private String rcptNo;
    private String medicalStatus;
    private String hospitalRcptYn;
    private int patientIdx;
    private String subjectCode;
    private String patientRrNo;
    private String patientHp;
    private String hopeHour;
    private String symptom;
    private String symptomImgCode;
    private int patientHeight;
    private int patientWeight;
    private String receiveMethod;
    private int mbAddressIdx;
    private int deliveryAmt;
    private String regdate;
    private String appStartHour;
    private String appEndHour;
    private int hospitalIdx;
    private String cancelDate;
    private String cancelReason;
    private int presIdx;
    private String billStlmIdx;
    private int nonReimbBillAmt;
    private int patientBillAmt;
    private int totalBillAmt;
    private int pharmacyReqIdx;
    private int mbCardIdx;
    private String medicalTime;
    private String isPres;
    private String isBillAmt;
    private String deliveryMsg;
    private String myDrYn;
    private int myDrPharmacyIdx;
    private int beforePresIdx;
    private String billStlmFailMsg;
}