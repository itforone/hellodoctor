package com.hellodoctor.model.app;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * 약 조제요청
 */
@Getter
@Setter
@NoArgsConstructor // 파라미터가 없는 기본 생성자 생성
@AllArgsConstructor // 모든 필드 값을 파라미터로 받는 생성자 생성
public class PharmacyRequest {
    private int idx;
    private int presIdx;
    private int medicalIdx;
    private int pharmacyIdx;
    private String reqStatus;
    private String regdate;
    private int billAmt;
    private String completeDate;
    private String courier;
    private String trackingNo;
    private String cancelReason;
    private String cancelDate;
    private int billStlmIdx;
}