package com.hellodoctor.model.app;

import lombok.*;

/**
 * 나의가족관리
 */

@Getter
@Setter
@NoArgsConstructor // 파라미터가 없는 기본 생성자 생성
@AllArgsConstructor // 모든 필드 값을 파라미터로 받는 생성자 생성
public class MemberFamily {
    private int idx;
    private int mbIdx;
    private String delYn;
    private String famRel;
    private String isDelegate;
    private String famName;
    private String rrNo;
    private String famHp;
    private String regdate;
    private String mbRrnoImg;
    private String famRrnoImg;
    private String famRelImg;
}
