package com.hellodoctor.model.app;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 결제카드등록
 */

@Getter
@Setter
@NoArgsConstructor // 파라미터가 없는 기본 생성자 생성
@AllArgsConstructor // 모든 필드 값을 파라미터로 받는 생성자 생성
public class MemberCreditCard {
    private int idx;
    private int mbIdx;
    private String delYn;
    private String moid;
    private String billKey;
    private String cardCode;
    private String regdate;
    private String delDate;
    private String cardNum;
    private String cardExpire;
    private String cardPwd;
    private String idNum;
}
