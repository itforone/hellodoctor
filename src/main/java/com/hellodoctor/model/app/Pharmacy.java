package com.hellodoctor.model.app;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * 약국 VO
 */
@Getter
@Setter
@NoArgsConstructor // 파라미터가 없는 기본 생성자 생성
@AllArgsConstructor // 모든 필드 값을 파라미터로 받는 생성자 생성
public class Pharmacy {
    private int idx;
    private String nonfaceSvcYn;
    private String designateSvcYn;
    private String PharmacyName;
    private String zipCode;
    private String mainAddr;
    private String detailAddr;
    private String areaSi;
    private String areaGu;
    private String areaDong;
    private BigDecimal lat;
    private BigDecimal lng;
    private String telNo;
    private String faxNo;
    private String brNo;
    private String brNoImg;
    private String brNoImgName;
    private String svcCertImg;
    private String svcCertImgName;
    private String mainImg;
    private String bankCode;
    private String accountName;
    private String accountNum;
    private String bankImg;
    private String bankImgName;
    private String regdate;
    private String delYn;
    private String authYn;
    private String tempYn;
    private String isUpdate;
}
