package com.hellodoctor.model.app;

import lombok.*;

/**
 * 원격진료 리뷰
 */

@Data
@NoArgsConstructor // 파라미터가 없는 기본 생성자 생성
@AllArgsConstructor // 모든 필드 값을 파라미터로 받는 생성자 생성
public class MedicalReview {
    private int idx;
    private int mbIdx;
    private int medicalIdx;
    private int reviewScore;
    private String symptom;
    private String content;
    private String onlyDoctorYn;
    private String checkItem;
    private String regdate;
    private String answer;
    private int doctorIdx;
    private String answerDate;
}
