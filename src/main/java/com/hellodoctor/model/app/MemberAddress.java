package com.hellodoctor.model.app;

import lombok.*;

import java.math.BigDecimal;

/**
 * 회원배송지관리
 */

@Getter
@Setter
@NoArgsConstructor // 파라미터가 없는 기본 생성자 생성
@AllArgsConstructor // 모든 필드 값을 파라미터로 받는 생성자 생성
public class MemberAddress {
    private int idx;
    private int mbIdx;
    private String zipCode;
    private String mainAddr;
    private String detailAddr;
    private String jibunAddr;
    private String areaSi;
    private String areaGu;
    private String areaDong;
    private BigDecimal lat;
    private BigDecimal lng;
    private char defaultAddr;
}
