package com.hellodoctor.model.app;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * 파일업로드
 */
@Getter
@Setter
@NoArgsConstructor // 파라미터가 없는 기본 생성자 생성
@AllArgsConstructor // 모든 필드 값을 파라미터로 받는 생성자 생성
public class FileUpload {
    private String matchCode;
    private String tblName;
    private String fileUrl;
    private String originFileName;
    private String regdate;
    private int sort;
}

