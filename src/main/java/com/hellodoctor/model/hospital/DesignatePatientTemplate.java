package com.hellodoctor.model.hospital;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 병원 지정환자등록 템플릿
 */
@Data
@AllArgsConstructor // 모든 생성자 생성
@NoArgsConstructor
public class DesignatePatientTemplate {
    private int idx;
    private int hospitalIdx;
    private String title;
    private String content;
    private String tempImg;
    private String tempImgName;
    private String regdate;
}