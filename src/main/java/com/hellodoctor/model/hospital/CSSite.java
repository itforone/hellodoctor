package com.hellodoctor.model.hospital;

import lombok.*;

@Data
@AllArgsConstructor // 모든 생성자 생성
@NoArgsConstructor
public class CSSite {
    private int idx;
    private int groupIdx;
    private int userIdx;
    private String groupType;
    private int category;
    private String subject;
    private String content;
    private String fileCode;
    private String regdate;
    private String answer;
    private String answerFileCode;
    private String answerDate;
    private int adminIdx;

    @Getter
    private String userName; // getter
}
