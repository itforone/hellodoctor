package com.hellodoctor.model.hospital;

import lombok.Getter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import java.util.List;

/**
 * 의사 회원정보 어댑터 클래스
 * https://sol-devlog.tistory.com/3
 */
@Getter
public class DoctorAccount extends User {
    private Doctor doctor;
    public DoctorAccount(Doctor doctor) {
        // ROLE 을 정할때는 ROLE_NAME 형식으로 사용, 시큐리티에서는 NAME 만 사용
        super(doctor.getUserId(), doctor.getUserPassword(),
                List.of(new SimpleGrantedAuthority("ROLE_DOCTOR")));
        this.doctor = doctor;
    }
}
