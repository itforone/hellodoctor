package com.hellodoctor.model.hospital;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 병원 지정환자
 */
@Data
@AllArgsConstructor // 모든 생성자 생성
@NoArgsConstructor
public class DesignatePatient {
    private int idx;
    private int hospitalIdx;
    private int doctorIdx;
    private int mbIdx;
    private String status;
    private String mbHp;
    private String regdate;
    private String acceptDate;
}