package com.hellodoctor.model.hospital;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor // 모든 생성자 생성
@NoArgsConstructor
public class MedicalOpinion {
    public int idx;
    public int medicalIdx;
    public int doctorIdx;
    // public String recordFileCode;
    // public String etcFileCode;
    public String symptom;
    public String currentHistory;
    public String oldHistoryCheck;
    public String oldHistoryInput;
    public String famHistory;
    public String diagnosis;
    public String plan;
    public String mediReco;
    public String workupReco;
    public String regdate;
}