package com.hellodoctor.model.hospital;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 병원 진료예약시간
 */
@Data
@AllArgsConstructor // 모든 생성자 생성
@NoArgsConstructor
public class HospitalAppointment {
    private int idx;
    private int hospitalIdx;
    private String patientType;
    private String yoilType;
    private String timeTable;
    private String regdate;
}