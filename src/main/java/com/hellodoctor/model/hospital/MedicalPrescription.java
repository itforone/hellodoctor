package com.hellodoctor.model.hospital;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 처방전
 */
@Data
@NoArgsConstructor // 파라미터가 없는 기본 생성자 생성
@AllArgsConstructor // 모든 필드 값을 파라미터로 받는 생성자 생성
public class MedicalPrescription {
    private int idx;
    private int medicalIdx;
    private int hospitalIdx;
    private String reRcptNo;
    private String presImg;
    private String presImgName;
    private int doctorIdx;
    private String regdate;
    private String presStatus;
    private String reRequestDate;
    private String reCompleteDate;
    private int billAmt;
    private int beforePresIdx;
    private String isReissue;
}