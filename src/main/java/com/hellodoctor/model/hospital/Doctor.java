package com.hellodoctor.model.hospital;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor // 모든 생성자 생성
@NoArgsConstructor
@Builder
public class Doctor {
    private int idx;
    private String originYn;
    private int hospitalIdx;
    private String userName;
    private String userId;
    private String userPassword;
    private String userHp;
    private String licenseNo;
    private String licenseNoImg;
    private String licenseNoImgName;
    private String doctorCertImg;
    private String doctorCertImgName;
    private String profileImg;
    private String history;
    private String introduce;
    private String regdate;
    private String lastDate;
    private String loginIp;
    private String role;
    private String subPosition;
    private String subSubjectCode;
    private String delYn;
    private String leaveDate;
    private String hospitalAuthYn;
}
