package com.hellodoctor.model.hospital;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 병원 진료과목
 */
@Data
@AllArgsConstructor // 모든 생성자 생성
@NoArgsConstructor
public class HospitalSubject {
    private int hospitalIdx;
    private String subjectCode;
}