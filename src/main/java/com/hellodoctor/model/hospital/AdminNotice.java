package com.hellodoctor.model.hospital;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 관리자 공지사항
 */
@Data
@AllArgsConstructor // 모든 생성자 생성
@NoArgsConstructor
@Builder
public class AdminNotice {
    private int idx;
    private int admIdx;
    private String delYn;
    private String readTarget;
    private int category;
    private String title;
    private String content;
    private int hit;
    private String regdate;
}
