<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<!--
DB 쿼리문을 관리하며 DB와 상호작용하는 파일

id : @Mapper에서 정의한 이름과 동일해야 함
parameterType: 파라미터 자료형
resultType: 리턴타입 자료형
-->
<mapper namespace="com.hellodoctor.mapper.app.RemoteMapper">
    <!--원격진료 INSERT-->
    <insert id="insertRemoteForm" parameterType="Medical" useGeneratedKeys="true" keyProperty="idx">
        INSERT INTO hd_medical SET
        mb_idx = #{mbIdx},
        medical_status = #{medicalStatus},
        hospital_rcpt_yn = #{hospitalRcptYn},
        patient_idx = #{patientIdx},
        subject_code = #{subjectCode},
        patient_rr_no = #{patientRrNo},
        patient_hp = #{patientHp},
        hope_hour = #{hopeHour},
        symptom = #{symptom},
        symptom_img_code = #{symptomImgCode},
        patient_height = #{patientHeight},
        patient_weight = #{patientWeight},
        receive_method = #{receiveMethod},
        mb_address_idx = #{mbAddressIdx},
        regdate = NOW(),
        delivery_msg = #{deliveryMsg}
    </insert>

    <!-- 원격진료신청 정보 search -->
    <select id="getRemoteForm" resultType="Medical">
        SELECT * FROM hd_medical
        WHERE
            idx = #{idx}
            <if test="status != ''">
            AND hospital_rcpt_yn = #{status}
            </if>
        ORDER BY idx DESC
    </select>

    <!--병원선택시 진료시간 변경-->
    <update id="changeRemoteFormHopeHour" parameterType="string">
        UPDATE hd_medical SET hope_hour = #{hour} WHERE idx = #{idx}
    </update>

    <!-- 병원선택시 전체목록 (진료가능병원) -->
    <select id="getAllHospitalList" resultType="map">
        SELECT
            (6371*acos(cos(radians(#{lat}))*cos(radians(A.lat))*cos(radians(A.lng)
            -radians(#{lng}))+sin(radians(#{lat}))*sin(radians(A.lat)))) AS distance,
            A.idx,
            A.hospital_name AS hospitalName,
            IFNULL((SELECT user_name FROM hd_doctor WHERE hospital_idx = A.idx AND origin_yn = 'Y' AND del_yn = 'N' ORDER BY idx ASC LIMIT 1),'') AS doctorName,
            A.main_addr AS mainAddr,
            A.detail_addr AS detailAddr,
            A.main_img AS mainImg,
            B.yoil,
            /* weekTimeTable : 해당요일 진료시간 */
            (SELECT time_table FROM hd_hospital_appointment
            WHERE hospital_idx = A.idx AND patient_type = 2 AND yoil_type = #{todayNum}
            ORDER BY idx DESC LIMIT 1) AS weekTimeTable,
            /* todayTimeTable : 특정일자(오늘) 진료시간 */
            (SELECT time_table FROM hd_hospital_appointment
            WHERE hospital_idx = A.idx AND patient_type = 2 AND yoil_type = #{today}
            ORDER BY idx DESC LIMIT 1) AS todayTimeTable,
            /* weekendCloseYn : 주말(6=토) 진료여부 */
            (SELECT close_day_yn FROM hd_hospital_hour
            WHERE hospital_idx = A.idx AND yoil = 6
            LIMIT 1) AS weekendCloseYn,
            /* 진료과목 */
            (SELECT GROUP_CONCAT(subject_code) FROM hd_hospital_subject where hospital_idx = A.idx) AS subjectCode
        FROM
            hd_hospital A INNER JOIN hd_hospital_hour B
            ON A.idx = B.hospital_idx AND B.yoil = #{todayNum} AND B.close_day_yn = 'N'  /* 진료요일 + 휴무일아님 */
        WHERE
            A.del_yn = 'N'
            AND A.nonface_svc_yn = 'Y' /* 비대면진료가능여부 */
            AND ( /* 진료과목-질환 매칭 */
                SELECT COUNT(*) FROM hd_hospital_subject
                WHERE hospital_idx = A.idx AND (
                <foreach collection="matchingCodeArr" item="code" separator=" OR ">
                    subject_code = #{code}
                </foreach>
                )
            ) > 0
        HAVING
            <![CDATA[
            distance < 100
            ]]>
            OR distance IS NULL
        ORDER BY distance ASC
    </select>

    <!-- 병원선택시 전체목록 (주치전담병원) -->
    <select id="getMyHospitalList" resultType="map">
        SELECT
            (6371*acos(cos(radians(#{lat}))*cos(radians(A.lat))*cos(radians(A.lng)
                -radians(#{lng}))+sin(radians(#{lat}))*sin(radians(A.lat)))) AS distance,
            A.idx,
            A.hospital_name AS hospitalName,
            IFNULL((SELECT user_name FROM hd_doctor WHERE hospital_idx = A.idx AND origin_yn = 'Y' AND del_yn = 'N' ORDER BY idx ASC LIMIT 1),'') AS doctorName,
            A.main_addr AS mainAddr,
            A.detail_addr AS detailAddr,
            A.main_img AS mainImg,
            /* weekTimeTable : 해당요일 진료시간 */
            (SELECT time_table FROM hd_hospital_appointment
             WHERE hospital_idx = A.idx AND patient_type = 2 AND yoil_type = #{todayNum}
             ORDER BY idx DESC LIMIT 1) AS weekTimeTable,
            /* todayTimeTable : 특정일자(오늘) 진료시간 */
            (SELECT time_table FROM hd_hospital_appointment
            WHERE hospital_idx = A.idx AND patient_type = 2 AND yoil_type = #{today}
                ORDER BY idx DESC LIMIT 1) AS todayTimeTable,
            /* weekendCloseYn : 주말(6=토) 진료여부 */
            (SELECT close_day_yn FROM hd_hospital_hour
            WHERE hospital_idx = A.idx AND yoil = 6
            LIMIT 1) AS weekendCloseYn,
            /* 진료과목 */
            (SELECT GROUP_CONCAT(subject_code) FROM hd_hospital_subject where hospital_idx = A.idx) AS subjectCode
        FROM hd_hospital A INNER JOIN hd_member B
        ON A.idx = B.my_hospital_idx AND B.idx = #{mbIdx}
        WHERE
            A.del_yn = 'N'
          AND A.designate_svc_yn = 'Y' /* 지정환자 비대면진료가능여부 */
            AND ( /* 진료과목-질환 매칭 */
            SELECT COUNT(*) FROM hd_hospital_subject
                WHERE hospital_idx = A.idx AND (
                <foreach collection="matchingCodeArr" item="code" separator=" OR ">
                subject_code = #{code}
                </foreach>
                )
            ) > 0
        HAVING
            <![CDATA[
            distance < 100
            ]]>
            OR distance IS NULL
        ORDER BY distance ASC
    </select>

    <!--병원선택 후 진료신청완료전 접수번호 순번조회-->
    <select id="searchReceiptOrder" resultType="MedicalReceiptOrder">
        SELECT number FROM hd_medical_receipt_order
        WHERE  rcpt_date = #{date}
        ORDER BY number DESC LIMIT 1
    </select>
    <!-- /*hospital_idx = #{hospitalIdx} AND*/ -->

    <!--접수번호 순번 INSERT-->
    <insert id="insertReceiptOrder" parameterType="MedicalReceiptOrder">
        INSERT INTO hd_medical_receipt_order SET
        hospital_idx = #{hospitalIdx},
        number = #{number},
        rcpt_date = #{rcptDate}
    </insert>

    <!--병원선택 후 진료신청완료-->
    <update id="updateRemoteForm" parameterType="String">
        UPDATE
            hd_medical
        SET
            rcpt_no = #{receiptNo},
            medical_status = 'R',
            regdate = now(),
            hospital_idx = #{hospitalIdx},
            app_start_hour = #{appStartHour},
            app_end_hour = #{appEndHour},
            mb_card_idx = #{cardIdx}
        WHERE idx = #{medicalIdx};
    </update>

    <!-- 진료신청완료 후 선택한 병원정보 (del_yn 여부는 view 에서 처리)-->
    <!--<select id="getRemoteStatusHospitalData" resultType="Hospital">
        SELECT *
        FROM hd_hospital
        WHERE
            idx = #{hospitalIdx} /*AND del_yn = 'N'*/
    </select>-->

    <!-- 원격진료 상세보기 > 진료신청완료 후 진료&병원 정보 -->
    <select id="getRemoteStatusData" resultType="map">
        SELECT
            (6371*acos(cos(radians(#{lat}))*cos(radians(B.lat))*cos(radians(B.lng)
            -radians(#{lng}))+sin(radians(#{lat}))*sin(radians(B.lat)))) AS distance,
            A.idx AS medicalIdx,
            A.mb_idx AS mbIdx,
            A.medical_status AS medicalStatus,
            A.hospital_rcpt_yn AS rcptYn,
            A.mb_address_idx AS mbAddressIdx,
            A.delivery_amt AS deliveryAmt,
            B.idx AS hospitalIdx,
            B.hospital_name AS hospitalName,
            B.main_addr AS hospitalAddr,
            B.introduce AS hospitalIntroduce,
            B.non_reimb_json AS nonReimbJson,   /* 비급여진료항목 */
            B.rest_start_hour AS restStartHour,
            B.rest_end_hour AS restEndHour,
            B.each_set_yn AS eachSetYn, /* 병원시간설정 (Y:요일별 개별설정, N:평일공통) */
            B.main_img AS mainImg,
            IFNULL((SELECT user_name FROM hd_doctor WHERE hospital_idx = A.hospital_idx AND origin_yn = 'Y' AND del_yn = 'N' ORDER BY idx ASC LIMIT 1),'') AS doctorName,/*의사명*/
            (SELECT GROUP_CONCAT(subject_code) FROM hd_hospital_subject where hospital_idx = A.hospital_idx) AS subjectCode
        FROM
            hd_medical A INNER JOIN hd_hospital B ON A.hospital_idx = B.idx
        WHERE
            A.idx = #{medicalIdx} AND A.medical_status IN ('R', 'Y')
    </select>

    <!-- 병원 진료시간 -->
    <select id="getHospitalHours" resultType="HospitalHour">
        SELECT *
        FROM hd_hospital_hour
        WHERE
            hospital_idx = #{idx} AND close_day_yn = 'N' AND start_hour != '' AND end_hour != ''
        ORDER BY yoil ASC;
    </select>

    <!-- 환자 진료신청취소 -->
    <update id="cancelRemoteForm" parameterType="int">
        UPDATE hd_medical
        SET
            medical_status = 'C',
            cancel_date = NOW(),
            cancel_reason = '환자 신청취소'
        WHERE idx = #{medicalIdx}
    </update>

    <!-- 환자 초진/재진 확인 (재진: 3개월 내 같은 질환으로 2회 이상 진료 예약한 환자) -->
    <select id="getHospitalVisitCount" resultType="int">
        SELECT COUNT(*) AS cnt
        FROM hd_medical
        WHERE
            medical_status NOT IN ('T', 'C') /*임시X, 취소X*/
            AND hospital_idx = #{hospitalIdx}
            AND (SELECT patient_idx FROM hd_medical WHERE idx = #{medicalIdx}) = patient_idx
            AND (SELECT subject_code FROM hd_medical WHERE idx = #{medicalIdx}) = subject_code
            AND DATE_FORMAT(regdate, '%Y-%m-%d') > DATE_SUB(#{rcptDate}, INTERVAL 3 MONTH) AND #{rcptDate} >= DATE_FORMAT(regdate, '%Y-%m-%d');
    </select>

    <!-- 원격진료 리뷰작성 -->
    <insert id="reviewWriteAction" parameterType="MedicalReview" useGeneratedKeys="true" keyProperty="idx">
        INSERT INTO hd_medical_review
        SET
        mb_idx = #{mbIdx}, medical_idx = #{medicalIdx}, review_score = #{reviewScore}, symptom = #{symptom}, content = #{content}, only_doctor_yn = #{onlyDoctorYn}, check_item = #{checkItem}, regdate = NOW()
    </insert>

    <!-- 원격진료 '진료접수' 목록 -->
    <select id="getRemoteListAppointment" resultType="map">
        SELECT
            IFNULL(B.user_name,'') AS doctorName,
            IFNULL(B.profile_img,'') AS doctorImg,
            (SELECT hospital_name FROM hd_hospital WHERE idx = A.hospital_idx) AS hospitalName,
            /* subjectCode : 진료과목 */
            (SELECT group_concat(subject_code) FROM hd_hospital_subject WHERE hospital_idx = A.hospital_idx) AS hospitalSubjectCode,
            A.idx,
            A.medical_status AS medicalStatus,
            A.hospital_rcpt_yn AS hospitalRcptYn,
            A.regdate AS regdate,
            A.subject_code AS medicalSubjectCode,
            A.app_start_hour AS appStartHour,
            A.app_end_hour AS appEndHour
        FROM
            hd_medical A
            LEFT JOIN hd_doctor B ON A.hospital_idx = B.hospital_idx AND B.origin_yn = 'Y' AND B.del_yn = 'N'
            LEFT JOIN hd_medical_opinion C ON A.idx = C.medical_idx
        WHERE
            A.mb_idx = #{mbIdx} AND A.medical_status = 'R' AND A.hospital_rcpt_yn != 'N' /* 병원거부아닌것 */
            AND C.idx IS NULL /* 진료소견 등록되지 않은 것 */
            <if test="todayDate != '' and beforeDate != ''">
            AND A.regdate BETWEEN #{beforeDate} AND #{todayDate}
            </if>
        ORDER BY idx DESC LIMIT #{startRecord}, #{endRecord};
    </select>

    <!-- 원격진료 '진료진행중' 목록 -->
    <select id="getRemoteListIng" resultType="map">
        SELECT
        IFNULL(B.user_name,'') AS doctorName,
        IFNULL(B.profile_img,'') AS doctorImg,
        (SELECT hospital_name FROM hd_hospital WHERE idx = A.hospital_idx) AS hospitalName,
        /* subjectCode : 진료과목 */
        (SELECT group_concat(subject_code) FROM hd_hospital_subject WHERE hospital_idx = A.hospital_idx) AS hospitalSubjectCode,
        A.idx,
        A.medical_status AS medicalStatus,
        A.hospital_rcpt_yn AS hospitalRcptYn,
        A.regdate AS regdate,
        A.subject_code AS medicalSubjectCode,
        A.app_start_hour AS appStartHour,
        A.app_end_hour AS appEndHour,
        A.hospital_idx AS hospitalIdx,
        A.pres_idx AS presIdx, /* 처방전 인덱스 */
        A.pharmacy_req_idx AS pharmacyReqIdx, /* 약조제요청 인덱스 */
        C.idx AS opinionIdx
        FROM
        hd_medical A
        LEFT JOIN hd_doctor B ON A.hospital_idx = B.hospital_idx AND B.origin_yn = 'Y' AND B.del_yn = 'N'
        LEFT JOIN hd_medical_opinion C ON A.idx = C.medical_idx
        WHERE
        A.mb_idx = #{mbIdx} AND A.medical_status = 'R' AND A.hospital_rcpt_yn = 'Y' /* 병원접수인것 */
        AND C.idx IS NOT NULL /* 진료소견 등록된 것 */
        /* 결제안함 */
        AND (SELECT COUNT(*) FROM hd_payment WHERE pay_cancel = 'N' AND pay_type = 'P' AND medical_idx = A.idx AND result_code = '0000') = 0
        <if test="todayDate != '' and beforeDate != ''">
        AND A.regdate BETWEEN #{beforeDate} AND #{todayDate}
        </if>
        ORDER BY idx DESC LIMIT #{startRecord}, #{endRecord};
    </select>

    <!-- 원격진료 '진료진행중' 목록 > 약국연계리스트 -->
    <select id="getPharmacySelectList" resultType="Pharmacy">
        select
            B.idx,
            B.pharmacy_name,
            B.main_addr,
            B.detail_addr
        from
            hd_designate_pharmacy A INNER JOIN hd_pharmacy B ON A.pharmacy_idx = B.idx
        where
            A.hospital_idx = #{hospitalIdx}
        order by A.idx asc;
    </select>

    <!-- 원격진료 '진료진행중' 목록 > 약국선택완료 > 약조제요청 등록 -->
    <insert id="insertPharmacyRequest" parameterType="PharmacyRequest" useGeneratedKeys="true" keyProperty="idx">
        INSERT INTO hd_pharmacy_request
        SET
            pres_idx = #{presIdx},
            medical_idx = #{medicalIdx},
            pharmacy_idx = #{pharmacyIdx},
            req_status = #{reqStatus},
            regdate = now()
    </insert>

    <!-- 원격진료 '진료진행중' 목록 > 약조제요청 등록 후 진료db 업데이트 -->
    <update id="updatePharmacyRequestIndex" parameterType="int">
        UPDATE hd_medical SET pharmacy_req_idx = #{requestIdx} WHERE idx = #{medicalIdx}
    </update>

    <!-- 원격진료 상세보기 > 조제중이면 약국,조제비 정보 조회 -->
    <select id="getRemoteStatusIngData" resultType="map">
        SELECT
            A.bill_amt AS billAmt, /* 조제비 청구금액 */
            A.req_status AS reqStatus,
            B.pharmacy_name AS pharmacyName,
            B.tel_no AS pharmacyTel,
            B.idx AS pharmacyIdx,
            (SELECT CONCAT(main_addr, " ", detail_addr) FROM hd_member_address WHERE idx = #{mbAddrIdx}) AS mbAddr /* 주소 */
        FROM
            hd_pharmacy_request A
            INNER JOIN hd_pharmacy B ON A.pharmacy_idx = B.idx
        WHERE A.idx = #{reqIdx} AND A.medical_idx = #{medicalIdx}
    </select>

    <!-- 원격진료 '진료취소' 목록 -->
    <select id="getRemoteListCancel" resultType="map">
        SELECT
        IFNULL(B.user_name,'') AS doctorName,
        IFNULL(B.profile_img,'') AS doctorImg,
        (SELECT hospital_name FROM hd_hospital WHERE idx = A.hospital_idx) AS hospitalName,
        /* subjectCode : 진료과목 */
        (SELECT group_concat(subject_code) FROM hd_hospital_subject WHERE hospital_idx = A.hospital_idx) AS hospitalSubjectCode,
        A.idx,
        A.medical_status AS medicalStatus,
        A.hospital_rcpt_yn AS hospitalRcptYn,
        A.regdate AS regdate,
        A.subject_code AS medicalSubjectCode,
        A.app_start_hour AS appStartHour,
        A.app_end_hour AS appEndHour
        FROM
        hd_medical A
        LEFT JOIN hd_doctor B ON A.hospital_idx = B.hospital_idx AND B.origin_yn = 'Y' AND B.del_yn = 'N'
        LEFT JOIN hd_medical_opinion C ON A.idx = C.medical_idx
        WHERE
        A.mb_idx = #{mbIdx} AND A.medical_status = 'C' /* 취소/거부된 것 */
        /*AND C.idx IS NULL*/ /* 진료소견 등록되지 않은 것 */
        <if test="todayDate != '' and beforeDate != ''">
        AND A.regdate BETWEEN #{beforeDate} AND #{todayDate}
        </if>
        ORDER BY idx DESC LIMIT #{startRecord}, #{endRecord};
    </select>

    <!-- 원격진료 '진료완료' 목록 -->
    <select id="getRemoteListComplete" resultType="map">
        SELECT
            /*IFNULL(B.user_name,'') AS doctorName,
            IFNULL(B.profile_img,'') AS doctorImg,*/
            IFNULL((SELECT user_name FROM hd_doctor WHERE hospital_idx = A.hospital_idx AND origin_yn = 'Y' AND del_yn = 'N' ORDER BY idx ASC LIMIT 1),'') AS doctorName,
            (SELECT hospital_name FROM hd_hospital WHERE idx = A.hospital_idx) AS hospitalName,
            /* subjectCode : 진료과목 */
            (SELECT group_concat(subject_code) FROM hd_hospital_subject WHERE hospital_idx = A.hospital_idx) AS hospitalSubjectCode,
            A.idx,
            A.medical_status AS medicalStatus,
            A.hospital_rcpt_yn AS hospitalRcptYn,
            A.regdate AS regdate,
            A.subject_code AS medicalSubjectCode,
            A.app_start_hour AS appStartHour,
            A.app_end_hour AS appEndHour,
            A.hospital_idx AS hospitalIdx,
            A.pres_idx AS presIdx, /* 처방전 인덱스 */
            A.pharmacy_req_idx AS pharmacyReqIdx, /* 약조제요청 인덱스 */
            C.idx AS opinionIdx,

            /* 진행완료 필드 추가 */
            A.total_bill_amt AS totalBillAmt,
            A.patient_hp AS patientHp,
            A.patient_idx AS patientIdx,
            A.delivery_amt AS deliveryAmt,
            A.mb_address_idx AS mbAddressIdx,
            C.symptom AS opSymptom,
            C.current_history AS opCurrentHistory,
            C.old_history_check AS opOldHistoryCheck,
            C.old_history_input AS opOldHistoryInput,
            C.fam_history AS opFamHistory,
            C.diagnosis AS opDiagnosis,
            C.plan opPlan,
            (SELECT COUNT(*) FROM hd_medical_review WHERE medical_idx = A.idx) AS reviewCnt,
            D.fam_rel AS familyRel,
            D.fam_name AS familyName
        FROM
            hd_medical A
            LEFT JOIN hd_medical_opinion C ON A.idx = C.medical_idx
            LEFT JOIN hd_member_family D ON A.mb_idx = D.mb_idx AND D.idx = A.patient_idx
        WHERE
            A.mb_idx = #{mbIdx} AND A.medical_status IN ('R','Y') AND A.hospital_rcpt_yn = 'Y' /* 병원접수인것 */
            AND C.idx IS NOT NULL /* 진료소견 등록된 것 */
            /* 조제비 결제함 */
            AND (SELECT COUNT(*) FROM hd_payment WHERE pay_cancel = 'N' AND pay_type = 'P' AND medical_idx = A.idx AND result_code = '0000') > 0
            <if test="todayDate != '' and beforeDate != ''">
            AND A.regdate BETWEEN #{beforeDate} AND #{todayDate}
            </if>
        ORDER BY idx DESC LIMIT #{startRecord}, #{endRecord};
    </select>

    <!-- 원격진료 '진료완료' 목록 > 상세보기 (결제정보, 조제비, 배송) -->
    <select id="getRemoteCompleteDetail" resultType="map">
        SELECT
            A.bill_amt AS phmBillAmt,       /* 조제비 */
            B.pharmacy_name AS phmName,     /* 약국명 */
            B.tel_no AS phmTelNo,		    /* 약국연락처 */
            (SELECT CONCAT(main_addr, " ", detail_addr) FROM hd_member_address WHERE idx = #{mbAddrIdx}) AS mbAddr, /* 주소 */
            /* 조제비 결제정보 */
            C.auth_date AS phmAuthDate,
            C.card_quota AS phmCardQuota,
            C.acqu_card_name AS phmAcquCardName,
            /* 진료비 결제정보 */
            D.auth_date AS hsAuthDate,
            D.card_quota AS hsCardQuota,
            D.acqu_card_name AS hsAcquCardName
        FROM
            hd_pharmacy_request A
            INNER JOIN hd_pharmacy B ON A.pharmacy_idx = B.idx
            LEFT JOIN hd_payment C ON A.medical_idx = C.medical_idx AND C.pay_cancel = 'N' AND C.pay_type = 'P' AND C.result_code = '0000'
            LEFT JOIN hd_payment D ON A.medical_idx = D.medical_idx AND D.pay_cancel = 'N' AND D.pay_type = 'H' AND D.result_code = '0000'
        WHERE
            A.idx = #{pharmacyReqIdx};
    </select>

    <!-- 원격진료 처방전보기 -->
    <select id="getPrescriptionImage" resultType="String">
        SELECT B.pres_img AS presImage
        FROM hd_medical A INNER JOIN hd_medical_prescription B ON A.idx = B.medical_idx
        WHERE A.idx = #{idx}
    </select>

    <!-- 리뷰보기 -->
    <select id="getMedicalReview" resultType="MedicalReview">
        SELECT * FROM hd_medical_review
        WHERE medical_idx = #{idx}
    </select>

</mapper>