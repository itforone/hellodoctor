/**
 * 약사 - 약 조제관리/약 조제내역 공통 js
 */

let nowUrl = "";
var requestIdx;
$(document).ready(function() {
    //<!--/* 현재메뉴 */-->
    let href = document.location.href;
    if(href.indexOf("fillRequest") != -1) nowUrl = "fillRequest"; //<!--/* 조제관리-조제요청 */-->
    else if(href.indexOf("fillReceive") != -1) nowUrl = "fillReceive"; //<!--/* 조제관리-조제접수 */-->
    else if(href.indexOf("fillList") != -1) nowUrl = "fillList"; //<!--/* 조제내역 */-->

    //<!--/* 목록 */-->
    getFillList(1);

    //<!--/* 조제 거부/취소 select */-->
    $("#cancelCode").change(function() {
        if(this.value == 1 || this.value == 2) {
            $("#cancelReason").val($("#cancelCode option:checked").text());
        } else {
            $("#cancelReason").val("");
        }
    });
});

//<!--/* 페이징 클릭시 ajax 호출 */-->
$(document).on("click", ".paging a", function(e) {
    e.preventDefault();
    let href = this.getAttribute("href");
    let page = href.split("page=")[1];

    $(".paging a").removeClass("active");
    $(this).addClass("active");

    getFillList(page);
});

//<!--/* 목록노출개수 */-->
var limit = 15; //<!--/* 디폴트 15 */-->
function limitFilter(filter) {
    limit = filter.value;
    document.querySelector("[name=limit]").value = filter.value;

    getFillList(1);
}

//<!--/* 약 조제 요청 취소 모달 */-->
function denyPop(idx) {
    requestIdx = idx;
    $("#deny_pop").modal("show");
}

//<!--/* 상태 - 취소/거부 처리 */-->
function statusReject() {
    var cancelReason = $("#cancelReason").val();
    if(cancelReason.trim().length == 0) {
        swal.fire({html: "취소 사유를 입력하세요."});
        return false;
    }

    var msg = "조제 요청이 취소되었습니다.";
    if(nowUrl == "fillReceive") msg = "조제 접수가 취소되었습니다.";

    $.ajax({
        url: "/pharmacy/statusReject",
        type: "POST",
        data: {idx: requestIdx, cancelReason: cancelReason},
        async: false,
        success: function(result) {
            if(result) {
                $("#deny_pop").modal("hide");
                swal.fire({
                    html: msg
                }).then(()=>{
                    getFillList(document.frmSearch.page.value);
                });
            }
        },
        error: function(result) {
            console.log(result);
        },
    });
}

//<!--/* 취소 사유 모달 */-->
function reasonPop(reason) {
    $("#reason_pop textarea").val(reason);
    $("#reason_pop").modal("show");
}

//<!--/* 처방전 정보 모달 */-->
function prescriptionPop(medicalIdx, presIdx) {
    if(presIdx == 0) {
        swal.fire({html: "처방전 정보가 없습니다."});
        return false;
    }

    $.ajax({
        url: "/pharmacy/prescriptionInfo",
        type: "POST",
        data: {medicalIdx: medicalIdx, presIdx: presIdx},
        async: false,
        success: function(result) {
            document.querySelector("#prescriptionInfo").innerHTML = result;
            $("#px_zoom_pop").modal("show");
        },
        error : function(result) {
            console.log(result);
        },
        beforeSend: function() {
            callPageLoading(1);
        },
        complete: function() {
            callPageLoading(0);
        }
    });
}

//<!--/* 배송지 정보 모달 */-->
function addrPop(medicalIdx) {
    $.ajax({
        url: "/pharmacy/addrInfo",
        type: "POST",
        data: {medicalIdx: medicalIdx},
        async: false,
        success: function(result) {
            document.querySelector("#addrInfo").innerHTML = result;
            $("#addr_pop").modal("show");
        },
        error : function(result) {
            console.log(result);
        },
        beforeSend: function() {
            callPageLoading(1);
        },
        complete: function() {
            callPageLoading(0);
        }
    });
}

//<!--/* 수령방식 변경 */-->
function receiveMethodChange(idx, medicalIdx) {
    var receiveMethod = document.querySelector(`#receiveMethod_${idx}`).value;
    $.ajax({
        url: "/pharmacy/receiveMethodChange",
        type: "POST",
        data: {medicalIdx: medicalIdx, receiveMethod: receiveMethod},
        async: false,
        success: function(result) {
            if(result) getFillList(document.frmSearch.page.value);
        },
        error: function(result) {
            console.log(result);
        },
    });
}

//<!--/* 복약지도 모달 */-->
function mediGuidePop(medicalIdx, mode) {
    $.ajax({
        url: "/pharmacy/mediGuideInfo",
        type: "POST",
        data: {medicalIdx: medicalIdx},
        async: false,
        success: function(result) {
            document.querySelector("#mediGuideInfo").innerHTML = result;
            if(mode == "comp") { //<!--/* 복약지도 등록 완료 */-->
                //<!--/* 추가/저장 버튼 숨김 */-->
                $(".compHide").attr("style", "display:none !important;");
                $(".validate").attr("readonly", true);
            }
            $("#medi_guide_pop").modal("show");
        },
        error : function(result) {
            console.log(result);
        },
        beforeSend: function() {
            callPageLoading(1);
        },
        complete: function() {
            callPageLoading(0);
        }
    });
}

//<!--/* 목록 */-->
function getFillList(page) {
    var f = document.frmSearch;
    if(page != undefined) f.page.value = page;

    let url = "";
    var msg = "";
    var schFlag = true;
    if(nowUrl == "fillList") { //<!--/* 조재내역 */-->
        msg = document.querySelector("#schDate").value;
        url = "/pharmacy/getFillList";
        if(f.schDateSt.value == "" && f.schDateEd.value == "") schFlag = false;        //<!--/* 요청일 or 완료일 */-->
    }
    else {
        if(nowUrl == "fillRequest") { //<!--/* 조제요청 */-->
            url = "/pharmacy/getFillRequest";
            msg = "요청일";
        }
        else if(nowUrl == "fillReceive") { //<!--/* 조제접수 */-->
            url = "/pharmacy/getFillReceive"
            msg = "접수일";
        }
        if(f.schDate.value == "") schFlag = false;
    }

    //<!--/* 날짜 검색 */-->
    if(!schFlag) {
        swal.fire({html: msg+"을 선택하세요."});
        return false;
    }

    //<!--/* 검색조건 */-->
    if (f.schKeyword.value.trim() != "" && f.schKeyword.value.trim().length < 2) {
        swal.fire({
            html:'검색어를 2자 이상 입력해 주세요.',
            confirmButtonText : '확인',
            didDestroy: function () {
                f.stx.focus();
            }
        });
        return false;
    }

    var formData = new FormData(f);
    $.ajax({
        url: url,
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        async: false,
        success: function(result) {
            document.querySelector(".fillList").innerHTML = result;
            document.querySelector("#limitFilter").value = limit; // 목록 갯수 필터
        },
        error : function(result) {
            console.log(result);
        },
        beforeSend: function() {
            callPageLoading(1);
        },
        complete: function() {
            setTimeout(function() {
                callPageLoading(0);
            }, 500);
        }
    });

    return false;
}