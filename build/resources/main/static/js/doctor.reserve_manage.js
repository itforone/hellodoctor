let weekDayArr = [1, 2, 3, 4, 5]; //<!--/* 월-금 */-->
//<!--/* db */-->
var hourYoil = [];
var hourStartHour = [];
var hourEndHour = [];
var timeYoil1 = [];
var timeTimeTable1 = [];
var timeYoil2 = [];
var timeTimeTable2 = [];
$(document).ready(function() {
    /** 영업시간 */
    //<!--/* 디폴트 월-금 선택 */-->
    $("[name=weekday_check]").each(function() {
        var num = Number(this.value);
        if(weekDayArr.includes(num)) this.checked = true;
    });

    //<!--/* db 데이터 */-->
    var dbHours = document.querySelectorAll("#dbHour span");
    dbHours.forEach((hour)=>{
        if(hour.id == "yoil") hourYoil.push(hour.innerText);
        if(hour.id == "startHour") hourStartHour.push(hour.innerText);
        if(hour.id == "endHour") hourEndHour.push(hour.innerText);
    });

    var dbTimes1 = document.querySelectorAll("#dbTime1 span");
    dbTimes1.forEach((time)=>{
        if(time.id == "yoilType1") timeYoil1.push(time.innerText);
        if(time.id == "timeTable1") timeTimeTable1.push(time.innerText);
    });

    var dbTimes2 = document.querySelectorAll("#dbTime2 span");
    dbTimes2.forEach((time)=>{
        if(time.id == "yoilType2") timeYoil2.push(time.innerText);
        if(time.id == "timeTable2") timeTimeTable2.push(time.innerText);
    });

    if(hourYoil.length != 0) {
        //<!--/* 초기화 */-->
        $(".sod_select").addClass("disabled"); //<!--/* 영업시간 select */-->
        $("[name=weekday_check]").prop("checked", false); //<!--/* 영업시간 요일 checkbox *-->
        document.querySelector(".week_0").style.setProperty("display", "none", "important"); //<!--/* 영업시간 평일 */-->

        for(var i=0; i<=8; i++) { //<!--/* 평일~일요일 */-->
            if(hourYoil.includes(i.toString())) { //<!--/* 데이터가 있으면 */-->
                document.querySelector(`#weekday_${i}`).checked = true;
                if(i==0) eachSetYnChk();

                //<!--/* select element */-->
                var $start_h = document.querySelector(`#start_h_${i}`);
                var $start_m = document.querySelector(`#start_m_${i}`);
                var $end_h = document.querySelector(`#end_h_${i}`);
                var $end_m = document.querySelector(`#end_m_${i}`);
                var key = getKeyByValue(hourYoil, i.toString()); //<!--/* 배열 키 */-->
                var value1 = hourStartHour[key]; //<!--/* 시작시간 */-->
                var value2 = hourEndHour[key]; //<!--/* 종료시간 */-->

                if(i == 8) {
                    //<!--/* 휴식 */-->
                    value1 = "[[${data.restStartHour}]]";
                    value2 = "[[${data.restEndHour}]]";
                }

                //<!--/* select data */-->
                if(value1 != undefined && value2 != undefined) {
                    $start_h.value = value1.slice(0,-2);
                    $start_m.value = value1.substring(value1.length-2);
                    $end_h.value = value2.slice(0,-2);
                    $end_m.value = value2.substring(value2.length-2);
                }
            }
        }
        weekdayCheck();
    }

    //<!--/* 요일 선택에 따른 요소 활성화/비활성화 */-->
    $("[name=weekday_check]").click(function() {
        if(this.id == "weekday_0") eachSetYnChk(); //<!--/* 평일 전체 설정 (선택 시 월-금 숨김) */-->
        weekdayCheck(this);
    });

    //<!--/* 병원영업시간 select change */-->
    $(".left select").change(function() {
        var num = Number(this.id.split("_")[2]);
        timeTableCheck(num);
    });

    /** 예약시간 */
    getTimeTable(); //<!--/* 진료예약시간 */-->
});

//<!--/* 평일 전체 설정 (선택 시 월-금 숨김) */-->
function eachSetYnChk() {
    var chk = document.querySelector("#weekday_0").checked;
    var el_eachSetYn = document.querySelector("[name=eachSetYn]");
    if (chk) {
        el_eachSetYn.value = "N";
        $(".week_0").show();
        $(".liWeekDay").hide();
        // $(".liWeekDay input").prop("checked", false);
    } else {
        el_eachSetYn.value = "Y";
        $(".week_0").hide();
        $(".liWeekDay").show();
    }
}

//<!--/* 요일 선택에 따른 요소 활성화/비활성화 */-->
let checkYoil = []; //<!--/* 선택요일 */-->
// function weekdayCheck() {
//     callPageLoading(0);
//     checkYoil = [];
//     var chkFlag = false;
//     var weekdays = document.querySelectorAll("[name=weekday_check]");
//     weekdays.forEach((weekday) => {
//         var num = Number(weekday.value);
//         if(num != 8) { //<!--/* 휴식 제외 */-->
//             var hourSlider = $(`#slider_${num}`); //<!--/* 병원영업시간 slider */-->
//             var checkboxTime = $(`.timeTab_${num} input`); //<!--/* 예약진료시간 checkbox */-->
//             if(weekday.checked) {
//                 checkYoil.push(num);
//                 chkFlag = true;
//                 hourSlider.attr("disabled", false);
//                 timeTableCheck(num); //<!--/* 진료예약시간 선택 */-->
//             } else {
//                 hourSlider.attr("disabled", true);
//                 if(num == 6 || num == 7) {
//                     checkboxTime.attr("disabled", true);
//                     checkboxTime.prop("checked", false); //<!--/* 체크박스 초기화 */-->
//                 }
//             }
//         }
//     });
//     checkYoil.push(8); //<!--/* 휴식은 필수 포함(?) */-->
//
//     //<!--/* 선택한 병원영업시간이 없을 때 (휴식 제외) */-->
//     if(!chkFlag) {
//         //<!--/* 예약진료시간 전체 초기화 */-->
//         $(".tab-content input").attr("disabled", true);
//         $(".tab-content input").prop("checked", false);
//     }
// }
function weekdayCheck(elem) {
    var num, selectHour, hourSlider;
    if(elem != undefined) {
        num = Number(elem.value);
        selectHour = $(`.week_${num} .sod_select`); //<!--/* 병원영업시간 select */-->
        hourSlider = $(`#slider_${num}`); //<!--/* 병원영업시간 slider */-->
        var checkboxTime = $(`.timeTab_${num} input`); //<!--/* 예약진료시간 checkbox */-->
        if(elem.checked) {
            selectHour.removeClass("disabled");
            hourSlider.attr("disabled", false);
            timeTableCheck(num); //<!--/* 진료예약시간 선택 */-->
        } else {
            selectHour.addClass("disabled");
            hourSlider.attr("disabled", true);
            if(num == 6 || num == 7) {
                checkboxTime.attr("disabled", true);
                checkboxTime.prop("checked", false); //<!--/* 체크박스 초기화 */-->
            }
        }
    }
    else {
        for(var i=0; i<hourYoil.length; i++) {
            num = hourYoil[i];
            selectHour = $(`.week_${num} .sod_select`); //<!--/* 병원영업시간 select */-->
            hourSlider = $(`#slider_${num}`); //<!--/* 병원영업시간 slider */-->
            selectHour.removeClass("disabled");
            hourSlider.attr("disabled", false);
            timeTableCheck(num); //<!--/* 진료예약시간 선택 */-->
        }
    }

    checkYoil = [];
    var chkFlag = false;
    var weekdays = document.querySelectorAll("[name=weekday_check]");
    weekdays.forEach((weekday) => {
        var num = Number(weekday.value);
        if(weekday.checked) {
            checkYoil.push(num);
            chkFlag = true;
        }
    });
    checkYoil.push(8); //<!--/* 휴식은 필수 포함(?) */-->

    //<!--/* 선택한 병원영업시간이 없을 때 (휴식 제외) */-->
    if(!chkFlag) {
        //<!--/* 예약진료시간 전체 초기화 */-->
        $(".tab-content input").attr("disabled", true);
        $(".tab-content input").prop("checked", false);
    }
    //<!--/* 평일/월-금 아무것도 선택안했을 때 */-->
    if(!checkYoil.includes(0) && !checkYoil.includes(1) && !checkYoil.includes(2) && !checkYoil.includes(3) && !checkYoil.includes(4) && !checkYoil.includes(5)) {
        $(".timeTab_0 input").attr("disabled", true);
        $(".timeTab_0 input").prop("checked", false);
    }
}

//<!--/* 병원영업시간 데이터 */-->
function hospitalHourChk() {
    let yoil = [];
    let startHour = [];
    let endHour = [];

    //<!--/* 0: 평일 1-5: 월-금 6: 토요일 7: 일요일 8: 휴식 */-->
    for(var i=0; i<checkYoil.length; i++) {
        if(document.querySelector("[name=eachSetYn]").value == "N") {
            if(weekDayArr.includes(Number(i))) continue;
        }
        var num = checkYoil[i];
        var start = returnTime(num, 'start');
        var end = returnTime(num, 'end');

        if(num == 8) {
            document.querySelector("[name=restStartHour]").value = start;
            document.querySelector("[name=restEndHour]").value = end;
        } else {
            yoil.push(num);
            startHour.push(start);
            endHour.push(end);
        }
    }
    document.querySelector("[name=yoil]").value = yoil;
    document.querySelector("[name=startHour]").value = startHour;
    document.querySelector("[name=endHour]").value = endHour;
}

//<!--/* 진료예약시간 */-->
function getTimeTable() {
    callPageLoading(1);

    setTimeout(function() {
        $.ajax({
            url: "/hospital/ajaxReserveManage",
            type: "POST",
            async: false,
            success: function(data) {
                $("#office_hours").append(data); //<!--/* 진료예약시간 */-->

                //<!--/* 일반 환자 시간 or 지정 환자 시간 선택 */-->
                $("[name=time_check]").click(function() {
                    if(this.value == 1) { //<!--/* 지정환자 */-->
                        $(".time02").show();
                        $(".time01").hide();
                    } else {
                        $(".time02").hide();
                        $(".time01").show();
                    }
                });

                //<!--/* 평일/주말/일요일/공휴일 시간 선택 */-->
                $(".checkTime").click(function() {
                    var target = this.name.split("Time")[0];
                    timeTableData(target);
                });

                var tmp = [1, 6, 7];
                for(var i=0; i<tmp.length; i++) {
                    timeTableCheck(tmp[i]); //<!--/* 진료예약시간 선택 */-->
                }
            },
            complete: function() {
                callPageLoading(0);
            }
        })
    }, 300);
}

//<!--/* 다중 range 라이브러리  */-->
let sliderList = document.querySelectorAll(".slider");
sliderList.forEach((slider) => {
    var num = Number(slider.id.split("_")[1]); //<!--/* 요일 구분값 */-->
    var rangeStart = 540; //<!--/* 첫번째 range 초기값 08:30 */-->
    var rangeEnd = 1080; //<!--/* 두번째 range 초기값 18:00 */-->

    if(num == 6 || num == 7) { //<!--/* 토 */-->
        rangeStart = 540; //<!--/* 08:30 */-->
        rangeEnd = 780; //<!--/* 13:00 */-->
        slider.setAttribute("disabled", true); //<!--/* 비활성화 */-->
    }
    else if(num == 8) { //<!--/* 휴식 */-->
        rangeStart = 750; //<!--/* 12:30 */-->
        rangeEnd = 840; //<!--/* 14:00 */-->
    }

    noUiSlider.create(slider, {
        start: [rangeStart, rangeEnd],
        step: 15, //<!--/* 15분 단위로 움직임 */-->
        connect: true, //<!--/* css 연결하는듯함, 없으면 color 없어짐 */-->
        range: {
            'min': 0, //<!--/* 최소 00:00 */-->
            'max': 1380 //<!--/* 최대 18:00 */-->
        }
    });

    //<!--/* 변경된 값에 대한 정보 가져옴 values: 값 / handle: 다중 선택 구분자 (index) */-->
    slider.noUiSlider.on('update', async (values, handle) => {
        //<!--/* 시작시간 */-->
        var hours1 = Math.floor(values[0] / 60);
        var minutes1 = values[0] - (hours1 * 60);
        if (minutes1 === 0) minutes1 = '00';
        //<!--/* 종료시간 */-->
        var hours2 = Math.floor(values[1] / 60);
        var minutes2 = values[1] - (hours2 * 60);
        if (minutes2 === 0) minutes2 = '00';

        if(handle === 0) {
            document.querySelector(`[name=start_h_${num}]`).value = hours1;
            document.querySelector(`[name=start_m_${num}]`).value = minutes1
        }
        else { //<!--/* handle === 1 */-->
            document.querySelector(`[name=end_h_${num}]`).value = hours2;
            document.querySelector(`[name=end_m_${num}]`).value = minutes2;
        }
    });

    slider.noUiSlider.on('end', async (values, handle) => {
        timeTableCheck(num);
    });
});

//<!--/* 병원 영업 시간에 따른 예약 진료 시간 자동 선택 */-->
function timeTableCheck(num, spec) { //<!--/* spec: 특정일 선택 */-->
    var element, element2, target, target2, timeKey, timeKey2;
    if(spec == "Y") { //<!--/* 특정일 */-->
        element = $("[name=specdayTime]");
        element2 = $("[name=d_specdayTime]");
        target = "specday"
        target2 = "d_specday"
    }
    else {
        if(weekDayArr.includes(num) || num == 0) { //<!--/* 평일/월-금 */-->
            element = $("[name=weekdayTime]");
            element2 = $("[name=d_weekdayTime]");
            target = "weekday";
            target2 = "d_weekday";
            timeKey = getKeyByValue(timeYoil1, '1'); //<!--/* 수정 시 사용 (저장된 데이터 표시) */-->
            timeKey2 = getKeyByValue(timeYoil2, '1'); //<!--/* 수정 시 사용 (저장된 데이터 표시) */-->
        }
        else if(num == 6) { //<!--/* 토 */-->
            element = $("[name=weekendTime]");
            element2 = $("[name=d_weekendTime]");
            target = "weekend";
            target2 = "d_weekend";
            timeKey = getKeyByValue(timeYoil1, num.toString()); //<!--/* 수정 시 사용 (저장된 데이터 표시) */-->
            timeKey2 = getKeyByValue(timeYoil2, num.toString()); //<!--/* 수정 시 사용 (저장된 데이터 표시) */-->
        }
        else if(num == 7) { //<!--/* 일 */-->
            element = $("[name=holidayTime]");
            element2 = $("[name=d_holidayTime]");
            target = "holiday";
            target2 = "d_holiday";
            timeKey = getKeyByValue(timeYoil1, num.toString()); //<!--/* 수정 시 사용 (저장된 데이터 표시) */-->
            timeKey2 = getKeyByValue(timeYoil2, num.toString()); //<!--/* 수정 시 사용 (저장된 데이터 표시) */-->
        }
    }

    if(element != undefined) timeShow(num, element, target, timeKey, 'g'); //<!--/* 일반환자 */-->
    if(element2 != undefined) timeShow(num, element2, target2, timeKey2, 'd'); //<!--/* 지정환자 */-->
}

 //<!--/* 예약 시간 표시 */-->
function timeShow(num, element, target, timeKey, gubun) {
    var dbTime = [];
    if(gubun == "g") {
        //<!--/* 수정 시 사용 (저장된 데이터 표시) */-->
        if(timeTimeTable1[timeKey] != undefined) dbTime = timeTimeTable1[timeKey].split(",");
    } else {
        if(timeTimeTable2[timeKey] != undefined) dbTime = timeTimeTable2[timeKey].split(",");
    }

    var start = returnTime(num, 'start');
    var end = returnTime(num, 'end');

    element.parent("div").addClass("timeHide"); //<!--/* 진료시간 숨김 */-->
    element.prop("checked", false);
    element.attr("disabled", true);
    element.each(function() {
        var timeValue = Number(this.value);
        if(timeValue >= start && timeValue <= end) {
            $(this).attr("disabled", false);
            $(this).parent("div").removeClass("timeHide"); //<!--/* 영업시간 범위 내의 진료시간 표시 */-->
            if(document.querySelector(`#weekday_${num}`).checked) { //<!--/* 요일이 선택되어 있으면 체크 */-->
                if(dbTime.length != 0) { //<!--/* 수정 시 사용 (저장된 데이터 표시) */-->
                    if (dbTime.includes(this.value)) {
                        this.checked = true;
                    } else {
                        this.checked = false;
                    }
                }
            }
        }
    });

    timeTableData(target);
    $("select").selectOrDie("update"); //<!--/* selecrOrDie 플러그인 */-->
}

//<!--/* 영업시간 시작일/종료일 리턴 */-->
function returnTime(num, gubun) {
    var start_h = document.querySelector(`#start_h_${num}`).value;
    var start_m = document.querySelector(`#start_m_${num}`).value;
    var end_h = document.querySelector(`#end_h_${num}`).value;
    var end_m = document.querySelector(`#end_m_${num}`).value;
    var start = Number(start_h + start_m);
    var end = Number(end_h + end_m);

    var returnData = "";
    if(gubun == "start") returnData = start;
    else returnData = end;

    return returnData;
}

//<!--/* 예약진료시간 데이터 (form data) */-->
function timeTableData(target) {
    let tempTimeArr = [];
    let times = document.querySelectorAll(`[name=${target}Time]`);
    times.forEach((time) => {
        if(time.checked) {
            var data = document.querySelector('[for='+time.id+']').textContent;
            data = Number(data.replace(":", ""));
            tempTimeArr.push(data);
        }
    });

    if(tempTimeArr.length !== 0 ) {
        document.querySelector(`[name=${target}TimeArr]`).value = tempTimeArr;
    } else {
        document.querySelector(`[name=${target}TimeArr]`).value = "";
    }
}