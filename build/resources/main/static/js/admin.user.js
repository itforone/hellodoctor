/**
 * 관리자페이지
 * 병원관리, 약국관리
 */
//<!--/* 페이징 클릭시 ajax 호출 */-->
$(document).on("click", ".paging a", function(e) {
    e.preventDefault();
    let href = this.getAttribute("href");
    let page = href.split("page=")[1];
    // document.sfrm.page.value = page;

    $(".paging a").removeClass("active");
    $(this).addClass("active");
    getList(page, PAGE_TARGET);
});

//<!--/* 필터 변경 */-->
$("select[name=listRow], select[name=svc], select[name=auth]").on("change", function() {
    getList(1, PAGE_TARGET);
});

//<!--/* 목록 */-->
function getList(curPage, target) {
    let f = document.sfrm;
    if (curPage != undefined) f.page.value = curPage;

    let data = {};
    data.page = f.page.value;
    data.svc = f.svc.value; //<!--/* 서비스종류 */-->
    data.auth = f.auth.value; //<!--/* 가입승인 */-->
    data.sfl= f.sfl.value;
    data.stx = f.stx.value.trim();
    data.listRow = document.querySelector("[name=listRow]").value;
    // console.log(data);

    // callPageLoading(1);

    if (data.stx != "" && data.stx.length < 2) {
        swal.fire({
            html:'검색어를 2자 이상 입력해 주세요.',
            confirmButtonText : '확인',
            didDestroy: function () {
                f.stx.focus();
            }
        });
        return false;
    }

    $.ajax({
        type : "get",
        url : (target=='P')? "./ajaxPharmacyList" : "./ajaxHospitalList",
        data : data,
    }).done(function(data, textStatus, xhr) {
        document.querySelector(`#listLoad`).innerHTML = data;

    }).fail(function(data, textStatus, errorThrown) {
        let html = `<table><tbody><tr><td colspan="10">목록을 불러오는데 실패했습니다.</td></tr></tbody></table>`;
        document.querySelector(`#listLoad`).innerHTML = html;

    }).always(function() {
        // callPageLoading();
    });

    return false;
}

//<!--/* 가입승인 (현재 취소는 X) */-->
function changeAuth(idx, isAuth, target) {
    swal.fire({
        title: (target=='P')? '약국 가입 승인' : '병원 가입 승인',
        html:'가입을 승인하시겠습니까?',
        denyButtonText: '취소',
        confirmButtonText: '승인',
        showDenyButton: true,
    }).then((result) => {
        if (result.isConfirmed) {
            let data = {};
            data.idx = idx;
            data.authYn = (isAuth)? "Y" : "N";
            let errMsg = "승인 상태변경에 실패했습니다.<br>잠시 후 다시 시도해 주세요.";

            $.ajax({
                type : "post",
                url : (target=='P')? "./changePharmacyAuth" : "./changeHospitalAuth",
                data : data,
            }).done(function(data, textStatus, xhr) {
                let json = JSON.parse(data);
                // console.log(json);
                if (!json.result) {
                    swal.fire({html: errMsg, confirmButtonText : '확인',});
                }

            }).fail(function(data, textStatus, errorThrown) {
                swal.fire({html: errMsg, confirmButtonText : '확인',});

            }).always(function() {
                let page = document.sfrm.page.value;
                getList(page, PAGE_TARGET);
            });
        }
    });
}

//<!--/* 계정정보 팝업 */-->
function getAccount(elem, target) {
    let idx = elem.getAttribute("data-idx");
    let cnt = elem.getAttribute("data-cnt");
    if (cnt < 1) {
        swal.fire({html: '등록된 계정이 없습니다.', confirmButtonText : '확인',});
        return false;
    }

    $.ajax({
        type : "get",
        url : (target=='P')? "./ajaxPharmacyAccount" : "./ajaxHospitalAccount",
        data : {idx: idx},
    }).done(function(data, textStatus, xhr) {
        document.querySelector(`#accountPop .modal-body`).innerHTML = data;
        $("#accountPop").modal();

    }).fail(function(data, textStatus, errorThrown) {
        swal.fire({html: '서버와의 통신에 문제가<br>발생했습니다.<br>잠시 후 다시 시도해 주세요.', confirmButtonText : '확인',});
    });
}

//<!--/* 약국/병원 정보상세 팝업 */-->
function getInfo(elem, target) {
    let idx = elem.getAttribute("data-idx");

    $.ajax({
        type : "get",
        url : (target=='P')? "./ajaxPharmacyInfo" : "./ajaxHospitalInfo",
        data : {idx: idx},
    }).done(function(data, textStatus, xhr) {
        document.querySelector(`#infoPop .modal-body`).innerHTML = data;
        $("#infoPop").modal();

    }).fail(function(data, textStatus, errorThrown) {
        swal.fire({html: '서버와의 통신에 문제가<br>발생했습니다.<br>잠시 후 다시 시도해 주세요.', confirmButtonText : '확인',});
    });
}

//<!--/* 약국/병원 정보상세 이미지 미리보기 팝업 */-->
function prevImage(elem) {
    let src = $(elem).next("input").val();
    let link = document.querySelector("#imgPrevLink");
    let img = document.querySelector("#infoPrevImg");

    link.setAttribute("href", "#");
    img.setAttribute("src", "/img/noimg.svg");

    if (src == "") {
        swal.fire({html: '등록된 파일이 없습니다.', confirmButtonText : '확인',});
        return false;
    }

    document.querySelector("#infoPrevImg").setAttribute("src", src);
    document.querySelector("#imgPrevLink").setAttribute("href", src);
    $("#zoom_pop").modal();
}

//<!--/* 수정요청 목록 */-->
function getUpdateList(curPage, target) {
    let f = document.sfrm;
    if (curPage != undefined) f.page.value = curPage;

    let data = {};
    data.page = f.page.value;
    data.sfl= f.sfl.value;
    data.stx = f.stx.value.trim();
    data.listRow = document.querySelector("[name=listRow]").value;
    // console.log(data);

    // callPageLoading(1);

    if (data.stx != "" && data.stx.length < 2) {
        swal.fire({
            html:'검색어를 2자 이상 입력해 주세요.',
            confirmButtonText : '확인',
            didDestroy: function () {
                f.stx.focus();
            }
        });
        return false;
    }

    $.ajax({
        type : "get",
        url : (target=='P')? "./ajaxPharmacyUpdateList" : "./ajaxHospitalUpdateList",
        data : data,
    }).done(function(data, textStatus, xhr) {
        document.querySelector(`#listLoad`).innerHTML = data;

    }).fail(function(data, textStatus, errorThrown) {
        let html = `<table><tbody><tr><td colspan="10">목록을 불러오는데 실패했습니다.</td></tr></tbody></table>`;
        document.querySelector(`#listLoad`).innerHTML = html;

    }).always(function() {
        // callPageLoading();
    });

    return false;
}