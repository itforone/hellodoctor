/**
 * 관리자페이지
 * CS문의 앱, 의/약
 */
//<!--/* 페이징 클릭시 ajax 호출 */-->
$(document).on("click", ".paging a", function(e) {
    e.preventDefault();
    let href = this.getAttribute("href");
    let page = href.split("page=")[1];
    // document.sfrm.page.value = page;

    $(".paging a").removeClass("active");
    $(this).addClass("active");
    getList(page);
});

//<!--/* 필터 변경 */-->
$("select[name=svc]").on("change", function() {
    getList(1);
});

//<!--/* 목록 */-->
function getList(curPage) {
    let f = document.sfrm;
    if (curPage != undefined) f.page.value = curPage;

    let data = {};
    data.page = f.page.value;
    data.svc= f.svc.value;
    data.sfl= f.sfl.value;
    data.stx = f.stx.value.trim();
    data.pageTarget = PAGE_TARGET;
    console.log(data);

    // callPageLoading(1);

    if (data.stx != "" && data.stx.length < 2) {
        swal.fire({
            html:'검색어를 2자 이상 입력해 주세요.',
            confirmButtonText : '확인',
            didDestroy: function () {
                f.stx.focus();
            }
        });
        return false;
    }

    $.ajax({
        type : "get",
        url : "./ajaxCSList",
        data : data,
    }).done(function(data, textStatus, xhr) {
        document.querySelector(`#listLoad`).innerHTML = data;
    }).fail(function(data, textStatus, errorThrown) {
        let html = `<div class="text-center">목록을 불러오는데 실패했습니다.</div>`;
        document.querySelector(`#listLoad`).innerHTML = html;
    }).always(function() {
        // callPageLoading();
    });

    return false;
}

// 상세보기
function getView(idx) {
    let f = document.sfrm;
    let paramsArr = [];
    if (f.svc.value != "") paramsArr.push(`svc=${f.svc.value}`);
    if (f.page.value > 1) paramsArr.push(`page=${f.page.value}`);
    if (f.stx.value.trim() != "") {
        paramsArr.push(`sfl=${f.sfl.value}`);
        paramsArr.push(`stx=${f.stx.value}`);
    }
    if (paramsArr.length > 0) {
        //<!--/* 검색파라미터로 url 변경 */-->
        let changeUrl = (PAGE_TARGET=="APP")? `./csListApp` : `./csList`;
        changeUrl += "?" + paramsArr.join("&");
        history.replaceState({data: "replaceState"}, "", changeUrl);
    }

    location.href = (PAGE_TARGET=="APP")? `./csViewApp/${idx}` : `./csView/${idx}`;
}